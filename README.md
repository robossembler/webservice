# Веб-сервис Robossembler

Сервис для сопровождении процесса/жизненного цикла разработки программ сборки изделий роботами и интеграции программных модулей [Фреймворка Робосборщик](https://gitlab.com/robossembler/framework).

## Мотивация

Для корректной работы фреймворка  необходима согласованная работа разнообразных программный модулей – результаты работы одних модулей должны передаваться через стандартизированные интерфейсы другим модулям. Результатами работы программных модулей являются исполняемые файлы программ, файлы 3D-моделей в форматах STL, FBX, Collada/DAE, OBJ, PLY и т.п., конфигурационные файлы в форматах yaml, json, ini, txt, веса нейронных сетей, описания роботов/сцен в форматах URDF, SDF, MJCF и т.д.

## Состав модулей сервиса

Каждая фаза жизненного цикла имеет своё представление в виде страницы в веб-сервисе:

1. Создание проекта сборки, загрузка CAD-проекта изделия - "Проекты"
2. Подготовка и генерация датасета для навыков машинного зрения - Вкладка "Датасеты"
3. Конфигурация сцены - Scene Builder - Вкладка "Сцена"
4. Создание дерева поведения из навыков - Вкладка "Поведение"
5. Просмотр результатов симуляции - Вкладка "Симуляция"
6. Оценка производительности навыков Вкладка "Анализ"

Веб-сервис написан на языке TypeScript для среды исполнения NodeJS. Для хранения артефактов используется база данных MongoDB. Исходный код проекта разработан в соответствии с концепцией «Чистой архитектуры», описанной Робертом Мартином в одноимённой книге. Данный подход позволяет систематизировать код, отделить бизнес-логику от остальной части приложения.

# Установка

## Требования

- Node.js (версия >= 20.11.1)
- MongoDB (версия >= 7.0.15)
- BlenderProc (для генерации датасетов)

## Последовательность команд для установки зависимостей

```bash
# установка nvm
curl --silent -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.2/install.sh | bash
source ~/.bashrc
# установка Node.js
nvm install 20.11.1
nvm alias default 20.11.1
nvm use default

# MongoDB
# импортировать открытый ключ для MongoDB
curl -fsSL https://pgp.mongodb.com/server-7.0.asc | sudo gpg -o /usr/share/keyrings/mongodb-server-7.0.gpg --dearmor
# добавить репозиторий MongoDB 7.0 в директорию /etc/apt/sources.list.d
echo "deb [ arch=amd64,arm64 signed-by=/usr/share/keyrings/mongodb-server-7.0.gpg ] https://repo.mongodb.org/apt/ubuntu jammy/mongodb-org/7.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-7.0.list
# установка MongoDB
apt-get update && apt-get install -y mongodb-org
# запуск
sudo systemctl start mongod
```
Убедиться в том, что сервер MongoDB работает, можно посмотрев его состояние:
```bash
sudo systemctl status mongod
```
Статус должен быть 'active'
```
● mongod.service - MongoDB Database Server
     Loaded: loaded (/lib/systemd/system/mongod.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2024-12-11 10:34:58 MSK; 1h 24min ago
       Docs: https://docs.mongodb.org/manual
   Main PID: 2439 (mongod)
     Memory: 256.7M
        CPU: 24.168s
     CGroup: /system.slice/mongod.service
             └─2439 /usr/bin/mongod --config /etc/mongod.conf
```

## Установка веб-сервиса

```bash
# клонирование исходников
cd ~/
git clone https://gitlab.com/robossembler/webservice
# сборка сервера
cd webservice/server
npm i tsc-watch -g
npm i
# сборка фронтэнда
cd ..
cd ui
npm i
```

## Запуск веб-сервиса

Необходимо открыть два терминала. В первом нужно запустить сервер:
```bash
cd ~/webservice/server
npm run dev
```
Во втором запустить UI:
```bash
cd ~/webservice/ui
npm run dev
```
Если установка была успешной, откроется браузер, на странице http://localhost:3000/ "Проекты".
