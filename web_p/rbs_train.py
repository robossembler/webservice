"""
  rbs_train
  Общая задача: web-service pipeline
  Реализуемая функция: обучение нейросетевой модели по заданному BOP-датасету

    python3 $PYTHON_EDUCATION --path /Users/idontsudo/webservice/server/build/public/7065d6b6-c8a3-48c5-9679-bb8f3a690296 \
        --name test1234 --datasetName 32123213

  27.04.2024 @shalenikol release 0.1
"""

import argparse
from train_Yolo import train_YoloV8
from train_Dope import train_Dope_i

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--path", required=True, help="Path for dataset")
    parser.add_argument("--name", required=True, help="String with result weights name")
    parser.add_argument("--datasetName", required=True, help="String with dataset name")
    parser.add_argument("--outpath", default="weights", help="Output path for weights")
    parser.add_argument("--type", default="ObjectDetection", help="Type of implementation")
    parser.add_argument("--epoch", default=3, type=int, help="How many training epochs")
    parser.add_argument('--pretrain', action="store_true", help="Use pretraining")
    args = parser.parse_args()

    if args.type == "ObjectDetection":
        train_YoloV8(args.path, args.name, args.datasetName, args.outpath, args.epoch, args.pretrain)
    else:
        train_Dope_i(args.path, args.name, args.datasetName, args.outpath, args.epoch, args.pretrain)
