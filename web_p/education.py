import shutil
import argparse
import os.path
from pathlib import Path

parser = argparse.ArgumentParser()
 
parser.add_argument("--path", required=True, help="Path for dataset")
parser.add_argument("--name", required=True, help="String with result weights name")
parser.add_argument("--datasetName", required=True, help="String with dataset name")
parser.add_argument("--outpath", default="weights", help="Output path for weights")
parser.add_argument("--type", default="ObjectDetection", help="Type of implementation")
parser.add_argument("--epoch", default=3, help="How many training epochs")
parser.add_argument('--pretrain', help="Use pretraining")
args = parser.parse_args()


def copy_and_move_folder(src, dst, folder):
    try:
        if os.path.exists(dst + "/education/") is False:
            Path(dst + "/education/").mkdir(parents=True, exist_ok=True)
        if os.path.exists(dst + "/education/" + folder + "/"):
            shutil.rmtree(dst + "/education/" + folder + "/")
        
        shutil.copytree(src, dst + "/education/" + folder + "/")

    except shutil.Error as e:
        print(f"Error: {e}")


source_folder = os.path.dirname(os.path.abspath(__file__)) + "/education"
copy_and_move_folder(source_folder, args.path, args.name)
