import shutil
import argparse
import os.path

parser = argparse.ArgumentParser()
parser.add_argument("--path")
parser.add_argument("--form")

args = parser.parse_args()


def copy_and_move_folder(src, dst):
    try:
        if os.path.exists(dst):
            shutil.rmtree(dst)
        shutil.copytree(src, dst)
        print(f"Folder {src} successfully copied")
    except shutil.Error as e:
        print(f"Error: {e}")


source_folder = os.path.dirname(os.path.abspath(__file__)) + "/train/"

copy_and_move_folder(source_folder, args.path)
