{
  description = "Robossembler Development Environments on Nix";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs?ref=master";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import nixpkgs { inherit system; overlays = []; };
        in
        {
          packages = {
            frontend = pkgs.writeShellApplication {
              name = "frontend";
              runtimeInputs = [ pkgs.nodejs ];
              text = ''
                cd ui && npm i && npm run dev
              '';
            };
            backend = pkgs.writeShellApplication {
              name = "frontend";
              runtimeInputs = [ pkgs.nodejs ];
              text = ''
                cd server && npm i && npm run dev
              '';
            };
          };

          devShells = {
            default = pkgs.mkShell { packages = with pkgs; [ nodejs ]; };
          }; 
        }
      );
}
