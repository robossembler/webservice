import "reflect-metadata";
import "antd/dist/antd.min.css";
import ReactDOM from "react-dom/client";
import { extensions } from "./core/extensions/extensions";
import { SocketListener } from "./features/socket_listener/socket_listener";
import { RouterProvider } from "react-router-dom";
import { router } from "./core/routers/routers";
import { configure } from "mobx";
import { ThemeStore } from "./core/store/theme_store";

configure({
  enforceActions: "never",
});

extensions();

const root = ReactDOM.createRoot(document.getElementById("root") as HTMLElement);
export const themeStore = new ThemeStore();

root.render(
  <>
    <SocketListener>
      <RouterProvider router={router} />
    </SocketListener>
  </>
);
