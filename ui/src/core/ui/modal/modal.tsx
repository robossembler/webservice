import React from "react";
import { themeStore } from "../../..";
import { CoreText, CoreTextType } from "../text/text";
import { InputV2 } from "../input/input_v2";
import { CoreButton } from "../button/button";
import { ButtonV2, ButtonV2Type } from "../button/button_v2";

interface ModalProps {
  isOpen: boolean;
  onClose: () => void;
  children: React.ReactNode;
  style?: React.CSSProperties;
}

export const CoreModal: React.FC<ModalProps> = ({ isOpen, onClose, children, style }) => {
  if (!isOpen) return null;

  return (
    <div
      style={{
        position: "fixed",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: themeStore.theme.fon,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
      onClick={() => onClose()}
    >
      <div
        onClick={(event) => event.stopPropagation()}
        style={{
          backgroundColor: themeStore.theme.black,
          border: `1px solid ${themeStore.theme.greenWhite}`,
          padding: 20,
          borderRadius: 5,
          width: 400,
          position: "relative",
        }}
      >
        {children}
      </div>
    </div>
  );
};
 