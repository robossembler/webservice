import * as React from "react";
import { Typography } from "antd";
import { Col, Row } from "antd";
import { LinkTypography } from "../link/link";
import { ReactComponent as LeftIcon } from "../../assets/icons/left_icon.svg";
import { useNavigate } from "react-router-dom";

const { Title } = Typography;

export interface IHeader {
  largeText?: string;
  minText?: string;
  click?: Function;
  needBackButton?: undefined | any;
}

export const Header: React.FunctionComponent<IHeader> = (props: IHeader) => {
  const navigate = useNavigate();
  const needBackButton = props.needBackButton !== undefined ? false : true;

  return (
    <Col style={{ textAlign: "center" }}>
      <Row
        style={{
          marginTop: "20px",
          marginRight: "20px",
          display: "contents",
        }}
      >
        {needBackButton ? (
          <>
            <div
              onClick={() => {
                navigate(-1);
              }}
              style={{
                position: "absolute",
                zIndex: 1,
                left: "10px",
                backgroundColor: "#456BD9",
                border: "0.1875em solid #0F1C3F",
                borderRadius: "50%",
                height: "60px",
                width: "60px",
                cursor: "pointer",
              }}
            >
              <LeftIcon
                style={{
                  pointerEvents: "none",

                  cursor: "pointer",
                  width: "40px",
                  height: "40px",
                  position: "relative",
                  bottom: "-8px",
                }}
              />
            </div>
            <Title
              style={{
                position: "relative",
                bottom: "-8px",
                left: "20px",
                width: "100vw",
              }}
              level={2}
            >
              {props.largeText}
            </Title>
          </>
        ) : (
          <>
            <Title style={{ justifyContent: "center" }} level={2}>
              {props.largeText}
            </Title>
          </>
        )}
      </Row>

      {/* {props.minText !== undefined ? (
        <LinkTypography
          style={{
            marginBottom: "40px",
          }}
          path={props.click!}
          text={props.minText}
        />
      ) : (
        <></>
      )} */}
    </Col>
  );
};
