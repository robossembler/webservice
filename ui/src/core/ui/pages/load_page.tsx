import * as React from "react";
import { Header, IHeader } from "../header/header";
import { Loader } from "../loader/loader";
import { ReactComponent as ErrorIcon } from "../../assets/icons/error.svg";
import { Typography } from "antd";
import { observer } from "mobx-react-lite";
const { Title } = Typography;

interface ILoadPage extends IHeader {
  isLoading: boolean;
  isError: boolean;
  children?: JSX.Element | JSX.Element[];
}

export const LoadPage: React.FunctionComponent<ILoadPage> = observer(
  (props: ILoadPage) => {
    return (
      <>
        <Header
          click={props.click}
          largeText={props.largeText}
          minText={props.minText}
          needBackButton={props.needBackButton}
        />
        {props.isError ? (
          <>
            <ErrorIcon
              style={{
                height: "100px",
                width: "-webkit-fill-available",
              }}
            />
            <Title style={{ textAlign: "center" }} level={3} type="danger">
              not expected error
            </Title>
          </>
        ) : (
          <>
            {props.isLoading ? (
              <div style={{ marginTop: "50px" }}>
                <Loader />
              </div>
            ) : (
              <>{props.children}</>
            )}
          </>
        )}
      </>
    );
  }
);
