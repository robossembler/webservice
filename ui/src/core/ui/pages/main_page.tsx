import React from "react";
import { DatasetsScreenPath } from "../../../features/dataset/dataset_screen";
import { Icon } from "../icons/icons";
import { useNavigate } from "react-router-dom";
import { Spin } from "antd";
import { LoadingOutlined } from "@ant-design/icons";
import { SceneManagerPath } from "../../../features/scene_manager/presentation/scene_manager";
import { AssemblesScreenPath } from "../../../features/assembles/assembles_screen";
import { SimulationScreenPath } from "../../../features/simulations/simulations_screen";
import { EstimateScreenPath } from "../../../features/estimate/estimate_screen";
import { CalculationInstanceScreenPath as SkillScreenPath } from "../../../features/calculation_instance/presentation/calculation_instance_screen";
import { UiBaseError } from "../../model/ui_base_error";
import { DetailsScreenPath } from "../../../features/details/details_screen";
import { BehaviorTreeManagerScreenPath } from "../../../features/behavior_tree_manager/behavior_tree_manager_screen";
export interface IBlockProps {
  name: string;
  isActive: boolean;
  path: string;
  icon?: string;
}
const Block = (props: IBlockProps) => {
  const navigate = useNavigate();

  return (
    <div onClick={() => navigate(props.path)} style={{ height: 56, cursor: "pointer" }}>
      <div
        style={
          props.isActive
            ? {
                textAlignLast: "center",
                height: 32,
                backgroundColor: "rgba(232, 222, 248, 1)",
                marginLeft: 5,
                marginRight: 5,
                alignContent: "center",
                borderRadius: 12,
              }
            : {
                textAlignLast: "center",
                alignContent: "center",
                height: 32,
              }
        }
      >
        <Icon type={props.icon ?? ""} />
      </div>
      <div style={{ textAlignLast: "center" }}>{props.name}</div>
    </div>
  );
};
export interface IMainPageProps {
  page: string;
  bodyChildren?: JSX.Element;
  panelChildren?: JSX.Element;
  panelStyle?: React.CSSProperties;
  isLoading?: boolean;
  maskLoader?: boolean;
  error?: UiBaseError[];
}

export const MainPage = (props: IMainPageProps) => {
  const blocksNames = [
    { name: "Детали", path: DetailsScreenPath, icon: "Setting" },
    { name: "Сборки", path: AssemblesScreenPath, icon: "Assembly" },
    { name: "Датасеты", path: DatasetsScreenPath, icon: "Datasets" },
    { name: "Сцена", path: SceneManagerPath, icon: "Scene" },
    { name: "Вычисления", path: SkillScreenPath, icon: "Layers" },
    { name: "Поведение", path: BehaviorTreeManagerScreenPath, icon: "Rocket" },
    { name: "Симуляция", path: SimulationScreenPath, icon: "Simulation" },
    { name: "Оценка", path: EstimateScreenPath, icon: "Grade" },
  ];
  const blocks: IBlockProps[] = blocksNames
    .map((el) => {
      return Object.assign({ isActive: false }, el);
    })
    .map((el) => {
      if (el.name.isEqual(props.page)) {
        el.isActive = true;
        return el;
      }
      return el;
    });
  React.useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "scroll";
    };
  });
  return (
    <div style={{ display: "flex" }}>
      <div
        style={{
          width: 90,
          height: window.innerHeight,
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <div style={{ paddingTop: 43 }}>
          <div style={{ textAlignLast: "center" }}>
            <Icon type="MenuFab" />
          </div>
          <div
            style={{
              textAlignLast: "center",
              width: 56,
              height: 56,
              borderRadius: 12,
              backgroundColor: "#ffd9e4",
              alignContent: "center",
            }}
          >
            <Icon style={{ marginTop: 3 }} type="Pencil" />
          </div>
        </div>
        <div>
          {blocks.map((el, index) => (
            <Block isActive={el.isActive} name={el.name} path={el.path} icon={el.icon} key={index} />
          ))}
        </div>
        <div style={{ paddingBottom: 10 }}>
          <Icon type={"Settings"} />
        </div>
      </div>
      {props.isLoading ? (
        <div style={{ alignContent: "center", width: "100%", textAlign: "center" }}>
          <Spin indicator={<LoadingOutlined style={{ fontSize: 68, color: "rgba(103, 80, 164, 1)" }} spin />} />
        </div>
      ) : (
        <>
          {props.error?.isNotEmpty() ? (
            <>
              {props.error.map((el) => (
                <div>{el.text}</div>
              ))}
            </>
          ) : (
            <>
              {props.maskLoader ? (
                <div
                  style={{
                    width: "100vw",
                    left: 241,
                    height: "100vh",
                    backgroundColor: "#000000b0",
                    position: "absolute",
                    zIndex: 100,
                    alignContent: "center",
                    textAlignLast: "center",
                  }}
                >
                  <Spin />
                </div>
              ) : null}
              <div
                style={Object.assign(
                  { width: 241, height: window.innerHeight, backgroundColor: "#F7F2FA", borderRadius: 16 },
                  props.panelStyle
                )}
              >
                {props.panelChildren}
              </div>
              {props.bodyChildren}
            </>
          )}
        </>
      )}
    </div>
  );
};
