import { IHeader } from "../header/header";
import { CoreText, CoreTextType } from "../text/text";
import { Spin } from "antd";

interface IPreviewPageProps extends IHeader {
  isLoading: boolean;
  isError: boolean;
  children?: JSX.Element | JSX.Element[];
}
export const PreviewPage = (props: IPreviewPageProps) => {
  return (
    <div style={{ width: "100%", height: "100%" }}>
      <div
        style={{
          width: "100%",
          marginTop: 10,
          textAlign: "center",
          position: "absolute",
        }}
      >
        <CoreText text={props.largeText ?? ""} type={CoreTextType.big} />
        <CoreText
          onClick={() => {
            if (props.click) props.click();
          }}
          text={props.minText ?? ""}
          type={CoreTextType.small}
          style={{ color: "rgba(68, 142, 247, 1)", cursor: "pointer" }}
        />
      </div>
      <div
        style={{
          height: "100%",
          width: "100%",
          padding: 60,
          paddingTop: 100,
        }}
      >
        <div
          style={{
            borderRadius: 7,
            border: "1px solid #CAC4D0",
            background: "#FEF7FF",
            height: "100%",
            width: "100%",
            overflowX: "auto",
          }}
        >
          {props.isLoading ? (
            <div style={{ width: "100%", height: "100%", justifyContent: "center", alignItems: "center" }}>
              <Spin />
            </div>
          ) : (
            props.children
          )}
        </div>
      </div>
    </div>
  );
};
