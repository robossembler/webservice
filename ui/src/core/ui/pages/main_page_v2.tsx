import React, { useEffect } from "react";
import { themeStore } from "../../..";
import { Icon } from "../icons/icons";
import { observer } from "mobx-react-lite";
import { makeAutoObservable } from "mobx";
import { RefDiv } from "../ref/ref_div";
export enum CycleState {
  isMoving = "isMoving",
  stop = "stop",
}
function delay(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

class MainPageStore {
  cycleState = CycleState.stop;
  isFirstClick: boolean = true;
  stepMs = { 1: 15, 2: 20, 3: 25, 4: 30, 5: 35, 6: 40, 7: 45, 8: 50 };
  icons: {
    icon: string;
    isActive: boolean;
    style: React.CSSProperties;
    left: number;
    top: number;
  }[] = [
    { icon: "Home", isActive: true, style: {}, left: 0, top: 0 },
    { icon: "ComposeSquares", isActive: false, style: {}, left: 0, top: 0 },
    { icon: "BottomSquare", isActive: false, style: {}, left: 0, top: 0 },
    { icon: "RobotARM", isActive: false, style: {}, left: 0, top: 0 },
    { icon: "Storage", isActive: false, style: {}, left: 0, top: 0 },
    { icon: "Graph", isActive: false, style: {}, left: 0, top: 0 },
    { icon: "PlayComputer", isActive: false, style: {}, left: 0, top: 0 },
    { icon: "DashBoard", isActive: false, style: {}, left: 0, top: 0 },
  ];
  paddingLeft: number = 0;
  offsetLeftCircle: number = 0;
  leftCircle = 0;
  constructor() {
    makeAutoObservable(this);
  }
  init = (paddingLeft: number) => {
    this.leftCircle = Math.abs(this.offsetLeftCircle - this.getActive()!.left) - paddingLeft;

    this.paddingLeft = paddingLeft;
  };
  getActive = () => this.icons.filter((el) => el.isActive).at(0);
  getActiveIndex = () => this.icons.findIndex((el) => el.isActive);
  selectIndex = async (i: number) => {
    const prevActive = this.getActive();
    const prevActiveIndex = this.getActiveIndex();
    this.icons
      .map((element) => {
        element.isActive = false;
        return element;
      })
      .map((element, index) =>
        i.isEqualR(index).fold(
          () => {
            element.isActive = true;
          },
          () => element
        )
      );
    const nextActive = this.getActive();
    const nextActiveIndex = this.getActiveIndex();

    this.cycleState = CycleState.isMoving;
    let distance = 0;

    distance = nextActive!.left - this.offsetLeftCircle;

    const step = distance / 10;

    for await (const num of Array.from({ length: 10 }, (_, i) => i + 1)) {
      // @ts-ignore
      await delay(this.stepMs[Math.abs(nextActiveIndex - prevActiveIndex)]);
      nextActive!.top = nextActive!.top + 3;
      if (!this.isFirstClick) prevActive!.top = prevActive!.top - 3;
      let offset = 0;
      if (num === 10) {
        offset = 6;
      }
      this.leftCircle = this.leftCircle + step - offset;
    }
    this.isFirstClick = false;
  };
  updateLeftStyle = (left: number, index: number) => this.icons.replacePropIndex({ left: left }, index);
}
export const MainPageV2: React.FC<{
  children: React.ReactNode;
  bgColor: string;
  style?: React.CSSProperties | void;
  rightChild?: React.ReactNode;
}> = observer(({ children, bgColor, style, rightChild }) => {
  const [store] = React.useState(() => new MainPageStore());
  useEffect(() => {
    store.init(4);
  }, []);
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        backgroundColor: bgColor,
        width: "100%",
        height: "100%",
        overflow: "auto",
      }}
    >
      <div
        style={{
          alignSelf: "center",
          width: 645,
          height: 60,
          backgroundColor: themeStore.theme.navBlue,
          paddingBottom: 60,
          borderRadius: 20,
          zIndex: 20,
        }}
      >
        <RefDiv
          style={{
            display: "flex",
            width: "100%",
            height: 60,
            justifyContent: "space-evenly",
            alignItems: "center",
          }}
          children={
            <>
              {store.icons.map((el, i) => (
                <div key={i} style={{ zIndex: 25 }}>
                  {el.isActive === true ? (
                    <>
                      <RefDiv
                        style={{ position: "relative", top: 30 }}
                        property="offsetLeft"
                        onChange={(text) => store.updateLeftStyle(Number(text), i)}
                      >
                        <Icon
                          type={el.icon}
                          style={{
                            margin: 10,
                            zIndex: 25,

                            position: "relative",
                          }}
                          onClick={() => {
                            store.icons
                              .map((element) => {
                                element.isActive = false;
                                return element;
                              })
                              .map((element, index) =>
                                i.isEqualR(index).fold(
                                  () => {
                                    element.isActive = true;
                                  },
                                  () => element
                                )
                              );
                          }}
                        />
                      </RefDiv>
                    </>
                  ) : (
                    <>
                      <RefDiv
                        style={{ position: "relative", top: el.top, zIndex: 23 }}
                        property="offsetLeft"
                        onChange={(text) => store.updateLeftStyle(Number(text), i)}
                      >
                        <Icon type={el.icon} style={{ margin: 10 }} onClick={() => store.selectIndex(i)} />
                      </RefDiv>
                    </>
                  )}
                </div>
              ))}
            </>
          }
        />
        <div style={{ width: 0, height: 0 }}>
          <RefDiv
            property="offsetLeft"
            onChange={(text) => {
              store.offsetLeftCircle = Number(text);
            }}
            style={{
              position: "relative",
              top: -30,
              left: store.leftCircle,
              backgroundColor: bgColor,
              width: 60,
              height: 60,
              borderRadius: "50%",
              paddingTop: 10,
              zIndex: 21,
            }}
          >
            <div
              style={{
                position: "relative",
                top: -5,
                left: 5,
                backgroundColor: themeStore.theme.greenWhite,
                width: 50,
                height: 50,
                borderRadius: 200,
              }}
            ></div>
          </RefDiv>
        </div>
        <div
          style={{
            position: "absolute",
            right: 0,
            top: 13,
            width: 300,
            height: 50,
            
          }}
        >
          {rightChild}
        </div>
      </div>

      <div style={Object.assign({ width: "100%" }, style)}>{children}</div>
    </div>
  );
});
