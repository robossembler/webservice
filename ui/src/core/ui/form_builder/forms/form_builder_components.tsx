import { Result } from "../../../helper/result";
import { SelectDatasetScreen } from "./select_dataset/presentation/select_dataset_screen";
import { SelectDetail } from "./select_detail/presentation/select_detail_screen";

export enum FormBuilderComponents {
  SelectDetail = "SelectDetail",
  SelectDataset = "SelectDataset",
}
export interface IFormBuilderComponentsProps<T> {
  dependency: T;
  onChange: (obj: any) => void;
}
export const getFormBuilderComponents = (
  name: string,
  dependency: any,
  onChange: (text: string) => void
): Result<string, React.ReactNode> => {
  if (name.isEqual(FormBuilderComponents.SelectDetail)) {
    return Result.ok(<SelectDetail dependency={dependency} onChange={onChange} />);
  }
  if (name.isEqual(FormBuilderComponents.SelectDataset)) {
    return Result.ok(<SelectDatasetScreen dependency={dependency} onChange={onChange} />);
  }
  return Result.error(name);
};
