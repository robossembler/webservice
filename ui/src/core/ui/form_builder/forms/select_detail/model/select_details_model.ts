import { makeAutoObservable } from "mobx";
import { Parts } from "../../../../../../features/details/details_http_repository";

export class SelectDetailViewModel {
  details: Parts[];
  constructor(parts: Parts[]) {
    
    this.details = parts ?? [];
    makeAutoObservable(this);
  }

  static empty = () => new SelectDetailViewModel([]);

  isSelected = (name: string) => {
    for (const el of this.details) {
      if (el.name.isEqual(name)) {
        return el.isSelect as boolean;
      }
    }
    return false;
  };
  toDependency = () => {
    this.details = this.details.filter((el) => el.isSelect === true);
    return this;
  };
  select = (part: Parts) =>
    this.details.indexOfR(part).fold(
      () =>
        this.details.forEach((el) =>
          el.name.isEqualR(part.name).map(() => {
            el.isSelect = !el.isSelect;
          })
        ),
      () => {
        part.isSelect = true;
        this.details.push(part);
      }
    );
}
