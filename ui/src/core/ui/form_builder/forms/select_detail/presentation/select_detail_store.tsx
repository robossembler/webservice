import makeAutoObservable from "mobx-store-inheritance";
import { DataSetHttpRepository } from "../../../../../../features/dataset/dataset_http_repository";
import { Parts } from "../../../../../../features/details/details_http_repository";
import { FormState } from "../../../../../store/base_store";
import { SelectDetailViewModel } from "../model/select_details_model";

export class SelectDetailStore extends FormState<SelectDetailViewModel, any> {
  viewModel = SelectDetailViewModel.empty();
  parts?: Parts[];
  isLoading: boolean = true;
  dataSetRepository: DataSetHttpRepository = new DataSetHttpRepository();
  constructor() {
    super();
    makeAutoObservable(this);
  }
  isSelected = (name: string) => {
    if (this.viewModel.details)
      for (const el of this.viewModel.details) {
        if (el.name.isEqual(name)) {
          return el.isSelect as boolean;
        }
      }
    return false;
  };
  init = async () => {
    await this.mapOk("parts", this.dataSetRepository.getLocalParts());
  };
  updateCheckbox(el: Parts): void {
    el.isSelect = true;
  }
}
