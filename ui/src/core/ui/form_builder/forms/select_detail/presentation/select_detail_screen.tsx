// @ts-nocheck
import React from "react";
import { IFormBuilderComponentsProps } from "../../form_builder_components";
import { observer } from "mobx-react-lite";
import { ListItem } from "./ui/list_item";
import { SelectDetailStore } from "./select_detail_store";
import { SelectDetailViewModel } from "../model/select_details_model";
import { plainToInstance } from "class-transformer";

export const SelectDetail = observer((props: IFormBuilderComponentsProps<SelectDetailViewModel>) => {
  const [store] = React.useState(() => new SelectDetailStore());
  React.useEffect(() => {
    console.log(props.dependency.details);
    store.viewModel = new SelectDetailViewModel(props.dependency.details);
    store.isLoading = false;
    store.init();
  }, []);
 
  return (
    <div>
      {store.isLoading ? (
        <></>
      ) : (
        <>
          {store.parts?.map((el) => {
            return (
              <ListItem
                status={store.viewModel.isSelected(el.name)}
                name={el.name}
                imgURL={el.image}
                onChange={() => (store.viewModel.select(el), props.onChange(store.viewModel.toDependency()))}
              />
            );
          })}
        </>
      )}
    </div>
  );
});
