import { useState } from "react";
import { Icon } from "../icons/icons";
import { CoreText, CoreTextType } from "../text/text";
import { set } from "ts-pattern/dist/patterns";
interface IToggleProps {
  name: string;
  child: React.ReactNode;
  isOpen: boolean;
}
export const Toggle = (props: IToggleProps) => {
  const [isOpen, setOpen] = useState(props.isOpen);
  return (
    <div style={{ border: "1px black solid", height: "max-content",width:"100%" }}>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          margin: 0,
          padding: 5,
          alignItems: "center",
        }}
        onClick={() => setOpen(!isOpen)}
      >
        <CoreText   text={props.name} type={CoreTextType.large} />
        <Icon type="PlusCircle" style={{ width: 33 }} />
      </div>
      {isOpen ? props.child : <></>}
    </div>
  );
};
