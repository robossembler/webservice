import React, { useRef, useState } from "react";
// TODO: need
export const CoreSlider = ({
  max,
  min,
  step,
  initialValue,
  onChange,
}: {
  max: number;
  min: number;
  step: number;
  initialValue: number;
  onChange: (value: number) => void;
}) => {
  const refSlider = useRef<HTMLDivElement>(null);
  const refWheel = useRef<HTMLDivElement>(null);
  const [value, setValue] = useState<number>(initialValue);

  React.useEffect(() => {
    if (refSlider.current && refWheel.current) {

      const oneStep = refSlider.current.clientWidth / ((max - min) / step);

      refWheel.current.style.left = ((value - min) / step) * oneStep + "px"; 
    }
  }, []);
  return (
    <div
      ref={refSlider}
      onClick={(event) => {

      }}
      style={{ width: "100%", height: 11, backgroundColor: "rgba(104, 80, 164, 1)" }}
    >
      <div
        style={{ position: "relative", width: 21, height: 18, background: "#D9D9D9", borderRadius: "20px", bottom: 3 }}
        ref={refWheel}
      ></div>
    </div>
  );
};
