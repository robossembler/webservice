import React from "react";
import { themeStore } from "../../..";
import { CoreText, CoreTextType } from "../text/text";

export const DrawerV2: React.FC<{
  isOpen?: boolean;
  title?: string;
  onClose: () => void;
  children: React.ReactNode;
}> = ({ isOpen, onClose, children, title }) => {
  return (
    <div
      style={{
        position: "fixed",
        top: 0,
        right: isOpen ? 0 : -300,
        width: 300,
        height: "100%",
        backgroundColor: themeStore.theme.darkSurface,
        boxShadow: "-2px 0 5px rgba(0, 0, 0, 0.5)",
        transition: "right 0.3s ease",
        zIndex: 1000,
        
      }}
    >
      <div style={{ height: "100%", width: "100%" }}>
        <div style={{ padding: 20 }}>
          <div
            style={{
              display: "flex",
              width: "100%",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <CoreText type={CoreTextType.header} text={title} color={themeStore.theme.darkOnSurfaceVariant} />
            <button
              style={{
                background: "none",
                border: "none",
                fontSize: 24,
                cursor: "pointer",
                color: themeStore.theme.darkOnSurfaceVariant,
              }}
              onClick={onClose}
            >
              &times;
            </button>
          </div>
          <div style={{ width: "100%", height: 1, backgroundColor: themeStore.theme.outlineVariant }}></div>
        </div>
        {children}
      </div>
    </div>
  );
};
