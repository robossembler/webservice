import { useRef, useEffect, useState } from "react";
import { themeStore } from "../../..";
import { ButtonV2 } from "../button/button_v2";
import { Icon } from "../icons/icons";
import { CoreText, CoreTextType } from "../text/text";

export const CoreCard: React.FC<{
  clickDeleteIcon: Function;
  clickGoToIcon: Function;
  date: number;
  descriptionBottom?: string;
  descriptionTop: string;
  descriptionMiddle?: string;
}> = ({ clickDeleteIcon, clickGoToIcon, descriptionBottom, descriptionTop, date, descriptionMiddle }) => {
  const ref = useRef<HTMLDivElement>(null);
  const [bgColor, setBgColor] = useState(themeStore.theme.surfaceContainerLow);
  useEffect(() => {
    ref.current?.addEventListener("mousemove", () => {
      setBgColor(themeStore.theme.surfaceContainerHighest);
    });
    ref.current?.addEventListener("mouseleave", () => {
      setBgColor(themeStore.theme.surfaceContainerLow);
    });
  }, []);
  return (
    <div
      ref={ref}
      style={{
        backgroundColor: bgColor,
        borderRadius: 12,
        border: `1.5px solid ${themeStore.theme.outlineVariantDark}`,
        padding: 10,
        marginTop: 10,
        marginRight: 10,
      }}
    >
      <div
        style={{ display: "flex", paddingTop: 10, paddingLeft: 20, paddingRight: 20, justifyContent: "space-between" }}
      >
        <div>
          <CoreText type={CoreTextType.mediumV2} text={descriptionTop} color={themeStore.theme.white} />
          <div style={{ height: 5 }} />
          <CoreText
            type={CoreTextType.mediumV2}
            text={new Date().fromUnixDate(date).formatDate()}
            color={themeStore.theme.outlineVariantLight}
          />
        </div>

        <div style={{ display: "flex" }}>
          <ButtonV2 onClick={() => clickGoToIcon()} text="Перейти" />
          <div style={{ width: 10 }} />
          <Icon
            width={10}
            height={10}
            type={"Bucket"}
            onClick={() => clickDeleteIcon()}
            style={{
              border: `1px solid ${themeStore.theme.greenWhite}`,
              height: 30,
              width: 30,
              textAlign: "center",
              alignContent: "center",
              borderRadius: 5,
              cursor: "pointer",
            }}
          />
        </div>
      </div>
      <div
        style={{
          width: "calc(100% - 30px)",
          height: 1,
          backgroundColor: themeStore.theme.outlineVariantDark,
          marginLeft: 15,
          marginRight: 15,
        }}
      />
      <div style={{ paddingRight: 30, paddingLeft: 30 }}>
        <div style={{ height: 20 }} />

        <CoreText type={CoreTextType.largeV2} text={descriptionMiddle} color={themeStore.theme.white} />
        <CoreText type={CoreTextType.mediumV2} text={descriptionBottom} color={themeStore.theme.white} />
        <div style={{ height: 20 }} />
      </div>
    </div>
  );
};
