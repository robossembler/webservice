import { Icon } from "../icons/icons";

interface ISwitchProps {
  isSelected: boolean;
  id: string;
  onChange: (status: boolean, id: string) => void;
}
export const CoreSwitch = (props: ISwitchProps) => {
  return (
    <div
      style={{ height: 40, width: 40, borderRadius: 2, alignContent: "center", cursor: "pointer" }}
      onClick={() => props.onChange(props.isSelected, props.id)}
    >
      <div style={{ backgroundColor: "rgba(104, 80, 164, 1)", width: 20, height: 20, textAlign: "center" }}>
        {props.isSelected ? <Icon type={"Check"} /> : null}
      </div>
    </div>
  );
};
