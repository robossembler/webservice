import { useEffect, useRef } from "react";

export const RefDiv: React.FC<{
  property?: string;
  onChange?: (property: string) => void;
  style?: React.CSSProperties;
  children?: React.ReactNode;
}> = ({ property, onChange, style, children }) => {
  const ref = useRef<HTMLDivElement>(null);
  useEffect(() => {
    if (ref.current && onChange) {
      // @ts-ignore
      onChange(String(ref.current[property]));
    }
  } );
  return (
    <div ref={ref} style={style}>
      {children}
    </div>
  );
};
