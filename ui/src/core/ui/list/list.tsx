import { Row } from "antd";
import { ReactComponent as AddIcon } from "../../assets/icons/add.svg";
import { ReactComponent as DeleteIcon } from "../../assets/icons/delete.svg";

import { observer } from "mobx-react-lite";
import { v4 } from "uuid";
import { ILinkTypography, LinkTypography } from "../link/link";

export type CallBackFunction = (el: ListElement, index: number) => void;

export interface ListElement {
  id?: string;
  text: string;
  color?: string;
}

export enum Icon {
  add,
  delete,
}
export interface IPropsList {
  values: ListElement[];
  headers?: string;
  link?: ILinkTypography;
  onClick?: CallBackFunction;
  icon: Icon;
}

export const List: React.FunctionComponent<IPropsList> = observer((props) => {
  props.values.map((el) => {
    if (el.id === undefined) {
      el.id = v4();
      return el;
    }
    return el;
  });
  return (
    <div>
      {props.headers !== undefined ? <>{props.headers}</> : <></>}
      {props.link !== undefined ? (
        <div>
          <LinkTypography path={props.link.path} text={props.link.text} />
        </div>
      ) : (
        <></>
      )}
      {props.values.map((el, index) => {
        return (
          <Row
            style={{
              width: "300px",
              backgroundColor: el.color ?? "ActiveBorder",
            }}
          >
            <div
              style={{
                width: "-webkit-fill-available",
                margin: "5px",
                marginLeft: "40px",
              }}
            />

            <div
              style={{
                marginLeft: "40px",
              }}
            >
              {el.text}
            </div>
            <div style={{ flexGrow: "1" }}></div>
            {props.icon === Icon.add ? (
              <>
                <AddIcon
                  style={{
                    width: "50px",
                    cursor: "pointer",
                    height: "50px",
                    marginRight: "40px",
                  }}
                  onClick={() => {
                    if (props.onClick !== undefined) {
                      props.onClick(el, index);
                    }
                  }}
                />
              </>
            ) : (
              <DeleteIcon
                style={{
                  width: "50px",
                  cursor: "pointer",
                  height: "50px",
                  marginRight: "40px",
                }}
                onClick={() => {
                  if (props.onClick !== undefined) {
                    props.onClick(el, index);
                  }
                }}
              />
            )}
          </Row>
        );
      })}
    </div>
  );
});
