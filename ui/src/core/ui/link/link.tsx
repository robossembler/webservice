import * as React from "react";
import { Typography } from "antd";
import { useNavigate } from "react-router-dom";
import { IStyle } from "../../model/style";

const { Link } = Typography;

export interface ILinkTypography extends IStyle {
  path: string;
  text: string;
}

export const LinkTypography: React.FunctionComponent<ILinkTypography> = (
  props: ILinkTypography
) => {
  const navigate = useNavigate();

  return (
    <Link
      style={props.style}
      onClick={() => {
        navigate(props.path);
      }}
    >
      {props.text}
    </Link>
  );
};
