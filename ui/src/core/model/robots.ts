export interface Robots {
    robots: Robot[];
}

export interface Robot {
    sid: string;
    DevicePackage: DevicePackage;
    Module: Module;
    Launch: Launch;
    DTwin: DTwin[];
    Settings: Setting[];
}

export interface DTwin {
    interface: Interface;
}

export interface Interface {
    input: Input[];
}

export interface Input {
    robot_namespace?: string;
    dof?: number;
}

export interface DevicePackage {
    name: string;
    version: string;
    format: string;
}

export interface Launch {
    package: string;
    executable: string;
}

export interface Module {
    name: string;
    description: string;
}

export interface Setting {
    name: string;
    description: string;
    defaultValue: string;
}
