export enum FormType {
  weights = "weights",
  robotName = "robot_name",
  cameraDeviceForm = "camera",
  topic = "topic",
  formBuilder = "formBuilder",
  moveToPose = "move_to_pose",
}
