import { DirectionalLight, Object3D, PointLight, Quaternion, SpotLight, Vector3 } from "three";
import { Result } from "../helper/result";
import { SceneModelsTypes } from "./scene_models_type";
import { CoreThreeRepository } from "../repository/core_three_repository";
import { Instance } from "./scene_asset";
import { Type } from "class-transformer";
export enum TypeLight {
  POINT = "POINT",
  DIRECTIONAL = "DIRECTIONAL",
  SPOT = "SPOT",
}
export class LightModel implements Instance {
  type = SceneModelsTypes.LIGHT;
  color: string;
  typeLight: TypeLight;
  intensity: number;
  width: number;
  height: number;
  distance: number;
  angle: number;
  penumbra: number;
  decay: number;
  @Type(() => Vector3)
  position: Vector3;
  orientation: number[];
  constructor() {}
  update = (coreThreeRepository: CoreThreeRepository) => this.toWebGl(coreThreeRepository);
  icon: string = "Light";
  name: string;
  isValid = (): Result<void, LightModel> => {
    // SDF -> point, directional, spot.
    // THREE -> PointLight,DirectionalLight,SpotLight
    return Result.ok(this);
  };
  toWebGl = (coreThreeRepository: CoreThreeRepository) => {
    // TODO: maybe change mapper
    let light: Object3D;
    this.typeLight
      .isEqualR(TypeLight.DIRECTIONAL)
      .map(() => (light = new DirectionalLight(this.color, this.intensity)));
    this.typeLight
      .isEqualR(TypeLight.POINT)
      .map(() => (light = new PointLight(this.color, this.intensity, this.distance, this.decay)));
    this.typeLight
      .isEqualR(TypeLight.SPOT)
      .map(
        () => (light = new SpotLight(this.color, this.intensity, this.distance, this.angle, this.penumbra, this.decay))
      );

    light!.castShadow = true;
    light!.position.copy(this.position);
    light!.quaternion.copy(new Quaternion().fromArray(this.orientation));

    coreThreeRepository.scene.add(light!);
  };
  static empty = () => new LightModel();
}
