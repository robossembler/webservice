import { IsNotEmpty } from "class-validator";
import { Result } from "../helper/result";
import { IDeviceDependency } from "./skill_model";
import { ValidationModel } from "./validation_model";

export class SidViewModel extends ValidationModel implements IDeviceDependency {
  @IsNotEmpty()
  sid: string;
  constructor(sid: string) {
    super();
    this.sid = sid;
  }

  static empty() {
    return new SidViewModel("");
  }
}
