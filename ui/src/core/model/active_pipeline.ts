export interface ActivePipeline {
  pipelineIsRunning: boolean;
  projectId?: string | null;
  lastProcessCompleteCount: number | null;
  error: any;
}
