import { Quaternion, Vector3 } from "three";
import { SceneModelsTypes } from "./scene_models_type";
import { Result } from "../helper/result";
import { Instance } from "./scene_asset";
import { CoreThreeRepository } from "../repository/core_three_repository";
import { Type } from "class-transformer";
import { URDFRobot } from "urdf-loader";
import { SceneBuilderStore, SceneItems } from "../../features/scene_builder/presentation/scene_builder_store";

export enum ToolTypes {
  RBS_GRIPPER = "RBS_GRIPPER",
}

interface RobotJoint {
  limit: {
    lower: number;
    upper: number;
  };
  angle: number;
  name: string;
}

export class RobotModel implements Instance {
  type = SceneModelsTypes.ROBOT;
  @Type(() => Vector3)
  position: Vector3;
  jointPosition: RobotJoint[];
  orientation: number[];
  name: string;
  httpUrl: string;
  nDof: number;
  toolType: string;
  constructor(
    position: Vector3,
    orientation: number[],
    name: string,
    httpUrl: string,
    nDof: number,
    toolType: string,
    jointPosition: RobotJoint[]
  ) {
    this.orientation = orientation;
    this.position = position;
    this.name = name;
    this.httpUrl = httpUrl;
    this.nDof = nDof;
    this.toolType = toolType;
    this.jointPosition = jointPosition;
  }
  icon: string = "Robot";
  toSceneItems = (sceneBuilderStore: SceneBuilderStore): SceneItems => {
    return {
      fn: () => {},
      name: this.name,
      isSelected: false,
      icon: "Robot",
    };
  };
  toWebGl = (coreThreeRepository: CoreThreeRepository) => {
    coreThreeRepository.loadUrdfRobot(this);
  };
  update = (coreThreeRepository: CoreThreeRepository) => {
    const robot = coreThreeRepository.scene.getObjectByName(this.name) as URDFRobot;
    robot.position.copy(this.position);
    robot.quaternion.copy(new Quaternion().fromArray(this.orientation));
    this.jointPosition.forEach((el) => {
      robot.setJointValue(el.name, el.angle);
    });
  };
  isValid(): Result<string, RobotModel> {
    return Result.ok(this);
  }
  static empty = () => new RobotModel(new Vector3(0, 0, 0), [0, 0, 0, 1], "", "", 1, ToolTypes.RBS_GRIPPER, []);
}
