export enum SceneModelsTypes {
  SOLID = "SOLID",
  ROBOT = "ROBOT",
  LIGHT = "LIGHT",
  CAMERA = "CAMERA",
  POINT = "POINT",
  ZONE = "ZONE",
}
