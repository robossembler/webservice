import { Object3D, Object3DEventMap, Quaternion, Vector3 } from "three";
import { SceneModelsTypes } from "./scene_models_type";
import { Instance } from "./scene_asset";
import { CoreThreeRepository } from "../repository/core_three_repository";
import { Type } from "class-transformer";
import { Parts } from "../../features/details/details_http_repository";
import { SceneItems } from "../../features/scene_builder/presentation/scene_builder_store";

export class SolidModel implements Instance {
  icon: string = "Solid";
  type = SceneModelsTypes.SOLID;
  @Type(() => Vector3)
  public position: Vector3;
  public name: string;
  constructor(
    vector3: Vector3,
    public orientation: number[],
    name:string,
    public solidType: string,
    public mesh: string,
    public collisionMesh: string,
    public spawnType: string
  ) {
    this.name = name;
    this.position = vector3;
  }
  toSceneItems = (): SceneItems => {
    return {
      fn: () => {},
      name: this.name,
      isSelected: false,
      icon: "Solid",
    };
  };

  update = (coreThreeRepository: CoreThreeRepository) => {
    const object = coreThreeRepository.getObjectsAtName(this.name) as Object3D<Object3DEventMap>;
    object.position.copy(this.position)
    object.quaternion.copy(new Quaternion().fromArray(this.orientation))
    
  };

  toWebGl = (coreThreeRepository: CoreThreeRepository) => {
    this.spawnType.isEqualR("BoundBox").fold(
      () =>
        coreThreeRepository.loader(
          this.mesh,
          () => (
            coreThreeRepository.raiseAnObjectAboveZeroVector(this.name),
            (this.orientation = coreThreeRepository.scene.getObjectByName(this.name)!.quaternion.toArray()),
            (this.position = coreThreeRepository.scene.getObjectByName(this.name)!.position)
          ),
          
          this.name,
          this.position
        ),
      () => console.log("UNKNOWN SPAWN TYPE SOLID MODEL")
    );
  };

  fromParts = (parts: Parts): SolidModel => {
    const solidModel = SolidModel.empty();
    solidModel.mesh = parts.daeUrl;
    solidModel.name = parts.name;
    solidModel.solidType = parts.solidType;
    return solidModel;
  };

  static empty = () => new SolidModel(new Vector3(0, 0, 0), [], "", "", "", "", "");
}
