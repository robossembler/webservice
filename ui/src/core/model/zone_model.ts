import { Vector3 } from "three";
import { Result } from "../helper/result";
import { SceneModelsTypes } from "./scene_models_type";
import { Instance } from "./scene_asset";
import { CoreThreeRepository } from "../repository/core_three_repository";
import { Type } from "class-transformer";

export class ZoneModel implements Instance {
  type = SceneModelsTypes.ZONE;
  color: string = "#E91E63";
  @Type(() => Vector3)
  position: Vector3;

  constructor(
    vector3: Vector3,
    public orientation: number[],
    public name: string,
    public width: number,
    public height: number,
    public length: number
  ) {
    this.position = vector3;
  }
  update = (coreThreeRepository: CoreThreeRepository) => this.toWebGl(coreThreeRepository)
  icon: string = "Zone";
  toWebGl = (coreThreeRepository: CoreThreeRepository) => coreThreeRepository.makeZone(this);
  isValid(): Result<string, ZoneModel> {
    return Result.ok();
  }
  static empty() {
    return new ZoneModel(new Vector3(0, 0, 0), [0, 0, 0, 0], "", 0, 0, 0);
  }
}
