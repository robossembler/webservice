export interface Cameras {
    camera: Camera[];
}

export interface Camera {
    sid: string;
    DevicePackage: DevicePackage;
    Module: Module;
    Launch: Launch;
    DTwin: DTwin[];
    Interface: InterfaceClass;
    Settings: Setting[];
}

export interface DTwin {
    interface: Interface;
}

export interface Interface {
    input: Input[];
}

export interface Input {
    camera_namespace?: string;
    camera_name?: string;
    serial_port?: string;
    pose?: string;
}

export interface DevicePackage {
    name: string;
    version: string;
    format: string;
}

export interface InterfaceClass {
    param: Param[];
}

export interface Param {
    type: string;
    dependency: Dependency;
}

export interface Dependency {
}

export interface Launch {
    package: string;
    executable: string;
}

export interface Module {
    name: string;
    description: string;
}

export interface Setting {
    name: string;
    description: string;
    type: string;
    defaultValue: string;
}
