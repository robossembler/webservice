import { Quaternion, Vector3 } from "three";
import { Result } from "../helper/result";
import { SceneModelsTypes } from "./scene_models_type";
import { Instance } from "./scene_asset";
import { CoreThreeRepository } from "../repository/core_three_repository";
import { Type } from "class-transformer";
import { Pose } from "../../features/behavior_tree_builder/presentation/ui/forms/move_to_pose/move_to_pose_robot_model";
import { SceneBuilderStore, SceneItems } from "../../features/scene_builder/presentation/scene_builder_store";

export class PointModel implements Instance {
  type = SceneModelsTypes.POINT;
  name: string;
  @Type(() => Vector3)
  position: Vector3;
  color: string = "#E91E63";
  size: number = 0.01;
  orientation: number[];
  icon: string = "Point";

  constructor() {}
  update = (coreThreeRepository: CoreThreeRepository) => this.toWebGl(coreThreeRepository);
  toWebGl = (coreThreeRepository: CoreThreeRepository) =>
    coreThreeRepository.makePoint(
      this.name,
      this.position,
      new Quaternion().fromArray(this.orientation),
      this.color,
      this.size
    );
  toSceneItems = (sceneMangerStore: SceneBuilderStore): SceneItems => {
    return {
      fn: () => {},
      name: this.name,
      isSelected: false,
      icon: this.icon,
    };
  };
  toDependency = (): Pose => {
    return {
      name: this.name,
      position: this.position,
      orientation: {
        x: this.orientation[0],
        y: this.orientation[1],
        z: this.orientation[2],
        w: this.orientation[3],
      },
    };
  };
  isValid(): Result<string, PointModel> {
    return Result.ok();
  }

  static empty() {
    return new PointModel();
  }
}
