export interface Topics {
    topics: Topic[];
}

export interface Topic {
    sid: string;
    name: string;
    type: string;
}
