import { Quaternion, Vector3, PerspectiveCamera, CameraHelper } from "three";
import { CoreThreeRepository, UserData } from "../repository/core_three_repository";
import { Result } from "../helper/result";
import { SceneModelsTypes } from "./scene_models_type";
import { Instance } from "./scene_asset";

export enum CameraTypes {
  RGB = "RGB",
}
export class CameraModel implements Instance {
  type = SceneModelsTypes.CAMERA;
  jointType = 'fixed';
  constructor(
    public orientation: number[],
    public position: Vector3,
    public name: string,
    public cameraType: CameraTypes,
    public width: number,
    public updateRate: number,
    public fov: number,
    public near: number,
    public far: number,
    public height: number,
    public topic: string,
    public aspect: number,
    public parent?: string,
    public fixed?: string
  ) {}
  update = (coreThreeRepository: CoreThreeRepository) => this.toWebGl(coreThreeRepository);
  icon: string = "Camera";
  toWebGl = (coreThreeRepository: CoreThreeRepository) => {
    const camera = coreThreeRepository.scene.getObjectByName(this.name);

    if (camera) {
      coreThreeRepository.scene.remove(coreThreeRepository.scene.getObjectByName(this.name + "camera_helper")!);
      coreThreeRepository.scene.remove(camera);
    }
    const perspectiveCamera = new PerspectiveCamera(this.fov, this.aspect, this.near, this.far);
    perspectiveCamera.name = this.name;
    perspectiveCamera.position.copy(this.position);
    perspectiveCamera.quaternion.copy(new Quaternion().fromArray(this.orientation));
    const cameraHelper = new CameraHelper(perspectiveCamera);
    cameraHelper.name = this.name + "camera_helper";
    perspectiveCamera.userData[UserData.selectedObject] = true;
    cameraHelper.userData[UserData.selectedObject] = true;
    coreThreeRepository.scene.add(...[perspectiveCamera, cameraHelper]);
  };
  validate = (): Result<string, CameraModel> => {
    return Result.ok(this);
  };

  static empty = () => new CameraModel([], new Vector3(0, 0, 0), "", CameraTypes.RGB, 0, 0, 50, 0.1, 2000, 0, "", 0);
}
