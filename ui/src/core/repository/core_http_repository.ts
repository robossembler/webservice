import { ClassConstructor, plainToInstance } from "class-transformer";
import { Result } from "../helper/result";
import { Parts } from "../../features/details/details_http_repository";
import { UUID } from "../../features/all_projects/data/project_http_repository";
import { SceneModel } from "../../features/scene_manager/model/scene_model";
import { SceneAsset } from "../model/scene_asset";

export enum HttpMethod {
  GET = "GET",
  POST = "POST",
  DELETE = "DELETE",
  PUT = "PUT",
}
export class HttpError extends Error {
  status: number;
  error: any;
  constructor(error: any, status: number) {
    super(error);
    this.error = error;
    this.status = status;
  }
}
export class HttpRepository {
  private server = "http://localhost:4001";
  public async _formDataRequest<T>(method: HttpMethod, url: string, formData: FormData): Promise<Result<HttpError, T>> {
    const reqInit = {
      body: formData,
      method: method,
    };

    const response = await fetch(this.server + url, reqInit);
    if (response.status !== 200) {
      throw Result.error(new Error(await response.json()));
    }
    return Result.ok(response.text as T);
  }
  public async _jsonRequest<T>(method: HttpMethod, url: string, data?: any): Promise<Result<HttpError, T>> {
    try {
      const reqInit = {
        body: data,
        method: method,
        headers: { "Content-Type": "application/json" },
      };
      if (data !== undefined) {
        reqInit["body"] = JSON.stringify(data);
      }
      const response = await fetch(this.server + url, reqInit);

      if (response.status !== 200) {
        return Result.error(new HttpError(this.server + url, response.status));
      }

      return Result.ok(await response.json());
    } catch (error) {
      return Result.error(new HttpError(error, 0));
    }
  }

  public async _request<T>(method: HttpMethod, url: string, data?: any): Promise<Result<HttpError, T>> {
    const reqInit = {
      body: data,
      method: method,
    };

    if (data !== undefined) {
      reqInit["body"] = data;
    }
    const response = await fetch(this.server + url, reqInit);
    if (response.status !== 200) {
      throw Result.error(new Error(await response.json()));
    }
    return Result.ok(response.text as T);
  }
  public async _arrayJsonToClassInstanceRequest<T>(
    method: HttpMethod,
    url: string,
    instance: ClassConstructor<T>,
    data?: any
  ): Promise<Result<HttpError, T[]>> {
    try {
      const reqInit = {
        body: data,
        method: method,
        headers: { "Content-Type": "application/json" },
      };
      if (data !== undefined) {
        reqInit["body"] = JSON.stringify(data);
      }
      const response = await fetch(this.server + url, reqInit);

      if (response.status !== 200) {
        return Result.error(new HttpError(this.server + url, response.status));
      }
      const array: any[] = await response.json();
      return Result.ok(
        array.map((el) => {
          return plainToInstance(instance, el) as T;
        })
      );
    } catch (error) {
      return Result.error(new HttpError(error, 0));
    }
  }
  public async _jsonToClassInstanceRequest<T>(
    method: HttpMethod,
    url: string,
    instance: ClassConstructor<T>,
    data?: any
  ) {
    try {
      const reqInit = {
        body: data,
        method: method,
        headers: { "Content-Type": "application/json" },
      };
      if (data !== undefined) {
        reqInit["body"] = JSON.stringify(data);
      }
      const response = await fetch(this.server + url, reqInit);

      if (response.status !== 200) {
        return Result.error(new HttpError(this.server + url, response.status));
      }
      return Result.ok(plainToInstance(instance, await response.json()) as T);
    } catch (error) {
      return Result.error(new HttpError(error, 0));
    }
  }
}

export class CoreHttpRepository extends HttpRepository {
  getAssetsActiveProject = async (): Promise<Result<HttpError, Parts[]>> => {
    return this._jsonRequest<Parts[]>(HttpMethod.GET, "/projects/assets");
  };
  
  getSceneAsset = (id: string) =>
    this._jsonToClassInstanceRequest(HttpMethod.GET, `/scenes/by_id?id=${id}`, SceneAsset) as Promise<
      Result<HttpError, SceneAsset>
    >;
  async getActiveProjectId() {
    return this._jsonRequest<UUID>(HttpMethod.GET, "/projects/get/active/project/id");
  }
  getAllScenes = () => this._jsonRequest<SceneModel[]>(HttpMethod.GET, "/scenes");
}
 