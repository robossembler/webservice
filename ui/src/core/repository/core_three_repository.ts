import {
  DirectionalLight,
  Object3D,
  PerspectiveCamera,
  Scene,
  WebGLRenderer,
  AmbientLight,
  Vector3,
  Mesh,
  Object3DEventMap,
  Box3,
  Sphere,
  LineBasicMaterial,
  Intersection,
  Raycaster,
  LineSegments,
  Vector2,
  Color,
  GridHelper,
  CameraHelper,
  Quaternion,
  MeshBasicMaterial,
  BoxGeometry,
  MeshStandardMaterial,
  LoadingManager,
} from "three";
import URDFLoader, { URDFLink } from "urdf-loader";
import { ColladaLoader } from "three/examples/jsm/loaders/ColladaLoader";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { OBJLoader } from "three/examples/jsm/loaders/OBJLoader";
import { STLLoader } from "three/examples/jsm/loaders/STLLoader";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { TransformControls } from "three/examples/jsm/controls/TransformControls";
import { SceneMode } from "../../features/scene_manager/model/scene_view";
import { UrdfTransforms, coordsToQuaternion } from "../../features/simulations/tranforms_model";
import { SolidModel } from "../model/solid_model";
import { Instance, SceneAsset } from "../model/scene_asset";
import { ZoneModel } from "../model/zone_model";
import { TypedEvent } from "../helper/typed_event";
import { Result } from "../helper/result";
import { RobotModel } from "../model/robot_model";
import { SceneItems } from "../../features/scene_builder/presentation/scene_builder_store";

Object3D.DEFAULT_UP = new Vector3(0, 0, 1);

export enum UserData {
  selectedObject = "selected_object",
  cameraInitialization = "camera_initialization",
  objectForMagnetism = "object_for_magnetism",
  loadObject = "load_object",
}

interface IEventDraggingChange {
  target: null;
  type: string;
  value: boolean;
}

interface IEmissiveCache {
  status: boolean;
  object3d: Object3D<Object3DEventMap>;
}
type SceneFrames = { [K in string]: URDFLink };

export class CoreThreeRepository extends TypedEvent<any> {
  scene = new Scene();
  camera: PerspectiveCamera;
  webGlRender: WebGLRenderer;
  htmlCanvasRef: HTMLCanvasElement;
  objectEmissive = new Map<string, IEmissiveCache>();
  transformControls: TransformControls;
  orbitControls: OrbitControls;
  htmlSceneWidth: number;
  htmlSceneHeight: number;
  sceneFrame?: SceneFrames;
  objLoader = new OBJLoader();
  glbLoader = new GLTFLoader();
  daeLoader = new ColladaLoader();
  stlLoader = new STLLoader();
  manager = new LoadingManager();
  urdfLoader = new URDFLoader(this.manager);

  watcherSceneEditorObject: Function;

  constructor(htmlCanvasRef: HTMLCanvasElement, watcherSceneEditorObject: Function) {
    super();
    this.htmlSceneWidth = window.innerWidth;
    this.htmlSceneHeight = window.innerHeight;
    this.watcherSceneEditorObject = watcherSceneEditorObject;
    const renderer = new WebGLRenderer({
      canvas: htmlCanvasRef as HTMLCanvasElement,
      antialias: true,
      alpha: true,
    });
    const aspectCamera = this.htmlSceneWidth / this.htmlSceneHeight;
    this.camera = new PerspectiveCamera(100, aspectCamera, 0.1, 2000);
    this.camera.position.set(1, 1, 1);

    this.webGlRender = renderer;
    this.htmlCanvasRef = htmlCanvasRef;

    this.transformControls = new TransformControls(this.camera, htmlCanvasRef);

    this.scene.add(this.transformControls);
    this.orbitControls = new OrbitControls(this.camera, this.htmlCanvasRef);
    this.scene.background = new Color("white");

    this.init();
  }
  updateInstance = (viewModel: Instance) => (
    this.scene.remove(this.scene.getObjectByName(viewModel.name)!), viewModel.toWebGl(this)
  );

  deleteAllObjectsScene = () => {
    this.getAllSceneNameModels().forEach((el) =>
      this.scene.remove(this.scene.getObjectByName(el) as Object3D<Object3DEventMap>)
    );
  };
  raiseAnObjectAboveZeroVector = (name: string) => {
    const mesh = this.scene.getObjectByName(name) as Object3D;
    mesh.position.sub(new Box3().setFromObject(mesh).min);
  };
  drawPoint(point: Vector3): Mesh<BoxGeometry, MeshBasicMaterial, Object3DEventMap> {
    var cube = new Mesh(new BoxGeometry(0.5, 0.5, 0.5), new MeshBasicMaterial({ color: 0x0095dd }));
    cube.position.add(point);
    this.scene.add(cube);
    return cube;
  }
  loadScene = (sceneAssets: SceneAsset) => sceneAssets.scene.forEach((el) => el.toWebGl(this));

  getCenterPoint = (object: Object3D<Object3DEventMap>) =>
    object.getWorldPosition(new Box3().setFromObject(object).getCenter(object.position));
  makeZone = (zoneModel: ZoneModel) => {
    const mesh = new Mesh(
      new BoxGeometry(zoneModel.width, zoneModel.height, zoneModel.length),
      new MeshBasicMaterial({
        color: zoneModel.color,
        transparent: true,
      })
    );
    mesh.name = zoneModel.name;
    mesh.userData[UserData.selectedObject] = "";
    mesh.position.copy(zoneModel.position);
    mesh.quaternion.copy(new Quaternion().fromArray(zoneModel.orientation));
    this.scene.add(mesh);
  };
  makeCube(inc: number, vector?: Vector3, color?: string, size?: number) {
    const mesh = new Mesh(
      new BoxGeometry(size ?? 10, size ?? 10, size ?? 10),
      new MeshBasicMaterial({ color: color ?? 0x0095dd, transparent: true, opacity: 0.5 })
    );
    mesh.userData[UserData.objectForMagnetism] = true;
    mesh.userData[UserData.selectedObject] = true;
    mesh.name = "cube" + String(inc);

    if (vector) {
      mesh.position.copy(vector);
    }
    this.scene.add(mesh);
  }

  makePoint(name: string, vector?: Vector3, quaternion?: Quaternion, color?: string, size?: number) {
    const cube = new Mesh(
      new BoxGeometry(size ?? 10, size ?? 10, size ?? 10),
      new MeshBasicMaterial({ color: color ?? 0x0095dd, transparent: true, opacity: 0.5 })
    );
    cube.userData[UserData.objectForMagnetism] = true;
    cube.userData[UserData.selectedObject] = true;

    if (vector) {
      cube.position.copy(vector);
    }
    if (quaternion) {
      cube.quaternion.copy(quaternion);
    }
    cube.name = name;
    this.scene.add(cube);
  }
  deleteSceneItemByName = (name: string) =>
    this.scene.children.forEach((el) => el.name.isEqualR(name).map(() => this.scene.remove(el)));

  deleteSceneItem = (item: SceneItems) =>
    item.icon.isEqualR("Camera").fold(
      () => this.deleteSceneItemByName(item.name + "camera_helper"),
      () => this.deleteSceneItemByName(item.name)
    );
  loadUrdfRobot = (robotModel: RobotModel) =>
    this.urdfLoader.load(robotModel.httpUrl, (robot) => {
      robot.userData[UserData.selectedObject] = true;
      robot.name = robotModel.name;
      if (robotModel.position) robot.position.copy(robotModel.position);
      if (robotModel.orientation) robot.quaternion.copy(new Quaternion().fromArray(robotModel.orientation));
      if (robotModel.jointPosition.isNotEmpty()) {
        robotModel.jointPosition.forEach((el) => {
          robot.setJointValue(el.name, el.angle);
        });
      }
      if (robotModel.jointPosition.isEmpty()) {
        Object.entries(robot.joints).forEach(([name, uRDFJoint]) => {
          if (uRDFJoint.jointType !== "fixed") {
            robotModel.jointPosition.push({
              angle: Number(uRDFJoint.angle),
              limit: {
                lower: Number(uRDFJoint.limit.lower),
                upper: Number(uRDFJoint.limit.upper),
              },
              name: name,
            });
          }
        });
      }

      this.scene.add(robot);

      // @ts-expect-error
      this.sceneFrame = robot.frames;
    });
  loadUrdf = (urlPath: string, vector3?: Vector3, quaternion?: Quaternion) =>
    this.urdfLoader.load(urlPath, (robot) => {
      robot.userData[UserData.selectedObject] = true;

      if (vector3) robot.position.copy(new Vector3(0, 0, 0));
      if (quaternion) robot.quaternion.copy(quaternion);
      Object.entries(robot.joints).forEach(([k, v]) => robot.setJointValue(k, 1));

      this.scene.add(robot);

      // @ts-expect-error
      this.sceneFrame = robot.frames;
    });

  loadHttpAndPreview(path: string, name: string, loadCallback?: Function) {
    this.loader(
      path,
      () => {
        // this.fitCameraToCenteredObject([name])
        // this.scene.getObjectByName(name)
      },
      name,
      new Vector3(0, 0, 0)
    );
  }

  setTransformMode(mode?: SceneMode) {
    switch (mode) {
      case undefined:
        this.transformControls.detach();
        this.transformControls.dispose();
        break;
      case SceneMode.Move:
        this.transformControls.setMode("translate");
        break;
      case SceneMode.Rotate:
        this.transformControls.setMode("rotate");
        break;
    }
  }

  updateSolidBody = (solidBodyModel: SolidModel) => {
    const mesh = this.scene.getObjectByName(solidBodyModel.name);
    mesh?.position.copy(solidBodyModel.position);
    mesh?.quaternion.copy(new Quaternion().fromArray(solidBodyModel.orientation));
  };

  disposeTransformControlsMode = () => this.transformControls.detach();

  setRayCastAndGetFirstObjectName(vector: Vector2): Result<void, string> {
    this.scene.add(this.transformControls);
    const raycaster = new Raycaster();
    raycaster.setFromCamera(vector, this.camera);
    const intersects = raycaster.intersectObjects(this.scene.children);
    if (intersects.length > 0) {
      return Result.ok(intersects[0].object.name);
    }

    return Result.error(undefined);
  }

  setTransformControlsAttach(object: Object3D<Object3DEventMap>) {
    if (object instanceof CameraHelper) {
      this.transformControls.attach(object.camera);
      return;
    }
    this.transformControls.attach(object);
  }

  setRayCastAndGetFirstObject(vector: Vector2): Result<void, Object3D<Object3DEventMap>> {
    try {
      const result = this.setRayCast(vector);
      return result.fold(
        (intersects) => {
          const result = intersects.find((element) => element.object.userData[UserData.selectedObject] !== undefined);
          if (result === undefined) {
            return Result.error(undefined);
          }
          return Result.ok(result.object);
        },
        (_error) => {
          return Result.error(undefined);
        }
      );
    } catch (error) {
      return Result.error(undefined);
    }
  }

  setRayCast = (vector: Vector2): Result<void, Intersection<Object3D<Object3DEventMap>>[]> => {
    const raycaster = new Raycaster();
    raycaster.setFromCamera(vector, this.camera);
    const intersects = raycaster.intersectObjects(this.scene.children);
    if (intersects.length > 0) {
      return Result.ok(intersects);
    }

    return Result.error(undefined);
  };

  setRayCastAndGetFirstObjectAndPointToObject = (vector: Vector2): Result<void, Vector3> =>
    this.setRayCast(vector).fold(
      (intersects) => {
        if (intersects.isNotEmpty()) {
          return Result.ok(intersects[0].point);
        }
      },
      () => Result.error(undefined)
    ) as Result<void, Vector3>;

  light() {
    const ambientLight = new AmbientLight(0xffffff, 0.7);
    this.scene.add(ambientLight);

    this.scene.add(new AmbientLight(0x222222));

    this.scene.add(new DirectionalLight(0xffffff, 1));
  }

  addListeners() {
    window.addEventListener(
      "resize",
      () => {
        this.camera.aspect = this.htmlSceneWidth / this.htmlSceneHeight;
        this.camera.updateProjectionMatrix();

        this.webGlRender.setSize(this.htmlSceneWidth, this.htmlSceneHeight);
      },
      false
    );

    this.transformControls.addEventListener("dragging-changed", (event) => {
      const e = event as unknown as IEventDraggingChange;
      this.orbitControls.enabled = !e.value;
    });
    this.transformControls.addEventListener("objectChange", (event) => {
      this.watcherSceneEditorObject(event.target.object);
    });
  }

  init() {
    this.light();
    this.addListeners();
    const gridHelper = new GridHelper(1000, 100, 0x888888, 0x444444);
    gridHelper.userData = {};
    gridHelper.userData[UserData.cameraInitialization] = true;
    gridHelper.rotation.x = -Math.PI / 2;

    this.scene.add(gridHelper);
  }

  render() {
    this.webGlRender.setSize(this.htmlSceneWidth, this.htmlSceneHeight);
    this.webGlRender.setAnimationLoop(() => {
      this.webGlRender.render(this.scene, this.camera);
    });
  }

  getAllSceneNameModels(): string[] {
    return this.scene.children.filter((el) => el.name !== "").map((el) => el.name);
  }

  getObjectsAtName(name: string): Object3D<Object3DEventMap> {
    return this.scene.children.filter((el) => el.name === name)[0];
  }

  loader(url: string, callBack: Function, name: string, position?: Vector3, quaternion?: Quaternion) {
    const ext = url.split(/\./g).pop()!.toLowerCase();
    switch (ext) {
      case "gltf":
      case "glb":
        this.glbLoader.load(
          url,
          (result) => {
            this.scene.add(result.scene);
            callBack();
          },
          (err) => {}
        );
        break;
      case "obj":
        this.objLoader.load(
          url,
          (result) => {
            result.userData[UserData.selectedObject] = true;
            result.children.forEach((el) => {
              el.userData[UserData.selectedObject] = true;
              el.name = name;

              if (position) el.position.copy(position);
              if (quaternion) el.quaternion.copy(quaternion);

              // this.emit(new StaticAssetItemModel(el.name, el.position, el.quaternion));
              this.scene.add(el);
              callBack();
            });
          },
          (err) => {}
        );
        break;
      case "dae":
        this.daeLoader.load(
          url,
          (result) => {
            result.scene.children.forEach((el) => {
              el.userData[UserData.selectedObject] = true;
              el.name = name;
            });
            result.scene.name = name;
            if (position) result.scene.position.copy(position);
            if (quaternion) result.scene.quaternion.copy(quaternion);
            this.scene.add(result.scene);
            callBack();
          },
          (err) => console.log(err)
        );
        break;
      case "stl":
        this.stlLoader.load(
          url,
          (result) => {
            const mesh = new Mesh(
              result,
              new MeshStandardMaterial({
                color: "red",
                metalness: 0.35,
                roughness: 1,
                opacity: 1.0,
                transparent: false,
              })
            );
            mesh.name = name;

            // var geometry = mesh.geometry;
            // geometry.computeBoundingBox(); // Вычисляем ограничивающий параллелепипед для геометрии

            if (position) mesh.position.copy(position);

            this.scene.add(mesh);
            callBack();
          },

          (err) => {}
        );
        break;
    }
  }

  fitCameraToCenteredObject(objects: string[], offset = 4) {
    const boundingBox = new Box3().setFromPoints(
      objects.map((el) => this.getObjectsAtName(el)).map((el) => el.position)
    );

    var vector3 = new Vector3();
    boundingBox.getSize(vector3);

    const fov = this.camera.fov * (Math.PI / 180);
    const fovh = 2 * Math.atan(Math.tan(fov / 2) * this.camera.aspect);
    let dx = vector3.z / 2 + Math.abs(vector3.x / 2 / Math.tan(fovh / 2));
    let dy = vector3.z / 2 + Math.abs(vector3.y / 2 / Math.tan(fov / 2));
    let cameraZ = Math.max(dx, dy);

    if (offset !== undefined && offset !== 0) cameraZ *= offset;

    this.camera.position.set(0, 0, cameraZ);

    const minZ = boundingBox.min.z;
    const cameraToFarEdge = minZ < 0 ? -minZ + cameraZ : cameraZ - minZ;

    this.camera.far = cameraToFarEdge * 3;
    this.camera.updateProjectionMatrix();
    let orbitControls = new OrbitControls(this.camera, this.htmlCanvasRef);

    orbitControls.maxDistance = cameraToFarEdge * 2;
    this.orbitControls = orbitControls;
  }

  switchObjectEmissive(name: string) {
    const mesh = this.getObjectsAtName(name);
    const result = this.objectEmissive.get(mesh.name);
    if (result?.status) {
      this.scene.remove(mesh);
      this.scene.add(result.object3d);
      this.objectEmissive.set(mesh.name, {
        status: false,
        object3d: mesh,
      });
    } else {
      this.objectEmissive.set(mesh.name, {
        status: true,
        object3d: mesh,
      });

      if (mesh instanceof Mesh) {
        const newMesh = new LineSegments(mesh.geometry, new LineBasicMaterial({ color: 0x000000 }));
        newMesh.name = mesh.name;
        newMesh.position.copy(mesh.position);

        this.scene.remove(mesh);
        this.scene.add(newMesh);
      }
    }
  }

  fitSelectedObjectToScreen(objects: string[]) {
    //https://stackoverflow.com/questions/14614252/how-to-fit-camera-to-object
    let boundSphere = new Box3()
      .setFromPoints(objects.map((el) => this.getObjectsAtName(el)).map((el) => el.position))
      .getBoundingSphere(new Sphere());
    let vFoV = this.camera.getEffectiveFOV();
    let hFoV = this.camera.fov * this.camera.aspect;
    let FoV = Math.min(vFoV, hFoV);
    let FoV2 = FoV / 2;
    let dir = new Vector3();
    this.camera.getWorldDirection(dir);
    let bsWorld = boundSphere.center.clone();
    let th = (FoV2 * Math.PI) / 180.0;
    let sina = Math.sin(th);
    let R = boundSphere.radius;
    let FL = R / sina;
    let cameraDir = new Vector3();
    let cameraOffs = cameraDir.clone();
    cameraOffs.multiplyScalar(-FL);

    let newCameraPos = bsWorld.clone().add(cameraOffs);

    this.camera.position.copy(newCameraPos);
    this.camera.lookAt(bsWorld);
    this.orbitControls = new OrbitControls(this.camera, this.htmlCanvasRef);
  }
  urdfTransforms = (urdfTransforms: UrdfTransforms) => {
    urdfTransforms.transforms.forEach((transform) => {
      if (this.sceneFrame) {
        const currentLink = this.sceneFrame[transform?.child_frame_id ?? ""];

        currentLink.quaternion.copy(coordsToQuaternion(transform.transform.rotation));
      }
    });
  };
}
