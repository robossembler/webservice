import { Socket, io } from "socket.io-client";
import { Result } from "../helper/result";
import { TypedEvent } from "../helper/typed_event";

export class SocketRepository extends TypedEvent<any> {
  serverURL = "ws://localhost:4001";
  socket: Socket | undefined;
  
  async connect():Promise<Result<boolean, boolean>> {
    const socket = io(this.serverURL);
    
    this.socket = socket;
    socket.connect();
    socket.on('realtime', (d) =>{
      this.emit({
        event:"realtime",
        payload:d
      })
    })
    if(socket.connected){
      return Result.ok(true)
    }
    return Result.error(false)
  }

}

export const socketRepository = new SocketRepository()