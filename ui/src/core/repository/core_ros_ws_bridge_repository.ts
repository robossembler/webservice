import { FoxgloveClient } from "@foxglove/ws-protocol"
import { MessageReader } from '@foxglove/rosmsg2-serialization';
import { parse as parseMessageDefinition } from '@foxglove/rosmsg';
import { TypedEvent } from "../helper/typed_event";

export class CoreRosWsBridgeRepository<T> extends TypedEvent<T> {
    client?: FoxgloveClient;
    connect(topic: string) {
        const client = new FoxgloveClient({
            ws: new WebSocket(`ws://localhost:8765`, [FoxgloveClient.SUPPORTED_SUBPROTOCOL]),
        });
        const deserializers = new Map();
        client.on("advertise", (channels) => {
            for (const channel of channels) {
                if (channel.topic.isEqual(topic)) {
                    const subId = client.subscribe(channel.id);
                    deserializers.set(subId, (data: any) => {
                        return {
                            data: data,
                            channel: channel
                        }
                    });
                }
            }
        });
        client.on("message", ({ subscriptionId, timestamp, data }) => {
            const channel = deserializers.get(subscriptionId)(data);
            this.emit(new MessageReader(parseMessageDefinition(channel.channel?.schema!, {
                ros2: true,
            })).readMessage(channel.data))

        });
    }
}
