import { createBrowserRouter } from "react-router-dom";
import { AllProjectScreen, AllProjectScreenPath } from "../../features/all_projects/presentation/all_projects_screen";
import { SceneManger, SceneManagerPath } from "../../features/scene_manager/presentation/scene_manager";
import {
  BehaviorTreeBuilderPath,
  BehaviorTreeBuilderScreen,
} from "../../features/behavior_tree_builder/presentation/behavior_tree_builder_screen";
import { DataSetScreen, DatasetsScreenPath } from "../../features/dataset/dataset_screen";
import { AssemblesScreen, AssemblesScreenPath } from "../../features/assembles/assembles_screen";
import { SimulationScreen, SimulationScreenPath } from "../../features/simulations/simulations_screen";
import { EstimateScreen, EstimateScreenPath } from "../../features/estimate/estimate_screen";
import {
  CalculationInstanceScreenPath,
  CalculationInstanceScreen,
} from "../../features/calculation_instance/presentation/calculation_instance_screen";
import { DetailsScreenPath, DetailsScreen } from "../../features/details/details_screen";
import { DigitalTwinsScreen, DigitalTwinsScreenPath } from "../../features/digital_twins/digital_twins_screen";
import { TopicsScreen, TopicsScreenPath } from "../../features/topics/topics_screen";
import { SkillsScreen, SkillsScreenPath } from "../../features/skills/skills_screen";
import { CalculationsTemplateScreenPath } from "../../features/calculations_template/calculations_template_screen";
import { BehaviorTreeManagerScreen, BehaviorTreeManagerScreenPath } from "../../features/behavior_tree_manager/behavior_tree_manager_screen";
import { SceneBuilderScreenPath, SceneBuilderScreen } from "../../features/scene_builder/presentation/scene_builder_screen";

const idURL = ":id";
export const router = createBrowserRouter([
  {
    path: AllProjectScreenPath,
    element: <AllProjectScreen />,
  },
  {
    path:SceneBuilderScreenPath + idURL,
    element:<SceneBuilderScreen/>
  },
  {
    path: SceneManagerPath + idURL,
    element: <SceneManger />,
  },
  {
    path: SceneManagerPath,
    element: <SceneManger />,
  },
  {
    path: BehaviorTreeBuilderPath(idURL),
    element: <BehaviorTreeBuilderScreen />,
  },
  {
    path: BehaviorTreeBuilderPath(),
    element: <BehaviorTreeBuilderScreen />,
  },
  {
    path: BehaviorTreeBuilderPath + idURL,
    element: <BehaviorTreeBuilderScreen />,
  },
  {
    path: DatasetsScreenPath,
    element: <DataSetScreen />,
  },
  {
    path: DetailsScreenPath,
    element: <DetailsScreen />,
  },
  {
    path: AssemblesScreenPath,
    element: <AssemblesScreen />,
  },
  {
    path: SimulationScreenPath,
    element: <SimulationScreen />,
  },
  {
    path: EstimateScreenPath,
    element: <EstimateScreen />,
  },
  {
    path: CalculationInstanceScreenPath,
    element: <CalculationInstanceScreen />,
  },
  {
    path: DigitalTwinsScreenPath,
    element: <DigitalTwinsScreen />,
  },
  {
    path: TopicsScreenPath,
    element: <TopicsScreen />,
  },
  {
    path: SkillsScreenPath,
    element: <SkillsScreen />,
  },
  {
    path: CalculationsTemplateScreenPath,
    element: <CalculationInstanceScreen />,
  },
  {
    path: BehaviorTreeManagerScreenPath,
    element: <BehaviorTreeManagerScreen />,
  },
]);
