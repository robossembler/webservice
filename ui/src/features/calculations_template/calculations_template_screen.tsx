import { observer } from "mobx-react-lite";
import { CalculationsTemplateStore } from "./calculations_template_store";
import React from "react";
import { useStore } from "../../core/helper/use_store";

export const CalculationsTemplateScreenPath = "/calculations/template";

export const CalculationsTemplateScreen = observer(() => {
  const store = useStore(CalculationsTemplateStore);

  return <></>;
});
