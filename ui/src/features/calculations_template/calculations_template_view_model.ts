import { ValidationModel } from "../../core/model/validation_model";

export class CalculationsTemplateViewModel extends ValidationModel {
  static empty() {
    return new CalculationsTemplateViewModel();
  }
}
