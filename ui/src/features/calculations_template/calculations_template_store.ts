import makeAutoObservable from "mobx-store-inheritance";
import { UiDrawerFormState } from "../../core/store/base_store";
import { HttpError } from "../../core/repository/core_http_repository";
import { CalculationsTemplateViewModel } from "./calculations_template_view_model";
import { NavigateFunction } from "react-router-dom";
export enum CalculationTemplateDrawer {
  newSkill = "Новый навык",
}
export class CalculationsTemplateStore extends UiDrawerFormState<CalculationsTemplateViewModel, HttpError> {
  viewModel: CalculationsTemplateViewModel = CalculationsTemplateViewModel.empty();
  constructor() {
    super(CalculationTemplateDrawer);
    makeAutoObservable(this);
  }
  init = async (navigate?: NavigateFunction | undefined): Promise<any> => {};
}
