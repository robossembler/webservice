import { Result } from "../../../core/helper/result";
import { HttpMethod, CoreHttpRepository } from "../../../core/repository/core_http_repository";
import { IProjectModel } from "../model/project_model";
export interface ICreateProjectViewModel {
  description: string;
}
export interface UUID {
  id: string;
}
export class ProjectHttpRepository extends CoreHttpRepository {
  featureApi = `/projects`;

  getAllProject = async () => this._jsonRequest<IProjectModel[]>(HttpMethod.GET, this.featureApi);
  saveProject = async (model: ICreateProjectViewModel): Promise<Result<Error, UUID>> =>
    await this._jsonRequest<UUID>(HttpMethod.POST, this.featureApi, model);
  setActivePipeline = async (id: string) =>
    this._jsonRequest(HttpMethod.POST, `${this.featureApi}/set/active/project?id=${id}`);
  setProjectRootFile = async (file: File, projectId: string) => {
    const formData = new FormData();
    formData.append("file", file);
    return await this._formDataRequest(HttpMethod.POST, `${this.featureApi}/upload?id=${projectId}`, formData);
  };
}
