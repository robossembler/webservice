
export interface IProjectModel {
  _id?: string;
  rootDir: string;
  description: string;
  isActive?: boolean;
}
export interface IProjectView {
  isActive: boolean;
  description: string;
  id: string;
}