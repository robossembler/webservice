import makeAutoObservable from "mobx-store-inheritance";
import { ProjectHttpRepository, UUID } from "../data/project_http_repository";
import { IProjectModel, IProjectView } from "../model/project_model";
import { ModalStore } from "../../../core/store/base_store";
import { message } from "antd";


export class ProjectView {
  isActive: boolean;
  description: string;
  id: string;
  constructor(view: IProjectView) {
    this.isActive = view.isActive;
    this.description = view.description;
    this.id = view.id;
  }
}
export class AllProjectStore extends ModalStore {
  projectsModels?: IProjectModel[];
  activeProjectId?: UUID;
  newProjectDescription: string = "";
  file?: File;

  projectHttpRepository: ProjectHttpRepository = new ProjectHttpRepository();
  constructor() {
    super();

    makeAutoObservable(this);
  }
  async getProjects(): Promise<void> {
    await this.mapOk<IProjectModel[]>("projectsModels", this.projectHttpRepository.getAllProject());
  }
  async getActiveProjectId(): Promise<void> {
    await this.mapOk<UUID>("activeProjectId", this.projectHttpRepository.getActiveProjectId());
  }
  setActiveProject = async (id: string) => {
    await this.messageHttp(this.projectHttpRepository.setActivePipeline(id), {
      successMessage: "проект активирован",
      errorMessage: "ошибка активации",
    });
    await this.mapOk<UUID>("activeProjectId", this.projectHttpRepository.getActiveProjectId());
    await this.mapOk<IProjectModel[]>("projectsModels", this.projectHttpRepository.getAllProject());
  };
  async init() {
    await Promise.all([this.getProjects(), this.getActiveProjectId()]);
    this.projectsModels?.map((el) => (el._id === this.activeProjectId ? ((el.isActive = true), el) : el));
  }
  setDescriptionToNewProject = (value: string) => {
    this.newProjectDescription = value;
  };
  async saveProject(): Promise<void> {
    if (this.newProjectDescription.isEmpty()) {
      message.error("Описание проекта не должно быть пустым");
      return;
    }
    if (this.file === undefined) {
      message.error("загрузите файл");
      return
    }

    this.isLoading = true;
    (
      await this.projectHttpRepository.saveProject({
        description: this.newProjectDescription,
      })
    ).fold(
      async (uuid) => {
        this.newProjectDescription = "";
        await this.projectHttpRepository.setProjectRootFile(this.file as File, uuid.id);
        this.isLoading = false;
        this.init();
      },
      async (_) => {
        this.isError = true;
      }
    );
  }
}
