import * as React from "react";
import { AllProjectStore } from "./all_projects_store";
import { observer } from "mobx-react-lite";
import { useNavigate } from "react-router-dom";
import { Modal, Upload } from "antd";
import { PreviewPage } from "../../../core/ui/pages/preview_page";
import { CoreText, CoreTextType } from "../../../core/ui/text/text";
import { CoreButton } from "../../../core/ui/button/button";
import { CoreInput } from "../../../core/ui/input/input";
import { DetailsScreenPath } from "../../details/details_screen";
import { DigitalTwinsScreenPath } from "../../digital_twins/digital_twins_screen";
import { TopicsScreenPath } from "../../topics/topics_screen";
import { SkillsScreenPath } from "../../skills/skills_screen";
import { CalculationsTemplateScreenPath } from "../../calculations_template/calculations_template_screen";
import { useStore } from "../../../core/helper/use_store";

export const AllProjectScreenPath = "/";
export const AllProjectScreen: React.FunctionComponent = observer(() => {
  const store = useStore(AllProjectStore);
  const navigate = useNavigate();

  return (
    <PreviewPage
      largeText={"Проекты"}
      needBackButton={false}
      minText="Создать новый проект?"
      click={() => store.modalShow()}
      isError={false}
      isLoading={store.isLoading}
      children={
        <>
          <div onClick={() => navigate(DigitalTwinsScreenPath)}>Digital twins</div>
          <div onClick={() => navigate(TopicsScreenPath)}>Topics</div>
          <div onClick={() => navigate(SkillsScreenPath)}>Skills</div>
          <div onClick={() => navigate(CalculationsTemplateScreenPath)}>Calculation template</div>

          {store.projectsModels?.map((el) => {
            return (
              <div
                style={{
                  border: "1px solid #CAC4D0",
                  margin: 10,
                  height: 60,
                  alignItems: "center",
                  borderRadius: 7,
                  display: "flex",
                  justifyContent: "space-between",
                  backgroundColor: el.isActive ? "rgba(104, 80, 164, 1)" : "rgba(255, 255, 255, 1)",
                }}
              >
                <CoreText
                  text={el.description}
                  type={CoreTextType.medium}
                  style={{ color: el.isActive ? "white" : "black", margin: 10 }}
                />
                {el.isActive ? (
                  <CoreButton
                    text="Перейти"
                    onClick={() => {
                      navigate(`${DetailsScreenPath}`);
                    }}
                    textStyle={{ color: "black", textAlign: "center" }}
                    style={{ marginRight: 10, backgroundColor: "white", width: 126 }}
                  />
                ) : (
                  <CoreButton
                    text="Активировать"
                    onClick={() => store.setActiveProject(el._id as string)}
                    style={{ marginRight: 10, width: 126 }}
                    filled={true}
                  />
                )}
              </div>
            );
          })}
          <Modal
            destroyOnClose={true}
            open={store.isModalOpen}
            footer={null}
            closable={false}
            closeIcon={null}
            onCancel={store.modalCancel}
          >
            <CoreText text="Новый проект" type={CoreTextType.big} />
            <CoreInput label={"название проекта"} onChange={(text) => store.setDescriptionToNewProject(text)} />
            <div style={{ height: 20 }} />
            <Upload
              onChange={(e) => {
                store.file = e.file.originFileObj;
              }}
            >
              <CoreButton text="файл проекта" />
            </Upload>
            <div style={{ height: 20 }} />
            <div style={{ display: "flex" }}>
              <CoreButton
                text="Сохранить"
                onClick={() => store.saveProject()}
                style={{ marginRight: 10 }}
                filled={true}
              />
              <CoreButton text="Отменить" onClick={() => store.modalCancel()} style={{ marginRight: 10 }} />
            </div>
          </Modal>
        </>
      }
    />
  );
});
