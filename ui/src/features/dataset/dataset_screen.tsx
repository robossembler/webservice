import * as React from "react";
import { Drawer } from "antd";
import { FormBuilder } from "../../core/ui/form_builder/form_builder";
import { DataSetStore, DrawersDataset } from "./dataset_store";
import { observer } from "mobx-react-lite";
import { MainPage } from "../../core/ui/pages/main_page";
import { CardDataSet, CardDataSetType } from "./card_dataset";
import { CoreInput } from "../../core/ui/input/input";
import { CoreButton } from "../../core/ui/button/button";
import { useStore } from "../../core/helper/use_store";

export const DatasetsScreenPath = "/dataset";

export const DataSetScreen: React.FunctionComponent = observer(() => {
  const store = useStore(DataSetStore);

  return (
    <>
      <MainPage
        isLoading={false}
        page="Датасеты"
        bodyChildren={
          <div
            style={{
              display: "grid",
              gridTemplateColumns: "repeat(auto-fill, minmax(272px, 1fr))",
              gridGap: 10,
              width: "100%",
              margin: 12,
              overflow: "auto",
              height: window.innerHeight,
            }}
          >
            {store.datasets?.map((el) => {
              return (
                <CardDataSet
                  type={CardDataSetType.COMPLETED}
                  objects={el.dataSetObjects}
                  unixDate={el.unixTime}
                  processStatus={el.processStatus}
                  neuralNetworkAction={el.datasetType}
                  neuralNetworkName={el.name}
                  id={el._id}
                  onClickButton={(id: string) => store.runProcess(id)}
                  onDelete={(id: string) => store.deleteDataset(id)}
                  onEdit={(id: string) => store.editDataset(id)}
                />
              );
            })}
            <CardDataSet
              type={CardDataSetType.EMPTY}
              onClickEmptyCard={() => {
                store.openEmptyCard();
              }}
            />
            <Drawer
              title={DrawersDataset.FormBuilderDrawer}
              destroyOnClose={true}
              onClose={() => store.editDrawer(DrawersDataset.FormBuilderDrawer, false)}
              open={store.drawers.find((el) => el.name === DrawersDataset.FormBuilderDrawer)?.status ?? false}
            >
              <FormBuilder
                formBuilder={store.dataSetModel.formBuilder}
                onChange={(el) => {
                  store.dataSetModel.formBuilder = el;
                }}
              />
              <div style={{ display: "flex" }}>
                <CoreButton
                  text="Сохранить"
                  filled={true}
                  onClick={() => store.editDrawer(DrawersDataset.FormBuilderDrawer, false)}
                />
                <div style={{ width: 10 }} />
                <CoreButton text="Отмена" onClick={() => store.editDrawer(DrawersDataset.FormBuilderDrawer, false)} />
              </div>
            </Drawer>
            <Drawer
              title={store.titleDrawer}
              destroyOnClose={true}
              onClose={() => store.editDrawer(DrawersDataset.NewDataset, false)}
              open={store.drawers.find((el) => el.name === DrawersDataset.NewDataset)?.status}
            >
              <div
                style={{ display: "flex", flexDirection: "column", justifyContent: "space-between", height: "100%" }}
              >
                <div>
                  <CoreInput
                    value={store.dataSetModel.name}
                    label={"Имя датасета"}
                    onChange={(e) => store.setNewDatasetName(e)}
                  />

                  <div style={{ width: 180, marginTop: 10, marginBottom: 30 }}>
                    <CoreButton
                      onClick={() => store.editDrawer(DrawersDataset.FormBuilderDrawer, true)}
                      text="Настройки датасета"
                      filled={true}
                    />
                  </div>
                </div>

                <div style={{ display: "flex" }}>
                  <CoreButton text="Сохранить" filled={true} onClick={() => store.saveDataset()} />
                  <div style={{ width: 10 }} />
                  <CoreButton text="Отмена" onClick={() => store.editDrawer(DrawersDataset.NewDataset, false)} />
                </div>
              </div>
            </Drawer>
          </div>
        }
      />
    </>
  );
});
