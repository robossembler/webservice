import { Spin } from "antd";
import { LoadingOutlined } from "@ant-design/icons";
import poseIMG from "../../core/assets/images/pose_estemation.jpg";
import { Icon } from "../../core/ui/icons/icons";
import { CoreText, CoreTextType } from "../../core/ui/text/text";
import { CoreButton } from "../../core/ui/button/button";
import type { MenuProps } from "antd";
import { Dropdown } from "antd";
import { ProcessStatus } from "./dataset_model";

export interface IMenuItem {
  onClick: Function;
  name: string;
}

export const enum CardDataSetType {
  EMPTY = "EMPTY",
  COMPLETED = "COMPLETED",
}

interface ICardDataSetProps {
  type: CardDataSetType;
  neuralNetworkAction?: string;
  neuralNetworkName?: string;
  objects?: string[];
  unixDate?: number;
  processStatus?: string;
  onClickButton?: (id: string) => void;
  onClickEmptyCard?: Function;
  onEdit?: Function;
  onDelete?: Function;
  id?: string;
}

export const CardDataSet = (props: ICardDataSetProps) => {
  const menu: IMenuItem[] = [
    {
      onClick: () => {
        if (props.onEdit) props.onEdit(props.id);
      },
      name: "Редактировать",
    },
    {
      onClick: () => {
        if (props.onDelete) props.onDelete(props.id);
      },
      name: "Удалить",
    },
  ];
  const items: MenuProps["items"] = menu.map((el, index) => {
    return {
      key: String(index),
      label: <CoreText text={el.name} type={CoreTextType.medium} />,
      onClick: () => el.onClick(props.id),
    };
  });
  return (
    <div
      style={{
        width: 272,
        height: 372,
        borderRadius: 12,
        border: "1px solid #CAC4D0",
        backgroundColor: "rgba(254, 247, 255, 1)",
        cursor: "pointer",
      }}
      onClick={() => {
        if (props.type.isEqual(CardDataSetType.EMPTY) && props.onClickEmptyCard) {
          props.onClickEmptyCard();
        }
      }}
    >
      <div style={{ height: 80 }}>
        {props.type.isEqual(CardDataSetType.EMPTY) ? null : (
          <div style={{ display: "flex", alignItems: "center", justifyContent: "space-between" }}>
            <div style={{ width: 70, marginTop: 11 }}></div>
            <div style={{ height: 80, alignContent: "center", marginRight: 40 }}>
              <CoreText text={props.neuralNetworkName ?? ""} type={CoreTextType.header} />
              <CoreText text={props.neuralNetworkAction ?? ""} type={CoreTextType.medium} />
            </div>
            <div>
              <Dropdown overlayStyle={{ backgroundColor: "rgba(243, 237, 247, 1)" }} menu={{ items }}>
                <div>
                  <Icon type="Settings" />
                </div>
              </Dropdown>
            </div>
          </div>
        )}
      </div>
      <img alt="pose" style={{ width: "100%" }} src={poseIMG}></img>
      <div
        style={{
          textAlignLast: props.type.isEqual(CardDataSetType.EMPTY) ? "center" : "auto",
          marginTop: props.type.isEqual(CardDataSetType.EMPTY) ? 80 : 10,
        }}
      >
        {props.type === CardDataSetType.EMPTY ? (
          <Icon type="PlusCircle" />
        ) : (
          <div style={{ margin: 10 }}>
            <CoreText text={`Объектов: ${props.objects?.length ?? 0}`} type={CoreTextType.large} />
            <CoreText text={Number(props.unixDate).unixFromDate()} type={CoreTextType.medium} color="#49454F" />
            <div style={{ height: 40 }} />
            <div style={{ width: 240, overflow: "hidden", whiteSpace: "nowrap", height: 40 }}>
              <CoreText text={props.objects?.join(", ") ?? ""} type={CoreTextType.medium} color="#49454F" />
            </div>
            <div style={{ display: "flex", flexDirection: "row", justifyContent: "flex-end", alignItems: "center" }}>
              {props.processStatus === ProcessStatus.RUN ? (
                <Spin indicator={<LoadingOutlined style={{ fontSize: 34, color: "rgba(103, 80, 164, 1)" }} spin />} />
              ) : null}
              <div style={{ width: 20 }} />
              {props.processStatus === ProcessStatus.NEW ? (
                <CoreButton
                  onClick={() => {
                    if (props.type.isEqual(CardDataSetType.COMPLETED) && props.onClickButton && props.id) {
                      props.onClickButton(props.id);
                    }
                  }}
                  filled={true}
                  text="Старт"
                />
              ) : null}
              {props.processStatus === ProcessStatus.END ? (  <CoreButton
                 
                  onClick={() => {
                    if (props.type.isEqual(CardDataSetType.COMPLETED) && props.onClickButton && props.id) {
                      props.onClickButton(props.id);
                    }
                  }}
                  
                  text="Завершен"
                />):null}
              {props.processStatus === ProcessStatus.RUN ? (<>
                <CoreButton
                  onClick={() => {
                    if (props.type.isEqual(CardDataSetType.COMPLETED) && props.onClickButton && props.id) {
                      props.onClickButton(props.id);
                    }
                  }}
                  block={true}
                  text="Стоп"
                />
              </>):null}
              {props.processStatus === ProcessStatus.ERROR ? (
                <CoreButton
                  style={{
                    backgroundColor: "red",
                  }}
                  onClick={() => {
                    if (props.type.isEqual(CardDataSetType.COMPLETED) && props.onClickButton && props.id) {
                      props.onClickButton(props.id);
                    }
                  }}
                  filled={true}
                  text="Ошибка"
                />
              ) : null}
            </div>
          </div>
        )}
      </div>
    </div>
  );
};
