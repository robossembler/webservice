import { Result } from "../../core/helper/result";
import makeAutoObservable from "mobx-store-inheritance";
import { FormBuilderValidationModel } from "../../core/model/form_builder_validation_model";

export enum ProcessStatus {
  END = "END",
  ERROR = "ERROR",
  NEW = "NEW",
  RUN = "RUN",
  NONE = "none",
}

export interface IDatasetModel {
  _id: string;
  dataSetObjects: string[];
  processStatus: ProcessStatus;
  projectId: string;
  name: string;
  formBuilder: FormBuilderValidationModel;
  unixTime: number;
  datasetType: string;
  local_path: string;
  __v: number;
  processLogs: string;
}

export interface Dataset {
  name: string;
  local_path: string;
  dataSetObjects: string[];
  unixDate: number;
  formBuilder: FormBuilderValidationModel;
}

export interface Asset {
  name: string;
  mesh: string;
  image: string;
}

 
export class DataSetModel {
  dataSetObjects: string[];
  datasetType: string;
  name: string;
  formBuilder: FormBuilderValidationModel = FormBuilderValidationModel.datasetEmpty();
  project?: string;
  processStatus?: string;
  isNew: boolean;
  _id?: string;
  constructor(
    dataSetObjects: string[],
    datasetType = datasetTypes[0],
    datasetName: string,
    isNew = true,
    id: string | undefined = undefined
  ) {
    this.dataSetObjects = dataSetObjects;
    this.datasetType = datasetType;
    this.name = datasetName;
    this.isNew = isNew;
    this._id = id;
    makeAutoObservable(this);
  }

  static empty() {
    return new DataSetModel([], "", "", true);
  }

  isValid(): Result<string, void> {
    if (this.project === undefined) {
      return Result.error("project is unknow");
    }
    if (this.dataSetObjects.isEmpty()) {
      return Result.error("Не выделены детали");
    }
    if (this.name.isEmpty()) {
      return Result.error("ВВедите имя датасета");
    }
    return Result.ok();
  }

  static fromIDatasetModel(model: IDatasetModel) {
    const dataSetModel = new DataSetModel(model.dataSetObjects, model.datasetType, model.name, false, model._id);
    dataSetModel.formBuilder = model.formBuilder;
    return dataSetModel;
  }
}

export const datasetTypes = ["Object Detection - YOLOv8", "Pose Estimation - DOPE"];

export const datasetFormMockResult = `
{
  "typedataset": \${typedataset:Enum<T>:ObjectDetection},
  "models_randomization":{
    "loc_range_low":  [\${LOC_RANGE_LOW_1:number:-1}, \${LOC_RANGE_LOW_2:number:-1},\${LOC_RANGE_LOW_3:number:0}],
    "loc_range_high": [\${LOC_RANGE_HIGH_1:number:1}, \${LOC_RANGE_HIGH_2:number:1},\${LOC_RANGE_HIGH_3:number:2}]
  },
  "scene":{
    "objects": \${OBJECTS_SCENE:Array<OBJECTS_SCENE>:[]},
    "lights": \${LIGHTS:Array<LIGHTS>:[]}
  },
  "camera_position":{
    "center_shell": [\${CENTER_SHELL_1:number:0}, \${CENTER_SHELL_2:number:0}, \${CENTER_SHELL_3:number:0}],
    "radius_range": [\${RADIUS_RANGE_1:number:1}, \${RADIUS_RANGE_2:number:1.4}],
    "elevation_range": [\${ELEVATION_RANGE_1:number:10}, \${ELEVATION_RANGE_2:number:90}]
  },
  "generation":{
    "n_cam_pose": \${N_CAM_POSE:number:5},
    "n_sample_on_pose": \${N_SAMPLE_ON_POSE:number:3},
    "n_series": \${N_SERIES:number:100},
    "image_format": \${image_format:Enum<F>:JPEG},
    "image_size_wh": [\${IMAGE_SIZE_WH_1:number:640}, \${IMAGE_SIZE_WH_2:number:480}]
  }
}
`;

export const datasetFormMockContext = `
ENUM T = "ObjectDetection","PoseEstimation";
ENUM L = "POINT","SUN";
ENUM F = "JPEG","PNG";
ENUM COLLISION_SHAPE = "SHAPE","COLLISION";
 
type OBJECTS_SCENE = {
"name": \${NAME:string:default},
"collision_shape": \${collision_shape:Enum<COLLISION_SHAPE>:BOX},
"loc_xyz": [\${LOC_XYZ_1:number:0}, \${LOC_XYZ_2:number:0}, \${LOC_XYZ_3:number:0}],
"rot_euler": [\${ROT_EULER_1:number:0},\${ROT_EULER_2:number:0}, \${ROT_EULER_3:number:0}],
"material_randomization": {
    "specular": [\${SPECULAR_1:number:0}, \${SPECULAR_2:number:1}],
    "roughness": [\${ROUGHNESS_1:number:0}, \${ROUGHNESS_2:number:1}],
    "metallic": [\${METALLIC_1:number:0}, \${METALLIC_2:number:1}],
    "base_color": [
        [
            \${BASE_COLOR_1:number:0},
            \${BASE_COLOR_2:number:0},
            \${BASE_COLOR_3:number:0},
            \${BASE_COLOR_4:number:1}
        ],
        [
            \${BASE_COLOR_5:number:1},
            \${BASE_COLOR_6:number:1},
            \${BASE_COLOR_7:number:1},
            \${BASE_COLOR_8:number:1}
        ]
    ]
}
};
type LIGHTS = {
"id": \${ID:number:1},
"type": \${type:Enum<L>:POINT},
"loc_xyz": [\${LOC_XYZ_1:number:5}, \${LOC_XYZ_2:number:5}, \${LOC_XYZ_3:number:5}],
"rot_euler": [\${ROT_EULER_1:number:-0.06}, \${ROT_EULER_2:number:0.61}, \${ROT_EULER_3:number:-0.19}],
"color_range_low": [\${COLOR_RANGE_LOW_1:number:0.5}, \${COLOR_RANGE_LOW_2:number:0.5}, \${COLOR_RANGE_LOW_3:number:0.5}],
"color_range_high":[\${COLOR_RANGE_HIGH_1:number:1}, \${COLOR_RANGE_HIGH_2:number:1}, $\{COLOR_RANGE_HIGH_3:number:1}],
"energy_range":[\${ENERGY_RANGE_1:number:400},\${ENERGY_RANGE_2:number:900}]
};
`;

export const defaultFormValue: any = {
  typedataset: "PoseEstimation",
  models_randomization: { loc_range_low: [-1, -1, 0], loc_range_high: [1, 1, 2] },
  scene: { objects: [], lights: [] },
  camera_position: { center_shell: [0, 0, 0], radius_range: [1, 1.4], elevation_range: [10, 90] },
  generation: { n_cam_pose: 5, n_sample_on_pose: 3, n_series: 100, image_format: "JPEG", image_size_wh: [640, 480] },
};
