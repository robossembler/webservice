import { CoreSwitch } from "../../core/ui/switch/switch";
import { CoreText, CoreTextType } from "../../core/ui/text/text";

export interface IListItemProps {
  name: string;
  imgURL: string;
  status: boolean;
  onChange: (status: boolean, id: string) => void;
}
export const ListItem = (props: IListItemProps) => {
  return (
    <div
      style={{
        backgroundColor: "rgba(254, 247, 255, 1)",
        border: "1px #6750a4 solid",
        width: "100%",
        height: 110,
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        borderRadius: 12,
        marginTop: 10,
        marginBottom: 10,
      }}
    >
      <img style={{ height: 50, margin: 10 }} src={props.imgURL} alt="" />
      <CoreText text={props.name} type={CoreTextType.large} />
      <CoreSwitch isSelected={props.status} id={props.name} onChange={props.onChange} />
    </div>
  );
};
