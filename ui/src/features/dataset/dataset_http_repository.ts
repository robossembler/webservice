import { Result } from "../../core/helper/result";
import { HttpError, HttpMethod, CoreHttpRepository } from "../../core/repository/core_http_repository";
import { Parts } from "../details/details_http_repository";
import { DataSetModel, Dataset, IDatasetModel } from "./dataset_model";

export class DataSetHttpRepository extends CoreHttpRepository {
  featureApi = `/datasets`;

  editDataset = (dataSetModel: DataSetModel) => this._jsonRequest<void>(HttpMethod.PUT, this.featureApi, dataSetModel);
  deleteDataset = (id: string) => this._jsonRequest<void>(HttpMethod.DELETE, `${this.featureApi}/dataset?id=${id}`);
  saveDataSet = async (model: DataSetModel) => this._jsonRequest<Dataset>(HttpMethod.POST, this.featureApi, model);
  getDatasetsActiveProject = async (): Promise<Result<HttpError, IDatasetModel[]>> =>
    this._jsonRequest(HttpMethod.GET, this.featureApi);
  execDatasetProcess = async (id: string) => this._jsonRequest(HttpMethod.POST, `${this.featureApi}/exec?id=${id}`);
  isRunningProcess = async () => this._jsonRequest(HttpMethod.GET, `${this.featureApi}/is/running`);
  deleteProcess = async () => this._jsonRequest(HttpMethod.GET, `${this.featureApi}/delete/process`);
  getLocalParts = () => this._jsonRequest<Parts>(HttpMethod.GET,`/projects/local/assets`)
}
