import xmlFormat from "xml-formatter";
import { NodeEditor } from "rete";
import { Position } from "rete-react-plugin";
import { AreaPlugin } from "rete-area-plugin";
import { TypedEvent } from "../../../core/helper/typed_event";
import { AreaExtra, Schemes } from "../presentation/ui/editor/editor";
import { Result } from "../../../core/helper/result";
import { Skills } from "../../../core/model/skill_model";

export interface BtDrawDragAndDropView {
  x: number;
  y: number;
  name: string;
  id: string;
}

export class BtNodeView extends TypedEvent<BtDrawDragAndDropView> {}
export enum UpdateEvent {
  DELETE = "DELETE",
  UPDATE = "UPDATE",
}
export interface IUpdateEvent {
  type: UpdateEvent;
  id: string;
}
export class NodeRerenderObserver extends TypedEvent<IUpdateEvent> {}
export class ReteForceUpdateObserver extends TypedEvent<any> {}
export class BehaviorTreeBuilderModel {
  public static nodeView: Map<string, Position> = new Map();
  public static btRootTag: string = '<root BTCPP_format="4"><BehaviorTree ID="Main">${bt}</BehaviorTree></root>';

  public static result = "";
  static fromReteScene(
    editor: NodeEditor<Schemes>,
    area: AreaPlugin<Schemes, AreaExtra>,
    skills?: Skills
  ): Result<string, string> {
    try {
      this.result = "";
      area.nodeViews.toArrayEntries().forEach((el) => this.nodeView.set(el.key, el.value.position));
      this.getFirstSequence(editor).map((sortedSequence) => {
        const firstNodeId = sortedSequence.getKeyFromValueIsExists(1) as string;
        this.findSequence(firstNodeId, editor, sortedSequence, 2);
        this.result += `<${this.getNodeLabelAtId(editor, firstNodeId, skills)}>`;
        this.toXML(sortedSequence as Map<string, number>, editor, area, firstNodeId, skills);
        this.result += `</${this.getNodeLabelAtId(editor, firstNodeId, skills)}>`;
      });

      return this.xmlFormat(this.btRootTag, this.result);
    } catch (error) {
      console.log(error);
      return Result.error("BtBuilderModel fromReteScene error");
    }
  }
  public static xmlFormat = (result: string, btRootTag: string): Result<string, string> => {
    try {
      return Result.ok(
        xmlFormat(
          '<root BTCPP_format="4"><BehaviorTree ID="Main">${bt}</BehaviorTree></root>'.replace(
            "${bt}",
            btRootTag.replace("${bt}", result)
          )
        )
      );
    } catch (error) {
      console.log(result);
      console.log(btRootTag);
      console.log(error);
      return Result.error(error as string);
    }
  };
  public static getNodeLabelAtId(editor: NodeEditor<Schemes>, id: string, skills?: Skills) {
    try {
      console.log(editor.getNode(id)!.label);
      if (skills?.getSkillsNames().find((el) => el.name.isEqual(editor.getNode(id)!.label))) {
        return `${skills.skillIsCondition(editor.getNode(id)!.label).fold(
          () => "Condition",
          () => "Action"
        )} ID=${skills.skillIsCondition(editor.getNode(id)!.label).fold(
          () => "Is",
          () => "RbsAction"
        )}  do="${skills.getSkillDo(editor.getNode(id)!.label)}" command="${editor.getNode(id)?.label}" sid=${id}"`;
      }
      return editor.getNode(id)?.label;
    } catch (error) {
      console.log(error);
    }
  }

  static toXML(
    sortedSequence: Map<string, number>,
    editor: NodeEditor<Schemes>,
    area: AreaPlugin<Schemes, AreaExtra>,
    activeElementId = "",
    skills?: Skills
  ) {
    const sources: string[] = [];
    editor.getConnections().forEach((el) => {
      if (el.source === activeElementId) {
        sources.push(el.target);
      }
    });
    sources
      .sort((a, b) => (this.nodeView.get(a)!.y > this.nodeView.get(b)!.y ? 1 : -1))
      .forEach((el) => {
        this.result += `<${this.getNodeLabelAtId(editor, el, skills)}>`;
        this.toXML(sortedSequence, editor, area, el, skills);
        this.result += `</${this.getNodeLabelAtId(editor, el)}>`;
      });
  }

  public static findSequence(
    nodeId: string,
    editor: NodeEditor<Schemes>,
    result: Map<string, boolean | number>,
    lvl: number
  ): Map<string, number> | undefined {
    const allConnections: { id: string; target: string }[] = [];

    editor.getConnections().forEach((el) => {
      if (el.source === nodeId) {
        allConnections.push({ id: el.id, target: el.target });
      }
    });

    const connectionsSort = allConnections.sort((a, b) => {
      if (this.nodeView.get(a.target)!.y > this.nodeView.get(b.target)!.y) {
        return 1;
      }
      return -1;
    });

    if (connectionsSort.isEmpty()) return result as Map<string, number>;
    let cashedLvl = lvl;

    connectionsSort.forEach((el) => {
      result.set(el.target, lvl);

      this.findSequence(el.target, editor, result, (cashedLvl += 1));
    });
  }
  public static getFirstSequence(editor: NodeEditor<Schemes>): Result<undefined, Map<string, boolean | number>> {
    const node = new Map<string, boolean | number>();
    editor.getNodes().forEach((el) => {
      node.set(el.id, 1);
    });

    editor.getConnections().forEach((el) => {
      node.set(el.target, false);
    });
    const key = node.getKeyFromValueIsExists(1);
    if (key === undefined) {
      return Result.error(undefined);
    }
    return Result.ok(node);
  }
}
