import { extensions } from "../../../core/extensions/extensions";
import { ISkillView } from "../presentation/ui/skill_tree/skill_tree";

extensions();

export enum SystemPrimitive {
  Sequence = "Sequence",
  Fallback = "Fallback",
  ReactiveSequence = "ReactiveSequence",
  SequenceWithMemory = "SequenceWithMemory",
  ReactiveFallback = "ReactiveFallback",
  Decorator = "Decorator",
  Inverter = "Inverter",
  ForceSuccess = "ForceSuccess",
  ForceFailure = "ForceFailure",
  Repeat = "Repeat",
  RetryUntilSuccessful = "RetryUntilSuccessful",
  KeepRunningUntilFailure = "KeepRunningUntilFailure",
  Delay = "Delay",
  RunOnce = "RunOnce",
}
interface ISystemPrimitive {
  label: string;
  group: string;
  css?: React.CSSProperties;
  input: boolean;
  output: boolean;
}

export class PrimitiveViewModel {
  primitives: ISystemPrimitive[] = [
    {
      label: SystemPrimitive.Inverter,
      group: SystemPrimitive.Decorator,
      input: true,
      output: true,
      css: {
        border: "1px solid 6A4C93",
        borderRadius: 5,
        background: "rgba(21, 15, 29, 1)",
      },
    },
    {
      label: SystemPrimitive.ForceSuccess,
      group: SystemPrimitive.Decorator,
      input: true,
      output: true,
      css: {
        border: "1px solid 6A4C93",
        borderRadius: 5,
        background: "rgba(21, 15, 29, 1)",
      },
    },
    {
      label: SystemPrimitive.ForceFailure,
      group: SystemPrimitive.Decorator,
      input: true,
      output: true,
      css: {
        border: "1px solid 6A4C93",
        borderRadius: 5,
        background: "rgba(21, 15, 29, 1)",
      },
    },
    {
      label: SystemPrimitive.Repeat,
      group: SystemPrimitive.Decorator,
      input: true,
      output: true,
      css: {
        border: "1px solid 6A4C93",
        borderRadius: 5,
        background: "rgba(21, 15, 29, 1)",
      },
    },
    {
      label: SystemPrimitive.RetryUntilSuccessful,
      group: SystemPrimitive.Decorator,
      input: true,
      output: true,
      css: {
        border: "1px solid 6A4C93",
        borderRadius: 5,
        background: "rgba(21, 15, 29, 1)",
      },
    },
    {
      label: SystemPrimitive.KeepRunningUntilFailure,
      group: SystemPrimitive.Decorator,
      input: true,
      output: true,
      css: {
        border: "1px solid 6A4C93",
        borderRadius: 5,
        background: "rgba(21, 15, 29, 1)",
      },
    },
    {
      label: SystemPrimitive.Delay,
      group: SystemPrimitive.Decorator,
      input: true,
      output: true,
      css: {
        border: "1px solid 6A4C93",
        borderRadius: 5,
        background: "rgba(21, 15, 29, 1)",
      },
    },
    {
      label: SystemPrimitive.RunOnce,
      group: SystemPrimitive.Decorator,
      input: true,
      output: true,
      css: {
        border: "1px solid 6A4C93",
        borderRadius: 5,
        background: "rgba(21, 15, 29, 1)",
      },
    },
    {
      label: SystemPrimitive.ReactiveSequence,
      group: SystemPrimitive.Sequence,
      input: true,
      output: true,
      css: {
        background: "rgba(28, 40, 8, 1)",
        border: "1px solid rgba(138, 201, 38, 1)",
        borderRadius: 5,
      },
    },
    {
      label: SystemPrimitive.Sequence,
      group: SystemPrimitive.Sequence,
      input: true,
      output: true,
      css: {
        background: "rgba(28, 40, 8, 1)",
        border: "1px solid rgba(138, 201, 38, 1)",
        borderRadius: 5,
      },
    },
    {
      label: SystemPrimitive.SequenceWithMemory,
      group: SystemPrimitive.Sequence,
      input: true,
      output: true,
      css: {
        background: "rgba(28, 40, 8, 1)",
        border: "1px solid rgba(138, 201, 38, 1)",
        borderRadius: 5,
      },
    },
    {
      label: SystemPrimitive.ReactiveFallback,
      group: SystemPrimitive.Fallback,
      input: true,
      output: true,
      css: {
        background: "rgba(40, 8, 8, 1)",
        border: "1px solid rgba(201, 38, 38, 1)",
        borderRadius: 5,
      },
    },
    {
      label: SystemPrimitive.Fallback,
      group: SystemPrimitive.Fallback,
      input: true,
      output: true,
      css: {
        background: "rgba(40, 8, 8, 1)",
        border: "1px solid rgba(201, 38, 38, 1)",
        borderRadius: 5,
      },
    },
  ];
  getPrimitiveAtLabel = (name: string) => {
    return this.primitives.find((el) => el.label.isEqual(name));
  };
  toSkillView = () =>
    this.primitives.reduce<ISkillView[]>((acc, primitive) => {
      acc
        .rFind<ISkillView>((el) => el.name.isEqual(primitive.group))
        .fold(
          () => {
            acc
              .at(acc.findIndex((element) => element.name === primitive.group))
              ?.children?.push({ name: primitive.label });
          },
          () => {
            acc.push({ name: primitive.group, children: [{ name: primitive.label }] });
          }
        );
      return acc;
    }, []);
}
