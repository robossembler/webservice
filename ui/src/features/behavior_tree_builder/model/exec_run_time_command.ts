import { IsArray, IsEnum, IsNumber, IsString } from "class-validator";
import { ValidationModel } from "../../../core/model/validation_model";

export enum Status {
  OK = "Ok",
  ERROR = "Error",
}
export class ExecRunTimeCommand extends ValidationModel {
  @IsString()
  execCommand: string;
  @IsString()
  date: string = Date();
  @IsEnum(Status)
  status: Status;
  @IsNumber()
  delay: number;
  @IsString()
  checkCommand: string;
  @IsArray()
  filter: string[] = [];
}
