import { Type } from "class-transformer";
import { Result } from "../../../core/helper/result";
import { Skills } from "../../../core/model/skill_model";
import { NodeBehaviorTree } from "./node_behavior_tree";
import { IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";
import { BehaviorTreeBuilderHttpRepository } from "../data/behavior_tree_builder_http_repository";
import { message } from "antd";
import { ValidationModel } from "../../../core/model/validation_model";

export class BehaviorTreeModel extends ValidationModel {
  __v?: string;
  @IsNumber()
  unixTime: number = Date.now();
  @IsString()
  public sceneId: string;
  @IsOptional()
  @Type(() => Skills)
  public skills?: Skills;
  public scene: NodeBehaviorTree[] = [];
  public xml: string;
  @IsString()
  @IsNotEmpty()
  public description: string;
  @IsString()
  @IsNotEmpty()
  public name: string;
  public project: string;
  public _id: string;
  constructor(skills: Skills, scene: NodeBehaviorTree[], xml: string, name: string, project: string, _id: string) {
    super();
    this.skills = skills;
    this.scene = scene;
    this.xml = xml;
    this.name = name;
    this.project = project;
    this._id = _id;
  }
  updateDependency(skills: Skills, scene: NodeBehaviorTree[], xml: string) {
    this.skills = skills;
    this.scene = scene;
    this.xml = xml;
  }
  getSceneDependency = async (behaviorTreeBuilderHttpRepository: BehaviorTreeBuilderHttpRepository) =>
    (await behaviorTreeBuilderHttpRepository.getSceneAsset(this.sceneId)).fold(
      (s) => {},
      (e) => {
        message.error("Get Scene Dependency error");
      }
    );

  static empty() {
    return new BehaviorTreeModel(Skills.empty(), [], "", "", "", "");
  }
  static isEmpty(model: BehaviorTreeModel) {
    return Skills.isEmpty(model.skills ?? Skills.empty()).map(() => {
      if (model.scene.isEmpty()) {
        return Result.error(undefined);
      }
      if (model.xml.isEmpty()) {
        return Result.error(undefined);
      }
      if (model.project.isEmpty()) {
        return Result.error(undefined);
      }
      if (model.name.isEmpty()) {
        return Result.error(undefined);
      }
      if (model._id.isEmpty()) {
        return Result.error(undefined);
      }
      return Result.ok();
    });
  }
}
