import { IsNotEmpty, IsNumber, IsString } from "class-validator";
import { Result } from "../../../core/helper/result";
import { ValidationModel } from "../../../core/model/validation_model";

export class BehaviorTreeViewModel extends ValidationModel {
  @IsNumber()
  unixTime: number = Date.now();
  @IsNotEmpty()
  @IsString()
  public name: string;
  @IsNotEmpty()
  @IsString()
  public description: string;
  @IsNotEmpty()
  @IsString()
  public project: string;
  @IsNotEmpty()
  @IsString()
  public sceneId: string;
  constructor(name: string, description: string, project: string, sceneId: string) {
    super();
    this.name = name;
    this.description = description;
    this.project = project;
    this.sceneId = sceneId;
  }
  static empty() {
    return new BehaviorTreeViewModel("", "", "", "");
  }
}
