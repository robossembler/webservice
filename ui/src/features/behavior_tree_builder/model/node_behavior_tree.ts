import { AreaExtra, Schemes } from "../presentation/ui/editor/editor";
import { NodeEditor } from "rete";
import { AreaPlugin } from "rete-area-plugin";

export interface Position2D {
  x: number;
  y: number;
}

export class NodeBehaviorTree {
  label: string;
  id: string;
  outputs: string[];
  inputs: string[];
  connectTo?: string;
  position: Position2D;

  constructor(label: string, id: string, outputs: string[], inputs: string[], position: Position2D) {
    this.label = label;
    this.id = id;
    this.outputs = outputs;
    this.inputs = inputs;
    this.connectTo = undefined;
    this.position = position;
  }

  static fromReteScene(editor: NodeEditor<Schemes>, area: AreaPlugin<Schemes, AreaExtra>): NodeBehaviorTree[] {
    const nodes = new Map<string, NodeBehaviorTree>();
    editor.getNodes().forEach((el) => {
      if (area.nodeViews.get(el.id)?.position)
        nodes.set(
          el.id,
          new NodeBehaviorTree(
            el.label,
            el.id,
            Object.keys(el.outputs),
            Object.keys(el.inputs),
            area.nodeViews.get(el.id)!.position
          )
        );
    });
    editor.getConnections().forEach((el) => nodes.overrideValue(el.target, { connectTo: el.source }));

    return nodes.toArray();
  }
}
