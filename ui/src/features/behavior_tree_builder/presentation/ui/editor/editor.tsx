import { createRoot } from "react-dom/client";
import { NodeEditor, GetSchemes, ClassicPreset } from "rete";
import { AreaPlugin, AreaExtensions } from "rete-area-plugin";
import { ConnectionPlugin, Presets as ConnectionPresets } from "rete-connection-plugin";
import { ReactPlugin, Presets, ReactArea2D } from "rete-react-plugin";
import { CustomConnection } from "./custom_connection";
import { behaviorTreeBuilderStore } from "../../behavior_tree_builder_screen";
import { SequenceNode } from "./nodes/controls_node";
import { CustomSocket } from "./custom_socket";
import { BaseSchemes } from "rete";
import {
  NodeRerenderObserver as NodeUpdateObserver,
  ReteForceUpdateObserver,
  UpdateEvent,
} from "../../../model/editor_view";
import { v4 } from "uuid";

export type Schemes = GetSchemes<ClassicPreset.Node, ClassicPreset.Connection<ClassicPreset.Node, ClassicPreset.Node>>;
export type AreaExtra = ReactArea2D<Schemes>;

export function addCustomBackground<S extends BaseSchemes, K>(area: AreaPlugin<S, K>) {
  const background = document.createElement("div");

  background.classList.add("background");
  background.classList.add("fill-area");
  area.area.content.add(background);
}

export async function createEditor(container: HTMLElement) {
  const socket = new ClassicPreset.Socket("socket");
  behaviorTreeBuilderStore.btNodeObserver.on(async (event) => {
    setTimeout(async () => {
      const node = new ClassicPreset.Node(event.name);
      const { x, y } = areaContainer.area.pointer;

      node.id = event.id;
      const { output, input } = behaviorTreeBuilderStore.getInputs(event.name);
      if (output) node.addOutput(output, new ClassicPreset.Output(socket));
      if (input) node.addInput("b", new ClassicPreset.Input(socket));
      await editor.addNode(node);
      await areaContainer.translate(node.id, { x, y });
    }, 100);
  });
  behaviorTreeBuilderStore.reteForceUpdateObserver = new ReteForceUpdateObserver();
  behaviorTreeBuilderStore.reteForceUpdateObserver!.on(async () => {
    for await (const el of behaviorTreeBuilderStore.nodeBehaviorTree) {
      const node = new ClassicPreset.Node(el.label);
      node.id = el.id;

      el.outputs.forEach((outputName) => {
        node.addOutput(outputName, new ClassicPreset.Output(socket));
      });
      el.inputs.forEach((inputName) => {
        node.addInput(inputName, new ClassicPreset.Input(socket));
      });
      await editor.addNode(node);
      await areaContainer.translate(node.id, el.position);
    }
    for await (const el of behaviorTreeBuilderStore.nodeBehaviorTree) {
      if (el.connectTo)
        editor.addConnection({
          id: v4(),
          sourceOutput: "a",
          targetInput: "b",
          source: el.connectTo as string,
          target: el.id,
        });
    }
  });
  const editor = new NodeEditor<Schemes>();
  const areaContainer = new AreaPlugin<Schemes, AreaExtra>(container);

  const connection = new ConnectionPlugin<Schemes, AreaExtra>();
  const render = new ReactPlugin<Schemes, AreaExtra>({ createRoot });
  render.addPipe((el) => {
    behaviorTreeBuilderStore.syncScene(editor, areaContainer);
    return el;
  });

  render.addPreset(
    Presets.classic.setup({
      customize: {
        node(_) {
          return SequenceNode;
        },
        socket(_context) {
          return CustomSocket;
        },
        connection(_context) {
          return CustomConnection;
        },
      },
    })
  );
  connection.addPreset(ConnectionPresets.classic.setup());

  addCustomBackground(areaContainer);

  editor.use(areaContainer);
  areaContainer.use(connection);
  areaContainer.use(render);

  AreaExtensions.simpleNodesOrder(areaContainer);
  const nodeUpdateObserver = new NodeUpdateObserver();
  behaviorTreeBuilderStore.nodeUpdateObserver = nodeUpdateObserver;
  for await (const el of behaviorTreeBuilderStore.nodeBehaviorTree) {
    const node = new ClassicPreset.Node(el.label);
    node.id = el.id;
    el.outputs.forEach((outputName) => {
      node.addOutput(outputName, new ClassicPreset.Output(socket));
    });
    el.inputs.forEach((inputName) => {
      node.addInput(inputName, new ClassicPreset.Input(socket));
    });
    await editor.addNode(node);
    await areaContainer.translate(node.id, el.position);
  }
  for await (const el of behaviorTreeBuilderStore.nodeBehaviorTree) {
    if (el.connectTo)
      editor.addConnection({
        id: v4(),
        sourceOutput: "a",
        targetInput: "b",
        source: el.connectTo as string,
        target: el.id,
      });
  }

  nodeUpdateObserver.on(async (event) => {
    if (event.type.isEqual(UpdateEvent.UPDATE)) {
      areaContainer.update("node", event.id);
    }
    if (event.type.isEqual(UpdateEvent.DELETE)) {
      editor
        .getConnections()
        .forEach((el) =>
          el.source.isEqual(event.id) || el.target.isEqual(event.id) ? editor.removeConnection(el.id) : null
        );
      areaContainer.removeNodeView(event.id);
     
      editor.removeNode(event.id)
  
    }
  
  });

  AreaExtensions.selectableNodes(areaContainer, AreaExtensions.selector(), {
    accumulating: AreaExtensions.accumulateOnCtrl(),
  });
  setTimeout(() => {
    AreaExtensions.zoomAt(areaContainer, editor.getNodes());
  }, 100);

  return {
    destroy: () => areaContainer.destroy(),
  };
}
