import * as React from "react";
import { ClassicScheme, RenderEmit, Presets } from "rete-react-plugin";
import styled, { css } from "styled-components";
import { $nodewidth, $socketmargin, $socketsize } from "./vars";
import { behaviorTreeBuilderStore } from "../../../behavior_tree_builder_screen";
import { themeStore } from "../../../../../..";
import { Icon } from "../../../../../../core/ui/icons/icons";
import { SystemPrimitive } from "../../../../model/primitive_view_model";
const { RefSocket, RefControl } = Presets.classic;

type NodeExtraData = { width?: number; height?: number };

export const NodeStyles = styled.div<NodeExtraData & { selected: boolean; styles?: (props: any) => any }>`
  background: #292a2d;
  border: 3px solid #715a7c;
  border-radius: 3px;
  cursor: pointer;
  box-sizing: border-box;
  width: ${(props) => (Number.isFinite(props.width) ? `${props.width}px` : `${$nodewidth}px`)};
  height: ${(props) => (Number.isFinite(props.height) ? `${props.height}px` : "auto")};
  color: white;
  user-select: none;
  &:hover {
    background: #333;
  }

  .title {
    color: black;
    font-family: sans-serif;
    font-size: 18px;
    position: relative;
    height: ${(props) => (Number.isFinite(props.height) ? `${props.height}px` : "auto")};
    text-align-last: center;
    align-content: center;
  }
  .output {
    text-align: right;
    align-content: center;
  }
  .input {
    text-align: left;
    align-content: center;
    width: min-content;
  }
  .output-socket {
    text-align: right;
    margin-right: -1px;
    display: inline-block;
    position: relative;
    left: -17px;
    top: -1px;
  }
  .input-socket {
    z-index: -10;
    text-align: left;
    margin-left: -1px;
    display: inline-block;
    align-content: center;
    position: relative;
    left: 17px;
    top: -1px;
  }

  .input-title,
  .output-title {
    vertical-align: middle;
    color: white;
    display: inline-block;
    font-family: sans-serif;
    font-size: 14px;
    margin: ${$socketmargin}px;
    line-height: ${$socketsize}px;
    align-content: center;
  }

  ${(props) => props.styles && props.styles(props)}
`;

function sortByIndex<T extends [string, undefined | { index?: number }][]>(entries: T) {
  entries.sort((a, b) => {
    const ai = a[1]?.index || 0;
    const bi = b[1]?.index || 0;

    return ai - bi;
  });
}

type Props<S extends ClassicScheme> = {
  data: S["Node"] & NodeExtraData;
  styles?: () => any;
  emit: RenderEmit<S>;
};
export type NodeComponent<Scheme extends ClassicScheme> = (props: Props<Scheme>) => JSX.Element;

export function SequenceNode<Scheme extends ClassicScheme>(props: Props<Scheme>) {
  const inputs = Object.entries(props.data.inputs);
  const outputs = Object.entries(props.data.outputs);
  const controls = Object.entries(props.data.controls);
  const selected = props.data.selected || false;
  const { id, label, width } = props.data;
  sortByIndex(inputs);
  sortByIndex(outputs);
  sortByIndex(controls);
  const refSelect = React.useRef<HTMLDivElement>(null);
  const refDelete = React.useRef<HTMLDivElement>(null);
  React.useEffect(() => {
    refSelect.current?.addEventListener("mouseup", () => behaviorTreeBuilderStore.setSelected(label, selected, id));
    refDelete.current?.addEventListener("mouseup", () => behaviorTreeBuilderStore.deleteBtAction(id));
  }, []);
  return (
    <div style={{ width: "100%" }}>
      <NodeStyles
        selected={selected}
        width={width}
        style={Object.assign({ width: "100%" }, behaviorTreeBuilderStore.getStylesByLabelNode(label))}
        data-testid="node"
        className="node"
        id="node"
      >
        <div style={{ display: "inline-flex", width: "100%", justifyContent: "space-between" }}>
          {controls.map(([key, control]) => {
            return control ? <RefControl key={key} name="control" emit={props.emit} payload={control} /> : null;
          })}

          {inputs.map(
            ([key, input]) =>
              input && (
                <div
                  style={{ position: "relative", left: "-22px", alignContent: "center" }}
                  className="input"
                  key={key}
                  data-testid={`input-${key}`}
                >
                  <RefSocket
                    name="input-socket"
                    emit={props.emit}
                    side="input"
                    socketKey={key}
                    nodeId={id}
                    payload={input.socket}
                  />

                  {input?.control && input?.showControl && (
                    <span className="input-control">
                      <RefControl key={key} name="input-control" emit={props.emit} payload={input.control} />
                    </span>
                  )}
                </div>
              )
          )}

          <div style={{ marginTop: 20, marginBottom: 20 }} onPointerDown={(e) => {}} data-testid="title">
            <div style={{ display: "flex", width: "100%", justifyContent: "space-between" }}>
              <div style={{ paddingLeft: 5 }}>{label}</div>
              <div style={{ display: "flex" }}>
                {Object.keys(SystemPrimitive)
                  .rFind<string>((el) => el.isEqual(label))
                  .fold(
                    () => (
                      <>
                        <div ref={refDelete}>
                          <Icon
                            style={{ height: "100%", marginRight: 10 }}
                            iconStyle={{ height: "100%" }}
                            type="Bucket"
                            color={themeStore.theme.onSurfaceVariant}
                            onClick={() => behaviorTreeBuilderStore.deleteBtAction(id)}
                          />
                        </div>
                      </>
                    ),
                    () => (
                      <>
                        <div
                          style={{
                            display: behaviorTreeBuilderStore.skillTemplates!.skillIsDependencyFilled(label).fold(
                              (result) => {
                                if (result) {
                                  return "none";
                                }
                              },
                              () => undefined
                            ),
                          }}
                          ref={refSelect}
                        >
                          <Icon type="SettingGear" />
                        </div>
                        <div style={{ width: 2 }} />

                        <div ref={refDelete}>
                          <Icon
                            type="Bucket"
                            color={themeStore.theme.onSurfaceVariant}
                            onClick={() => behaviorTreeBuilderStore.deleteBtAction(id)}
                          />
                        </div>
                      </>
                    )
                  )}
              </div>
            </div>
            <div>{behaviorTreeBuilderStore.getBodyNode(label, id)}</div>
          </div>
          {outputs.map(
            ([key, output]) =>
              output && (
                <div
                  className="output"
                  key={key}
                  data-testid={`output-${key}`}
                  style={{ position: "relative", left: "22px" }}
                >
                  <RefSocket
                    name="output-socket"
                    side="output"
                    emit={props.emit}
                    socketKey={key}
                    nodeId={id}
                    payload={output.socket}
                  />
                </div>
              )
          )}
          {outputs.isEmpty() ? <div style={{ width: 10 }} /> : null}
        </div>
      </NodeStyles>
    </div>
  );
}
