import * as React from "react";
import { ClassicPreset } from "rete";
import styled from "styled-components";

const Styles = styled.div`
  display: inline-block;
  cursor: pointer;
  border: 1px solid rgb(255 255 255);
  width: 10px;
  height: 10px;
  vertical-align: middle;
  background: white;
  z-index: 2;
  box-sizing: border-box;
  &:hover {
    background: #ddd;
  }
  border-radius: 50%;
`;

export function CustomSocket<T extends ClassicPreset.Socket>(props: { data: T }) {
  return <Styles title={props.data.name} />;
}
