import { TypedEvent } from "../../../../../core/helper/typed_event";
export interface BtDrawView {
  x: number;
  y: number;
  name: string;
}
export class BtNodeView extends TypedEvent<BtDrawView> {}
