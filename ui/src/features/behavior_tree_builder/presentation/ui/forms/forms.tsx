import { FormType } from "../../../../../core/model/form";
import { FormBuilderValidationModel } from "../../../../../core/model/form_builder_validation_model";
import { DependencyViewModel } from "../../../../../core/model/skill_model";
 import { BehaviorTreeBuilderStore } from "../../behavior_tree_builder_store";
import { FormBuilderForm } from "./form_builder/form_builder_form";
import { TopicDependencyViewModel } from "./topics_form/topic_dependency_view_model";
import { TopicsForm } from "./topics_form/topics_form";

export interface IPropsForm<T> {
  dependency: T;
  store?: BehaviorTreeBuilderStore;
  onChange: (dependency: DependencyViewModel) => void;
}

export interface IForms {
  name: string;
  component: JSX.Element;
}
 
export interface BtDependencyFormBuilder {
  form: FormType;
  component?: React.ReactNode;
}

export const btDependencyFormBuilder = (onChange: (dependency: DependencyViewModel) => void) => [
  { form: FormType.weights, component: null },
  {
    form: FormType.topic,
    component: <TopicsForm store={undefined} dependency={TopicDependencyViewModel.empty()} onChange={onChange} />,
  },
  {
    form: FormType.formBuilder,
    component: (
      <FormBuilderForm store={undefined} dependency={FormBuilderValidationModel.empty()} onChange={onChange} />
    ),
  },
];
export const forms = (
  props: any,
  onChange: (dependency: DependencyViewModel) => void,
  store: BehaviorTreeBuilderStore
): IForms[] => [
  { name: FormType.topic, component: <TopicsForm store={store} dependency={props} onChange={onChange} /> },
  { name: FormType.formBuilder, component: <FormBuilderForm store={store} dependency={props} onChange={onChange} /> },
];
