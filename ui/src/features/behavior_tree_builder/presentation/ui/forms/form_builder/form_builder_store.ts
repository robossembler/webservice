import makeAutoObservable from "mobx-store-inheritance";
import { NavigateFunction } from "react-router-dom";
import { FormState, CoreError } from "../../../../../../core/store/base_store";
import { BehaviorTreeBuilderStore } from "../../../behavior_tree_builder_store";
import { FormBuilderValidationModel } from "../../../../../../core/model/form_builder_validation_model";
 
export class FormsFormBuilderStore extends FormState<FormBuilderValidationModel, CoreError> {
  isBtScreen = false;
  isModalOpen = false;
  viewModel: FormBuilderValidationModel = FormBuilderValidationModel.empty();
  constructor() {
    super();
    makeAutoObservable(this);
  }
  errorHandingStrategy = (error: CoreError) => {};
  init = async (navigate?: NavigateFunction | undefined) => {};
  initParam = (isSkillScreen: boolean, store: BehaviorTreeBuilderStore | undefined) => {
    this.isBtScreen = isSkillScreen;
  };
  openModal() {
    this.isModalOpen = true;
  } 
}
