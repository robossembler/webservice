import { useEffect } from "react";
import { observer } from "mobx-react-lite";
import { Modal, message } from "antd";
import { FormsFormBuilderStore } from "./form_builder_store";
import { IPropsForm } from "../forms";
import { useStore } from "../../../../../../core/helper/use_store";
import { isBtScreen } from "../../../behavior_tree_builder_screen";
import { CoreInput } from "../../../../../../core/ui/input/input";
import { CoreButton } from "../../../../../../core/ui/button/button";
import { FormBuilder } from "../../../../../../core/ui/form_builder/form_builder";
import { FormBuilderValidationModel } from "../../../../../../core/model/form_builder_validation_model";
import { ButtonV2 } from "../../../../../../core/ui/button/button_v2";

export const FormBuilderForm = observer((props: IPropsForm<Partial<FormBuilderValidationModel>>) => {
  const store = useStore(FormsFormBuilderStore);
  useEffect(() => {
    store.initParam(isBtScreen(), props.store);
    store.loadClassInstance(FormBuilderValidationModel, props.dependency as FormBuilderValidationModel);
  }, []);

  return (
    <div style={{ overflowX: "scroll", height: "100%" }}>
      <div>FormBuilder</div>
      {store.isBtScreen ? (
        <div>
          <FormBuilder
            formBuilder={store.viewModel}
            onChange={(form) => {
              store.viewModel = form;
              console.log(form);
            }}
          />
          <div style={{ height: 100 }} />
          <ButtonV2
            style={{ width: 80 }}
            text="save"
            onClick={async () =>
              (await store.viewModel.valid<FormBuilderValidationModel>()).fold(
                (model) => props.onChange(model),
                (e) => message.error(e)
              )
            }
          />
          <div style={{ height: 100 }} />
        </div>
      ) : (
        <>
          <CoreInput
            style={{ height: "100%" }}
            value={store.viewModel.result}
            label="Result"
            styleContentEditable={{ height: "100%" }}
            onChange={(text) => store.updateForm({ result: text })}
          />
          <CoreInput
            style={{ height: "100%" }}
            value={store.viewModel.context}
            label="Context"
            styleContentEditable={{ height: "100%" }}
            onChange={(text) => store.updateForm({ context: text })}
          />
          <div style={{ width: 100 }}>
            <CoreButton text="preview" onClick={() => store.openModal()} />
            <CoreButton
              text="save"
              onClick={async () =>
                (await store.viewModel.valid<FormBuilderValidationModel>()).fold(
                  (model) => props.onChange(model),
                  (error) => message.error(error)
                )
              }
            />
          </div>
          <Modal
            destroyOnClose={true}
            open={store.isModalOpen}
            footer={null}
            closable={false}
            closeIcon={null}
            onCancel={() => {
              store.isModalOpen = false;
            }}
          >
            <FormBuilder formBuilder={store.viewModel} onChange={() => {}} />
          </Modal>
        </>
      )}
    </div>
  );
});
