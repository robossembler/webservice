import { Vector3 } from "three";
import { Result } from "../../../../../../core/helper/result";
export interface IMoveToPoseRobotModel {
  robot_name: string;
  pose: Pose;
}

export interface Pose {
  name: string;
  position: Vector3;
  orientation: Orientation;
}

export interface Orientation {
  x: number;
  y: number;
  z: number;
  w: number;
}

export class MoveToPoseRobotModel implements IMoveToPoseRobotModel {
  robot_name: string;
  pose: Pose;
  constructor() {}

  valid(): Result<string, MoveToPoseRobotModel> {
    return Result.ok(this);
  }
  static empty() {
    const moveToPoseRobotModel = new MoveToPoseRobotModel();
    moveToPoseRobotModel.robot_name = "";
    moveToPoseRobotModel.pose = {
      name: "",
      position: new Vector3(0, 0, 0),
      orientation: {
        x: 0,
        y: 0,
        z: 0,
        w: 0,
      },
    };
    return moveToPoseRobotModel;
  }
}
