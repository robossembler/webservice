import makeAutoObservable from "mobx-store-inheritance";
import { NavigateFunction } from "react-router-dom";
import { MoveToPoseRobotModel } from "./move_to_pose_robot_model";
import { FormState, CoreError } from "../../../../../../core/store/base_store";
import { BehaviorTreeBuilderStore } from "../../../behavior_tree_builder_store";

export class MoveToPoseStore extends FormState<MoveToPoseRobotModel, CoreError> {
  behaviorTreeBuilderStore: BehaviorTreeBuilderStore;

  constructor(behaviorTreeBuilderStore: BehaviorTreeBuilderStore) {
    super();
    this.behaviorTreeBuilderStore = behaviorTreeBuilderStore;
    makeAutoObservable(this);
  }
  viewModel: MoveToPoseRobotModel = MoveToPoseRobotModel.empty();
  errorHandingStrategy = (error: CoreError) => {};
  init = async (navigate?: NavigateFunction | undefined) => {};
}
