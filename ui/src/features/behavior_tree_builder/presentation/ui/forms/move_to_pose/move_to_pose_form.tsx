import React from "react";
import { observer } from "mobx-react-lite";
import { MoveToPoseStore } from "./move_to_pose_store";
import { IPropsForm } from "../forms";
import { MoveToPoseRobotModel } from "./move_to_pose_robot_model";
import { CoreSelect } from "../../../../../../core/ui/select/select";
import { PointModel } from "../../../../../../core/model/point_model";
import { ObjectIsNotEmpty } from "../../../../../../core/extensions/object";
import { CoreButton } from "../../../../../../core/ui/button/button";
import { message } from "antd";
import { BehaviorTreeBuilderStore } from "../../../behavior_tree_builder_store";

export const MoveToPose = observer((props: IPropsForm<MoveToPoseRobotModel | undefined>) => {
  const propStore = props.store as BehaviorTreeBuilderStore;
  const [store] = React.useState(() => new MoveToPoseStore(propStore));
  React.useEffect(() => {
    if (ObjectIsNotEmpty(props.dependency)) store.loadDependency(props.dependency);
    store.init();
  }, [store]);
  return (
    <div>
      <CoreSelect
        items={propStore.sceneAsset?.getAllRobotsTopics() ?? ["ERROR"]}
        value={store.viewModel.robot_name ?? ""}
        label={"Робот"}
        onChange={(text) => store.updateForm({ robot_name: text })}
      />
      <CoreSelect
        items={propStore.sceneAsset?.getAllPoints() ?? ["ERROR"]}
        label={"Точка"}
        onChange={(text) =>
          store.updateForm({ pose: propStore.sceneAsset?.getElementByName<PointModel>(text).toDependency() })
        }
        value={store.viewModel?.pose?.name ?? ""}
      />
      <CoreButton
        style={{ margin: 10, width: 100 }}
        text="OK"
        onClick={() => {
          store.viewModel.valid().fold(
            (s) => {
              // props.onChange(s);
            },
            (e) => message.error(e)
          );
        }}
      />
    </div>
  );
});
