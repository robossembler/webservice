import makeAutoObservable from "mobx-store-inheritance";
import { FormState, CoreError } from "../../../../../../core/store/base_store";
import { DataSetHttpRepository } from "../../../../../dataset/dataset_http_repository";
import { ISkils, CalculationHttpRepository } from "../../../../../calculation_instance/data/calculation_http_repository";
import { WeightsViewModel } from "./weights_view_model";
import { Parts } from "../../../../../details/details_http_repository";

export class WeightsFormStore extends FormState<WeightsViewModel, CoreError> {
    weights?: ISkils[];
    parts?: Parts[];
    suitableWeights: string[] = [];
    viewModel: WeightsViewModel = WeightsViewModel.empty();
    skillsHttpRepository: CalculationHttpRepository = new CalculationHttpRepository();
    datasetHttpRepository: DataSetHttpRepository = new DataSetHttpRepository();
    constructor() {
      super();
      makeAutoObservable(this);
    }
    init = async () => {
      await this.mapOk("weights", this.skillsHttpRepository.getAllTemplates());
      await this.mapOk("parts", this.datasetHttpRepository.getAssetsActiveProject());
    };
    changeDimensions = (index: number, value: number) => {
      this.viewModel.dimensions[index] = value;
    };
    selectAsset = (): void => {
      this.suitableWeights =
        this.weights
          ?.filter((el) =>
            el.datasetId.dataSetObjects.filter((datasetObject: string) =>
              this.viewModel.object_name.isEqual(datasetObject)
            )
          )
          .map((el) => el.name) ?? [];
    };
    updateWeights = (text: string) => {
      const model = this.weights
        ?.filter((el) =>
          el.datasetId.dataSetObjects.filter((datasetObject: string) => this.viewModel.object_name.isEqual(datasetObject))
        )
        .at(0);
  
      this.updateForm({ weights_file: `${model?.datasetId.local_path}/weights/${model?.name}/${model?.name}.pt` });
    };
    errorHandingStrategy = (_: CoreError) => {};
  }
  
  