import { Result } from "../../../../../../core/helper/result";
import { IWeightsDependency } from "../../../../../../core/model/skill_model";

export class WeightsViewModel implements IWeightsDependency {
    constructor(
      public object_name: string = "",
      public weights_file: string = "",
      public dimensions: number[] = [],
      public weights_name = ""
    ) { }
    static empty() {
      return new WeightsViewModel();
    }
    isEmpty = (): Result<string, undefined> => {
      if (this.weights_name.isEmpty()) {
        return Result.error("weights_name is empty");
      }
      if (this.object_name.isEmpty()) {
        return Result.error("object name is empty");
      }
      if (this.weights_file.isEmpty()) {
        return Result.error("weights_file is empty");
      }
      if (this.dimensions.isEmpty()) {
        return Result.error("dimensions is empty");
      }
      return Result.ok(undefined);
    };
  }
  