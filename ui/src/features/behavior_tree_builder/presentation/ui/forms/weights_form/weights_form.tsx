import { observer } from "mobx-react-lite";
import React from "react";
import { Select, message } from "antd";
import { WeightsFormStore } from "./weights_store";
import { IWeightsDependency } from "../../../../../../core/model/skill_model";
import { CoreButton } from "../../../../../../core/ui/button/button";
import { CoreInput } from "../../../../../../core/ui/input/input";
import { CoreText, CoreTextType } from "../../../../../../core/ui/text/text";


interface IWeightsFormProps {
  dependency?: IWeightsDependency;
  onChange: (dependency: IWeightsDependency) => void;
}

export const WeightsForm = observer((props: IWeightsFormProps) => {
  const [store] = React.useState(() => new WeightsFormStore());
  React.useEffect(() => {
    store.init();
  }, [store]);

  return (
    <div style={{ border: '1px solid', padding: 10, margin: 10 }}>
      <CoreText text="Weights" type={CoreTextType.header} style={{ padding: 10 }} />
      <Select
        showSearch
        placeholder="Выберите деталь"
        optionFilterProp="children"
        defaultValue={props.dependency?.object_name}
        onChange={(e) => {
          store.updateForm({ object_name: e });
          store.selectAsset();
        }}
        filterOption={(input: string, option?: { label: string; value: string }) =>
          (option?.label ?? "").toLowerCase().includes(input.toLowerCase())
        }
        style={{ width: "100%" }}
        options={
          store.parts?.map((el) => {
            return { label: el.name, value: el.name };
          }) ?? []
        }
      />
      <Select
        showSearch
        placeholder="Выберите деталь"
        optionFilterProp="children"
        defaultValue={props.dependency?.weights_name}
        onChange={(text) => {
          store.updateForm({ weights_name: text });
          store.updateWeights(text);
        }}
        filterOption={(input: string, option?: { label: string; value: string }) =>
          (option?.label ?? "").toLowerCase().includes(input.toLowerCase())
        }
        style={{ width: "100%" }}
        options={
          store.suitableWeights?.map((el) => {
            return { label: el, value: el };
          }) ?? []
        }
      />
      <div style={{ height: 5 }} />
      <CoreInput
        label={"dimensions 1"}
        onChange={(text) => store.changeDimensions(0, Number(text))}
        validation={(text) => Number().isValid(text)}
        value={props.dependency?.dimensions?.at(0)?.toString()}
      />
      <div style={{ height: 15 }} />

      <CoreInput
        label={"dimensions 2"}
        onChange={(text) => store.changeDimensions(1, Number(text))}
        validation={(text) => Number().isValid(text)}
        value={props.dependency?.dimensions?.at(1)?.toString()}
      />
      <div style={{ height: 15 }} />

      <CoreInput
        label={"dimensions 3"}
        onChange={(text) => store.changeDimensions(2, Number(text))}
        validation={(text) => Number().isValid(text)}
        value={props.dependency?.dimensions?.at(2)?.toString()}
      />
      <div style={{ height: 15 }} />
      <CoreButton
        onClick={() => {
          store.viewModel.isEmpty().fold(
            () => {
              props.onChange(store.viewModel);
            },
            (e) => {
              message.error(e);
            }
          );
        }}
        text="OK"
        style={{ width: 100 }}
      />
    </div>
  );
});

