import makeAutoObservable from "mobx-store-inheritance";
import { NavigateFunction } from "react-router-dom";
import { Cameras } from "../../../../../../core/model/cameras";
import { SidViewModel } from "../../../../../../core/model/device_dependency_view_model";
import { FormState, CoreError } from "../../../../../../core/store/base_store";
import { CameraDeviceHttpRepository } from "./camera_device_form_http_repository";

export class CameraDeviceStore extends FormState<SidViewModel, CoreError> {
    constructor() {
        super();
        makeAutoObservable(this);
    }
    viewModel: SidViewModel = SidViewModel.empty();
    cameras?: Cameras;
    cameraDeviceHttpRepository: CameraDeviceHttpRepository = new CameraDeviceHttpRepository();
    errorHandingStrategy = (error: CoreError) => { }
    init = async (navigate?: NavigateFunction | undefined) => {
        this.mapOk('cameras', this.cameraDeviceHttpRepository.getAllCameras())
    }

}
