import React from "react";
import { observer } from "mobx-react-lite";
import { message } from "antd";
import { CameraDeviceStore } from "./camera_device_store";
import { IPropsForm } from "../forms";
import { IDeviceDependency } from "../../../../../../core/model/skill_model";
import { CoreButton } from "../../../../../../core/ui/button/button";
import { CoreSelect } from "../../../../../../core/ui/select/select";
import { CoreText, CoreTextType } from "../../../../../../core/ui/text/text";
import { SidViewModel } from "../../../../../../core/model/device_dependency_view_model";

export const CameraDeviceForm = observer((props: IPropsForm<Partial<IDeviceDependency>>) => {
  const [store] = React.useState(() => new CameraDeviceStore());
  React.useEffect(() => {
    store.init();
  }, [store]);
  return (
    <div style={{ border: "1px solid", margin: 10, padding: 10 }}>
      <CoreText text={"Cameras"} type={CoreTextType.header} style={{ padding: 10 }} />
      <CoreSelect
        items={store.cameras?.camera.map((el) => el.sid) ?? []}
        value={props.dependency?.sid ?? ""}
        label={"Выберите камеру"}
        onChange={(value: string) => store.updateForm({ sid: value })}
      />
      <CoreButton
        style={{ margin: 10, width: 100 }}
        text="OK"
        onClick={async () => {
          (await store.viewModel.valid<SidViewModel>()).fold(
            (s) => {
              // props.onChange(s);
            },
            (e) => message.error(e)
          );
        }}
      />
    </div>
  );
});
