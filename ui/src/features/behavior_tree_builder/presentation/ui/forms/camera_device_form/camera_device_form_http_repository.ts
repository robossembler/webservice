import { Cameras } from "../../../../../../core/model/cameras";
import { HttpMethod, CoreHttpRepository } from "../../../../../../core/repository/core_http_repository";

export class CameraDeviceHttpRepository extends CoreHttpRepository {
    getAllCameras = () => this._jsonRequest<Cameras>(HttpMethod.GET, '/behavior/trees/cameras');
}