import makeAutoObservable from "mobx-store-inheritance";
import { ValidationModel } from "../../../../../../core/model/validation_model";
import { IsNotEmpty, IsString } from "class-validator";
import { StoreTopicType } from "./topics_form_store";
import { DependencyViewModel } from "../../../../../../core/model/skill_model";
import { BehaviorTreeBuilderStore } from "../../../behavior_tree_builder_store";
import { CoreText, CoreTextType } from "../../../../../../core/ui/text/text";
import { FormType } from "../../../../../../core/model/form";
 
export class TopicDependencyViewModel extends ValidationModel implements DependencyViewModel {
  @IsNotEmpty()
  @IsString()
  type = FormType.topic;
  topicType?: string;

  mode?: StoreTopicType;
  sid?: string;
  topicOut?: string;
  constructor() {
    super();
    makeAutoObservable(this);
  }
  toView = (store: BehaviorTreeBuilderStore | undefined): React.ReactNode => {
    if (store && this.sid) {
      if (store.filledOutTemplates.topicsStack.some((el) => el.sid?.isEqual(this.sid ?? ""))) {
        return <div style={{ color: "white" }}>{this.topicOut}</div>;
      } else {
        return <CoreText type={CoreTextType.header} text="Error" color="red" />;
      }
    }
    return <CoreText type={CoreTextType.header} text="Error" color="red" />;
  };

  static empty() {
    return new TopicDependencyViewModel();
  }
}
