import { Topics } from "../../../../../../core/model/topics";
import { HttpMethod, CoreHttpRepository } from "../../../../../../core/repository/core_http_repository";

export class TopicsFormHttpRepository extends CoreHttpRepository {
    getAllTopics = () => this._jsonRequest<Topics>(HttpMethod.GET, '/behavior/trees/topics');
}
