import makeAutoObservable from "mobx-store-inheritance";
import { NavigateFunction } from "react-router-dom";
import { TopicDependencyViewModel } from "./topic_dependency_view_model";
import { FormState, CoreError } from "../../../../../../core/store/base_store";
import { TopicsFormHttpRepository } from "./topics_form_http_repository";
import { BehaviorTreeBuilderStore } from "../../../behavior_tree_builder_store";
import { ITopicModel } from "../../../../../topics/topic_view_model";
export enum StoreTopicType {
  specifiesDependencies = "specifiesDependencies",
  btExecute = "btExecute",
}
export class TopicsFormStore extends FormState<TopicDependencyViewModel, CoreError> {
  type: StoreTopicType = StoreTopicType.btExecute;
  topics?: ITopicModel[];
  behaviorTreeBuilderStore?: BehaviorTreeBuilderStore;
  viewModel: TopicDependencyViewModel = TopicDependencyViewModel.empty();
  constructor() {
    super();
    makeAutoObservable(this);
  }
  topicsFormHttpRepository: TopicsFormHttpRepository = new TopicsFormHttpRepository();
  errorHandingStrategy = (error: CoreError) => {};
  init = async (navigate?: NavigateFunction | undefined) => {
    // await this.mapOk('topics', this.cameraDeviceHttpRepository.getAllTopics())
  };
  getAllTopicsMatched = () => this.topics?.filter((el) => el.type.isEqual(this.viewModel.topicType ?? ""));

  initParam = (isBtScreen: boolean, behaviorTreeBuilderStore: BehaviorTreeBuilderStore | undefined) => {
    isBtScreen ? (this.type = StoreTopicType.btExecute) : (this.type = StoreTopicType.specifiesDependencies);
    this.behaviorTreeBuilderStore = behaviorTreeBuilderStore;
    this.topics = behaviorTreeBuilderStore?.getAllTopics();
  };
}
