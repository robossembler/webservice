import React from "react";
import { observer } from "mobx-react-lite";
import { TopicsFormStore } from "./topics_form_store";
import { IPropsForm } from "../forms";
import { TopicDependencyViewModel } from "./topic_dependency_view_model";
import { CoreText, CoreTextType } from "../../../../../../core/ui/text/text";
import { match } from "ts-pattern";
import { CoreInput } from "../../../../../../core/ui/input/input";
import { CoreButton } from "../../../../../../core/ui/button/button";
import { message } from "antd";
import { themeStore } from "../../../../../..";
import { isBtScreen } from "../../../behavior_tree_builder_screen";

export const TopicsForm = observer((props: IPropsForm<Partial<TopicDependencyViewModel>>) => {
  const [store] = React.useState(() => new TopicsFormStore());

  React.useEffect(() => {
    store.init();
    store.initParam(isBtScreen(), props.store);

    store.loadClassInstance(TopicDependencyViewModel, props.dependency as TopicDependencyViewModel);
  }, [props]);

  return (
    <div style={{ height: "100%", overflow: "scroll" }}>
      <CoreText
        text={"Topics"}
        type={CoreTextType.header}
        style={{ padding: 10 }}
        color={themeStore.theme.darkOnSurfaceVariant}
      />
      {match(isBtScreen())
        .with(true, () => (
          <>
            <CoreText
              text={"Выберите топик"}
              type={CoreTextType.header}
              style={{ padding: 10 }}
              color={themeStore.theme.darkOnSurfaceVariant}
            />
            {store
              .getAllTopicsMatched()

              ?.map((el) => (
                <div
                  style={{
                    display: "flex",
                    border: el.name.isEqual(store.viewModel.topicOut ?? "") ? "1px solid beige" : undefined,
                  }}
                  onClick={() => (store.updateForm({ topicOut: el.name }), store.updateForm({ sid: el.sid }))}
                >
                  <CoreText
                    text={`name:${el.name}`}
                    type={CoreTextType.header}
                    style={{ padding: 10 }}
                    color={themeStore.theme.darkOnSurfaceVariant}
                  />
                  <CoreText
                    text={`type:${el.type}`}
                    type={CoreTextType.header}
                    style={{ padding: 10 }}
                    color={themeStore.theme.darkOnSurfaceVariant}
                  />
                </div>
              ))}
            <CoreButton
              text="OK"
              style={{ width: 100 }}
              onClick={async () => {
                (await store.viewModel.valid<TopicDependencyViewModel>()).fold(
                  (model) => props.onChange(model),
                  (e) => message.error(e)
                );
              }}
            />
          </>
        ))
        .with(false, () => (
          <>
            <CoreInput label={"Тип топика"} onChange={(text) => store.updateForm({ topicType: text })} />
            <div style={{ height: 10 }} />
            <CoreButton
              text="OK"
              style={{ width: 100 }}
              onClick={async () => {
                (await store.viewModel.valid<TopicDependencyViewModel>()).fold(
                  (s) => props.onChange(s),
                  (e) => message.error(e)
                );
              }}
            />
          </>
        ))
        .otherwise(() => (
          <></>
        ))}
    </div>
  );
});
