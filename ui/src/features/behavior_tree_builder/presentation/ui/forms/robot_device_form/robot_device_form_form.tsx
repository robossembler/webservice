import React from "react";
import { observer } from "mobx-react-lite";
import { RobotDeviceFormStore } from "./robot_device_form_store";
import { IPropsForm } from "../forms";
import { SidViewModel } from "../../../../../../core/model/device_dependency_view_model";
 
export const RobotDeviceForm = observer((props: IPropsForm<Partial<SidViewModel>>) => {
  const [store] = React.useState(() => new RobotDeviceFormStore());
  React.useEffect(() => {
    store.init();
  }, [store]);

  return <div></div>;
});
