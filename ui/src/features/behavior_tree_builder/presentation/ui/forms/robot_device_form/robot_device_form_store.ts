import { makeAutoObservable } from "mobx";
import { NavigateFunction } from "react-router-dom";
import { FormState, CoreError } from "../../../../../../core/store/base_store";
import { RobotDeviceFormHttpRepository } from "./robot_device_form_http_repository";
import { SidViewModel } from "../../../../../../core/model/device_dependency_view_model";

export class RobotDeviceFormStore extends FormState<SidViewModel, CoreError> {
    constructor() {
        super();
        makeAutoObservable(this);
    }
    viewModel: SidViewModel = SidViewModel.empty();
    robotDeviceFormHttpRepository:  RobotDeviceFormHttpRepository = new RobotDeviceFormHttpRepository();
    errorHandingStrategy = (error: CoreError) => { }
    init = async (navigate?: NavigateFunction | undefined) => {
      
    }

}
