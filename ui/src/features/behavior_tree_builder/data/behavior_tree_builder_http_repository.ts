import { Result } from "../../../core/helper/result";
import { SkillModel, Skills } from "../../../core/model/skill_model";
import { HttpError, HttpMethod, CoreHttpRepository } from "../../../core/repository/core_http_repository";
import { BehaviorTreeModel } from "../model/behavior_tree_model";
import { BehaviorTreeViewModel } from "../model/behavior_tree_view_model";

export class BehaviorTreeBuilderHttpRepository extends CoreHttpRepository {
  featureApi = `/behavior/trees`;

  getAllBtInstances = async () => this._jsonRequest<BehaviorTreeModel[]>(HttpMethod.GET, this.featureApi);
  getBtSkills = async (): Promise<Result<HttpError, SkillModel[]>> => {
    return (await this._arrayJsonToClassInstanceRequest<SkillModel>(
      HttpMethod.GET,
      `/skills`,
      SkillModel
    )) as unknown as Promise<Result<HttpError, SkillModel[]>>;
  };
  getBtById = async (id: string): Promise<Result<HttpError, BehaviorTreeModel>> =>
    this._jsonToClassInstanceRequest<BehaviorTreeModel>(
      HttpMethod.GET,
      `${this.featureApi}/by_id?id=${id}`,
      BehaviorTreeModel
    ) as unknown as Promise<Result<HttpError, BehaviorTreeModel>>;
  deleteBt = (id: string) => this._jsonRequest(HttpMethod.DELETE, `${this.featureApi}?id=${id}`);
  editBt = async (model: BehaviorTreeModel) => {
    await this._jsonRequest(HttpMethod.POST, `${this.featureApi}/fill/tree`, model);
    model.__v = undefined
    return await this._jsonRequest(HttpMethod.PUT, this.featureApi, model);
  };
}
