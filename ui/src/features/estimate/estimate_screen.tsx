import * as React from "react";
import { MainPage } from "../../core/ui/pages/main_page";

export interface IEstimateScreenProps {}
export const EstimateScreenPath = "/estimate";
export function EstimateScreen(props: IEstimateScreenProps) {
  return <MainPage page={"Оценка"}></MainPage>;
}
