import { HttpMethod, CoreHttpRepository } from "../../../core/repository/core_http_repository";
import { CalculationModel } from "../model/calculation_model";

export interface ISkils {
  _id: string;
  name: string;
  processStatus: string;
  epoch: number;
  isFinished: boolean;
  datasetId: any;
  project: any;
  numberOfTrainedEpochs: number;
  __v: number;
}

export class CalculationHttpRepository extends CoreHttpRepository {
  
  featureApi = `/calculations/instances`;
  subFeatureApi = `/calculations/template`;

  editCalculation = (model: CalculationModel) => this._jsonRequest(HttpMethod.PUT, this.featureApi, model);
  createNewInstance = (model: CalculationModel) => this._jsonRequest(HttpMethod.POST, this.featureApi, model);
  createNewTemplate = (model: CalculationModel) => this._jsonRequest(HttpMethod.POST, this.subFeatureApi, model);
  getAllInstances = () => this._jsonRequest(HttpMethod.GET, this.featureApi);
  execInstance = (id: string) => this._jsonRequest(HttpMethod.GET, `${this.featureApi}/exec?id=${id}`);
  getAllTemplates = async () => this._jsonRequest(HttpMethod.GET, this.subFeatureApi);
  deleteTemplate = (id: string) => this._jsonRequest(HttpMethod.DELETE, `${this.subFeatureApi}?id=${id}`);
  deleteInstance = (id: string) => this._jsonRequest(HttpMethod.DELETE, `${this.featureApi}?id=${id}`);
}
