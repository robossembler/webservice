import { TypedEvent } from "../../../core/helper/typed_event";
import { SocketRepository, socketRepository } from "../../../core/repository/core_socket_repository";
import { ProcessStatus } from "../../dataset/dataset_model";

export interface ProcessUpdate {  
  id: string;
  status: ProcessStatus;
}

export class CalculationSocketRepository extends TypedEvent<ProcessUpdate> {
  socketRepository: SocketRepository = socketRepository;
  constructor() {
    super();
    this.socketRepository.on((e) => {
      if (e.event === "realtime") {
        if (e.payload !== undefined && e.payload.value !== undefined && e.payload.value.id !== undefined) {
          this.emit({
            id: String(e.payload.value.id),
            status: ProcessStatus.END,
          });
        }
        if (e.payload !== undefined && e.payload.error !== undefined && e.payload.error.id !== undefined) {
          this.emit({
            id: String(e.payload.error.id),
            status: ProcessStatus.ERROR,
          });
        }
      }
    });
  }
}
