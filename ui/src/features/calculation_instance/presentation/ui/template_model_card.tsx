import { CoreText, CoreTextType } from "../../../../core/ui/text/text";
import { CalculationModel } from "../../model/calculation_model";

export const TemplateModelCard = ({
  model,
  isSelect,
  onClick,
  onDelete,
}: {
  model: CalculationModel;
  isSelect: boolean;
  onClick?: Function;
  onDelete?: Function;
}) => {
  return (
    <div
      onClick={() => {
        if (onClick) onClick();
      }}
      style={{
        backgroundColor: "rgba(104, 80, 164, 1)",
        width: 180,
        height: 180,
        borderRadius: 10,
        padding: 10,
        margin: 10,
        border: isSelect ? "4px solid red" : undefined,
      }}
    >
      <div
        onClick={() => {
          if (onDelete) onDelete();
        }}
      >
        DELETE
      </div>
      <CoreText text={model.type} type={CoreTextType.small} color="white" />
      <CoreText text={new Date().fromUnixDate(model.createDate).formatDate()} type={CoreTextType.small} color="white" />

      <CoreText text={model.name} type={CoreTextType.small} color="white" />
    </div>
  );
};
