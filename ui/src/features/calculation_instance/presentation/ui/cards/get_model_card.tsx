import { match } from "ts-pattern";
import { PoseEstimateCard } from "./pose_estimate_card/model_card";
import { Dropdown, MenuProps, message } from "antd";
import { CoreText, CoreTextType } from "../../../../../core/ui/text/text";
import { IMenuItem } from "../../../../dataset/card_dataset";
import { Icon } from "../../../../../core/ui/icons/icons";
import { CalculationModel } from "../../../model/calculation_model";

export enum CardModel {
  pose_estimate = "pose_estimate",
}

export const getModelCard = (
  calculationModel: CalculationModel,
  onEdit: Function,
  onDelete: Function,
  onPlay: Function,
  onPause: Function
) => {
  const menu: IMenuItem[] = [
    {
      onClick: () => onEdit(),
      name: "Редактировать",
    },
    {
      onClick: () => onDelete(),
      name: "Удалить",
    },
  ];

  const items: MenuProps["items"] = menu.map((el, index) => {
    return {
      key: String(index),
      label: <CoreText text={el.name} type={CoreTextType.medium} />,
      onClick: () => el.onClick(),
    };
  });

  return (
    <div
      style={{
        display: "flex",
        margin: 10,
        padding: 10,
        background: "#f7f2fa",
        justifyContent: "space-between",
        alignItems: "flex-start",
        height: 150,
      }}
    >
      <div>
        <div style={{ display: "flex" }}>
          {calculationModel.isEnd ? (
            <Icon type="Pause" onClick={() => onPause()} />
          ) : (
            <Icon type="Play" onClick={() => onPlay()} />
          )}
          <div>
            <div onClick={() => onPlay()}>EXEC</div>
            <div>STATUS:{calculationModel.processStatus}</div>
          </div>

          <Icon
            type="Log"
            onClick={async () =>
              window.prompt("Copy to clipboard: Ctrl+C, Enter", calculationModel.lastProcessLogs ?? "Not found logs")
            }
          />
          <Icon
            type="Command"
            onClick={async () =>
              window.prompt(
                "Copy to clipboard: Ctrl+C, Enter",
                calculationModel.lastProcessExecCommand ?? "Not last process command logs"
              )
            }
          />
        </div>
        <CoreText text={calculationModel.type} type={CoreTextType.small} />
        <CoreText text={calculationModel.name} type={CoreTextType.small} />

        <CoreText text={calculationModel.instanceName} type={CoreTextType.small} />

        {match(calculationModel.card ?? "")
          .with(CardModel.pose_estimate, () => {
            return <PoseEstimateCard dependency={calculationModel} empty={false} />;
          })
          .otherwise(() => {
            return <></>;
          })}
      </div>

      <div style={{ height: 10 }}>
        <Dropdown overlayStyle={{ backgroundColor: "rgba(243, 237, 247, 1)" }} menu={{ items }}>
          <div>
            <Icon type="Settings" />
          </div>
        </Dropdown>
      </div>
    </div>
  );
};
