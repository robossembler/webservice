import makeAutoObservable from "mobx-store-inheritance";
import { ButtonV2 } from "../../../../../../core/ui/button/button_v2";
import { HttpMethod, HttpRepository } from "../../../../../../core/repository/core_http_repository";
import { useStore } from "../../../../../../core/helper/use_store";
import { UiErrorState } from "../../../../../../core/store/base_store";
import { NavigateFunction } from "react-router-dom";

export interface IModelCardProps {
  _id?: string;
  dependency?: Object;
  processStatus?: string;
  name?: string;
  isFinished?: boolean;
  id?: string;
  emptyOnClick?: Function;
  startOnClick?: (id: string) => void;
  continueOnClick?: (id: string) => void;
  empty: boolean;
  onEdit?: (id: string) => void;
  epoch?: number;
  onDelete?: (id: string) => void;
  datasetName?: string;
  epochNextTraining?: number;
}
export class PoseEstimateRepository extends HttpRepository {
  execAnalyze = (id: string) => this._jsonRequest(HttpMethod.POST, `/run_time/exec/analyze?id=${id}`);
}
export class PoseEstimateStore extends UiErrorState<any> {
  navigate?: NavigateFunction;
  poseEstimateRepository = new PoseEstimateRepository();
  constructor() {
    super();
    makeAutoObservable(this);
  }
  async init(navigate?: NavigateFunction | undefined) {
    this.navigate = navigate;
  }
  onClickExecAnalyze = async (id: string) =>
    (await this.poseEstimateRepository.execAnalyze(id)).fold(
      () => {
        setTimeout(() => {
          window.open("http://localhost:6006/", "mozillaWindow", "popup");
        }, 3000);
      },
      () => {}
    );
}
export const PoseEstimateCard = (props: IModelCardProps) => {
  const store = useStore(PoseEstimateStore);
  // @ts-ignore
  return <ButtonV2 onClick={() => store.onClickExecAnalyze(props.dependency._id ?? "")} text="Запустить анализ" />;
};
