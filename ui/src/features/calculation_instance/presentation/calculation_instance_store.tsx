import makeAutoObservable from "mobx-store-inheritance";
import { NavigateFunction } from "react-router-dom";
import { HttpError } from "../../../core/repository/core_http_repository";
import { Drawer, UiDrawerFormState } from "../../../core/store/base_store";
import { CalculationHttpRepository } from "../data/calculation_http_repository";
import { message } from "antd";
import { UUID } from "../../all_projects/data/project_http_repository";
import { CalculationModel } from "../model/calculation_model";
import { ProcessUpdate, CalculationSocketRepository } from "../data/calculation_socket_repository";
import { match } from "ts-pattern";
import { plainToInstance } from "class-transformer";

export enum DrawersSkill {
  NEW_SKILL = "Новое вычисление",
  EDIT_SKILL = "Редактировать вычисление",
}
export enum StoreTypes {
  newType = "newType",
  newModel = "newModel",
  empty = "empty",
}

export class CalculationInstanceStore extends UiDrawerFormState<CalculationModel, HttpError> {
  calculationHttpRepository: CalculationHttpRepository = new CalculationHttpRepository();
  calculationSocketRepository: CalculationSocketRepository = new CalculationSocketRepository();
  activeProjectId?: UUID;
  storeType: StoreTypes = StoreTypes.empty;
  viewModel: CalculationModel = CalculationModel.empty();
  modelTemplate?: CalculationModel[];
  editProcess?: CalculationModel;
  calculationInstances?: CalculationModel[];
  selectTemplate?: CalculationModel;
  titleDrawer: string = DrawersSkill.NEW_SKILL;
  drawers: Drawer[];
  isModalOpen: boolean = false;
  constructor() {
    super({});
    this.drawers = Object.entries(DrawersSkill).map((k, v) => {
      return {
        name: k.at(1) ?? "",
        status: false,
      };
    });
    this.viewModel = CalculationModel.empty();
    this.calculationSocketRepository.on(this.socketUpdate);
    makeAutoObservable(this);
  }
  deleteInstance = async (id: string) => {
    await this.calculationHttpRepository.deleteInstance(id);
    await this.init();
  };
  socketUpdate = (data: ProcessUpdate) => {
    this.calculationInstances?.map((el) => {
      if (el?._id && el._id.isEqual(data.id)) {
        el.processStatus = data.status;
      }
      return el;
    });
  };
  getSkillById = (id: string) => {
    // this.machineLearningProcess?.find((el) => el._id === id);
  };

  execSkill = async (id: string) =>
    this.messageHttp(this.calculationHttpRepository.execInstance(id), {
      errorMessage: "Ошибка",
      successMessage: "Процесс запущен",
    });

  setSelectTemplate = (el: CalculationModel) => {
    this.selectTemplate = el;
    const instance = plainToInstance(CalculationModel, el);
    instance.instanceName = this?.viewModel.instanceName;
    this.viewModel = instance;
  };
  selectType = (newType: StoreTypes) => (this.storeType = newType);

  init = async (navigate?: NavigateFunction | undefined) => {
    await this.mapOk("modelTemplate", this.calculationHttpRepository.getAllTemplates());
    await this.mapOk("activeProjectId", this.calculationHttpRepository.getActiveProjectId());
    await this.mapOk("calculationInstances", this.calculationHttpRepository.getAllInstances());
  };
  clickSave = async () =>
    match(this.storeType)
      .with(StoreTypes.empty, () => {})
      .with(StoreTypes.newType, async () =>
        (await this.viewModel.valid<CalculationModel>()).fold(
          async (model) => {
            model.project = this.activeProjectId?.id;

            await this.messageHttp(this.calculationHttpRepository.createNewTemplate(model), {
              successMessage: "Создан новый тип процесса",
            });
            await this.init();

            this.editDrawer(DrawersSkill.NEW_SKILL, false);
          },
          async (e) => message.error(e)
        )
      )
      .with(StoreTypes.newModel, async () => {
        (await this.viewModel.valid<CalculationModel>()).fold(
          async (model) => {
            delete model._id;
            model.project = this.activeProjectId?.id;
            await this.messageHttp(this.calculationHttpRepository.createNewInstance(model), {
              successMessage: "Создан новый экземпляр процесса",
            });
            await this.init();

            this.editDrawer(DrawersSkill.NEW_SKILL, false);
          },
          async (e) => {
            message.error(e);
          }
        );
      });

  editDrawer(drawerName: DrawersSkill, status: boolean): void {
    this.titleDrawer = drawerName;
    this.drawers = this.drawers.map((el) => {
      if (el.name === drawerName) {
        el.status = status;
      }
      return el;
    });
  }

  saveEdiSkill = async () => {
    this.editDrawer(DrawersSkill.EDIT_SKILL, false);
    (await this.viewModel.valid<CalculationModel>()).fold(
      async (model) => await this.calculationHttpRepository.editCalculation(model),
      async (err) => message.error(err)
    );
  };
  makeEditProcess = (el: CalculationModel) => {
    this.editProcess = el;
    this.loadClassInstance(CalculationModel, el);
    this.editDrawer(DrawersSkill.EDIT_SKILL, true);
  };
  deleteTemplate = async (el: CalculationModel) => {
    await this.messageHttp(this.calculationHttpRepository.deleteTemplate(el._id ?? ""), {
      successMessage: "Удален",
    });
    await this.mapOk("modelTemplate", this.calculationHttpRepository.getAllTemplates());
  };
}
