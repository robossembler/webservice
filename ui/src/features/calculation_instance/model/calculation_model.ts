 import { IsNotEmpty, IsString } from "class-validator";
import { ValidationModel } from "../../../core/model/validation_model";
import { FormBuilderValidationModel } from "../../../core/model/form_builder_validation_model";

export enum ModelMachineLearningTypes {
  OBJECT_DETECTION = "OBJECT_DETECTION",
  POSE_ESTIMATE = "POSE_ESTIMATE",
}

export class CalculationModel extends ValidationModel {
  _id?: string;
  processStatus?:string;
  instanceName: string;
  @IsString()
  @IsNotEmpty()
  script: string;
  formBuilder: FormBuilderValidationModel;
  @IsString()
  @IsNotEmpty()
  type: string;
  @IsString()
  @IsNotEmpty()
  name: string;
  isEnd: boolean = false;
  createDate: number = Date.now();
  card?: string;
  lastProcessLogs?: string;
  lastProcessExecCommand?: string;
  lastExecDate?: Date;
  project?: string;
  constructor(script: string, formBuilderValidationModel: FormBuilderValidationModel, type: string, card: string) {
    super();
    this.script = script;
    this.formBuilder = formBuilderValidationModel;
    this.type = type;
    this.card = card;
  }
  static empty = () => new CalculationModel("", FormBuilderValidationModel.empty(), "", "");
}
