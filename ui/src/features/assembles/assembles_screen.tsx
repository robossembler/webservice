import * as React from "react";
import { MainPage } from "../../core/ui/pages/main_page";

export interface IAssemblesScreenProps {}
export const AssemblesScreenPath = "/assembles";
export function AssemblesScreen(props: IAssemblesScreenProps) {
  return <MainPage page={"Сборки"}></MainPage>;
}
