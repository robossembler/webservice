import makeAutoObservable from "mobx-store-inheritance";
import { FormState } from "../../core/store/base_store";
import { HttpError } from "../../core/repository/core_http_repository";
import { ITopicModel, TopicViewModel as TopicModel } from "./topic_view_model";
import { NavigateFunction } from "react-router-dom";
import { TopicsHttpRepository } from "./topics_repository";

export class TopicsStore extends FormState<TopicModel, HttpError> {
  viewModel: TopicModel = TopicModel.empty();
  topics?: ITopicModel[];
  topicsHttpRepository: TopicsHttpRepository = new TopicsHttpRepository();
  constructor() {
    super();
    makeAutoObservable(this);
  }
  init = async (navigate?: NavigateFunction | undefined) => {
    await this.mapOk("topics", this.topicsHttpRepository.getAllTopics());
  };
}
