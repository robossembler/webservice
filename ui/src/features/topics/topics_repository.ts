import { HttpMethod, HttpRepository } from "../../core/repository/core_http_repository";

export class TopicsHttpRepository extends HttpRepository {
  featureApi = "/topics";
  getAllTopics = () => this._jsonRequest(HttpMethod.GET, this.featureApi);
}
