import { IsNotEmpty, IsString } from "class-validator";
import { ValidationModel } from "../../core/model/validation_model";

export class TopicViewModel extends ValidationModel {
  @IsNotEmpty()
  @IsString()
  name: string;
  @IsNotEmpty()
  @IsString()
  type: string;
  sid?: string;
  constructor(name: string, type: string) {
    super();
    this.name = name;
    this.type = type;
  }
  static empty() {
    return new TopicViewModel("", "");
  }
}
export interface ITopicModel {
  digitalTwinId?: string;
  sid?: string;
  name: string;
  type: string;
}
