import React from "react";
import { observer } from "mobx-react-lite";
import { TopicsStore } from "./topics_store";
import { useStore } from "../../core/helper/use_store";

export const TopicsScreenPath = "/topics";

export const TopicsScreen = observer(() => {
  const store = useStore(TopicsStore);

  return (
    <>
      {store.topics?.map((el) => (
        <div style={{ margin: 10, border: "1px solid red" }}>
          <div>{el.name}</div>
          <div>{el.type}</div>
        </div>
      ))}
    </>
  );
});
