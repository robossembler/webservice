import makeAutoObservable from "mobx-store-inheritance";
import { UiDrawerFormState } from "../../core/store/base_store";
import { NavigateFunction } from "react-router-dom";
import { HttpError } from "../../core/repository/core_http_repository";
import { DependencyViewModel, ISkill, ParamViewModel, SkillModel } from "../../core/model/skill_model";
import { message } from "antd";
import { SkillsHttpRepository } from "./skills_http_repository";
import { FormType } from "../../core/model/form";
import { plainToInstance } from "class-transformer";
import { isValidJson } from "./skills_screen";

export enum DrawersSkills {
  newSkill = "Новый навык",
  importSkill = "Импорт навыка",
}
export class SkillsStore extends UiDrawerFormState<SkillModel, HttpError> {
  isModalOpen: boolean = false;
  activeIndex?: number;
  viewModel: SkillModel = SkillModel.empty();
  selectParam?: {
    form: FormType;
    component?: React.ReactNode;
  };
  skills: SkillModel[];
  skillsHttpRepository: SkillsHttpRepository = new SkillsHttpRepository();
  formBuilderModal: boolean = false;
  constructor() {
    super(DrawersSkills);
    makeAutoObservable(this);
  }
  importSkill = (text: string) =>
    isValidJson<ISkill>(text.replace(/[^\x00-\x7F]/g, "")).fold(
      async (model) =>
        (await (await plainToInstance(SkillModel, model)).valid<SkillModel>()).fold(
          async (skillModel) => {
            skillModel._id = undefined;
            await this.messageHttp(this.skillsHttpRepository.createNewSkill(skillModel), {
              successMessage: "Навык импортирован",
            });
            await this.init();
            this.editDrawer(DrawersSkills.importSkill, false);
          },
          async (error) => message.error(error)
        ),
      async (error) => message.error(error)
    );
  handleFormBuilderModalCancel = () => (this.formBuilderModal = false);
  init = async (navigate?: NavigateFunction | undefined) => {
    this.mapOk("skills", this.skillsHttpRepository.getAllSkills());
  };
  deleteSkill = async (id: string) => (await this.skillsHttpRepository.deleteSkill(id), this.init());
  modalShow = () => {
    this.isModalOpen = true;
  };

  handleOk = () => {
    this.isModalOpen = false;
  };

  handleCancel = () => {
    this.isModalOpen = false;
  };
  addNewParam = (index: number) => {
    this.activeIndex = index;
    this.modalShow();
  };
  onChangeBtDependency = (dependency: DependencyViewModel) =>
    this.viewModel.BTAction.at(this.activeIndex ?? 0)
      ?.validParam(this.selectParam?.form ?? "")
      .fold(
        () => {
          this.updateForm({
            BTAction: this.viewModel.BTAction.replacePropIndex(
              {
                param: this.viewModel.BTAction.at(this.activeIndex ?? 0)?.param.add(
                  new ParamViewModel(this.selectParam?.form ?? "", dependency)
                ),
              },
              this.activeIndex ?? 0
            ),
          });
          this.handleCancel();
        },
        () => {
          message.error(`${this.selectParam?.form} is filled`);
          this.handleCancel();
        }
      );

  clickParam(el: { form: FormType; component: null } | { form: FormType; component: React.JSX.Element }): void {
    this.selectParam = el;
    if (el.component === null) this.onChangeBtDependency(DependencyViewModel.empty());
  }
  saveNewSkill = async () => {
    (await this.viewModel.valid<SkillModel>()).fold(
      async (model) => (
        await this.skillsHttpRepository.createNewSkill(model),
        this.handleCancel(),
        await this.init(),
        this.editDrawer(DrawersSkills.newSkill, false)
      ),
      async (e) => message.error(e)
    );
  };
  copySkill = (skillModel: SkillModel) => window.prompt("Copy to clipboard: Ctrl+C, Enter", JSON.stringify(skillModel));
}
