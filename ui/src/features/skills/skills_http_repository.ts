import { SkillModel } from "../../core/model/skill_model";
import { HttpMethod, HttpRepository } from "../../core/repository/core_http_repository";

export class SkillsHttpRepository extends HttpRepository {
  featureApi = "/skills";
  createNewSkill = (model: SkillModel) => this._jsonRequest(HttpMethod.POST, this.featureApi, model);
  deleteSkill = (id: string) => this._jsonRequest(HttpMethod.DELETE, `${this.featureApi}?id=${id}`);
  getAllSkills = () => this._arrayJsonToClassInstanceRequest(HttpMethod.GET, this.featureApi, SkillModel);
}
