import { CoreHttpRepository, HttpMethod } from "../../core/repository/core_http_repository";
import { BehaviorTreeModel } from "../behavior_tree_builder/model/behavior_tree_model";
import { BehaviorTreeViewModel } from "../behavior_tree_builder/model/behavior_tree_view_model";

export class BehaviorTreeManagerHttpRepository extends CoreHttpRepository {
  featureApi = `/behavior/trees`;
  deleteBt = (id: string) => this._jsonRequest(HttpMethod.DELETE, `${this.featureApi}?id=${id}`);
  saveNewBt = async (model: BehaviorTreeViewModel) => this._jsonRequest(HttpMethod.POST, this.featureApi, model);
  getAllBtInstances = async () => this._jsonRequest<BehaviorTreeModel[]>(HttpMethod.GET, this.featureApi);
}
