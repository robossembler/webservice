import makeAutoObservable from "mobx-store-inheritance";
import { UiDrawerFormState, CoreError } from "../../core/store/base_store";
import { BehaviorTreeViewModel } from "../behavior_tree_builder/model/behavior_tree_view_model";
import { NavigateFunction } from "react-router-dom";
import { BehaviorTreeBuilderPath } from "../behavior_tree_builder/presentation/behavior_tree_builder_screen";
import { BehaviorTreeModel } from "../behavior_tree_builder/model/behavior_tree_model";
import { SceneModel } from "../scene_manager/model/scene_model";
import { BehaviorTreeManagerHttpRepository } from "./behavior_tree_manager_repository";
import { message } from "antd";
export enum DrawerState {
  newBehaviorTree = "Новое дерево поведения",
}
export class BehaviorTreeManagerStore extends UiDrawerFormState<BehaviorTreeViewModel, CoreError> {
  viewModel: BehaviorTreeViewModel = BehaviorTreeViewModel.empty();
  navigate?: NavigateFunction;
  btTreeModels: BehaviorTreeModel[] = [];
  scenes?: SceneModel[];
  behaviorTreeManagerHttpRepository = new BehaviorTreeManagerHttpRepository();
  isModalOpen: boolean = false;
  activeProject?: string;
  constructor() {
    super(DrawerState);
    makeAutoObservable(this);
  }
  init = async (navigate?: NavigateFunction | undefined): Promise<void> => {
    this.navigate = navigate;
    (await this.behaviorTreeManagerHttpRepository.getActiveProjectId()).map((el) => {
      this.activeProject = el.id;
      this.viewModel.project = this.activeProject;
    });

    await this.mapOk("btTreeModels", this.behaviorTreeManagerHttpRepository.getAllBtInstances());
    await this.mapOk("scenes", this.behaviorTreeManagerHttpRepository.getAllScenes());
  };
  deleteBt = async (id: string) => (
    await this.behaviorTreeManagerHttpRepository.deleteBt(id),
    await this.mapOk("btTreeModels", this.behaviorTreeManagerHttpRepository.getAllBtInstances())
  );
  saveNewBt = async () =>
    (await this.viewModel.valid<BehaviorTreeViewModel>()).fold(
      async (model) => {
        await this.messageHttp(this.behaviorTreeManagerHttpRepository.saveNewBt(model), {
          successMessage: "Новое дерево создано",
        });
        await this.mapOk("btTreeModels", this.behaviorTreeManagerHttpRepository.getAllBtInstances());
      },
      async (error) => message.error(error)
    );
  goToBt = (id: string) => {
    if (this.navigate) this.navigate(BehaviorTreeBuilderPath(id));
  };
  modalShow = () => {
    this.isModalOpen = true;
  };

  modalCancel = () => {
    this.isModalOpen = false;
  };
}
