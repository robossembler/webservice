import { observer } from "mobx-react-lite";
import { BehaviorTreeManagerStore } from "./behavior_tree_manager_store";
import React from "react";
import { useStore } from "../../core/helper/use_store";
import { ButtonV2, ButtonV2Type } from "../../core/ui/button/button_v2";
import { CoreCard } from "../../core/ui/card/card";
import { Icon } from "../../core/ui/icons/icons";
import { themeStore } from "../..";
import { InputV2 } from "../../core/ui/input/input_v2";
import { CoreModal } from "../../core/ui/modal/modal";
import { SelectV2 } from "../../core/ui/select/select_v2";
import { CoreText, CoreTextType } from "../../core/ui/text/text";
import { MainPageV2 } from "../../core/ui/pages/main_page_v2";

export const BehaviorTreeManagerScreenPath = "/behavior/tree/manager";

export const BehaviorTreeManagerScreen = observer(() => {
  const store = useStore(BehaviorTreeManagerStore);

  return (
    <>
      <MainPageV2
        children={
          <>
            <div style={{ height: "100%", overflowY: "auto", overflowX: "hidden" }}>
              <div style={{ height: 20 }} />
              <ButtonV2
                icon={<Icon type={"Plus"} style={{ alignSelf: "center", marginLeft: 10, marginRight: 10 }} />}
                text="СОЗДАТЬ ДЕРЕВО ПОВЕДЕНИЯ"
                onClick={() => store.modalShow()}
              />
              <div style={{ height: 50 }} />

              <div style={{ display: "inline-grid", gridTemplateColumns: "1fr 1fr 1fr", width: "100%" }}>
                {store.btTreeModels?.map((el, index) => (
                  <CoreCard
                    key={index}
                    clickDeleteIcon={() => store.deleteBt(el._id)}
                    clickGoToIcon={() => store.goToBt(el._id)}
                    descriptionMiddle={el.description}
                    descriptionTop={el.name}
                    date={el.unixTime}
                  />
                ))}
              </div>
            </div>
            <CoreModal
              isOpen={store.isModalOpen}
              onClose={() => store.modalCancel()}
              children={
                <>
                  <div style={{ height: 30 }} />
                  <CoreText
                    text="Создание дерева поведения"
                    type={CoreTextType.header}
                    color={themeStore.theme.white}
                    style={{ textAlignLast: "center" }}
                  />
                  <div style={{ height: 30 }} />
                  <div
                    style={{
                      height: 1,
                      width: "calc(100% - 30px)",
                      backgroundColor: themeStore.theme.outlineVariant,
                    }}
                  />
                  <div style={{ height: 20 }} />
                  <InputV2  trim={true} label={"Название"} onChange={(text) => store.updateForm({ name: text })} />
                  <div style={{ height: 20 }} />
                  <InputV2 trim={true} label={"Описание"} onChange={(text) => store.updateForm({ description: text })} />
                  <div style={{ height: 20 }} />
                  <SelectV2
                    items={store.scenes?.map((el) => ({ name: el.name, value: el._id })) ?? []}
                    initialValue={""}
                    label={"Сцена"}
                    onChange={(value: string) => store.updateForm({ sceneId: value })}
                  />
                  <div style={{ height: 30 }} />
                  <div
                    style={{
                      height: 1,
                      width: "calc(100% - 30px)",
                      backgroundColor: themeStore.theme.outlineVariant,
                    }}
                  />
                  <div style={{ display: "flex", paddingTop: 20, justifyContent: "center" }}>
                    <ButtonV2 text="Сохранить" onClick={() => store.saveNewBt()} type={ButtonV2Type.default} />
                    <div style={{ width: 20 }} />
                    <ButtonV2 text="Отменить" onClick={() => store.modalCancel()} type={ButtonV2Type.default} />
                  </div>
                </>
              }
            />
          </>
        }
        bgColor={themeStore.theme.black}
      />
    </>
  );
});
