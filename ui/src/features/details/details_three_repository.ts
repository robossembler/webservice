import { Box3, GridHelper, Mesh, Object3D, Vector3 } from "three";
import { CoreThreeRepository } from "../../core/repository/core_three_repository";

export class DetailsThreeRepository extends CoreThreeRepository {
  
    loadHttpAndPreview = (path: string, name: string, loadCallback?: Function) => {

        this.loader(
            path,
            () => {
                this.raiseAnObjectAboveZeroVector(name)
                this.getCenterPoint(this.scene.children.filter((el) =>  el instanceof GridHelper).at(0) as Object3D)
            },
            name,
            new Vector3(0, 0, 0)
        );

    }

}