import makeAutoObservable from "mobx-store-inheritance";
import { CoreError, UiDrawerFormState } from "../../core/store/base_store";
import { NavigateFunction } from "react-router-dom";
import { DetailsHttpRepository, Parts } from "./details_http_repository";
import { DetailsThreeRepository } from "./details_three_repository";
import { EnvelopmentViewModel } from "./envelopment_view_model";
import { message } from "antd";

interface IDetailViewModel {
  label: string;
  selected: boolean;
  httpUrl: string;
}
export enum DrawersDetailsStore {
  NewEnvelopment = "Новое окружение",
}

export class DetailsStore extends UiDrawerFormState<EnvelopmentViewModel, CoreError> {
  viewModel: EnvelopmentViewModel = EnvelopmentViewModel.empty();
  detailsViewModel: IDetailViewModel[] = [];
  parts: Parts[] = [];
  detailsHttpRepository: DetailsHttpRepository = new DetailsHttpRepository();
  detailsThreeRepository?: DetailsThreeRepository;
  constructor() {
    super(DrawersDetailsStore);
    makeAutoObservable(this);
    this.init();
  }
  errorHandingStrategy = (error: CoreError) => {};
  init = async (navigate?: NavigateFunction | undefined): Promise<void> => {
    await this.mapOk("parts", this.detailsHttpRepository.getAssetsActiveProject());

  
    
    this.detailsViewModel = this.parts.map((el) => {
      return {
        label: el.name,
        selected: false,
        httpUrl: el.stlUrl,
      };
    });
  };

  selectedDetail = (label: string) => {
    this.detailsViewModel.map((el) => {
      el.selected = false;
      return el;
    });
    this.detailsViewModel.map((el) => {
      if (el.label.match(label)) {
        el.selected = true;
        this.detailsThreeRepository?.deleteAllObjectsScene();
        this.detailsThreeRepository?.loadHttpAndPreview(el.httpUrl, el.label, () => {});
        return el;
      }
      return el;
    });
  };
  loadScene = async (ref: HTMLCanvasElement) => {
    this.detailsThreeRepository = new DetailsThreeRepository(ref, () => {});
    this.detailsThreeRepository.render();
  };
  uploadEnv = () =>
    this.viewModel.isValid().fold(
      async (model) => {
        await this.detailsHttpRepository.uploadNewEnv(model);
        await this.init();
        this.editDrawer(DrawersDetailsStore.NewEnvelopment, false);
      },
      async (e) => message.error(e)
    );
}
