import { Result } from "../../core/helper/result";

export class EnvelopmentViewModel {
  public name: string;
  public blend?: File;
  public ply?: File;
  public glb?: File;
  public dae?: File;
  public stl?: File;
  public fbx?: File;
  public png?: File;
  public mass: number = 0;
  public inertia = {
    ixx: 0.1,
    ixy: 0,
    ixz: 0,
    iyy: 0.1,
    iyz: 0,
    izz: 0.1,
  };

  constructor(name: string) {
    this.name = name;
  }
  static empty = () => new EnvelopmentViewModel("");
  isValid = (): Result<string, EnvelopmentViewModel> => {
    if (this.name.isEmpty()) {
      return Result.error("name is empty");
    }
    return Result.ok(this);
  };
}
