import { HttpMethod, CoreHttpRepository } from "../../core/repository/core_http_repository";
import { EnvelopmentViewModel } from "./envelopment_view_model";
export interface Parts {
  isSelect?: boolean;
  name: string;
  mass?:number;
  part_path: string;
  material_path: string;
  stlUrl: string;
  image: string;
  glUrl: string;
  solidType: string;
  daeUrl: string;
  objUrl: string;
}

export class DetailsHttpRepository extends CoreHttpRepository {
  uploadNewEnv = (model: EnvelopmentViewModel) => {
    const formData = new FormData();
    Object.entries(model).forEach(([k, v]) => {
      formData.append(k, v);
    });
    this._formDataRequest(HttpMethod.POST, "/projects/upload/env", formData);
  };
}
