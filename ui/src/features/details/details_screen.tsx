import { observer } from "mobx-react-lite";
import { DetailsStore, DrawersDetailsStore } from "./details_store";
import { MainPage } from "../../core/ui/pages/main_page";
import React from "react";
import { CoreText, CoreTextType } from "../../core/ui/text/text";
import { CoreButton } from "../../core/ui/button/button";
import { Drawer, Upload } from "antd";
import { CoreInput } from "../../core/ui/input/input";
import { useStore } from "../../core/helper/use_store";

export const DetailsScreenPath = "/details";

export const DetailsScreen = observer(() => {
  const store = useStore(DetailsStore);
  React.useEffect(() => {
    store.loadScene(canvasRef.current!);
  }, []);
  const canvasRef = React.useRef<HTMLCanvasElement>(null);

  return (
    <MainPage
      page={"Детали"}
      panelStyle={{ overflowX: "auto" }}
      bodyChildren={<canvas ref={canvasRef} style={{ overflow: "hidden", width: "100%" }} />}
      panelChildren={
        <>
          <CoreButton
            filled={true}
            onClick={() => store.editDrawer(DrawersDetailsStore.NewEnvelopment, true)}
            text="Добавить окружение"
            style={{ margin: 10 }}
          />
          {store.detailsViewModel.map((el, i) => (
            <div key={i} onClick={() => store.selectedDetail(el.label)} style={{ margin: 5, cursor: "pointer" }}>
              <div style={el.selected ? { width: "100%", background: "#E8DEF8", borderRadius: 100 } : {}}>
                <CoreText
                  style={{ textAlign: "start", padding: 10, paddingLeft: 40 }}
                  type={CoreTextType.large}
                  text={el.label}
                />
              </div>
            </div>
          ))}
          <Drawer
            title={store.titleDrawer}
            destroyOnClose={true}
            onClose={() => store.editDrawer(DrawersDetailsStore.NewEnvelopment, false)}
            open={store.drawers.find((el) => el.name === DrawersDetailsStore.NewEnvelopment)?.status}
          >
            <CoreInput label="имя" onChange={(text) => store.updateForm({ name: text })} />
            <Upload onChange={(file) => store.updateForm({ blend: file.file.originFileObj })}>
              <CoreButton filled={true} text="blend" />
            </Upload>
            <Upload onChange={(file) => store.updateForm({ ply: file.file.originFileObj })}>
              <CoreButton filled={true} text="ply" />
            </Upload>
            <Upload onChange={(file) => store.updateForm({ stl: file.file.originFileObj })}>
              <CoreButton filled={true} text="stl" />
            </Upload>
            <Upload onChange={(file) => store.updateForm({ glb: file.file.originFileObj })}>
              <CoreButton filled={true} text="glb" />
            </Upload>
            <Upload onChange={(file) => store.updateForm({ dae: file.file.originFileObj })}>
              <CoreButton filled={true} text="dae" />
            </Upload>
            <Upload onChange={(file) => store.updateForm({ fbx: file.file.originFileObj })}>
              <CoreButton filled={true} text="fbx" />
            </Upload>
            <Upload onChange={(file) => store.updateForm({ png: file.file.originFileObj })}>
              <CoreButton filled={true} text="png" />
            </Upload>
            <div style={{ height: 10 }} />
            <CoreButton text="Сохранить" onClick={() => store.uploadEnv()} />
          </Drawer>
        </>
      }
    />
  );
});
