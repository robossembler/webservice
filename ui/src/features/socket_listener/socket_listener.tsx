import * as React from "react";
import { socketListenerStore } from "./socket_listener_store";
import { observer } from "mobx-react-lite";
import { CoreText, CoreTextType } from "../../core/ui/text/text";

export const SocketListener: React.FC<{ children?: JSX.Element }> = observer(({ children }) => {
  React.useEffect(() => {
    socketListenerStore.init();
  }, []);
  return (
    <>
      {false ? (
        <div style={{ display: "flex", width: "100%", backgroundColor: "red", justifyContent: "center", height: 35 }}>
          <CoreText
            text="Server error reconnect..."
            type={CoreTextType.header}
            style={{ color: "white", textAlign: "center", marginTop: 1 }}
          />
          <div style={{ width: 20 }} />
          <div className="loading">Loading&#8230;</div>
        </div>
      ) : (
        <></>
      )}

      {children}
    </>
  );
});
