import { makeAutoObservable } from "mobx";
import { SocketRepository, socketRepository } from "../../core/repository/core_socket_repository";

class SocketListenerStore {
  socketRepository: SocketRepository;
  socketHasDisconnect = false;

  constructor(repository: SocketRepository) {
    this.socketRepository = repository;
    makeAutoObservable(this);
    this.init();
  }
  async init() {
   
    (await this.socketRepository.connect()).fold(
      () => {
        this.socketHasDisconnect = false;
      },
      () => {
        this.socketHasDisconnect = true;
      }
    );
  }
  async reconnect() {
    await this.socketRepository.connect();

    this.socketHasDisconnect = false;
  }
}

export const socketListenerStore = new SocketListenerStore(socketRepository);
