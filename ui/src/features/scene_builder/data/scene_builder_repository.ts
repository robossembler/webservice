import { SceneAsset } from "../../../core/model/scene_asset";
import { CoreHttpRepository, HttpMethod } from "../../../core/repository/core_http_repository";

export class SceneBuilderHttpRepository extends CoreHttpRepository {
    editScene = (scene: SceneAsset) => this._jsonRequest(HttpMethod.PUT, "/scenes", scene);
}
