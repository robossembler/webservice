import React from "react";
import { observer } from "mobx-react-lite";
import { SceneBuilderStore } from "./scene_builder_store";
import { useStore } from "../../../core/helper/use_store";
import { useParams } from "react-router-dom";
import { Popover } from "antd";
import { CoreButton } from "../../../core/ui/button/button";
import { Icon } from "../../../core/ui/icons/icons";
import { MainPage } from "../../../core/ui/pages/main_page";
import { CoreText, CoreTextType } from "../../../core/ui/text/text";
import { SceneMode } from "../../scene_manager/model/scene_view";
import { sceneManagerForms } from "./forms/scene_manager_forms";

export const SceneBuilderScreenPath = "/scene/builder/";

export const SceneBuilderScreen = observer(() => {
  const store = useStore(SceneBuilderStore);
  const canvasRef = React.useRef<HTMLCanvasElement>(null);
  const id = useParams().id as string;
  store.initParam(id);

  React.useEffect(() => {
    store.loadScene(canvasRef.current!);
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "scroll";
      store.dispose();
    };
  }, [id, store]);
  return (
    <MainPage
      page={"Сцена"}
      panelChildren={
        <>
          <div style={{ width: "100%", height: "100%" }}>
            <div style={{ height: 260, width: "100%", padding: 10 }}>
              <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
                <CoreText text="Сцена" type={CoreTextType.large} />
                <Popover
                  placement="bottom"
                  color="rgba(104, 80, 164, 1)"
                  content={
                    <div>
                      {store.popoverItems.map((el, i) => (
                        <div key={i} onClick={() => el.fn()}>
                          <CoreText text={el.name} type={CoreTextType.medium} color="white" />
                        </div>
                      ))}
                    </div>
                  }
                >
                  <span>
                    <CoreButton text="Добавить" filled={true} />
                  </span>
                </Popover>
                <div style={{ position: "relative" }}>
                  <div style={{ position: "absolute" }}>
                    <div style={{ position: "relative", left: 20 }}>
                      {store.sceneHelperInstruments.map((el, i) => (
                        <div
                          key={i}
                          onClick={() => el.onClick()}
                          style={{
                            marginTop: 4,
                            width: 50,
                            height: 50,
                            backgroundColor: el.isSelected ? "rgba(160, 132, 255, 1)" : "rgba(99, 81, 159, 1)",
                            border: "1px solid",
                            borderRadius: 5,
                          }}
                        >
                          <Icon type={el.icon} />
                        </div>
                      ))}
                      {store.isVisibleSaveButton ? (
                        <>
                          <div
                            onClick={() => store.sceneSave()}
                            style={{
                              marginTop: 4,
                              width: 50,
                              height: 50,
                              backgroundColor: "rgba(99, 81, 159, 1)",
                              border: "1px solid",
                              borderRadius: 5,
                            }}
                          >
                            <Icon type={SceneMode.Save} />
                          </div>
                        </>
                      ) : null}
                    </div>
                  </div>
                </div>
              </div>
              <div style={{ height: 10 }} />
              <div
                style={{
                  borderRadius: 7,
                  height: 200,
                  width: "-webkit-fill-available",
                  backgroundColor: "white",
                  overflowX: "auto",
                }}
              >
                {store.sceneItems.map((el, index) => (
                  <div
                    key={index}
                    style={{
                      display: "flex",
                      backgroundColor: el.isSelected
                        ? "rgba(104, 80, 164, 0.47)"
                        : index.isEven()
                        ? "rgba(217, 217, 217, 0.27)"
                        : "",
                      alignItems: "center",
                      flexDirection: "row",
                      justifyContent: "space-between",
                      width: "100%",
                    }}
                    onClick={() => store.selectSceneItems(el.name, index, !el.isSelected, el.icon)}
                  >
                    <Icon width={13} height={13} type={el.icon} style={{ marginLeft: 10 }} />
                    <div style={{ width: 10 }} />
                    <CoreText text={el.name} type={CoreTextType.small} />
                    <Icon
                      type="DeleteCircle"
                      width={13}
                      isNeedStopPropagation={true}
                      height={13}
                      onClick={() => store.deleteSceneItem(el)}
                      style={{ marginRight: 10 }}
                    />
                  </div>
                ))}
              </div>
            </div>
            <div style={{ height: 20 }} />
            <div
              style={{
                width: "-webkit-fill-available",
                height: "60%",
                borderRadius: 7,
                backgroundColor: "white",
                margin: 10,
              }}
            >
              {sceneManagerForms(store.activeFormDependency ?? {}, store).map((el, i) =>
                el.name.isEqualR(store.activeFormType ?? "").fold(
                  () => <span key={i}>{el.component}</span>,
                  () => <span key={i}></span>
                )
              )}
            </div>
          </div>
        </>
      }
      bodyChildren={
        <>
          <>
            <canvas ref={canvasRef} style={{ overflow: "hidden" }} />
          </>
        </>
      }
    />
  );
});
