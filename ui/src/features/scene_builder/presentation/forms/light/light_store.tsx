import { NavigateFunction } from "react-router-dom";
import { FormState, CoreError } from "../../../../../core/store/base_store";
import { LightModel } from "../../../../../core/model/light_model";

export class LightStore extends FormState<LightModel, CoreError> {
  viewModel: LightModel = LightModel.empty();
  constructor() {
    super();
  }
  errorHandingStrategy = (error: CoreError) => {};
  init(navigate?: NavigateFunction | undefined): Promise<any> {
    throw new Error("Method not implemented.");
  }
}
