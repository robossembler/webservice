import makeAutoObservable from "mobx-store-inheritance";
import { FormState, CoreError } from "../../../../../core/store/base_store";
import { CoreHttpRepository } from "../../../../../core/repository/core_http_repository";
import { Parts } from "../../../../details/details_http_repository";
import { Vector2 } from "three";
import { SpawnPositionTypes } from "../../../../../core/model/spawn_position_types";
import { isPreviewMode } from "../scene_manager_forms";
import { SolidModel } from "../../../../../core/model/solid_model";
import { SceneBuilderStore } from "../../scene_builder_store";

export enum SolidBodyStoreType {
  selectBody = "selectBody",
  selectSpawnPositionType = "selectSpawnPositionType",
  spawn2DVector = "spawn2DVector",
  previewSolid = "previewSolid",
}

export class SolidBodyStore extends FormState<SolidModel, CoreError> {
  viewModel: SolidModel = SolidModel.empty();
  parts: Parts[] = [];
  spawnPositionTypes = Object.keys(SpawnPositionTypes);
  coreHttpRepository: CoreHttpRepository = new CoreHttpRepository();
  solidBodyStoreType: SolidBodyStoreType = SolidBodyStoreType.selectBody;
  selectBody: Parts;
  spawnType: string;
  sceneBuilderStore: SceneBuilderStore;
  vector2d?: Vector2;

  constructor(sceneBuilderStore: SceneBuilderStore) {
    super();
    this.sceneBuilderStore = sceneBuilderStore;
    makeAutoObservable(this);
  }
  init = async () => {
    isPreviewMode(this.sceneBuilderStore.activeFormDependency).map(() =>
      this.sceneBuilderStore.scene
        .rFind<SolidModel>((el) => el.name.isEqual(this.sceneBuilderStore.selectedItemName ?? ""))
        .fold(
          (solidBodyModel) => {
            this.loadDependency(solidBodyModel);
            this.solidBodyStoreType = SolidBodyStoreType.previewSolid;
          },
          () =>
            console.log(
              `Unknown FormType ${this.sceneBuilderStore.selectedItemName} : ${JSON.stringify(
                this.sceneBuilderStore.scene
              )}`
            )
        )
    );
    this.mapOk("parts", this.coreHttpRepository.getAssetsActiveProject());
  };
  errorHandingStrategy = (error: CoreError) => {};

  dispose = () => {
    window.removeEventListener("click", this.clickLister);
  };
  selectSpawnType = () => {
    this.solidBodyStoreType = SolidBodyStoreType.spawn2DVector;

    setTimeout(() => window.addEventListener("click", this.clickLister), 1000);
  };
  clickLister = (event: MouseEvent) =>
    this.solidBodyStoreType.isEqualR(SolidBodyStoreType.spawn2DVector).map(() =>
      this.sceneBuilderStore!.clickScene(event, this.sceneBuilderStore!.canvasOffsetX).map((vector3) => {
        this.viewModel.position = vector3;
        this.viewModel.spawnType = "BoundBox";
        this.viewModel.toWebGl(this.sceneBuilderStore.coreThreeRepository!);
        this.sceneBuilderStore.sceneItems.push(this.viewModel.toSceneItems());
        this.sceneBuilderStore.scene.push(this.viewModel);
        window.removeEventListener("click", this.clickLister);
        this.sceneBuilderStore.activeFormType = undefined;
        this.sceneBuilderStore.visibleSaveButton()
      })
    );
  updateBodySimulation = () =>
    this.solidBodyStoreType
      .isEqualR(SolidBodyStoreType.previewSolid)
      .map(() => this.viewModel.update(this.sceneBuilderStore.coreThreeRepository!));
  clickSelectBody = (el: Parts) => {
    this.selectBody = el;
    this.solidBodyStoreType = SolidBodyStoreType.selectSpawnPositionType;
    this.viewModel = this.viewModel.fromParts(el);
  };
}
