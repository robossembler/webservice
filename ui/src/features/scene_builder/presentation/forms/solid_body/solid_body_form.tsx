import React from "react";
import { CoreText, CoreTextType } from "../../../../../core/ui/text/text";
import { IDefaultSceneManagerFormProps } from "../scene_manager_forms";
import { SolidBodyStore, SolidBodyStoreType } from "./solid_body_store";
import { observer } from "mobx-react-lite";
import { CoreButton } from "../../../../../core/ui/button/button";
import { match } from "ts-pattern";
import { SpawnPositionTypesForm } from "../../components/spawn_position_types";
import { CoreInput } from "../../../../../core/ui/input/input";
import { CoordsForm } from "../../components/coords_form";
import { useStoreClass } from "../../../../../core/helper/use_store";
import { Toggle } from "../../../../../core/ui/toggle/toggle";

export const SolidBodyForm = observer((props: IDefaultSceneManagerFormProps) => {
  const store = useStoreClass(new SolidBodyStore(props.store));

  return (
    <div style={{ display: "flex", flexDirection: "column", alignItems: "center", overflow: "auto", height: "100%",width:'100%' }}>
      <>
        {match(store.solidBodyStoreType)
          .with(SolidBodyStoreType.previewSolid, () => (
            <>
              <Toggle
                name={"Настройки твердого тела"}
                child={
                  <>
                    <CoreText text={"Тип твердого тела: " + store.viewModel.solidType} type={CoreTextType.header} />
                  </>
                }
                isOpen={false}
              />
              <div style={{height:10}}/>
              <CoordsForm store={store} update={store.updateBodySimulation} />
            </>
          ))
          .with(SolidBodyStoreType.selectBody, () => (
            <>
              <CoreText text={"Твердое тело"} type={CoreTextType.header} />

              {store.parts.map((el, i) => (
                <div key={i} style={{ padding: 10, margin: 10 }}>
                  <CoreText text={el.name} type={CoreTextType.medium} />
                  <img src={el.image} />
                  <CoreButton text="Выбрать" onClick={() => store.clickSelectBody(el)} />
                </div>
              ))}
            </>
          ))
          .with(SolidBodyStoreType.selectSpawnPositionType, () => (
            <>
              <SpawnPositionTypesForm onClick={store.selectSpawnType} />
            </>
          ))
          .with(SolidBodyStoreType.spawn2DVector, () => (
            <>
              <CoreText text={"Выберите точку размещения"} type={CoreTextType.header} />
            </>
          ))
          .otherwise(() => (
            <></>
          ))}
      </>
    </div>
  );
});
