import { Result } from "../../../../core/helper/result";
import { SceneBuilderStore } from "../scene_builder_store";
import { SceneMangerStore } from "../../../scene_manager/presentation/scene_manager_store";
import { CameraForm } from "./camera/camera_form";
import { LightForm } from "./light/light_form";
import { PointForm } from "./point/point_form";
import { RobotForm } from "./robot_form/robot_form";
import { SolidBodyForm } from "./solid_body/solid_body_form";
import { Trajectory } from "./trajectory/trajectory_form";
import { ZoneForm } from "./zone/zone_form";

export enum SceneManagerForms {
  robot = "robot",
  camera = "camera",
  solidBody = "SolidBody",
  previewSolidBody = "previewSolidBody",
  light = "Light",
  point = "point",
  trajectory = "trajectory",
  zone = "zone",
  capturePoints = "capturePoints",
}
interface IForms {
  name: string;
  component: JSX.Element;
}
export interface IDefaultSceneManagerFormProps {
  dependency: Object;
  store: SceneBuilderStore;
}
export const isPreviewMode = (dependency: Object): Result<void, void> => {
  if (Object.hasOwn(dependency, "type")) return Result.ok();
  return Result.error(undefined);
};
export const sceneManagerForms = (props: Object, store: SceneBuilderStore): IForms[] => [
  { name: SceneManagerForms.camera, component: <CameraForm dependency={props} store={store} /> },
  { name: SceneManagerForms.robot, component: <RobotForm dependency={props} store={store} /> },
  { name: SceneManagerForms.solidBody, component: <SolidBodyForm dependency={props} store={store} /> },
  { name: SceneManagerForms.point, component: <PointForm dependency={props} store={store} /> },
  { name: SceneManagerForms.light, component: <LightForm dependency={props} store={store} /> },
  { name: SceneManagerForms.zone, component: <ZoneForm dependency={props} store={store} /> },
  { name: SceneManagerForms.trajectory, component: <Trajectory dependency={props} store={store} /> },
  { name: SceneManagerForms.capturePoints, component: <CameraForm dependency={props} store={store} /> },
];
