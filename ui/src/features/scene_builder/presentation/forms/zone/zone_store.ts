import makeAutoObservable from "mobx-store-inheritance";
import { NavigateFunction } from "react-router-dom";
import { ZoneModel } from "../../../../../core/model/zone_model";

import { FormState, CoreError } from "../../../../../core/store/base_store";
import { isPreviewMode } from "../scene_manager_forms";
import { SceneMangerStore } from "../../../../scene_manager/presentation/scene_manager_store";
import { SceneBuilderStore } from "../../scene_builder_store";
export enum ZoneStoreType {
  preview = "preview",
}
export class ZoneStore extends FormState<ZoneModel, CoreError> {
  constructor(sceneBuilderStore: SceneBuilderStore) {
    super();
    this.sceneBuilderStore = sceneBuilderStore;
    makeAutoObservable(this);
  }
  storeType: ZoneStoreType;
  sceneBuilderStore: SceneBuilderStore;
  viewModel: ZoneModel = ZoneModel.empty();
  errorHandingStrategy = (_error: CoreError) => {};
  init = async (_navigate?: NavigateFunction | undefined) => {
    isPreviewMode(this.sceneBuilderStore.activeFormDependency).map(() =>
      this.sceneBuilderStore.scene
        .rFind<ZoneModel>((el) => el.name.isEqual(this.sceneBuilderStore.selectedItemName ?? ""))
        .fold(
          (zoneModel) => {
            this.loadDependency(zoneModel);
            this.storeType = ZoneStoreType.preview;
          },
          () =>
            console.log(
              `Unknown FormType ${this.sceneBuilderStore.selectedItemName} : ${JSON.stringify(
                this.sceneBuilderStore.scene
              )}`
            )
        )
    );
  };
  updateWebGl = () =>
    this.storeType
      .isEqualR(ZoneStoreType.preview)
      .map(() => this.sceneBuilderStore.coreThreeRepository!.updateInstance(this.viewModel));
}
