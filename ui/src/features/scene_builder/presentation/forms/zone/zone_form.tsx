import React from "react";
import { observer } from "mobx-react-lite";
import { ZoneStore } from "./zone_store";
import { IDefaultSceneManagerFormProps } from "../scene_manager_forms";
import { CoreText, CoreTextType } from "../../../../../core/ui/text/text";
import { CoreInput } from "../../../../../core/ui/input/input";

export const ZoneForm = observer((props: IDefaultSceneManagerFormProps) => {
  const [store] = React.useState(() => new ZoneStore(props.store));
  React.useEffect(() => {
    store.init();
  }, [store]);

  return (
    <>
      <CoreText text={"Зона"} type={CoreTextType.header} />
      <CoreText text={store.viewModel.name} type={CoreTextType.medium} />
      <CoreInput
        label="Длина"
        value={String(store.viewModel.length)}
        onChange={(text) => (store.updateForm({ length: Number(text) }), store.updateWebGl())}
      />
      <CoreInput
        label="Высота"
        value={String(store.viewModel.height)}
        onChange={(text) => (store.updateForm({ height: Number(text) }), store.updateWebGl())}
      />
      <CoreInput
        label="Ширина"
        value={String(store.viewModel.width)}
        onChange={(text) => (store.updateForm({ width: Number(text) }), store.updateWebGl())}
      />
    </>
  );
});
