import React from "react";
import { CoreButton } from "../../../../../core/ui/button/button";
import { IDefaultSceneManagerFormProps, isPreviewMode } from "../scene_manager_forms";
import { CoreInput } from "../../../../../core/ui/input/input";
import { CoreText, CoreTextType } from "../../../../../core/ui/text/text";
import { CoreSelect } from "../../../../../core/ui/select/select";
import { CameraFormStore } from "./camera_store";
import { observer } from "mobx-react-lite";
import { CameraTypes } from "../../../../../core/model/camera_model";
import { useStoreClass } from "../../../../../core/helper/use_store";
import { CoordsForm } from "../../components/coords_form";
import { Toggle } from "../../../../../core/ui/toggle/toggle";

export const CameraForm = observer((props: IDefaultSceneManagerFormProps) => {
  const store = useStoreClass(new CameraFormStore(props.store));
   return (
    <div style={{  overflowY: "auto", height: "100%" }}>
 
      <Toggle
        name={"Настройки камеры"}
        isOpen={false}
        child={
          <>
            <CoreInput
              value={store.viewModel.topic}
              label={"Топик"}
              onChange={(text) => store.updateForm({ topic: text })}
            />
            <CoreInput
              value={store.viewModel.name}
              label={"Имя"}
              onChange={(text) => store.updateForm({ name: text })}
            />
            <CoreInput
              value={String(store.viewModel.updateRate)}
              validation={(text) => Number().isValid(text)}
              label={"Update Rate"}
              onChange={(text) => store.updateForm({ updateRate: Number(text) })}
            />
            <CoreInput
              value={String(store.viewModel.height)}
              validation={(text) => Number().isValid(text)}
              label={"Height"}
              onChange={(text) => store.updateForm({ height: Number(text) })}
            />
            <CoreInput
              value={String(store.viewModel.width)}
              validation={(text) => Number().isValid(text)}
              label={"Width"}
              onChange={(text) => store.updateForm({ width: Number(text) })}
            />
            <CoreInput
              value={String(store.viewModel.fov)}
              label={"Fov"}
              validation={(text) => Number().isValid(text)}
              onChange={(text) => (store.updateForm({ fov: Number(text) }), store.updateCameraScene())}
            />
            <CoreInput
              value={String(store.viewModel.far)}
              label={"Far"}
              validation={(text) => Number().isValid(text)}
              onChange={(text) => (store.updateForm({ far: Number(text) }), store.updateCameraScene())}
            />
            <CoreInput
              value={String(store.viewModel.near)}
              label={"Near"}
              validation={(text) => Number().isValid(text)}
              onChange={(text) => (store.updateForm({ near: Number(text) }), store.updateCameraScene())}
            />
            <CoreSelect
              items={Object.entries(CameraTypes).map(([_, v]) => v)}
              value={store.viewModel.cameraType}
              label={"Тип камеры"}
              onChange={(text) => store.updateForm({ cameraType: text as CameraTypes })}
            />
          </>
        }
      />
      <div style={{ height: 10 }} />
      <CoordsForm store={store} update={() => store.updateCameraScene()} />
      <div style={{ height: 10 }} />

      {store.type !== "preview" ? (
        <CoreButton text="Создать" style={{ width: 96 }} onClick={() => store.clickNewCamera()} />
      ) : null}
      <div style={{ height: 50 }} />
    </div>
  );
});
