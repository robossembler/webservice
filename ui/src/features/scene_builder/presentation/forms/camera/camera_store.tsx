import makeAutoObservable from "mobx-store-inheritance";
import { NavigateFunction } from "react-router-dom";
import { FormState, CoreError } from "../../../../../core/store/base_store";
import { isPreviewMode } from "../scene_manager_forms";
import { CameraModel } from "../../../../../core/model/camera_model";
import { message } from "antd";
import { SceneBuilderStore } from "../../scene_builder_store";

export class CameraFormStore extends FormState<CameraModel, CoreError> {
  viewModel: CameraModel = CameraModel.empty();
  sceneBuilderStore: SceneBuilderStore;
  type?: string;
  constructor(sceneBuilderStore: SceneBuilderStore) {
    super();
    makeAutoObservable(this);
    this.sceneBuilderStore = sceneBuilderStore;
  }

  errorHandingStrategy = (error: CoreError) => {};
  updateCameraScene = () =>
    this.type?.isNotEmptyR().map(() => this.viewModel.toWebGl(this.sceneBuilderStore.coreThreeRepository!));
  clickNewCamera = () =>
    this.viewModel.validate().fold(
      (model) => {
        this.sceneBuilderStore.addNewCamera(model);
        this.sceneBuilderStore.activeFormType = undefined;
        this.sceneBuilderStore.visibleSaveButton()

      },
      (error) => message.error(error)
    );

  init = async (navigate?: NavigateFunction | undefined) => {
    isPreviewMode(this.sceneBuilderStore.activeFormDependency).map(() =>
      this.sceneBuilderStore.scene
        .rFind<CameraModel>((el) => el.name.isEqual(this.sceneBuilderStore.selectedItemName ?? ""))
        .fold(
          (cameraModel) => {
            console.log(200)
            this.loadDependency(cameraModel);
            this.type = "preview";
          },
          () => console.log(`Unknown FormType`)
        )
    );
  };
}
