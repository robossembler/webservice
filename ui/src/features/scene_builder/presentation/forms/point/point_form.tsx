import React from "react";
import { observer } from "mobx-react-lite";
import { PointStore, PointStoreType } from "./point_store";
import { IDefaultSceneManagerFormProps } from "../scene_manager_forms";
import { CoreText, CoreTextType } from "../../../../../core/ui/text/text";
import { CoreInput } from "../../../../../core/ui/input/input";
import { CoreButton } from "../../../../../core/ui/button/button";
import { match } from "ts-pattern";
import { SpawnPositionTypesForm } from "../../components/spawn_position_types";
import { CoordsForm } from "../../components/coords_form";

export const PointForm = observer((props: IDefaultSceneManagerFormProps) => {
  const [store] = React.useState(() => new PointStore(props.store));
  React.useEffect(() => {
    store.init();
    return () => {
      store.dispose();
    };
  }, [store]);

  return (
    <div>
      {match(store.storeType)
        .with(PointStoreType.previewPoint, () => (
          <>
            <CoreText text="Точка" type={CoreTextType.header} />
            <CoreText text={store.viewModel.name} type={CoreTextType.medium} />
            <CoordsForm store={store} update={store.updateWebGl} />
          </>
        ))
        .with(PointStoreType.initNewPoint, () => (
          <>
            <CoreText text="Точка" type={CoreTextType.header} />
            <CoreInput label="Имя" onChange={(text) => store.updateForm({ name: text })} />
            <CoreButton
              text="Следующий этап"
              onClick={() => store.onClickNext(PointStoreType.makeSceneSolidAndEditPosition)}
            />
          </>
        ))
        .with(PointStoreType.makeSceneSolidAndEditPosition, () => (
          <>
            <SpawnPositionTypesForm onClick={store.selectSpawnType} />
          </>
        ))
        .with(PointStoreType.awaitClick, () => (
          <>
            <CoreText type={CoreTextType.medium} text="Ожидание клика мыши" />
          </>
        ))
        .otherwise(() => (
          <></>
        ))}
    </div>
  );
});
