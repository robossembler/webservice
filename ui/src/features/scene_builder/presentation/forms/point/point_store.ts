import makeAutoObservable from "mobx-store-inheritance";
import { NavigateFunction } from "react-router-dom";
import { PointModel } from "../../../../../core/model/point_model";
import { FormState, CoreError } from "../../../../../core/store/base_store";
import { SpawnPositionTypes } from "../../../../../core/model/spawn_position_types";
import { isPreviewMode } from "../scene_manager_forms";
import { SceneBuilderStore } from "../../scene_builder_store";

export enum PointStoreType {
  makeSceneSolidAndEditPosition = "makeSceneSolidAndEditPosition",
  initNewPoint = "initNewPoint",
  previewPoint = "previewPoint",
  awaitClick = "awaitClick",
}

export class PointStore extends FormState<PointModel, CoreError> {
  viewModel: PointModel = PointModel.empty();
  storeType: PointStoreType = PointStoreType.initNewPoint;
  spawnPositionTypes: SpawnPositionTypes;
  sceneBuilderStore: SceneBuilderStore;
  listener: Function;

  constructor(sceneBuilderStore: SceneBuilderStore) {
    super();
    makeAutoObservable(this);
    this.sceneBuilderStore = sceneBuilderStore;
  }

  clickLister = (event: MouseEvent) =>
    this.storeType.isEqualR(PointStoreType.awaitClick).map(() =>
      this.sceneBuilderStore!.clickScene(event, this.sceneBuilderStore!.canvasOffsetX).map((vector3) => {
        this.viewModel.position = vector3;
        this.viewModel.orientation = [0, 0, 0, 1];
        this.viewModel.toWebGl(this.sceneBuilderStore.coreThreeRepository!);
        this.sceneBuilderStore.activeFormType = undefined;
        this.sceneBuilderStore.sceneItems.push(this.viewModel.toSceneItems(this.sceneBuilderStore));
        this.sceneBuilderStore.scene.push(this.viewModel);
        window.removeEventListener("click", this.clickLister);
        this.sceneBuilderStore.visibleSaveButton();
      })
    );

  selectSpawnType = (type: SpawnPositionTypes) => {
    this.spawnPositionTypes = type;
    this.storeType = PointStoreType.awaitClick;
    setTimeout(() => window.addEventListener("click", this.clickLister), 1000);
  };
  dispose = () => window.removeEventListener("click", this.clickLister);
  updateWebGl = () =>
    this.storeType
      .isEqualR(PointStoreType.previewPoint)
      .map(() => this.sceneBuilderStore.coreThreeRepository!.updateInstance(this.viewModel));
  onClickNext = (pointStoreType: PointStoreType) => (this.storeType = pointStoreType);

  errorHandingStrategy = (error: CoreError) => {};
  init = async (navigate?: NavigateFunction | undefined) =>
    isPreviewMode(this.sceneBuilderStore.activeFormDependency).map(() =>
      this.sceneBuilderStore.scene
        .rFind<PointModel>((el) => el.name.isEqual(this.sceneBuilderStore.selectedItemName ?? ""))
        .fold(
          (solidBodyModel) => {
            this.loadDependency(solidBodyModel);
            this.storeType = PointStoreType.previewPoint;
          },
          () =>
            console.log(
              `Unknown FormType ${this.sceneBuilderStore.selectedItemName} : ${JSON.stringify(
                this.sceneBuilderStore.scene
              )}`
            )
        )
    );
}
