import React from "react";
import { observer } from "mobx-react-lite";
import { RobotFormStore, RobotStoreType } from "./robot_form_store";
import { IDefaultSceneManagerFormProps } from "../scene_manager_forms";
import { CoreText, CoreTextType } from "../../../../../core/ui/text/text";
import { CoreInputNumber } from "../../../../../core/ui/inputNumber/input_number";
import { CoreInput, CoreInputType } from "../../../../../core/ui/input/input";
import { match } from "ts-pattern";
import { CoreSelect } from "../../../../../core/ui/select/select";
import { RobotModel, ToolTypes } from "../../../../../core/model/robot_model";
import { CoreButton } from "../../../../../core/ui/button/button";
import { SpawnPositionTypesForm } from "../../components/spawn_position_types";
import { CoordsForm } from "../../components/coords_form";
import { useStoreClass } from "../../../../../core/helper/use_store";
import { Toggle } from "../../../../../core/ui/toggle/toggle";

export const RobotForm = observer((props: IDefaultSceneManagerFormProps) => {
  const store = useStoreClass(new RobotFormStore(props.store));

  return (
    <div style={{  overflowY: "auto", height: "100%" }}>
      {match(store.storeType)
        .with(RobotStoreType.awaitMouseClick, () => (
          <>
            <CoreText type={CoreTextType.medium} text="Ожидание клика мыши" />
          </>
        ))
        .with(RobotStoreType.selectSpawnPosition, () => (
          <>
            <SpawnPositionTypesForm onClick={store.selectSpawnType} />
          </>
        ))
        .with(RobotStoreType.initNewRobot, () => (
          <>
            <CoreText text="Новый робот" type={CoreTextType.header} />
            <CoreInput
              type={CoreInputType.small}
              value={store.viewModel.nDof.toString()}
              label="Имя"
              onChange={(text) => store.updateForm({ name: text })}
            />
            <CoreInput
              type={CoreInputType.small}
              value={store.viewModel.nDof.toString()}
              label="Ndof"
              onChange={(text) => store.updateForm({ nDof: Number(text) })}
            />
            <CoreSelect
              items={Object.keys(ToolTypes)}
              value={store.viewModel.toolType}
              label={"ToolType"}
              onChange={(text) => store.updateForm({ toolType: text })}
            />
            <CoreButton onClick={() => store.createNewRobot()} text="Создать" />
          </>
        ))
        .with(RobotStoreType.previewRobot, () => (
          <>
            <Toggle
              name={"Управление соединениями"}
              child={
                <>
                  {store.viewModel.jointPosition.map((el, i) => (
                    <div>
                      <CoreInputNumber
                        key={i}
                        step={0.1}
                        max={Number(el.limit.upper)}
                        min={Number(el.limit.lower)}
                        value={el.angle}
                        onChange={(value) => (
                          store.updateForm({
                            jointPosition: store.viewModel.jointPosition.map((element, index) =>
                              index.isEqualR(i).fold(
                                () => {
                                  element.angle = value;
                                  return element;
                                },
                                () => element
                              )
                            ),
                          }),
                          store.updateScene()
                        )}
                        label={el.name}
                      />
                    </div>
                  ))}
                </>
              }
              isOpen={false}
            />

            <div style={{ height: 10 }} />
            <CoordsForm store={store} update={() => store.updateScene()} />
            <div style={{ height: 40 }} />
          </>
        ))
        .otherwise(() => (
          <></>
        ))}
    </div>
  );
});
