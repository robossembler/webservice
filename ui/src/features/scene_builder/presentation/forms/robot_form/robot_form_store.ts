import makeAutoObservable from "mobx-store-inheritance";
import { NavigateFunction } from "react-router-dom";
import { RobotFormHttpRepository } from "./robot_form_http_repository";
import { FormState, CoreError } from "../../../../../core/store/base_store";
import { RobotModel } from "../../../../../core/model/robot_model";
import { isPreviewMode } from "../scene_manager_forms";
import { message } from "antd";
import { SceneBuilderStore } from "../../scene_builder_store";

export enum RobotStoreType {
  previewRobot = "previewRobot",
  selectSpawnPosition = "selectSpawnPosition",
  initNewRobot = "createNewRobot",
  awaitMouseClick = "awaitMouseClick",
}
export class RobotFormStore extends FormState<RobotModel, CoreError> {
  sceneBuilderStore: SceneBuilderStore;
  storeType: RobotStoreType = RobotStoreType.initNewRobot;
  viewModel: RobotModel = RobotModel.empty();
  robotFormHttpRepository: RobotFormHttpRepository = new RobotFormHttpRepository();
  spawnType: string;
  listener: Function;
  clickLister = (event: MouseEvent) => {
    this.storeType.isEqualR(RobotStoreType.awaitMouseClick).map(() =>
      this.sceneBuilderStore!.clickScene(event, this.sceneBuilderStore!.canvasOffsetX).map((vector3) => {
        this.viewModel.position = vector3;
        this.viewModel.toWebGl(this.sceneBuilderStore.coreThreeRepository!);
        this.sceneBuilderStore.activeFormType = undefined;
        this.sceneBuilderStore.sceneItems.push(this.viewModel.toSceneItems(this.sceneBuilderStore));
        this.sceneBuilderStore.scene.push(this.viewModel);
        window.removeEventListener("click", this.clickLister);
        this.sceneBuilderStore.visibleSaveButton();
      })
    );
  };

  selectSpawnType = (type: string) => {
    this.spawnType = type;
    setTimeout(() => window.addEventListener("click", this.clickLister), 1000);
    this.storeType = RobotStoreType.awaitMouseClick;
  };
  dispose = (): void => {
    window.removeEventListener("click", this.clickLister);
  };
  constructor(sceneBuilderStore: SceneBuilderStore) {
    super();
    makeAutoObservable(this);
    this.sceneBuilderStore = sceneBuilderStore;
  }
  updateScene = () =>
    this.storeType
      .isEqualR(RobotStoreType.previewRobot)
      .map(
        () => (
          this.viewModel.update(this.sceneBuilderStore.coreThreeRepository!), this.sceneBuilderStore.visibleSaveButton()
        )
      );
  createNewRobot = () =>
    this.viewModel.isValid().fold(
      async (s) =>
        (await this.robotFormHttpRepository.createNewRobot(s)).fold(
          (s) => {
            this.viewModel.httpUrl = s.robotUrl;
            this.storeType = RobotStoreType.selectSpawnPosition;
          },
          (e) => {
            message.error(e.error);
          }
        ),
      async (e) => message.error(e)
    );
  errorHandingStrategy = (error: CoreError) => {};
  init = async (navigate?: NavigateFunction | undefined) =>
    isPreviewMode(this.sceneBuilderStore.activeFormDependency).map(() =>
      this.sceneBuilderStore.scene
        .rFind<RobotModel>((el) => el.name.isEqual(this.sceneBuilderStore.selectedItemName ?? ""))
        .fold(
          (robotModel) => {
            this.loadDependency(robotModel);
            this.storeType = RobotStoreType.previewRobot;
          },
          () =>
            console.log(
              `Unknown FormType ${this.sceneBuilderStore.selectedItemName} : ${JSON.stringify(
                this.sceneBuilderStore.scene
              )}`
            )
        )
    );
}
