import { RobotModel } from "../../../../../core/model/robot_model";
import { HttpMethod, HttpRepository } from "../../../../../core/repository/core_http_repository";
export interface RobotURL {
  robotUrl: string;
}

export class RobotFormHttpRepository extends HttpRepository {
  createNewRobot = (robotModel: RobotModel) =>
    this._jsonRequest<RobotURL>(HttpMethod.POST, "/scenes/create/robot", robotModel);
}
