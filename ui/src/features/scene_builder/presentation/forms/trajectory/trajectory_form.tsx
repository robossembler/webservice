import React from "react";
import { observer } from "mobx-react-lite";
import { TrajectoryStore } from "./trajectory_store";
import { IDefaultSceneManagerFormProps } from "../scene_manager_forms";

export const Trajectory = observer((props: IDefaultSceneManagerFormProps) => {
  const [store] = React.useState(() => new TrajectoryStore());
  React.useEffect(() => {
    store.init();
  }, [store]);

  return <>Trajectory</>;
});
