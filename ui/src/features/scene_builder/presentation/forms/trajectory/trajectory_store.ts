import makeAutoObservable from "mobx-store-inheritance";
import { NavigateFunction } from "react-router-dom";
import { TrajectoryViewModel } from "./trajectory_view_model";
import { TrajectoryHttpRepository } from "./trajectory_http_repository";
import { FormState, CoreError } from "../../../../../core/store/base_store";

export class TrajectoryStore extends FormState<TrajectoryViewModel, CoreError> {
    constructor() {
        super();
        makeAutoObservable(this);
    }
    viewModel: TrajectoryViewModel = TrajectoryViewModel.empty();
    cameraDeviceHttpRepository:  TrajectoryHttpRepository = new TrajectoryHttpRepository();
    errorHandingStrategy = (error: CoreError) => { }
    init = async (navigate?: NavigateFunction | undefined) => {
      
    }

}
  