import { Result } from "../../../../../core/helper/result";

 
export class TrajectoryViewModel {
  constructor() {}
  isValid(): Result<string, TrajectoryViewModel> {
    return Result.ok();
  }
  static empty() {
    return new TrajectoryViewModel();
  }
}
