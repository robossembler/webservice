import React from "react";

export const SceneWidget = () => {
  const [pressed, setPressed] = React.useState(false);
  const [position, setPosition] = React.useState({ x: 0, y: 0 });

  React.useEffect(() => {
    if (pressed) {
      window.addEventListener("mousemove", onMouseMove);
      window.addEventListener("mouseup", togglePressed);
    }

    return () => {
      window.removeEventListener("mousemove", onMouseMove);
      window.removeEventListener("mouseup", togglePressed);
    };
  }, [position, pressed]);

  const onMouseMove = (event: any) => {
    const x = position.x + event.movementX;
    const y = position.y + event.movementY;
    setPosition({ x, y });
  };

  const togglePressed = () => {
    setPressed((prev) => !prev);
  };

  const quickAndDirtyStyle = {
    transform: "rotate(0deg)",
    background: "rgb(73 73 73)",
    color: "#FFFFFF",
    fontSize: "20px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderColor: "aqua",
    border: "solid",
    borderRadius: "30px",
  };
  return (
    <div style={{ position: "absolute", top: "0px", left: "0px" }}>
      <div
        className={pressed ? "box_0-active" : "box-0"}
        style={{
          ...quickAndDirtyStyle,
          marginLeft: `${position.x}px`,
          marginTop: `${position.y}px`,
        }}
        onClickCapture={(event) => {
          event.stopPropagation();
          setPressed(false);
        }}
        onMouseDown={togglePressed}
      >
        <p>{pressed ? "Dragging..." : "Press to drag"}</p>
        <h1
          onClick={(event) => {
            event.stopPropagation();
          }}
        >
          HYO
        </h1>
      </div>
    </div>
  );
};
