interface SceneMenuProps {
  x?: number;
  y?: number;
}

export const SceneMenu = (props: SceneMenuProps) => {
  const sceneMenuStyle = {
    transform: "rotate(0deg)",
    background: "rgb(73 73 73)",
    color: "#FFFFFF",
    fontSize: "20px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderColor: "aqua",
    border: "solid",
    borderRadius: "30px",
    width: "150px",
    height: "300px",
  };

  return (
    <div style={{ position: "absolute", top: "0px", left: "0px" }}>
      <div style={{ ...sceneMenuStyle, marginLeft: `${props.x}px`, marginTop: `${props.y}px` }}></div>
    </div>
  );
};
