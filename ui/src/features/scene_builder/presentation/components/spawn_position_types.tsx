import { SpawnPositionTypes } from "../../../../core/model/spawn_position_types";
import { CoreButton } from "../../../../core/ui/button/button";
import { CoreText, CoreTextType } from "../../../../core/ui/text/text";

export const SpawnPositionTypesForm = ({ onClick: onClick }: { onClick: Function }) => {
  return (
    <>
      <CoreText text={"Тип размещения"} type={CoreTextType.header} />
      {Object.entries(SpawnPositionTypes).map(([_, value], i) => (
        <CoreButton key={i} text={value} onClick={() => onClick(value)} />
      ))}
    </>
  );
};
