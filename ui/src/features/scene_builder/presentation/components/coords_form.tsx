import { Instance } from "../../../../core/model/scene_asset";
import { FormState } from "../../../../core/store/base_store";
import { CoreInput } from "../../../../core/ui/input/input";
import { CoreText, CoreTextType } from "../../../../core/ui/text/text";
import { Toggle } from "../../../../core/ui/toggle/toggle";

export const CoordsForm = ({ store, update }: { store: FormState<any, any>; update: Function }) => {
  return (
    <>
      <Toggle
        name={"Координаты"}
        child={
          <div  >
            <CoreText text="Позиция" type={CoreTextType.header} />

            <CoreInput
              value={String(store.viewModel.position.x)}
              label="X"
              onChange={(text) => {
                store.updateForm({ position: store.viewModel.position.setX(Number(text)) });
                update();
              }}
            />
            <CoreInput
              value={String(store.viewModel.position.x)}
              label="Y"
              onChange={(text) => {
                store.updateForm({ position: store.viewModel.position.setY(Number(text)) });
                update();
              }}
            />
            <CoreInput
              value={String(store.viewModel.position.x)}
              label="Z"
              onChange={(text) => {
                store.updateForm({ position: store.viewModel.position.setZ(Number(text)) });
                update();
              }}
            />
            <CoreText text="Кватернион" type={CoreTextType.header} />
            <CoreInput
              value={String(store.viewModel.orientation[0])}
              label="X"
              onChange={(text) => {
                store.updateForm({
                  orientation: store.viewModel.orientation.map((el: any, i: number) => {
                    if (i === 1) {
                      return Number(text);
                    }
                    return el;
                  }),
                });
                update();
              }}
            />
            <CoreInput
              value={String(store.viewModel.orientation[1])}
              label="Y"
              onChange={(text) => {
                store.updateForm({
                  orientation: store.viewModel.orientation.map((el: any, i: number) => {
                    if (i === 2) {
                      return Number(text);
                    }
                    return el;
                  }),
                });
                update();
              }}
            />
            <CoreInput
              value={String(store.viewModel.orientation[2])}
              label="Z"
              onChange={(text) => {
                store.updateForm({
                  orientation: store.viewModel.orientation.map((el: any, i: number) => {
                    if (i === 3) {
                      return Number(text);
                    }
                    return el;
                  }),
                });
                update();
              }}
            />
            <CoreInput
              value={String(store.viewModel.orientation[3])}
              label="W"
              onChange={(text) => {
                store.updateForm({
                  orientation: store.viewModel.orientation.map((el: any, i: number) => {
                    if (i === 4) {
                      return Number(text);
                    }
                    return el;
                  }),
                });
                update();
              }}
            />
          </div>
        }
        isOpen={false}
      />
    </>
  );
};
