import makeAutoObservable from "mobx-store-inheritance";
import { Vector2, Vector3, Object3D } from "three";
import { CameraModel } from "../../../core/model/camera_model";
import { PointModel } from "../../../core/model/point_model";
import { RobotModel } from "../../../core/model/robot_model";
import { SceneAsset, Instance } from "../../../core/model/scene_asset";
import { SolidModel } from "../../../core/model/solid_model";
import { UiBaseError } from "../../../core/model/ui_base_error";
import { ZoneModel } from "../../../core/model/zone_model";
import { HttpError } from "../../../core/repository/core_http_repository";
import { CoreThreeRepository } from "../../../core/repository/core_three_repository";
import { SceneModel } from "../../scene_manager/model/scene_model";
import { SceneMode } from "../../scene_manager/model/scene_view";
import { SceneViewModel } from "../../scene_manager/model/scene_view_model";
import { SceneManagerForms } from "./forms/scene_manager_forms";
import { FormState } from "../../../core/store/base_store";
import { Result } from "../../../core/helper/result";
import { SceneBuilderHttpRepository } from "../data/scene_builder_repository";
import { SceneModelsTypes } from "../../../core/model/scene_models_type";
export enum StoreMode {
  sceneInstance = "sceneInstance",
  allScenes = "allScenes",
}
export interface ISpawnHelper {
  url: string;
  spawn: SceneModelsTypes;
  name: string;
  isFinished: boolean;
  type: string;
}
export interface IPopoverItem {
  name: string;
  fn: Function;
}
export interface SceneItems {
  fn: Function;
  name: string;
  isSelected: boolean;
  icon: string;
}
export class SceneBuilderStore extends FormState<SceneViewModel, HttpError> {
  activeFormType?: string;
  activeSceneId: string = "";
  selectedItemName?: string;
  activeFormDependency: Object = {};
  viewModel: SceneViewModel = SceneViewModel.empty();
  sceneMode: SceneMode;
  sceneItems: SceneItems[] = [];
  isVisibleSaveButton: boolean = false;
  coreThreeRepository: null | CoreThreeRepository = null;
  sceneBuilderHttpRepository: SceneBuilderHttpRepository;
  isSceneMenuShow = false;
  robossemblerAssets?: SceneAsset;
  objectForMagnetism: string;
  objectMagnetism: string;
  scenes: SceneModel[] = [];
  canvasRef?: HTMLCanvasElement;
  mousePosition?: Vector2;
  spawnHelper?: ISpawnHelper;
  selectSceneObject?: Object;
  isLoadingForm: boolean = false;
  scene: (Instance | SolidModel | CameraModel | RobotModel | PointModel | ZoneModel)[] = [];

  sceneHelperInstruments: {
    icon: string;
    onClick: Function;
    isSelected: boolean;
  }[] = [
    { icon: SceneMode.Select, onClick: () => this.setMode(SceneMode.Select), isSelected: false },
    { icon: SceneMode.Move, onClick: () => this.setMode(SceneMode.Move), isSelected: false },
    { icon: SceneMode.Rotate, onClick: () => this.setMode(SceneMode.Rotate), isSelected: false },
  ];
  popoverItems: IPopoverItem[] = [
    { name: "Камера", fn: () => this.createNewForm(SceneManagerForms.camera, true) },
    { name: "Твердое тело", fn: () => this.createNewForm(SceneManagerForms.solidBody, true) },
    { name: "Источник света", fn: () => this.createNewForm(SceneManagerForms.light, true) },
    { name: "Робот", fn: () => this.createNewForm(SceneManagerForms.robot, true) },
    { name: "Точка", fn: () => this.createNewForm(SceneManagerForms.point, true) },
    { name: "Траектория", fn: () => this.createNewForm(SceneManagerForms.trajectory, true) },
    { name: "Зона", fn: () => this.createNewForm(SceneManagerForms.zone, true) },
    { name: "Точки захвата", fn: () => this.createNewForm(SceneManagerForms.capturePoints, true) },
  ];
  canvasOffsetX?: number;
  sceneName?: string;
  constructor() {
    super();
    makeAutoObservable(this);

    this.sceneBuilderHttpRepository = new SceneBuilderHttpRepository();
    this.sceneMode = SceneMode.Select;
  }
  sceneSave = () =>
    this.messageHttp(this.sceneBuilderHttpRepository.editScene(SceneAsset.newScene(this.scene, this.sceneName ?? "")), {
      successMessage: "Сцена сохранена",
    }).then(() => (this.isVisibleSaveButton = false));

  iconToSceneManagerForm = (icon: string): SceneManagerForms => {
    if (icon.isEqual("Camera")) {
      this.activeFormDependency = {
        type: "Preview",
      };
      return SceneManagerForms.camera;
    }
    if (icon.isEqual("Zone")) {
      this.activeFormDependency = {
        type: "Preview",
      };
      return SceneManagerForms.zone;
    }
    if (icon.isEqual("Robot")) {
      this.activeFormDependency = {
        type: "Preview",
      };
      return SceneManagerForms.robot;
    }
    if (icon.isEqual("Point")) {
      this.activeFormDependency = {
        type: "Preview",
      };
      return SceneManagerForms.point;
    }

    this.activeFormDependency = {
      type: "Preview",
    };
    return SceneManagerForms.solidBody;
  };
  selectSceneItems = (name: string, index: number, selected: boolean, icon: string) => {
    this.sceneItems.map((el) => {
      el.isSelected = false;
      return el;
    });
    this.sceneItems.map((el, i) => i.isEqualR(index).map(() => (el.isSelected = selected)));
    if (selected) {
      this.createNewForm(this.iconToSceneManagerForm(icon));
      this.selectedItemName = name;
    }

    if (!selected) {
      this.createNewForm(undefined);
      this.selectedItemName = undefined;
    }
  };

  setMode = (mode: SceneMode) => {
    this.sceneHelperInstruments.map((el) => {
      el.isSelected = false;
      el.icon.isEqualR(mode).map(() => (el.isSelected = true));
      return el;
    });
    this.sceneMode = mode;
    this.coreThreeRepository?.setTransformMode(this.sceneMode);
  };
  createNewForm = (formType: SceneManagerForms | undefined, isNeedClearDependency?: boolean) => {
    this.activeFormDependency = Object.assign(this.activeFormDependency, { store: this });
    if (isNeedClearDependency) this.activeFormDependency = { store: this };
    this.activeFormType = formType;
  };

  deleteSceneItem = (item: SceneItems) => {
    this.sceneItems = this.sceneItems.filter((el) => !el.name.isEqual(item.name));
    this.scene = this.scene.filter((el) => !el.name.isEqual(item.name));
    this.coreThreeRepository?.deleteSceneItem(item);
    this.visibleSaveButton();
  };

  visibleSaveButton = () => {
    this.isVisibleSaveButton = true;
  };

  addNewCamera = (model: CameraModel) => {
    model.position = this.coreThreeRepository!.camera.position;
    model.orientation = this.coreThreeRepository!.camera.quaternion.toArray();
    model.aspect = this.coreThreeRepository!.camera.aspect;
    this.sceneItems.push({ name: model.name, icon: "Camera", fn: () => {}, isSelected: false });
    this.scene.push(model);
    model.toWebGl(this.coreThreeRepository!);
    this.visibleSaveButton();
  };

  hiddenMenu = () => (this.isSceneMenuShow = false);

  init = async () => {};
  initParam = (id: string | undefined) => {
    if (id) this.activeSceneId = id;
  };
  errorHandingStrategy = (error: HttpError) =>
    error.status.isEqualR(404).map(() => this.errors.push(new UiBaseError(`not found to project`)));

  loadScene = (canvasRef: HTMLCanvasElement) => {
    this.canvasRef = canvasRef;
    this.loadWebGl(canvasRef);
  };

  loadWebGl = async (canvasRef: HTMLCanvasElement): Promise<void> => {
    this.coreThreeRepository = new CoreThreeRepository(canvasRef as HTMLCanvasElement, this.watcherSceneEditorObject);

    this.coreThreeRepository.render();

    this.canvasOffsetX = canvasRef.getBoundingClientRect().x;
    canvasRef.addEventListener("click", (event) => this.clickLister(event, canvasRef.getBoundingClientRect().x));

    (await this.sceneBuilderHttpRepository.getSceneAsset(this.activeSceneId ?? "")).fold(
      (sceneAssets) => {
        this.sceneName = sceneAssets.name;
        this.coreThreeRepository?.loadScene(sceneAssets);
        this.sceneItems = sceneAssets.toSceneItems();
        this.scene = sceneAssets.scene;
      },
      () => {}
    );
  };
  clickScene = (event: MouseEvent, offset: number = 0): Result<void, Vector3> => {
    const vector = new Vector2();
    const boundingRect = this.canvasRef!.getBoundingClientRect();

    vector.x = ((event.clientX - offset) / boundingRect.width) * 2 - 1;
    vector.y = -(event.clientY / boundingRect.height) * 2 + 1;
    return this.coreThreeRepository!.setRayCastAndGetFirstObjectAndPointToObject(vector);
  };
  clickLister = (event: MouseEvent, offset: number = 0) => {
    const vector = new Vector2();
    const boundingRect = this.canvasRef!.getBoundingClientRect();

    vector.x = ((event.clientX - offset) / boundingRect.width) * 2 - 1;
    vector.y = -(event.clientY / boundingRect.height) * 2 + 1;

    if (this.sceneMode.isEqual(SceneMode.Select)) {
      return;
    }

    if (this.sceneMode.isEqualMany([SceneMode.Move, SceneMode.Rotate])) {
      this.transformContollsCall(vector);
    }
  };

  watcherSceneEditorObject = (mesh: Object3D) => (
    (this.scene = this.scene.map((el) =>
      el.name.isEqualR(mesh.name).fold(
        () => {
          el.position = mesh.position;
          el.orientation = mesh.quaternion.toArray();
          return el;
        },
        () => el
      )
    )),
    this.visibleSaveButton()
  );

  transformContollsCall = (vector: Vector2) =>
    // @ts-ignore
    this.coreThreeRepository?.setRayCastAndGetFirstObject(vector).fold(
      (object) => this.coreThreeRepository?.setTransformControlsAttach(object),
      (_) => this.coreThreeRepository?.disposeTransformControlsMode()
    );

  dispose = () => {
    window.removeEventListener("click", this.clickLister);
  };
}
