import * as React from "react";
import { MainPage } from "../../core/ui/pages/main_page";
import { SimulationStore } from "./simulations_store";
import { useStore } from "../../core/helper/use_store";
interface ISimulationScreenProps {}
export const SimulationScreenPath = "/simulation";

export const SimulationScreen = (props: ISimulationScreenProps) => {
  const store = useStore(SimulationStore);
  const canvasRef = React.useRef<HTMLCanvasElement>(null);
  React.useEffect(() => {
    store.loadScene(canvasRef.current!);
  }, []);
  return <MainPage page={"Симуляция"} bodyChildren={<canvas ref={canvasRef} style={{ overflow: "hidden" }} />} />;
};
