import { Quaternion, Vector3 } from "three";

export interface UrdfTransforms {
    transforms: TransformElement[];
}

export interface TransformElement {
    header: Header;
    child_frame_id: string;
    transform: TransformTransform;
}

export interface Header {
    stamp: Stamp;
    frame_id: string;
}

export interface Stamp {
    sec: number;
    nsec: number;
}

export interface TransformTransform {
    translation: Coords;
    rotation: Coords;
}
export interface Coords {
    x: number;
    y: number;
    z: number;
    w?: number;
}

export const coordsToVector = (coords: Coords): Vector3 => {
    return new Vector3(coords.x, coords.y, coords.z)
}
export const coordsToQuaternion = (coords: Coords): Quaternion => {
    return new Quaternion(coords.x, coords.y, coords.z, coords.w)
}
 