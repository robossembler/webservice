import makeAutoObservable from "mobx-store-inheritance";
import { CoreRosWsBridgeRepository } from "../../core/repository/core_ros_ws_bridge_repository";
import { CoreThreeRepository } from "../../core/repository/core_three_repository";
import { UrdfTransforms } from "./tranforms_model";

export class SimulationStore {
    rosWsUrdfTransfomsListner: CoreRosWsBridgeRepository<UrdfTransforms> = new CoreRosWsBridgeRepository();
    coreThreeRepository?: CoreThreeRepository;

    constructor() {
        makeAutoObservable(this)
    }
    init = () => {
        this.rosWsUrdfTransfomsListner.connect('/tf')
    }
    async loadScene(canvasRef: HTMLCanvasElement) {
        await this.loadWebGl(canvasRef);
        this.coreThreeRepository?.loadUrdf('http://localhost:4001/robot.xml');
        if (this.coreThreeRepository?.urdfTransforms) {
            this.rosWsUrdfTransfomsListner.on(this.coreThreeRepository.urdfTransforms)

        }
    }
    loadWebGl = async (canvasRef: HTMLCanvasElement) => {
        this.coreThreeRepository = new CoreThreeRepository(canvasRef as HTMLCanvasElement, () => { });
        this.coreThreeRepository.on(() => { });
        this.coreThreeRepository.render();
    }
}