import { HttpMethod, CoreHttpRepository } from "../../../core/repository/core_http_repository";
import { SceneViewModel } from "../model/scene_view_model";

export class SceneHttpRepository extends CoreHttpRepository {
  deleteScene = (sceneId: string) => this._jsonRequest(HttpMethod.DELETE, `/scenes?id=${sceneId}`);
  newScene = (sceneViewModel: SceneViewModel) => this._jsonRequest(HttpMethod.POST, "/scenes", sceneViewModel);
}
