import makeAutoObservable from "mobx-store-inheritance";
import { Vector2 } from "three";
import { message } from "antd";
import { CoreThreeRepository } from "../../../core/repository/core_three_repository";
import { HttpError } from "../../../core/repository/core_http_repository";
import { UiDrawerFormState } from "../../../core/store/base_store";
import { UiBaseError } from "../../../core/model/ui_base_error";
import { SceneHttpRepository } from "../data/scene_http_repository";
import { Instance, SceneAsset } from "../../../core/model/scene_asset";
import { SceneViewModel } from "../model/scene_view_model";
import { SceneModel } from "../model/scene_model";
import { CameraModel } from "../../../core/model/camera_model";
import { SolidModel } from "../../../core/model/solid_model";
import { PointModel } from "../../../core/model/point_model";
import { RobotModel } from "../../../core/model/robot_model";
import { ZoneModel } from "../../../core/model/zone_model";

export enum DrawersSceneManager {
  NewScene = "Новая сцена",
}

export class SceneMangerStore extends UiDrawerFormState<SceneViewModel, HttpError> {
  activeFormType?: string;
  activeSceneId: string = "";
  selectedItemName?: string;
  viewModel: SceneViewModel = SceneViewModel.empty();
  isVisibleSaveButton: boolean = false;
  coreThreeRepository: null | CoreThreeRepository = null;
  sceneHttpRepository: SceneHttpRepository;
  isSceneMenuShow = false;
  robossemblerAssets?: SceneAsset;
  objectForMagnetism: string;
  objectMagnetism: string;
  scenes: SceneModel[] = [];
  canvasRef?: HTMLCanvasElement;
  mousePosition?: Vector2;
  selectSceneObject?: Object;
  isLoadingForm: boolean = false;
  scene: (Instance | SolidModel | CameraModel | RobotModel | PointModel | ZoneModel)[] = [];

  canvasOffsetX?: number;
  sceneName?: string;
  constructor() {
    super(DrawersSceneManager);
    makeAutoObservable(this);

    this.sceneHttpRepository = new SceneHttpRepository();
  }

  createNewScene = () =>
    this.viewModel.valid().fold(
      async (s) => {
        await this.sceneHttpRepository.newScene(s);
        this.init();
        this.editDrawer(DrawersSceneManager.NewScene, false);
      },
      async (e) => message.error(e)
    );

  visibleSaveButton = () => {
    this.isVisibleSaveButton = true;
  };

  hiddenMenu = () => (this.isSceneMenuShow = false);

  init = async () => {
    this.mapOk("scenes", this.sceneHttpRepository.getAllScenes());
  };

  errorHandingStrategy = (error: HttpError) =>
    error.status.isEqualR(404).map(() => this.errors.push(new UiBaseError(`not found to project`)));

  clickDeleteScene = async (sceneId: string) => {
    console.log(sceneId)
    await this.messageHttp(this.sceneHttpRepository.deleteScene(sceneId), {
      successMessage: "сцена удалена",
    });
    this.mapOk("scenes", this.sceneHttpRepository.getAllScenes());
  };
}
