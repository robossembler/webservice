import * as React from "react";
import { DrawersSceneManager, SceneMangerStore } from "./scene_manager_store";
import { observer } from "mobx-react-lite";
import { Drawer } from "antd";
import { useNavigate } from "react-router-dom";
import { MainPage } from "../../../core/ui/pages/main_page";
import { CoreText, CoreTextType } from "../../../core/ui/text/text";
import { CoreButton } from "../../../core/ui/button/button";
import { CoreInput } from "../../../core/ui/input/input";
import { DrawersDataset } from "../../dataset/dataset_store";
import { Icon } from "../../../core/ui/icons/icons";
import { useStore } from "../../../core/helper/use_store";
import { SceneBuilderScreenPath } from "../../scene_builder/presentation/scene_builder_screen";

export const SceneManagerPath = "/scene/manager/";

export const SceneManger = observer(() => {
  const store = useStore(SceneMangerStore);
  const navigate = useNavigate();

  return (
    <MainPage
      page={"Сцена"}
      panelStyle={{
        display: "none",
      }}
      bodyChildren={
        <>
          <div
            style={{
              width: "100%",
              height: "100vh",
              background: "rgba(246, 242, 249, 1)",
              padding: 50,
              paddingLeft: 70,
              paddingRight: 70,
            }}
          >
            <div style={{ width: "100%", height: "100%", background: "white" }}>
              <div style={{ width: "100%", textAlign: "center" }}>
                <CoreText text="Сцены" type={CoreTextType.big} />
                <CoreText
                  text="Создать новую сцену?"
                  onClick={() => store.editDrawer(DrawersSceneManager.NewScene, true)}
                  type={CoreTextType.small}
                  color="rgba(68, 142, 247, 1)"
                  style={{ cursor: "pointer" }}
                />

                <div>
                  {store.scenes.map((el, i) => (
                    <div
                      key={i}
                      style={{
                        border: "1px solid #CAC4D0",
                        margin: 10,
                        height: 60,
                        alignItems: "center",
                        borderRadius: 7,
                        display: "flex",
                        justifyContent: "space-between",
                        backgroundColor: "rgba(104, 80, 164, 1)",
                        color: "white",
                        paddingLeft: 10,
                      }}
                    >
                      <CoreText text={el.name} type={CoreTextType.large} color="white" />
                      <div style={{ display: "flex", alignItems: "center", marginLeft: 10, marginRight: 10 }}>
                        <CoreButton
                          text="Перейти"
                          onClick={() => navigate(`${SceneBuilderScreenPath}${el._id}`)}
                          textStyle={{ color: "black", textAlign: "center" }}
                          style={{ marginRight: 10, backgroundColor: "white", width: 126 }}
                        />
                        <Icon
                          style={{ height: 20 }}
                          type={"DeleteCircle"}
                          color="red"
                          onClick={() => store.clickDeleteScene(el._id as string)}
                        />
                      </div>
                    </div>
                  ))}
                </div>
              </div>
              <div></div>
            </div>

            <div
              style={{
                display: "flex",
                flexDirection: "row",
                alignContent: "center",
                justifyContent: "space-between",
                position: "absolute",
                width: "100vw",
              }}
            />
            <Drawer
              title={store.titleDrawer}
              destroyOnClose={true}
              onClose={() => store.editDrawer(DrawersSceneManager.NewScene, false)}
              open={store.drawers.find((el) => el.name === DrawersSceneManager.NewScene)?.status}
            >
              <div
                style={{ display: "flex", flexDirection: "column", justifyContent: "space-between", height: "100%" }}
              >
                <CoreInput
                  value={store.viewModel.name}
                  label={"Имя сцены"}
                  onChange={(text) => store.updateForm({ name: text })}
                />

                <div style={{ display: "flex" }}>
                  <CoreButton text="Сохранить" filled={true} onClick={() => store.createNewScene()} />
                  <div style={{ width: 10 }} />
                  <CoreButton text="Отмена" onClick={() => store.editDrawer(DrawersDataset.NewDataset, false)} />
                </div>
              </div>
            </Drawer>
          </div>
        </>
      }
    />
  );
});
