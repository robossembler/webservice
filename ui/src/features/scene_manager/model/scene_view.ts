
export interface SceneManagerView {
  name: string;
  clickHandel: Function;
}

export enum SceneMode {
  Rotate = "Rotate",
  Move = "Move",
  Select = "Select",
  Save = 'Save',
}
