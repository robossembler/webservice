import { Result } from "../../../core/helper/result";

export class SceneViewModel {
  name: string;
  constructor(name: string) {
    this.name = name;
  }
  static empty() {
    return new SceneViewModel("");
  }
  valid = (): Result<string,SceneViewModel> => {
    if (this.name.isEmpty()) {
      return Result.error("name is empty");
    }
    return Result.ok(this);
  };
}
