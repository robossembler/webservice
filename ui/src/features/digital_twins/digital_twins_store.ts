import makeAutoObservable from "mobx-store-inheritance";
import { UiDrawerFormState } from "../../core/store/base_store";
import { DigitalTwinsModel, DigitalTwinsTemplate as DigitalTwins } from "./digital_twins_model";
import { NavigateFunction } from "react-router-dom";
import { message } from "antd";
import { DigitalTwinsHttpRepository } from "./digital_twins_http_repository";
import { HttpError } from "../../core/repository/core_http_repository";
import { match } from "ts-pattern";

export enum DrawersDigitalTwin {
  newTwinTemplate = "Новый шаблон двойника",
  newInstanceTwinTemplate = "Новый инстанц шаблона двойника",
}
export enum DigitalTwinStoreType {
  selectTwinTemplate = "selectTwinTemplate",
  editInstanceTemplate = "editInstanceTemplate",
}
export class DigitalTwinsStore extends UiDrawerFormState<DigitalTwinsModel, HttpError> {
  type: DigitalTwinStoreType = DigitalTwinStoreType.selectTwinTemplate;
  digitalTwinsTemplates?: DigitalTwins[];
  digitalTwinsInstances?: DigitalTwins[];
  digitalTwinsHttpRepository: DigitalTwinsHttpRepository = new DigitalTwinsHttpRepository();
  viewModel: DigitalTwinsModel = DigitalTwinsModel.empty();
  isModalOpen: boolean = false;
  init = async (_navigate?: NavigateFunction | undefined): Promise<any> => this.refresh();
  constructor() {
    super(DrawersDigitalTwin);
    makeAutoObservable(this);
  }
  refresh = async () => {
    this.mapOk("digitalTwinsTemplates", this.digitalTwinsHttpRepository.getAllDigitalTwinsTemplate());
    this.mapOk("digitalTwinsInstances", this.digitalTwinsHttpRepository.getAllDigitalTwinsInstance());
  };
  saveInstanceDigitalTwin = async () =>
    match(this.type)
      .with(DigitalTwinStoreType.editInstanceTemplate, async () => {

        this.viewModel._id = undefined;
        (await this.digitalTwinsHttpRepository.createNewDigitalTwinsInstance(this.viewModel)).map(async (id) => {
          await this.digitalTwinsHttpRepository.execDigitalTwinsInstance(id);
          await this.refresh();
        });
        this.editDrawer(DrawersDigitalTwin.newInstanceTwinTemplate, false);
      })
      .with(DigitalTwinStoreType.selectTwinTemplate, () => {
 
        // this.editDrawer(DrawersDigitalTwin.newInstanceTwinTemplate, false),
        // await (
        //   await this.digitalTwinsHttpRepository.createNewDigitalTwinsInstance(this.viewModel)
        // ).map(() => this.refresh())
      });
  createInstanceTwins = (el: DigitalTwins): void => {
    this.loadDependency(el as any);
    this.type = DigitalTwinStoreType.editInstanceTemplate;
  };
  saveTemplateDigitalTwin = async () =>
    (await this.viewModel.valid<DigitalTwinsModel>()).fold(
      async (model) => {
        (await this.digitalTwinsHttpRepository.createNewDigitalTwinsTemplate(model)).map(() => {
          this.refresh();
        });
      },
      async (e) => message.error(e)
    );
  deleteDigitalTwinsTemplate = async (id: string) =>
    (await this.digitalTwinsHttpRepository.deleteDigitalTwinsTemplate(id)).map(() => this.refresh());
}
