import { HttpMethod, HttpRepository } from "../../core/repository/core_http_repository";
import { UUID } from "../all_projects/data/project_http_repository";
import { DigitalTwinsModel } from "./digital_twins_model";

export class DigitalTwinsHttpRepository extends HttpRepository {
  featureApi = `/digital/twins/template`;
  subFeatureApi = `/digital/twins/instance`;
  getAllDigitalTwinsTemplate = () => this._jsonRequest(HttpMethod.GET, this.featureApi);
  getAllDigitalTwinsInstance = () => this._jsonRequest(HttpMethod.GET, this.subFeatureApi);
  createNewDigitalTwinsTemplate = (model: DigitalTwinsModel) =>
    this._jsonRequest(HttpMethod.POST, this.featureApi, model);
  createNewDigitalTwinsInstance = (model: DigitalTwinsModel) =>
    this._jsonRequest<UUID>(HttpMethod.POST, this.subFeatureApi, model);
  deleteDigitalTwinsTemplate = (id: string) => this._jsonRequest(HttpMethod.DELETE, `${this.featureApi}?id=${id}`);
  deleteDigitalTwinsInstance = (id: string) => this._jsonRequest(HttpMethod.DELETE, `${this.subFeatureApi}?id=${id}`);
  execDigitalTwinsInstance = (id: UUID) =>
    this._jsonRequest(HttpMethod.POST, `${this.subFeatureApi}/exec/instance?id=${id.id}`);
}
