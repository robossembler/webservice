import React from "react";
import { observer } from "mobx-react-lite";
import { DigitalTwinStoreType, DigitalTwinsStore, DrawersDigitalTwin } from "./digital_twins_store";
import { Drawer, Modal } from "antd";
import { CoreButton } from "../../core/ui/button/button";
import { FormBuilder } from "../../core/ui/form_builder/form_builder";
import { CoreInput } from "../../core/ui/input/input";
import { CoreSelect } from "../../core/ui/select/select";
import { DigitalTwinsModel, DigitalTwinsTypes } from "./digital_twins_model";
import { match } from "ts-pattern";
import { useStore } from "../../core/helper/use_store";

export const DigitalTwinsScreenPath = "/digital_twins";

export const DigitalTwinsScreen = observer(() => {
  const store = useStore(DigitalTwinsStore);

  return (
    <>
      <div style={{ display: "flex" }}>
        <CoreButton
          text="Новый шаблон двойника"
          filled={true}
          onClick={() => store.editDrawer(DrawersDigitalTwin.newTwinTemplate, true)}
        />
        <CoreButton
          text="Новый двойник"
          filled={true}
          onClick={() => store.editDrawer(DrawersDigitalTwin.newInstanceTwinTemplate, true)}
        />
      </div>
      <div>{store.digitalTwinsInstances?.map((el) => <>{el.name}</>)}</div>
      <Drawer
        width={(window.innerWidth / 100) * 50}
        title={store.titleDrawer}
        destroyOnClose={true}
        onClose={() => {
          store.editDrawer(DrawersDigitalTwin.newInstanceTwinTemplate, false);
          store.type = DigitalTwinStoreType.selectTwinTemplate;
          store.viewModel = DigitalTwinsModel.empty();
        }}
        open={store.drawers.find((el) => el.name === DrawersDigitalTwin.newInstanceTwinTemplate)?.status}
      >
        <div style={{ display: "flex", flexDirection: "column", justifyContent: "space-between", height: "100%" }}>
          {match(store.type)
            .with(DigitalTwinStoreType.editInstanceTemplate, () => (
              <>
                <CoreInput onChange={(text) => store.updateForm({ instanceName: text })} label={"Instance name"} />
                <FormBuilder
                  formBuilder={store.viewModel.formBuilder}
                  onChange={(change) => store.updateForm({ formBuilder: change })}
                />
              </>
            ))
            .with(DigitalTwinStoreType.selectTwinTemplate, () => (
              <>
                {store.digitalTwinsTemplates?.map((el, i) => (
                  <div
                    key={i}
                    style={{
                      backgroundColor: "rgba(104, 80, 164, 1)",
                      width: 180,
                      height: 180,
                      borderRadius: 10,
                      padding: 10,
                      margin: 10,
                    }}
                  >
                    <div onClick={() => store.deleteDigitalTwinsTemplate(el._id!)}>DELETE</div>
                    <div onClick={() => store.createInstanceTwins(el)}>CREATE</div>
                    {el.name}
                  </div>
                ))}
              </>
            ))
            .otherwise(() => (
              <></>
            ))}
          <div style={{ display: "flex" }}>
            <CoreButton text="Сохранить" filled={true} onClick={() => store.saveInstanceDigitalTwin()} />
            <div style={{ width: 10 }} />
            <CoreButton
              text="Отмена"
              onClick={() => store.editDrawer(DrawersDigitalTwin.newInstanceTwinTemplate, false)}
            />
          </div>
        </div>
      </Drawer>

      <Drawer
        width={(window.innerWidth / 100) * 50}
        title={store.titleDrawer}
        destroyOnClose={true}
        onClose={() => store.editDrawer(DrawersDigitalTwin.newTwinTemplate, false)}
        open={store.drawers.find((el) => el.name === DrawersDigitalTwin.newTwinTemplate)?.status}
      >
        <div style={{ display: "flex", flexDirection: "column", justifyContent: "space-between", height: "100%" }}>
          <CoreInput onChange={(text) => store.updateForm({ name: text })} label={"Имя"} />
          <CoreSelect
            items={Object.keys(DigitalTwinsTypes)}
            value={Object.keys(DigitalTwinsTypes)[0]}
            label={"entity"}
            onChange={(value: string) => store.updateForm({ entity: value as any })}
          />
          <CoreInput onChange={(text) => store.updateForm({ description: text })} label={"Описание"} />
          <CoreInput onChange={(text) => store.updateForm({ command: text })} label={"Команда"} />
          <CoreInput onChange={(text) => store.updateForm({ interfaces: { cmd: text } })} label={"Cmd"} />
          <CoreInput onChange={(text) => store.updateForm({ package: text })} label={"package"} />
          <CoreInput onChange={(text) => store.updateForm({ executable: text })} label={"executable"} />
          <CoreInput
            label="FormBuilder Result"
            onChange={(text) => (store.viewModel.formBuilder.result = text)}
            style={{ height: 200, overflow: "overlay" }}
          />
          <CoreInput
            label="FormBuilder Context"
            onChange={(text) => (store.viewModel.formBuilder.context = text)}
            style={{ height: 200, overflow: "overlay" }}
          />
          <div style={{ height: 10 }} />
          <CoreButton
            style={{ width: 206 }}
            text={"Посмотреть FormBuilder "}
            onClick={() => (store.isModalOpen = true)}
          />
          <div style={{ height: 100 }} />

          <div style={{ display: "flex" }}>
            <CoreButton text="Сохранить" filled={true} onClick={() => store.saveTemplateDigitalTwin()} />
            <div style={{ width: 10 }} />
            <CoreButton text="Отмена" onClick={() => store.editDrawer(DrawersDigitalTwin.newTwinTemplate, false)} />
          </div>
        </div>
      </Drawer>

      <Modal
        destroyOnClose={true}
        open={store.isModalOpen}
        footer={null}
        closable={false}
        closeIcon={null}
        onCancel={() => {
          store.isModalOpen = false;
        }}
      >
        <FormBuilder formBuilder={store.viewModel.formBuilder} onChange={() => {}} />
      </Modal>
    </>
  );
});
