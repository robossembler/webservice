import { ValidationModel } from "../../core/model/validation_model";
import { Type } from "class-transformer";
import { IsEnum, IsNotEmpty, IsString } from "class-validator";
import { FormBuilderValidationModel } from "../../core/model/form_builder_validation_model";
 
export enum DigitalTwinsTypes {
  CAMERA = "CAMERA",
  ROBOT = "ROBOT",
}
export class Interfaces {
  @IsString()
  cmd: string;
}

export class DigitalTwinsModel extends ValidationModel {
  _id?: string;
  path: string = "";
  name: string = "";
  instanceName: string = "";
  @IsEnum(DigitalTwinsTypes)
  entity: DigitalTwinsTypes;
  @IsString()
  description: string;
  @IsString()
  command: string;
  @Type(() => Interfaces)
  interfaces: Interfaces;
  @IsNotEmpty()
  @IsString()
  package: string;
  @IsNotEmpty()
  @IsString()
  executable: string;
  formBuilder = FormBuilderValidationModel.empty();
  static empty() {
    return new DigitalTwinsModel();
  }
}
export interface DigitalTwinsTemplate {
  interfaces: Interfaces;
  _id?: string;
  path: string;
  name: string;
  entity: string;
  description: string;
  command: string;
  __v: string;
  formBuilder?: FormBuilderValidationModel;
}
