import makeAutoObservable from "mobx-store-inheritance";
import { NavigateFunction } from "react-router-dom";
import { NumberTriviaModel } from "./number_trivia";
import { FormState, CoreError } from "../../../../../../core/store/base_store";
import { {{name.pascalCase()}}HttpRepository } from "./{{name.snakeCase()}}_http_repository";

export class {{name.pascalCase()}}Store extends FormState<NumberTriviaModel, CoreError> {
    constructor() {
        super();
        makeAutoObservable(this);
    }
    viewModel: NumberTriviaModel = NumberTriviaModel.empty();
    cameraDeviceHttpRepository:  {{name.pascalCase()}}HttpRepository = new {{name.pascalCase()}}HttpRepository();
    errorHandingStrategy = (error: CoreError) => { }
    init = async (navigate?: NavigateFunction | undefined) => {
      
    }

}
