import React from "react";
import { observer } from "mobx-react-lite";
import { {{ name.pascalCase() }}Store } from "./{{name.snakeCase()}}_store";
import { IPropsForm } from "../forms";
import { NumberTriviaModel } from "./number_trivia";

export const {{ name.pascalCase()}} = observer((props: IPropsForm<Partial<NumberTriviaModel>>) => {
  const [store] = React.useState(() => new {{ name.pascalCase() }}Store());
  React.useEffect(() => {
    store.init();
  }, [store]);

  return <></>;
});
