import { Schema, model } from "mongoose";

export interface ITestModel {
  _id?: string;
  result: string;
}

export const TestSchema = new Schema({
  result: String,
});

const schema = "Test";

export const TestDBModel = model<ITestModel>(schema, TestSchema);
