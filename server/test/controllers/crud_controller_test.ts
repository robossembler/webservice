import { CrudController } from "../../src/core/controllers/crud_controller";
import { ClassValidatorMocker } from "../../src/core/helpers/class_validator_mocker";
import { HttpRepository } from "../../src/core/repository/http_repository";

function instanceOfObjectAndHaveId(s: any): string {
  if (s instanceof Object && "id" in s) {
    return s.id;
  }
  if (s instanceof Object && "_id" in s) {
    return s._id;
  }
  throw Error(`${s} is not instance object or not have property _id`);
}

export class CrudControllerTest {
  controllerTest: CrudController<any, any>;
  httpRepository: HttpRepository;

  constructor(port: number, controller: CrudController<any, any>) {
    this.controllerTest = controller;
    this.httpRepository = new HttpRepository(`http://localhost:${port}`);
  }

  async call() {
    let result = false;
    const mockModel = ClassValidatorMocker.create<any>(this.controllerTest.validationModel);
    const postRequestBody = await this.httpRepository.jsonRequest(this.controllerTest.mainURL, "POST", mockModel);

    await postRequestBody.map(async (s) => {
      const id = instanceOfObjectAndHaveId(s);
      const getRequestBody = await this.httpRepository.jsonRequest(this.controllerTest.mainURL, "GET");
      await getRequestBody.map(async (el) => {
        if (el instanceof Array) {
          const firstElement = el.firstElement();
          const mockModelUpdate = ClassValidatorMocker.create<any>(this.controllerTest.validationModel);
          Object.assign(firstElement, mockModelUpdate);
          delete firstElement.__v;
          const putReqBody = await this.httpRepository.jsonRequest(this.controllerTest.mainURL, "PUT", firstElement);
          await putReqBody.map(async () => {
            (await this.httpRepository.jsonRequest(this.controllerTest.mainURL + "?id=" + id, "DELETE")).map(() => {
              result = true;
            });
          });
        }
      });
    });
    return result;
  }
}
