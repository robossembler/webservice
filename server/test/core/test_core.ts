import mongoose from "mongoose";
import { delay } from "../../src/core/helpers/delay";
import { Result } from "../../src/core/helpers/result";
import { TypedEvent } from "../../src/core/helpers/typed_event";
import { DropDataBaseUseCase } from "../../src/core/usecases/drop_database_usecase";
import { ExitAppUseCase } from "../../src/core/usecases/exit_app_usecase";

export const before = async () => {
  new DropDataBaseUseCase().call();
  new ExitAppUseCase().call();
};

export class TestCore {
  allTests = 0;
  testErr = 0;
  testOk = 0;
  private static _instance: TestCore;

  public static get instance() {
    return this._instance || (this._instance = new this());
  }

  assert = (test: boolean, testName: string) => {
    this.allTests += 1;
    if (test) {
      console.log("\x1b[32m", "✅  - " + testName);
      this.testOk += 1;
      return;
    }
    this.testErr += 1;
    console.log("\x1b[31m", "❌ -  " + testName);
  };

  testResult = async () => {
    console.log("\x1b[32m", "=============");

    if (this.allTests - this.testOk === 0) {
      console.log("\x1b[32m", `✅ All test success! ${this.allTests}/${this.testOk}`);
      return;
    }
    if (this.testErr !== 0) {
      console.log("\x1b[31m", "❌ test error:" + String(this.testErr));
      console.log("\x1b[32m", `✅ test success! ${this.testOk}`);
    }
    await before();
  };
  resultTest = async (
    eventClass: TypedEvent<Result<any, any>> | any,
    args: any,
    testName: string,
    isOk: boolean,
    delayTime = 1000
  ) => {
    let testIsOk = false;
    eventClass.call(...args);
    const listener = eventClass.on((e: { fold: (arg0: (_s: any) => void, arg1: (_e: any) => void) => void }) => {
      e.fold(
        () => {
          if (isOk) {
            testIsOk = true;
          }
        },
        () => {
          if (!isOk) {
            testIsOk = true;
          }
        }
      );
    });
    await delay(delayTime);
    this.assert(testIsOk, testName);
    listener.dispose();
  };
}
