import "reflect-metadata";
import { TestCore } from "./core/test_core";
import { dirname } from "path";
import { ExecutorProgramServiceTest } from "./services/executor_program_service_test";
import { FilesChangerTest } from "./services/files_change_notifier_service_test";
import { CreateDataBaseModelUseCaseTest } from "./usecases/create_database_model_usecase_test";
import { DeleteDataBaseModelUseCaseTest } from "./usecases/delete_database_model_usecase_test";
import { ReadDataBaseModelUseCaseTest } from "./usecases/read_database_model_usecase_test";
import { UpdateDataBaseModelUseCaseTest } from "./usecases/update_database_model_usecase";
import { PaginationDataBaseModelUseCaseTest } from "./usecases/pagination_database_model_usecase_test";
import { extensions } from "../src/core/extensions/extensions";
import { CrudControllerTest } from "./controllers/crud_controller_test";

import { App, Environment, ServerStatus } from "../src/core/controllers/app";
import { httpRoutes } from "../src/core/controllers/routes";
import { DataBaseConnectUseCase } from "../src/core/usecases/database_connect_usecase";

const testCore = TestCore.instance;
export const dirname__: string = dirname(__filename);
export const assert = testCore.assert;
export const resultTest = testCore.resultTest;

const tests = [
  CreateDataBaseModelUseCaseTest,
  DeleteDataBaseModelUseCaseTest,
  ReadDataBaseModelUseCaseTest,
  UpdateDataBaseModelUseCaseTest,
  PaginationDataBaseModelUseCaseTest,
];
const init = async () => {
  await new DataBaseConnectUseCase().call();
};

const unitTest = async () => {
  await init();
  await new ExecutorProgramServiceTest(dirname__).test();
  await new FilesChangerTest(dirname__).test();
  await new CreateDataBaseModelUseCaseTest().test();
  await new CreateDataBaseModelUseCaseTest().test();
  await new DeleteDataBaseModelUseCaseTest().test();
  await new ReadDataBaseModelUseCaseTest().test();
  await new UpdateDataBaseModelUseCaseTest().test();

  for await (const usecase of tests) {
    testCore.assert(await new usecase().test(), usecase.name);
  }
};
const presentationCrudControllers = [];
const e2eTest = async () => {
  const app = new App(httpRoutes, [], Environment.E2E_TEST);
  await new Promise((resolve, reject) => {
    app.on(async (e) => {
      if (e === ServerStatus.finished) {
        for await (const el of presentationCrudControllers) {
          testCore.assert(await new CrudControllerTest(app.port, el).call(), el.constructor.name);
        }
        resolve(e);
      }
      if (e === ServerStatus.error) {
        console.log(e);
        reject(e);
      }
    });
  });
};
const main = async () => {
  extensions();
  if (process.env.NODE_ENV === "unit") {
    await unitTest();
  }

  if (process.env.NODE_ENV === "e2e") {
    await e2eTest();
  }
  await testCore.testResult();
};

main();
