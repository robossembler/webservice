import * as fs from "fs";
import { FilesChangeNotifierService } from "../../src/core/services/files_change_notifier_service";
import { EventsFileChanger } from "../../src/core/models/meta_data_file_manager_model";
import { assert, dirname__ } from "../test";
import { delay } from "../../src/core/helpers/delay";

export class FilesChangerTest extends FilesChangeNotifierService {
  directory = dirname__ + "/context/";
  data = () => {
    return "This is a file containing a collection";
  };
  filePath = this.directory + "programming.txt";

  public hashUnitEqualTo(hashEqualValue: EventsFileChanger, testName: string) {
    let testIsOk = false;

    for (const [_key, value] of Object.entries(this.hashes)) {
      if ((value.event === hashEqualValue, testName)) {
        testIsOk = true;
      }
    }
    assert(testIsOk, testName);
  }
  public async createFile() {
    this.call();
    await delay(2000);
    fs.writeFileSync(this.filePath, this.data());
    await delay(1000);
    this.hashUnitEqualTo(EventsFileChanger.create, "FilesChangeNotifierService create file");

    this.cancel();
  }
  public async updateFile() {
    this.call();
    fs.writeFileSync(this.filePath, this.data() + "132321");
    await delay(1000);
    fs.writeFileSync(this.filePath, this.data() + "132");
    await delay(500);
    this.hashUnitEqualTo(EventsFileChanger.update, "FilesChangeNotifierService update file");
    this.cancel();
  }
  public async initFile() {
    this.init();
    await delay(500);
    this.hashUnitEqualTo(EventsFileChanger.static, "FilesChangeNotifierService init file");
  }
  public async deleteFile() {
    this.call();
    fs.unlinkSync(this.filePath);
    await delay(1000);
    this.hashUnitEqualTo(EventsFileChanger.delete, "FilesChangeNotifierService delete file");
    this.cancel();
  }
  public async notExistsDirectory() {
    await delay(1000);
    this.directory = "";
    const result = this.call();
    assert(result.isFailure(), "Not exists directory");
    this.directory = dirname__ + "/context/";
  }
  public async test() {
    await this.createFile();
    await this.updateFile();
    await this.initFile();
    await this.deleteFile();
    await this.notExistsDirectory();
    await this.testClear();
  }
  public testClear() {
    if (fs.existsSync(this.filePath)) {
      fs.unlinkSync(this.filePath);
    }
  }
}
