import { delay } from "../../src/core/helpers/delay";
import { EXEC_TYPE } from "../../src/core/models/exec_error_model";
import { ExecutorResult } from "../../src/core/models/executor_result";
import { ExecutorProgramService } from "../../src/core/services/executor_program_service";
import { TestCore } from "../core/test_core";
import { resultTest as resultTest, dirname__ } from "../test";
import { Worker } from "node:cluster";

export class ExecutorProgramServiceTest extends ExecutorProgramService {
  timeCancel = 1000;
  public test = async () => {
    await this.resultsTests();
    await this.longTimeCancelTest();
    await this.logWriteAndEventEndTestTypeExec();
    await this.logWriteAndEventEndTypeSpawn();
  };
  private async logWriteAndEventEndTypeSpawn() {
    const executorProgramService = await new ExecutorProgramService(dirname__ + "/");
    executorProgramService.call(EXEC_TYPE.SPAWN, "node", ["./mocks/log_code"]);
    const test = TestCore.instance;
    let testIsOk = false;
    let logEvent = false;

    executorProgramService.on((e) => {
      if (e.isSuccess()) {
        const executorResult = e.value as ExecutorResult;
        if (logEvent == false) {
          logEvent = executorResult.data != null && executorResult.data != undefined;
        }
        testIsOk = executorResult.event == "END" && logEvent;
      }
    });
    await delay(8000);
    test.assert(testIsOk, "ExecutorProgramService EXEC_TYPE.SPAWN end event and log write");
  }
  private async logWriteAndEventEndTestTypeExec() {
    const executorProgramService = await new ExecutorProgramService(dirname__);
    executorProgramService.call(EXEC_TYPE.EXEC, "node ./test/mocks/log_code");
    const test = TestCore.instance;
    executorProgramService.on((e) => {
      if (e.isSuccess()) {
        const executorResult = e.value as ExecutorResult;
        test.assert(
          executorResult.data != undefined && executorResult.event == "END",
          "ExecutorProgramService EXEC_TYPE.EXEC end event and log write"
        );
      }
    });
    await delay(7000);
  }
  private async longTimeCancelTest() {
    const executorProgramService = await new ExecutorProgramService("", 1000);
    executorProgramService.call(EXEC_TYPE.EXEC, "node ./test/mocks/long_code");
    await delay(1500);
    const worker = executorProgramService.worker as Worker;
    const test = TestCore.instance;
    test.assert(worker.isDead(), "ExecutorProgramService long time cancel");
  }
  private resultsTests = async () => {
    await resultTest(
      new ExecutorProgramService(dirname__),
      [EXEC_TYPE.EXEC, "node ./mocks/error"],
      "ExecutorProgramService EXEC_TYPE.EXEC on Result.error",
      false,
      4000
    );
    await delay(400);
    await resultTest(
      new ExecutorProgramService(dirname__),
      [EXEC_TYPE.EXEC, "ls"],
      "ExecutorProgramService EXEC_TYPE.EXEC on Result.ok",
      true
    );

    await resultTest(
      new ExecutorProgramService(dirname__),
      [EXEC_TYPE.SPAWN, "ls"],
      "ExecutorProgramService EXEC_TYPE.SPAWN on Result.ok",
      true
    );
    await resultTest(
      new ExecutorProgramService(dirname__),
      [EXEC_TYPE.SPAWN, "python3 ./mocks/s"],
      "ExecutorProgramService EXEC_TYPE.SPAWN on Result.error",
      false,
      2000
    );
    await resultTest(
      new ExecutorProgramService(dirname__),
      [EXEC_TYPE.SPAWN, "ls"],
      "ExecutorProgramService EXEC_TYPE.SPAWN on Result.ok",
      true,
      2000
    );
  };
}
