import { CreateDataBaseModelUseCase } from "../../src/core/usecases/create_database_model_usecase";
import { ReadByIdDataBaseModelUseCase } from "../../src/core/usecases/read_by_id_database_model_usecase";
import { ITestModel, TestDBModel } from "../model/test_db_mongo_model";

export class ReadDataBaseModelUseCaseTest {
  async test() {
    let testIsSuccess = false;

    const result = await new CreateDataBaseModelUseCase<ITestModel>(TestDBModel).call({
      result: "test",
    });
    await result.fold(
      async (s) => {
        const r = await new ReadByIdDataBaseModelUseCase<ITestModel>(TestDBModel).call(s.id);
        await r.fold(
          (_s1) => {
            testIsSuccess = true;
          },
          (_e) => {}
        );
      },
      async (_e) => {}
    );

    return testIsSuccess;
  }
}
