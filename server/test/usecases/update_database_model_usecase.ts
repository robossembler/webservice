import { CreateDataBaseModelUseCase } from "../../src/core/usecases/create_database_model_usecase";
import { UpdateDataBaseModelUseCase } from "../../src/core/usecases/update_database_model_usecase";
import { ITestModel, TestDBModel } from "../model/test_db_mongo_model";

export class UpdateDataBaseModelUseCaseTest {
  async test() {
    let testIsSuccess = false;

    const model = await new CreateDataBaseModelUseCase<ITestModel>(TestDBModel).call({
      result: "test",
    });
    await model.fold(
      async (s) => {
        (
          await new UpdateDataBaseModelUseCase<any, ITestModel>(TestDBModel).call({
            _id: s.id,
            result: "complete",
          })
        ).fold(
          (s1) => {
            testIsSuccess = s1.result === "complete";
          },
          (_e1) => {}
        );
      },
      async (_e) => {}
    );

    return testIsSuccess;
  }
}
