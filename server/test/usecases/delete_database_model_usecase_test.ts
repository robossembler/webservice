import { CreateDataBaseModelUseCase } from "../../src/core/usecases/create_database_model_usecase";
import { DeleteDataBaseModelUseCase } from "../../src/core/usecases/delete_database_model_usecase";
import { ITestModel, TestDBModel } from "../model/test_db_mongo_model";

export class DeleteDataBaseModelUseCaseTest {
  async test() {
    let testIsSuccess = false;

    const result = await new CreateDataBaseModelUseCase<ITestModel>(TestDBModel).call({
      result: "test",
    });

    await result.fold(
      async (s) => {
        (await new DeleteDataBaseModelUseCase(TestDBModel).call(s.id)).fold(
          (_s1) => {
            testIsSuccess = true;
          },
          (_e1) => {}
        );
      },
      async (_e) => {
        return;
      }
    );

    return testIsSuccess;
  }
}
