import { CreateDataBaseModelUseCase } from "../../src/core/usecases/create_database_model_usecase";
import { ITestModel, TestDBModel } from "../model/test_db_mongo_model";

export class CreateDataBaseModelUseCaseTest {
  async test() {
    return (
      await new CreateDataBaseModelUseCase<ITestModel>(TestDBModel).call({
        result: "test",
      })
    ).isSuccess();
  }
}
