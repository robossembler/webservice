import { PaginationDataBaseModelUseCase } from "../../src/core/usecases/pagination_database_model_usecase";
import { ITestModel, TestDBModel } from "../model/test_db_mongo_model";

export class PaginationDataBaseModelUseCaseTest {
  async test() {
    let testIsSuccess = false;
    await (
      await new PaginationDataBaseModelUseCase<ITestModel>(TestDBModel, 1).call(1)
    ).fold(
      (s) => {
        testIsSuccess = s.length === 1;
      },
      (_e) => {}
    );
    return testIsSuccess;
  }
}
