import "reflect-metadata";
import { App } from "./core/controllers/app";
import { SocketSubscriber } from "./core/controllers/socket_controller";
import { extensions } from "./core/extensions/extensions";
import { httpRoutes } from "./core/controllers/routes";
import { SpawnProcessUseCase, executorProgramService } from "./core/usecases/exec_process_usecase";
import { ProcessWatcher } from "./features/runtime/service/process_watcher";

extensions();

const socketSubscribers = [new SocketSubscriber(executorProgramService, "realtime")];

new App(httpRoutes, socketSubscribers).listen();
