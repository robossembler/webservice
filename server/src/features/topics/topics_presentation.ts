import { CrudController } from "../../core/controllers/crud_controller";
import { TopicDatabaseModel } from "./topic_database_model";
import { TopicValidationModel } from "./topic_validation_model";

export class TopicsPresentation extends CrudController<TopicValidationModel, typeof TopicDatabaseModel> {
  constructor() {
    super({
      url: "topics",
      validationModel: TopicValidationModel,
      databaseModel: TopicDatabaseModel,
    });
  }
}
