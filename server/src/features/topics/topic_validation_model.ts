import { Type } from "class-transformer";
import { IsEnum, IsString } from "class-validator";
import { ITopicModel } from "./topic_database_model";

export class TopicValidationModel implements ITopicModel {
  @IsString()
  name: string;
  @IsString()
  type: string;
}
