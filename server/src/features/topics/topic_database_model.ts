import { Schema, model } from "mongoose";
import { digitalTwinsInstanceSchema } from "../digital_twins_instance/model/digital_twins_instance_database_model";

export interface ITopicModel {
  digitalTwinId?: string;
  name: string;
  type: string;
}

export const TopicSchema = new Schema({
  name: {
    type: String,
  },
  type: {
    type: String,
  },
  digitalTwinId: {
    type: Schema.Types.ObjectId,
    ref: digitalTwinsInstanceSchema,
    autopopulate: true,
  },
}).plugin(require("mongoose-autopopulate"));

export const topicSchema = "topics";

export const TopicDatabaseModel = model<ITopicModel>(topicSchema, TopicSchema);
