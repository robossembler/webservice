import { Schema, model } from "mongoose";
import { FormBuilderValidationModel } from "../../datasets/models/dataset_validation_model";
import { IProjectModel, projectSchema } from "../../projects/models/project_model_database_model";

export interface ICalculationInstance {
  script: string;
  instancePath: string;
  formBuilder: FormBuilderValidationModel;
  type: string;
  name: string;
  isEnd: boolean;
  createDate: Date;
  card?: string;
  lastProcessLogs?: string;
  lastProcessExecCommand?: string;
  lastExecDate?: Date;
  processStatus: string;
  project?: IProjectModel | string;
}

export const CalculationInstanceSchema = new Schema({
  script: {
    type: String,
  },
  formBuilder: {
    type: Schema.Types.Mixed,
  },
  type: {
    type: String,
  },
  instanceName: {
    type: String,
  },
  name: {
    type: String,
  },
  isEnd: {
    type: Boolean,
  },
  createDate: {
    type: String,
  },
  card: {
    type: String,
  },
  lastProcessLogs: {
    type: String,
  },
  lastProcessExecCommand: {
    type: String,
  },
  lastExecDate: {
    type: String,
  },
  processStatus: {
    type: String,
  },
  path: {
    type: String,
  },
  instancePath: {
    type: String,
  },
  project: {
    type: Schema.Types.ObjectId,
    ref: projectSchema,
    autopopulate: true,
    require: true,
  },
}).plugin(require("mongoose-autopopulate"));

export const calculationInstanceSchema = "calculations_instances";

export const CalculationInstanceDBModel = model<ICalculationInstance>(
  calculationInstanceSchema,
  CalculationInstanceSchema
);
