import { IsNotEmpty, IsString, IsMongoId } from "class-validator";
import { ICalculationInstance } from "./calculations_instance_database_model";
import { FormBuilderValidationModel } from "../../datasets/models/dataset_validation_model";
import { IProjectModel } from "../../projects/models/project_model_database_model";

export class CalculationInstanceValidationModel implements ICalculationInstance {
  @IsNotEmpty()
  @IsString()
  instanceName: string;
  instancePath: string;
  processStatus: string;
  @IsString()
  path: string;
  project?: string | IProjectModel;
  @IsString()
  @IsNotEmpty()
  script: string;
  formBuilder: FormBuilderValidationModel;
  @IsString()
  @IsNotEmpty()
  type: string;
  @IsString()
  @IsNotEmpty()
  name: string;
  isEnd: boolean = false;
  createDate: Date = new Date();
  card?: string;
  lastProcessLogs?: string;
  lastProcessExecCommand?: string;
  lastExecDate?: Date;
}
