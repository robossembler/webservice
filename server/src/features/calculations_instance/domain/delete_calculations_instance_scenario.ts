import { DeleteInstanceScenario } from "../../../core/scenarios/delete_instance_scenario";
import { CalculationInstanceDBModel } from "../models/calculations_instance_database_model";
import { CalculationInstanceValidationModel } from "../models/calculations_instance_validation_model";

export class DeleteCalculationsInstanceScenario extends DeleteInstanceScenario<CalculationInstanceValidationModel> {
  databaseModel = CalculationInstanceDBModel;
}
