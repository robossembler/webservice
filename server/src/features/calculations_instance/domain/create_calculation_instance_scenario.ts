import { CreateInstanceScenario } from "../../../core/scenarios/create_intance_scenario";
import { CalculationInstanceDBModel } from "../models/calculations_instance_database_model";
import { CalculationInstanceValidationModel } from "../models/calculations_instance_validation_model";

export class CreateCalculationInstanceScenario extends CreateInstanceScenario<CalculationInstanceValidationModel> {
  databaseModel = CalculationInstanceDBModel;
  validationModel: CalculationInstanceValidationModel = new CalculationInstanceValidationModel();
}
