import { CrudController } from "../../core/controllers/crud_controller";
import { ExecCalculationInstanceProcessScenario } from "./domain/exec_calculations_instance_process_scenario";
import { CalculationInstanceValidationModel } from "./models/calculations_instance_validation_model";
import { CalculationInstanceDBModel } from "./models/calculations_instance_database_model";
import { CreateCalculationInstanceScenario } from "./domain/create_calculation_instance_scenario";
import { DeleteCalculationsInstanceScenario } from "./domain/delete_calculations_instance_scenario";

export class CalculationsInstancesPresentation extends CrudController<
  CalculationInstanceValidationModel,
  typeof CalculationInstanceDBModel
> {
  constructor() {
    super({
      url: "calculations/instances",
      validationModel: CalculationInstanceValidationModel,
      databaseModel: CalculationInstanceDBModel,
    });

    super.delete(new DeleteCalculationsInstanceScenario().call);
    super.post(new CreateCalculationInstanceScenario().call);
    
    this.subRoutes.push({
      method: "GET",
      subUrl: "exec",
      fn: new ExecCalculationInstanceProcessScenario(),
    });
  }
}
