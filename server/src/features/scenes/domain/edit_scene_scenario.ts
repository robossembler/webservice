import { CallbackStrategyWithValidationModel, ResponseBase } from "../../../core/controllers/http_controller";
import { Result } from "../../../core/helpers/result";
import { StaticFilesProject } from "../../../core/models/static_files";
import { CreateFileUseCase } from "../../../core/usecases/create_file_usecase";
import { SearchManyDataBaseModelUseCase } from "../../../core/usecases/search_many_database_model_usecase";
import { IProjectModel, ProjectDBModel } from "../../projects/models/project_model_database_model";
import { SceneValidationModel } from "../model/scene_validation_model";

export class EditSceneScenario extends CallbackStrategyWithValidationModel<SceneValidationModel> {
  validationModel: SceneValidationModel;
  call = async (model: SceneValidationModel): ResponseBase =>
    (
      await new SearchManyDataBaseModelUseCase<IProjectModel>(ProjectDBModel).call(
        { isActive: true },
        "is dont active projects"
      )
    ).map(async (projectModel) => {
      const { rootDir } = projectModel[0];

      return (
        await new CreateFileUseCase().call(
          `${rootDir}${StaticFilesProject.scenes}${model.name}.json`,
          Buffer.from(JSON.stringify(model))
        )
      ).map(async () => Result.ok("Edit"));
    });
}
