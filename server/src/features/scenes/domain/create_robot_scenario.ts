import { CallbackStrategyWithValidationModel, ResponseBase } from "../../../core/controllers/http_controller";
import { Result } from "../../../core/helpers/result";
import { RobotModel } from "../model/robot_model";
import { GetServerAddressUseCase } from "../../../core/usecases/get_server_address_usecase";
import { SearchManyDataBaseModelUseCase } from "../../../core/usecases/search_many_database_model_usecase";
import { IProjectModel, ProjectDBModel } from "../../projects/models/project_model_database_model";
import { ExecProcessUseCase } from "../../../core/usecases/exec_process_usecase";
import { StaticFilesProject } from "../../../core/models/static_files";

export class CreateRobotScenario extends CallbackStrategyWithValidationModel<RobotModel> {
  validationModel: RobotModel = new RobotModel();
  call = async (model: RobotModel): ResponseBase =>
    (
      await new SearchManyDataBaseModelUseCase<IProjectModel>(ProjectDBModel).call(
        { isActive: true },
        "is dont active projects"
      )
    ).map((projectModel) => {
      const { rootDir } = projectModel[0];
  
      return new GetServerAddressUseCase().call().map(async (serverAddress) =>
        (
          await new ExecProcessUseCase().call(
            rootDir,
            `python3 $PYTHON_ROBOT_BUILDER --path  ${
              projectModel[0].rootDir + "/" + StaticFilesProject.robots + "/"
            }  --name ${model.name} --nDOF ${model.nDof} --toolType ${model.toolType}`,
            ""
          )
        ).map((log) =>
          Result.ok({
            robotUrl: `${serverAddress}/${
              rootDir.match(new RegExp(/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/gm))[0]
            }/${StaticFilesProject.robots}/${model.name}/robot.xml`,
          })
        )
      );
    });
}

// PYTHON_ROBOT_BUILDER
