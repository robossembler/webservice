import { CallbackStrategyWithIdQuery, ResponseBase } from "../../../core/controllers/http_controller";
import { Result } from "../../../core/helpers/result";
import { StaticFilesProject } from "../../../core/models/static_files";
import { DeleteDataBaseModelUseCase } from "../../../core/usecases/delete_database_model_usecase";
import { DeleteFileUseCase } from "../../../core/usecases/delete_file_usecase";
import { ReadByIdDataBaseModelUseCase } from "../../../core/usecases/read_by_id_database_model_usecase";
import { SearchManyDataBaseModelUseCase } from "../../../core/usecases/search_many_database_model_usecase";
import { CoreValidation } from "../../../core/validations/core_validation";
import { MongoIdValidation } from "../../../core/validations/mongo_id_validation";
import { IProjectModel, ProjectDBModel } from "../../projects/models/project_model_database_model";
import { SceneModel } from "./create_new_scene_scenario";

export class DeleteSceneScenario extends CallbackStrategyWithIdQuery {
  idValidationExpression: CoreValidation = new MongoIdValidation();
  call = async (id: string): ResponseBase =>
    (await new ReadByIdDataBaseModelUseCase<SceneModel>(SceneModel).call(id)).map(async (sceneModel) =>
      (await new DeleteDataBaseModelUseCase(SceneModel).call(id)).map(async () =>
        (
          await new SearchManyDataBaseModelUseCase<IProjectModel>(ProjectDBModel).call(
            { isActive: true },
            "is dont active projects"
          )
        ).map(async (projectModel) => {
          const { rootDir } = projectModel[0];

          (await new DeleteFileUseCase()).call(`${rootDir}${StaticFilesProject.scenes}${sceneModel.name}.json`);
          return Result.ok("Delete scene");
        })
      )
    );
}
