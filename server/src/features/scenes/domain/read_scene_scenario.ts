import { CallbackStrategyWithIdQuery, ResponseBase } from "../../../core/controllers/http_controller";
import { Result } from "../../../core/helpers/result";
import { StaticFilesProject } from "../../../core/models/static_files";
import { ReadByIdDataBaseModelUseCase } from "../../../core/usecases/read_by_id_database_model_usecase";
import { ReadFileAndParseJsonUseCase } from "../../../core/usecases/read_file_and_parse_json";
import { SearchManyDataBaseModelUseCase } from "../../../core/usecases/search_many_database_model_usecase";
import { CoreValidation } from "../../../core/validations/core_validation";
import { MongoIdValidation } from "../../../core/validations/mongo_id_validation";
import { IProjectModel, ProjectDBModel } from "../../projects/models/project_model_database_model";
import { SceneModel } from "./create_new_scene_scenario";
import { SceneDBModel } from "../model/scene_database_model";

export class ReadSceneScenario extends CallbackStrategyWithIdQuery {
  idValidationExpression: CoreValidation = new MongoIdValidation();
  call = async (id: string): ResponseBase =>
    (await new ReadByIdDataBaseModelUseCase<SceneModel>(SceneDBModel).call(id)).map(async (model) =>
      (
        await new SearchManyDataBaseModelUseCase<IProjectModel>(ProjectDBModel).call(
          { isActive: true },
          "is dont active projects"
        )
      ).map(async (projectModel) => {
        const { rootDir } = projectModel[0];

        return (await new ReadFileAndParseJsonUseCase().call(`${rootDir}${StaticFilesProject.scenes}${model.name}.json`)).map(
          async (sceneAsset) => Result.ok(sceneAsset)
        );
      })
    );
}
