import { IsString } from "class-validator";
import { CallbackStrategyWithValidationModel, ResponseBase } from "../../../core/controllers/http_controller";
import { Result } from "../../../core/helpers/result";
import { CreateFileUseCase } from "../../../core/usecases/create_file_usecase";
import { SearchManyDataBaseModelUseCase } from "../../../core/usecases/search_many_database_model_usecase";
import { IProjectModel, ProjectDBModel } from "../../projects/models/project_model_database_model";
import { StaticFilesProject } from "../../../core/models/static_files";
import { CreateDataBaseModelUseCase } from "../../../core/usecases/create_database_model_usecase";
import { SceneDBModel } from "../model/scene_database_model";
export class SceneModel {
  @IsString()
  name: string;
}
export class SceneAssets {
  name: string;
  scene: [] = [];
  constructor(name: string) {
    this.name = name;
  }
  static empty = () => new SceneAssets("");
}
export class CreateNewSceneScenario extends CallbackStrategyWithValidationModel<SceneModel> {
  validationModel: SceneModel = SceneModel;

  call = async (model: SceneModel): ResponseBase =>
    (
      await new SearchManyDataBaseModelUseCase<IProjectModel>(ProjectDBModel).call(
        { isActive: true },
        "is dont active projects"
      )
    ).map(async (projectModel) => {
      const { rootDir } = projectModel[0];

      return (
        await new CreateFileUseCase().call(
          `${rootDir}${StaticFilesProject.scenes}${model.name}.json`,
          Buffer.from(JSON.stringify(new SceneAssets(model.name)))
        )
      ).map(async () =>
        (await new CreateDataBaseModelUseCase(SceneDBModel).call(model)).map(() => Result.ok("Create"))
      );
    });
}
