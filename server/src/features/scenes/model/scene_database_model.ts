import { Schema, model } from "mongoose";
export interface IScene {
  name: string;
}
export const SceneSchema = new Schema({
  name: {
    type: String,
  },
}).plugin(require("mongoose-autopopulate"));

export const schemaSceneName = "Scene";

export const SceneDBModel = model<IScene>(schemaSceneName, SceneSchema);
