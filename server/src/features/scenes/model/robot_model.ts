import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class RobotModel {
  @IsNotEmpty()
  @IsString()
  name: string;
  @IsNumber()
  nDof: number;
  @IsNotEmpty()
  @IsString()
  toolType: string;
}
