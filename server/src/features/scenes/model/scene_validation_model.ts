import { IsString } from "class-validator";
import { IScene } from "./scene_database_model";

export class SceneValidationModel implements IScene {
  @IsString()
  public name: string;
}
