import { CrudController } from "../../core/controllers/crud_controller";
import { CreateNewSceneScenario } from "./domain/create_new_scene_scenario";
import { CreateRobotScenario } from "./domain/create_robot_scenario";
import { EditSceneScenario } from "./domain/edit_scene_scenario";
import { ReadSceneScenario } from "./domain/read_scene_scenario";
import { SceneDBModel } from "./model/scene_database_model";
import { SceneValidationModel } from "./model/scene_validation_model";

export class ScenesPresentation extends CrudController<SceneValidationModel, typeof SceneDBModel> {
  constructor() {
    super({
      url: "scenes",
      validationModel: SceneValidationModel,
      databaseModel: SceneDBModel,
    });

    super.post(new CreateNewSceneScenario().call);
    super.put(new EditSceneScenario().call);
    this.subRoutes.push({
      method: "POST",
      subUrl: "create/robot",
      fn: new CreateRobotScenario(),
    });
    this.subRoutes.push({
      method: "GET",
      subUrl: "by_id",
      fn: new ReadSceneScenario(),
    });
  }
}
