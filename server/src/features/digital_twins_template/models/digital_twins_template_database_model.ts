import { Schema, model } from "mongoose";

export interface IDigitalTwinsTemplateTemplateModel {}

export const DigitalTwinsTemplateSchema = new Schema({
  path: String,
  name: String,
  entity: String,
  description: String,
  command: String,
  package: {
    type: String,
  },
  executable: {
    type: String,
  },
  interfaces: {
    cmd: String,
  },
  formBuilder: {
    type: Schema.Types.Mixed,
  },
}).plugin(require("mongoose-autopopulate"));

export const digitalTwinsTemplateSchema = "digital_twins_templates";

export const DigitalTwinsTemplateDBModel = model<IDigitalTwinsTemplateTemplateModel>(
  digitalTwinsTemplateSchema,
  DigitalTwinsTemplateSchema
);
