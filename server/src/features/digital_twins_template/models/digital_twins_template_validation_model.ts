import { Type } from "class-transformer";
import { IsEnum, IsString } from "class-validator";

enum DigitalTwinsTypes {
  CAMERA = "CAMERA",
  ROBOT = "ROBOT",
}
export class Interfaces {
  @IsString()
  cmd: string;
}
export class DigitalTwinsTemplateValidationModel {
  path: string;
  name: string;
  @IsEnum(DigitalTwinsTypes)
  entity: DigitalTwinsTypes;
  @IsString()
  description: string;
  @IsString()
  command: string;
  @Type(() => Interfaces)
  interfaces: Interfaces;
}
