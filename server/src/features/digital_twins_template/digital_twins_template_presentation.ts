import { CrudController } from "../../core/controllers/crud_controller";
import { CreateDigitalTwinsTemplateScenario } from "./domain/create_digital_twins_template";
import { DeleteDigitalTwinsTemplateScenario } from "./domain/delete_digital_twins_template";
import { DigitalTwinsTemplateDBModel } from "./models/digital_twins_template_database_model";
import { DigitalTwinsTemplateValidationModel } from "./models/digital_twins_template_validation_model";

export class DigitalTwinsTemplatePresentation extends CrudController<
  DigitalTwinsTemplateValidationModel,
  typeof DigitalTwinsTemplateDBModel
> {
  constructor() {
    super({
      url: "digital/twins/template",
      validationModel: DigitalTwinsTemplateValidationModel,
      databaseModel: DigitalTwinsTemplateDBModel,
    });
    super.post(new CreateDigitalTwinsTemplateScenario().call);
    super.delete(new DeleteDigitalTwinsTemplateScenario().call);
  }
}
