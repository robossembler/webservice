import { StaticFilesServer } from "../../../core/models/static_files";
import { CreateTemplateScenario } from "../../../core/scenarios/create_template_scenario";
import { DigitalTwinsTemplateDBModel } from "../models/digital_twins_template_database_model";
import { DigitalTwinsTemplateValidationModel } from "../models/digital_twins_template_validation_model";

export class CreateDigitalTwinsTemplateScenario extends CreateTemplateScenario<DigitalTwinsTemplateValidationModel> {
  validationModel: DigitalTwinsTemplateValidationModel = new DigitalTwinsTemplateValidationModel();
  databaseModel = DigitalTwinsTemplateDBModel;
  path: string = StaticFilesServer.digitalTwins;
}
