import { DeleteTemplateScenario } from "../../../core/scenarios/delete_template_scenario";
import { DigitalTwinsTemplateDBModel } from "../models/digital_twins_template_database_model";
 
export class DeleteDigitalTwinsTemplateScenario extends DeleteTemplateScenario {
  databaseModel = DigitalTwinsTemplateDBModel;
}
