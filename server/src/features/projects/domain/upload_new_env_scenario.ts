import { CallbackStrategyWithFilesUploads, ResponseBase } from "../../../core/controllers/http_controller";
import { Result } from "../../../core/helpers/result";
import { CreateFileUseCase } from "../../../core/usecases/create_file_usecase";
import { ReadFileAndParseJsonUseCase } from "../../../core/usecases/read_file_and_parse_json";
export interface Parts {
  name: string;
  part_path: string;
  material_path: string;
  mass: number;
  inertia: {
    ixx: number;
    ixy: number;
    ixz: number;
    iyy: number;
    iyz: number;
    izz: number;
  };
  visual: string;
  collision: string;
  type: string;
}
export class UploadNewEnvScenario {
  call = async (
    files: { name: string; file?: Buffer }[],
    name: string,
    inertia: number,
    mass: number,
    projectPath: string
  ): ResponseBase => {
    await files.map(
      async (el) => await new CreateFileUseCase().call(projectPath + `/assets/libs/objects/${name}.${el.name}`, el.file)
    );
    // "part_path": "parts/objects/body_down.stl",
    await new CreateFileUseCase().call(
      projectPath + `/parts/objects/${name}.${files.find((el) => el.name.isEqual("stl")).name}`,
      files.find((el) => el.name.isEqual("stl")).file
    );
    return (await new ReadFileAndParseJsonUseCase().call<Parts[]>(projectPath + "/assets/parts.json")).map(
      async (el) => {
        el.push({
          name,
          inertia: {
            ixx: 0.1,
            ixy: 0,
            ixz: 0,
            iyy: 0.1,
            iyz: 0,
            izz: 0.1,
          },
          mass,
          visual: `/assets/libs/objects/${name}.dae`,
          collision: `/assets/libs/objects/${name}.stl`,
          type: "env",
          material_path: "",
          part_path: `/libs/objects/${name}.stl`,
        });
        await new CreateFileUseCase().call(projectPath + "/assets/parts.json", Buffer.from(JSON.stringify(el), "utf8"));
      }
    );
  };
}
