import { Result } from "../../../core/helpers/result";
import { ActivePipeline } from "../../../core/models/active_pipeline_model";
import { pipelineRealTimeService } from "../../../core/scenarios/set_active_pipeline_to_realtime_service_scenario";
 
export class PipelineStatusUseCase {
  async call(): Promise<Result<Error, ActivePipeline>> {
    try {
      const status = pipelineRealTimeService.status;
      if (status.projectId !== null) {
        return Result.ok(status);
      }

      if (status.projectId === null) {
        return Result.error(new Error("pipelineRealTimeService does not have an active project instance"));
      }
    } catch (error) {
      return Result.error(error as Error);
    }
  }
}

