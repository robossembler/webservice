import { CallbackStrategyWithEmpty, ResponseBase } from "../../../core/controllers/http_controller";
import { Result } from "../../../core/helpers/result";
import { StaticFilesProject } from "../../../core/models/static_files";
import { GetServerAddressUseCase } from "../../../core/usecases/get_server_address_usecase";
import { ReadFileAndParseJsonUseCase } from "../../../core/usecases/read_file_and_parse_json";
import { SearchManyDataBaseModelUseCase } from "../../../core/usecases/search_many_database_model_usecase";
import { IProjectModel, ProjectDBModel } from "../models/project_model_database_model";
export interface Parts {
  name: string;
  part_path: string;
  material_path: string;
  stlUrl: string;
  image: string;
  glUrl: string;
  daeUrl: string;
  objUrl: string;
  solidType: string;
}

export class RobossemblerAssetsNetworkMapperScenario extends CallbackStrategyWithEmpty {
  call = async (): ResponseBase =>
    (
      await new SearchManyDataBaseModelUseCase<IProjectModel>(ProjectDBModel).call(
        { isActive: true },
        "is dont active projects"
      )
    ).map((projectModel) => {
      const { rootDir } = projectModel[0];
      return new GetServerAddressUseCase().call().map(async (serverAddress) =>
        (await new ReadFileAndParseJsonUseCase().call<Parts[]>(rootDir + StaticFilesProject.parts)).map((model) => {
          const assetAddress =
            serverAddress +
            "/" +
            rootDir.match(new RegExp(/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/gm))[0] +
            "/assets/";
          model.map((el) => {
            const assetLibsAddress = assetAddress + "/libs/objects/" + el.name;
            console.log(assetLibsAddress);
            el.stlUrl = `${assetAddress}${el.part_path}`;
            el.glUrl = `${assetLibsAddress}.glb`;
            el.daeUrl = `${assetLibsAddress}.dae`;
            el.objUrl = `${assetLibsAddress}.obj`;
            el.image = `${assetLibsAddress}.png`;
            el.solidType = 'active'
            return el;
          });
          return Result.ok(model);
        })
      );
    });
}
