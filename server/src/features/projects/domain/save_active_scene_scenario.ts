import { CallbackStrategyWithValidationModel, ResponseBase } from "../../../core/controllers/http_controller";
import { Result } from "../../../core/helpers/result";
import { RobossemblerAssets } from "../../../core/models/robossembler_assets";
import { StaticFilesProject } from "../../../core/models/static_files";
import { ReadingJsonFileAndConvertingToInstanceClassScenario } from "../../../core/scenarios/read_file_and_json_to_plain_instance_class_scenario";
import { WriteFileSystemFileUseCase } from "../../../core/usecases/write_file_system_file_usecase";
import { PipelineStatusUseCase } from "./pipeline_status_usecase";
 
export class SaveActiveSceneScenario extends CallbackStrategyWithValidationModel<RobossemblerAssets> {
  validationModel: RobossemblerAssets = new RobossemblerAssets();
  async call(model: RobossemblerAssets): ResponseBase {
    return (await new PipelineStatusUseCase().call()).map(async (activeInstanceModel) =>
      (
        await new ReadingJsonFileAndConvertingToInstanceClassScenario(RobossemblerAssets).call(
          `${activeInstanceModel.path}${StaticFilesProject.robossembler_assets}`.pathNormalize()
        )
      ).map(async (prevModel) => {
        model.assets = prevModel.assets;
        return (
          await new WriteFileSystemFileUseCase().call(
            `${activeInstanceModel.path}${StaticFilesProject.robossembler_assets}`.pathNormalize(),
            JSON.stringify(model)
          )
        ).map(() => Result.ok("assets is rewrite"));
      })
    );
  }
}
