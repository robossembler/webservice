import { CallbackStrategyWithFileUpload, ResponseBase } from "../../../core/controllers/http_controller";
import { IFile } from "../../../core/interfaces/file";
import { CreateFileUseCase } from "../../../core/usecases/create_file_usecase";
import { MongoIdValidation } from "../../../core/validations/mongo_id_validation";
import { ReadByIdDataBaseModelUseCase } from "../../../core/usecases/read_by_id_database_model_usecase";
import { ExecProcessUseCase } from "../../../core/usecases/exec_process_usecase";
import { Result } from "../../../core/helpers/result";
import { CreateManyFolderScenario } from "../../../core/scenarios/create_many_folder_scenario";
import { IProjectModel, ProjectDBModel } from "../models/project_model_database_model";

export enum FolderStructure {
  assets = "assets",
  weights = "weights",
  datasets = "datasets",
  behaviorTrees = "behavior_trees",
  robots = "robots",
  scenes = "scenes",
}

export class UploadCadFileToProjectScenario extends CallbackStrategyWithFileUpload {
  checkingFileExpression: RegExp = new RegExp(".FCStd");
  idValidationExpression = new MongoIdValidation();

  async call(file: IFile, id: string): ResponseBase {
    return (await new ReadByIdDataBaseModelUseCase<IProjectModel>(ProjectDBModel).call(id)).map(async (databaseModel) =>
      (await new CreateFileUseCase().call(`${databaseModel.rootDir}/${file.name}`, file.data)).map(async () =>
        (
          await new ExecProcessUseCase().call(
            `${databaseModel.rootDir}/`,
            `python3 $PYTHON_BLENDER --path '${databaseModel.rootDir}/assets/'`,
            ""
          )
        ).map(async () =>
          (
            await new CreateManyFolderScenario().call(
              databaseModel.rootDir,
              Object.keys(FolderStructure).map((el) => `/${el}`)
            )
          ).map(() => Result.ok("file upload and save"))
        )
      )
    );
  }
}
