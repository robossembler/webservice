import { CallbackStrategyWithEmpty } from "../../../core/controllers/http_controller";
import { Result } from "../../../core/helpers/result";
import { SearchOneDataBaseModelUseCase } from "../../../core/usecases/search_database_model_usecase";
import { IProjectModel, ProjectDBModel } from "../models/project_model_database_model";

export class GetActiveProjectIdScenario extends CallbackStrategyWithEmpty {
  async call(): Promise<Result<any, { id: string }>> {
    return (
      await new SearchOneDataBaseModelUseCase<IProjectModel>(ProjectDBModel).call({ isActive: true }, "no active projects")
    ).map((model) => Result.ok({ id: model._id }));
  }
}
