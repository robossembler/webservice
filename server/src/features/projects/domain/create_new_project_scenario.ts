import { App } from "../../../core/controllers/app";
import { Result } from "../../../core/helpers/result";
import { v4 as uuidv4 } from "uuid";
import { IsString } from "class-validator";
import { CreateFolderUseCase } from "../../../core/usecases/create_folder_usecase";
import { ProjectDBModel } from "../models/project_model_database_model";

export class ProjectValidationModel {
  @IsString()
  description: string;
}
export class CreateNewProjectInstanceScenario {
  call = async (model: ProjectValidationModel): Promise<Result<any, any>> => {
    try {
      const projectFolder = uuidv4();
      return (await new CreateFolderUseCase().call(App.staticFilesStoreDir() + projectFolder)).map(async (_) => {
        for await (const el of await ProjectDBModel.find({ isActive: true })) {
          el.isActive = false;
          await el.save();
        }

        const projectDbModel = await new ProjectDBModel({
          isActive: true,
          rootDir: App.staticFilesStoreDir() + projectFolder,
          description: model.description,
        }).save();
        return Result.ok({ id: projectDbModel._id });
      });
    } catch (error) {
      return Result.error(error as Error);
    }
  };
}
