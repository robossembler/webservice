import { App } from "../../../core/controllers/app";
import { CallbackStrategyWithIdQuery, ResponseBase } from "../../../core/controllers/http_controller";
import { Result } from "../../../core/helpers/result";
import { SetLastActivePipelineToRealTimeServiceScenario } from "../../../core/scenarios/set_active_pipeline_to_realtime_service_scenario";
import { CreateFolderUseCase } from "../../../core/usecases/create_folder_usecase";
import { ReadByIdDataBaseModelUseCase } from "../../../core/usecases/read_by_id_database_model_usecase";
import { UpdateDataBaseModelUseCase } from "../../../core/usecases/update_database_model_usecase";
import { MongoIdValidation } from "../../../core/validations/mongo_id_validation";
import { IProjectModel, ProjectDBModel } from "../models/project_model_database_model";

export class SetActiveProjectScenario extends CallbackStrategyWithIdQuery {
  idValidationExpression = new MongoIdValidation();

  call = async (id: string): ResponseBase =>
    await (
      await new ReadByIdDataBaseModelUseCase<IProjectModel>(ProjectDBModel).call(id)
    ).map(
      async (model) =>
        await (
          await new CreateFolderUseCase().call(model.rootDir)
        ).map(async () => {
          model.isActive = true;
          return (await new UpdateDataBaseModelUseCase(ProjectDBModel).call(model)).map(async (el) => {
            await ProjectDBModel.updateMany({ _id: { $ne: el._id }, isActive: { $eq: true } }, { isActive: false });
            await new SetLastActivePipelineToRealTimeServiceScenario().call();
            return Result.ok(`project ${id} is active`);
          });
        })
    );
}
