import { CallbackStrategyWithEmpty, ResponseBase } from "../../../core/controllers/http_controller";
import { Result } from "../../../core/helpers/result";
import { StaticFilesProject } from "../../../core/models/static_files";
import { ReadFileAndParseJsonUseCase } from "../../../core/usecases/read_file_and_parse_json";
import { SearchManyDataBaseModelUseCase } from "../../../core/usecases/search_many_database_model_usecase";
import { IProjectModel, ProjectDBModel } from "../models/project_model_database_model";

export interface Parts {
  name: string;
  part_path: string;
  material_path: string;
  stlUrl: string;
  image: string;
  glUrl: string;
  daeUrl: string;
  fbx: string;
  objUrl: string;
  solidType: string;
}

export class RobossemblerAssetsLocaleMapperScenario extends CallbackStrategyWithEmpty {
  call = async (): ResponseBase =>
    (
      await new SearchManyDataBaseModelUseCase<IProjectModel>(ProjectDBModel).call(
        { isActive: true },
        "is dont active projects"
      )
    ).map(async (projectModel) => {
      const { rootDir } = projectModel[0];
      return (await new ReadFileAndParseJsonUseCase().call<Parts[]>(rootDir + StaticFilesProject.parts)).map((model) =>
        Result.ok(
          model.map((el) => {
            const assetLibsAddress = rootDir + "/assets/libs/objects/" + el.name;
            el.fbx = `${assetLibsAddress}.fbx`;
            el.stlUrl = `${assetLibsAddress}${el.part_path}`;
            el.glUrl = `${assetLibsAddress}.glb`;
            el.daeUrl = `${assetLibsAddress}.dae`;
            el.objUrl = `${assetLibsAddress}.obj`;
            el.image = `${assetLibsAddress}.png`;
            el.solidType = "active";

            return el;
          })
        )
      );
    
    });
}
