import { CrudController } from "../../core/controllers/crud_controller";
import { SearchManyDataBaseModelUseCase } from "../../core/usecases/search_many_database_model_usecase";
import { CreateNewProjectInstanceScenario, ProjectValidationModel } from "./domain/create_new_project_scenario";
import { GetActiveProjectIdScenario } from "./domain/get_active_project_id_scenario";
import { RobossemblerAssetsLocaleMapperScenario } from "./domain/robossembler_assets_absolute_path_mapper_scenario";
import { RobossemblerAssetsNetworkMapperScenario } from "./domain/robossembler_assets_network_mapper_scenario";
import { SetActiveProjectScenario } from "./domain/set_active_project_use_scenario";
import { UploadCadFileToProjectScenario } from "./domain/upload_file_to_to_project_scenario";
import { UploadNewEnvScenario } from "./domain/upload_new_env_scenario";
import { IProjectModel, ProjectDBModel } from "./models/project_model_database_model";

export class ProjectsPresentation extends CrudController<ProjectValidationModel, typeof ProjectDBModel> {
  constructor() {
    super({
      validationModel: ProjectValidationModel,
      url: "projects",
      databaseModel: ProjectDBModel,
    });

    super.post(new CreateNewProjectInstanceScenario().call);

    this.subRoutes.push({
      method: "POST",
      subUrl: "set/active/project",
      fn: new SetActiveProjectScenario(),
    });

    this.subRoutes.push({
      method: "GET",
      subUrl: "get/active/project/id",
      fn: new GetActiveProjectIdScenario(),
    });
    this.subRoutes.push({
      method: "POST",
      subUrl: "upload",
      fn: new UploadCadFileToProjectScenario(),
    });
    this.subRoutes.push({
      method: "GET",
      subUrl: "assets",
      fn: new RobossemblerAssetsNetworkMapperScenario(),
    });
    this.subRoutes.push({
      method: "GET",
      subUrl: "local/assets",
      fn: new RobossemblerAssetsLocaleMapperScenario(),
    });
     
    this.router.post("/projects/upload/env", async (req, res) => {
      try {
      } catch (error) {}
      (
        await new SearchManyDataBaseModelUseCase<IProjectModel>(ProjectDBModel).call(
          { isActive: true },
          "is dont active projects"
        )
      ).map(async (projectModel) => {
        const { rootDir } = projectModel[0];
        const files: { name: string; file?: Buffer }[] = [
          { name: "blend" },
          { name: "fbx" },
          { name: "ply" },
          { name: "glb" },
          { name: "dae" },
          { name: "fbx" },
          { name: "png" },
          { name: "stl" },
        ];
        // @ts-ignore
        const reqFiles = req.files;

        files.forEach((el) => {
          reqFiles[el.name].data;
          el.file = reqFiles[el.name].data;
        });

        await new UploadNewEnvScenario().call(files, req.body.name, req.body.inertia, req.body.mass, rootDir);
        return res.status(200).json("");
      });
    });
  }
}
