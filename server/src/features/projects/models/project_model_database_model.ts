import { Schema, model } from "mongoose";

export interface IProjectModel {
  _id?: string;
  rootDir: string;
  description: string;
  isActive: boolean;
}

export const ProjectSchema = new Schema({
  description: {
    type: String,
  },
  rootDir: {
    type: String,
  },
  isActive: {
    type: Boolean,
    default: false,
  },
}).plugin(require("mongoose-autopopulate"));

export const projectSchema = "Projects";

export const ProjectDBModel = model<IProjectModel>(projectSchema, ProjectSchema);
