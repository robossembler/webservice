import { IsOptional, IsString } from "class-validator";

export class ProjectInstanceValidationModel {
  @IsString()
  public description: string;

  @IsOptional()
  public rootDir: string;

  @IsOptional()
  public isActive: boolean;
}
