import { Type } from "class-transformer";
import { IsArray, IsOptional, IsString, ValidateNested } from "class-validator";
import { IProjectModel } from "../../projects/models/project_model_database_model";

export class FormBuilderValidationModel {
  @IsString()
  public result: string;
  @IsString()
  public context: string;
  @IsArray()
  public form: [];
  output: any;
}
export enum ProcessStatus {
  END = "END",
  ERROR = "ERROR",
  NEW = "NEW",
}
export interface IDatasetModel {
  _id?: string;
  name: string;
  local_path: string;
  dataSetObjects: string[];
  formBuilder: FormBuilderValidationModel;
  processLogs: string;
  processStatus: ProcessStatus;
  project?: IProjectModel;
}

export class DatasetValidationModel implements IDatasetModel {
  @IsString()
  public name: string;
  @IsArray()
  public dataSetObjects: string[];
  @ValidateNested()
  @Type(() => FormBuilderValidationModel)
  public formBuilder: FormBuilderValidationModel;
  public local_path: string;
  @IsOptional()
  @IsString()
  public processStatus: ProcessStatus;
  public projectId: string;
  public processLogs: string;
}
