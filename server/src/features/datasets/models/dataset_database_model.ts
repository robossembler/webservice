import { Schema, model } from "mongoose";
import { IDatasetModel } from "./dataset_validation_model";
import { projectSchema } from "../../projects/models/project_model_database_model";
  
export const DatasetSchema = new Schema({
  name: {
    type: String,
  },
  local_path: {
    type: String,
  },
  dataSetObjects: {
    type: Array,
    of: String,
  },
  formBuilder: {
    type: Schema.Types.Mixed,
    of: String,
  },
  unixTime: {
    type: Number,
    default: Date.now(),
  },
  neuralNetworkAction: {
    type: String,
  },
  neuralNetworkName: {
    type: String,
  },
  processStatus: {
    type: String,
    default: "none",
  },
  project: {
    type: Schema.Types.ObjectId,
    ref: projectSchema,
    autopopulate: true,
    require: true,
  },
  processLogs: {
    type: String,
  },
  datasetType: {
    type: String,
  },
}).plugin(require("mongoose-autopopulate"));

export const datasetSchema = "Dataset";

export const DatasetDBModel = model<IDatasetModel>(datasetSchema, DatasetSchema);
