import { CrudController } from "../../core/controllers/crud_controller";
import { IsHaveActiveProcessUseCase, KillLastProcessUseCase } from "../../core/usecases/exec_process_usecase";
import { CreateDataSetScenario } from "./domain/create_dataset_scenario";
import { DeleteDatasetUseCase } from "./domain/delete_dataset_use_case";
import { ExecDatasetProcessScenario } from "./domain/exec_process_scenario";
import { GetDatasetActiveProjectScenario } from "./domain/get_dataset_active_project_scenario";
import { DatasetDBModel } from "./models/dataset_database_model";
import { DatasetValidationModel } from "./models/dataset_validation_model";

export class DatasetsPresentation extends CrudController<DatasetValidationModel, typeof DatasetDBModel> {
  constructor() {
    super({
      url: "datasets",
      validationModel: DatasetValidationModel,
      databaseModel: DatasetDBModel,
    });
    super.post(new CreateDataSetScenario().call);
    super.get(new GetDatasetActiveProjectScenario().call);
    super.delete(null)
    this.subRoutes.push({
      method: "POST",
      subUrl: "exec",
      fn: new ExecDatasetProcessScenario(),
    });
    this.subRoutes.push({
      method: "GET",
      subUrl: "is/running",
      fn: new IsHaveActiveProcessUseCase(),
    });
    this.subRoutes.push({
      method: "GET",
      subUrl: "delete/process",
      fn: new KillLastProcessUseCase(),
    });
    this.subRoutes.push({
      method: "DELETE",
      subUrl: "dataset",
      fn: new DeleteDatasetUseCase()
    })

  }
}
