import { ObjectId } from "mongoose";
import { CallbackStrategyWithValidationModel, ResponseBase } from "../../../core/controllers/http_controller";
import { Result } from "../../../core/helpers/result";
import { TypedEvent } from "../../../core/helpers/typed_event";
import { EXEC_EVENT, ExecError, SpawnError } from "../../../core/models/exec_error_model";
import { ExecutorResult } from "../../../core/models/executor_result";
import { SearchOneDataBaseModelUseCase } from "../../../core/usecases/search_database_model_usecase";
import { DatasetDBModel } from "../models/dataset_database_model";
import { DatasetValidationModel, ProcessStatus } from "../models/dataset_validation_model";
import { IProjectModel, ProjectDBModel } from "../../projects/models/project_model_database_model";

export class ProcessWatcherAndDatabaseUpdateService<A> extends TypedEvent<
  Result<ExecError | SpawnError, ExecutorResult>
> {
  databaseId: ObjectId;
  model: A;
  constructor(databaseId: ObjectId, model: A) {
    super();
    this.databaseId = databaseId;
    this.model = model;
    this.on((event) => this.lister(event));
  }

  lister(event: Result<ExecError | SpawnError, ExecutorResult>) {
    event.fold(
      async (success) => {
        if (success.event == EXEC_EVENT.END) {
          // @ts-expect-error
          const dbModel = await this.model.findById(this.databaseId);
          if (dbModel !== null) {
            dbModel.local_path;
            dbModel.processStatus = ProcessStatus.END;
            dbModel.lastProcessLogs = success.data;
            await dbModel.save();
          }
        }
      },
      async (error) => {
        // @ts-expect-error
        const dbModel = await this.model.findById(this.databaseId);
        if (dbModel !== null) {
          dbModel.processStatus = ProcessStatus.ERROR;
          dbModel.lastProcessLogs = error.message;
          await dbModel.save();
        }
      }
    );
  }
}
export class CreateDataSetScenario extends CallbackStrategyWithValidationModel<DatasetValidationModel> {
  validationModel: DatasetValidationModel;
  call = async (model: DatasetValidationModel): ResponseBase => {
    return (
      await new SearchOneDataBaseModelUseCase<IProjectModel>(ProjectDBModel).call({ isActive: true }, "no active projects")
    ).map(async (project) => {
      model.processStatus = ProcessStatus.NEW;
      model.local_path = project.rootDir;
      model.projectId = project._id;
      const d = new DatasetDBModel();
      Object.assign(d, model);
      await d.save();

      return Result.ok("create dataset ok");
    });
  };
}
