import { CallbackStrategyWithEmpty, ResponseBase } from "../../../core/controllers/http_controller";
import { Result } from "../../../core/helpers/result";
import { SearchOneDataBaseModelUseCase } from "../../../core/usecases/search_database_model_usecase";
import { IProjectModel, ProjectDBModel } from "../../projects/models/project_model_database_model";
import { DatasetDBModel } from "../models/dataset_database_model";

export class GetDatasetActiveProjectScenario extends CallbackStrategyWithEmpty {
  call = async (): ResponseBase => {
    return (
      await new SearchOneDataBaseModelUseCase<IProjectModel>(ProjectDBModel).call({ isActive: true }, "no active projects")
    ).map(async (project) => {
      return Result.ok(await DatasetDBModel.find({ project: project._id }));
    });
  };
}
