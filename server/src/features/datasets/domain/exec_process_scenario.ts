import { ObjectId } from "mongoose";
import { CallbackStrategyWithIdQuery, ResponseBase } from "../../../core/controllers/http_controller";
import { ExecProcessUseCase, IsHaveActiveProcessUseCase } from "../../../core/usecases/exec_process_usecase";
import { ReadByIdDataBaseModelUseCase } from "../../../core/usecases/read_by_id_database_model_usecase";
import { MongoIdValidation } from "../../../core/validations/mongo_id_validation";
import { DatasetDBModel } from "../models/dataset_database_model";
import { IDatasetModel } from "../models/dataset_validation_model";
import { ProcessWatcherAndDatabaseUpdateService } from "./create_dataset_scenario";
import { FolderStructure } from "../../projects/domain/upload_file_to_to_project_scenario";

export class ExecDatasetProcessScenario extends CallbackStrategyWithIdQuery {
  idValidationExpression = new MongoIdValidation();

  call = async (id: string): ResponseBase => {
    return (await new ReadByIdDataBaseModelUseCase<IDatasetModel>(DatasetDBModel).call(id)).map(async (model) => {
      return (await new IsHaveActiveProcessUseCase().call()).map(async () => {
        await DatasetDBModel.findById(id).updateOne({ processStatus: "RUN" });
        model.local_path = `${model.local_path}/${FolderStructure.datasets}/`;
        
        return new ExecProcessUseCase().call(
          `${model.project.rootDir}/`,
          `blenderproc run $PYTHON_BLENDER_PROC  --cfg '${JSON.stringify(model)}'`,
          id,
          new ProcessWatcherAndDatabaseUpdateService(id as unknown as ObjectId, DatasetDBModel)
        );
      });
    });
  };
}
