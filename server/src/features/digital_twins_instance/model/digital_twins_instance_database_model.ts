import { Schema, model } from "mongoose";

export interface IDigitalTwinsInstanceModel {
  instancePath: string;
  status?: string;
}

export const DigitalTwinsInstanceSchema = new Schema({
  path: String,
  name: String,
  entity: String,
  description: String,
  command: String,
  status: {
    type: String,
  },
  interfaces: {
    cmd: String,
  },
  instancePath: {
    type: String,
  },
  package: {
    type: String,
  },
  executable: {
    type: String,
  },
  formBuilder: {
    type: Schema.Types.Mixed,
  },
}).plugin(require("mongoose-autopopulate"));

export const digitalTwinsInstanceSchema = "digital_twins_instance";

export const DigitalTwinsInstanceDatabaseModel = model<IDigitalTwinsInstanceModel>(
  digitalTwinsInstanceSchema,
  DigitalTwinsInstanceSchema
);
