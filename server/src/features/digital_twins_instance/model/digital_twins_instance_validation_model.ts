import { Type } from "class-transformer";
import { IsEnum, IsString } from "class-validator";
import { Instance } from "../../../core/models/instance";

enum DigitalTwinsTypes {
  CAMERA = "CAMERA",
  ROBOT = "ROBOT",
}
export class Interfaces {
  @IsString()
  cmd: string;
}
export class DigitalTwinsInstanceValidationModel implements Instance {
  instanceName: string;
  instancePath: string;
  path: string;
  name: string;
  @IsEnum(DigitalTwinsTypes)
  entity: DigitalTwinsTypes;
  @IsString()
  description: string;
  @IsString()
  command: string;
  @Type(() => Interfaces)
  interfaces: Interfaces;
}
