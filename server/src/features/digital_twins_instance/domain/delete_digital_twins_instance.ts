import { DeleteInstanceScenario } from "../../../core/scenarios/delete_instance_scenario";
import { DigitalTwinsInstanceDatabaseModel } from "../model/digital_twins_instance_database_model";
import { DigitalTwinsInstanceValidationModel } from "../model/digital_twins_instance_validation_model";

export class DeleteDigitalTwinsInstanceScenario extends DeleteInstanceScenario<DigitalTwinsInstanceValidationModel> {
  databaseModel = DigitalTwinsInstanceDatabaseModel;
}
