import { CallbackStrategyWithIdQuery, ResponseBase } from "../../../core/controllers/http_controller";
import { ExecProcessUseCase } from "../../../core/usecases/exec_process_usecase";
import { ReadByIdDataBaseModelUseCase } from "../../../core/usecases/read_by_id_database_model_usecase";
import { CoreValidation } from "../../../core/validations/core_validation";
import { MongoIdValidation } from "../../../core/validations/mongo_id_validation";
import {
  DigitalTwinsInstanceDatabaseModel,
  IDigitalTwinsInstanceModel,
} from "../model/digital_twins_instance_database_model";
import { ITopicModel } from "../../topics/topic_database_model";
import { ExecInstanceTwinsProcessService } from "../services/exec_instance_twins_process_service";

export interface Topics {
  topics: ITopicModel[];
}

export class ExecInstanceScenario extends CallbackStrategyWithIdQuery {
  idValidationExpression: CoreValidation = new MongoIdValidation();
  call = async (id: string): ResponseBase =>
    (
      await new ReadByIdDataBaseModelUseCase<IDigitalTwinsInstanceModel>(DigitalTwinsInstanceDatabaseModel).call(id)
    ).map(
      (document) => (
        console.log('DOCUMeNT PATH'),
        console.log(document.instancePath.pathNormalize()),
        new ExecProcessUseCase().call(
          document.instancePath,
          `python3 $GET_INTERFACES --path ${document.instancePath.pathNormalize()} --package '${JSON.stringify(
            document
          )}'`,
          "",
          new ExecInstanceTwinsProcessService(document.instancePath, document)
        )
      )
    );
}
