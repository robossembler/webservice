import { CreateInstanceScenario } from "../../../core/scenarios/create_intance_scenario";
import { DigitalTwinsInstanceDatabaseModel } from "../model/digital_twins_instance_database_model";
import { DigitalTwinsInstanceValidationModel } from "../model/digital_twins_instance_validation_model";

 

export class CreateDigitalTwinsInstanceScenario extends CreateInstanceScenario<DigitalTwinsInstanceValidationModel> {
  databaseModel = DigitalTwinsInstanceDatabaseModel;
  validationModel: DigitalTwinsInstanceValidationModel = new DigitalTwinsInstanceValidationModel();
}
