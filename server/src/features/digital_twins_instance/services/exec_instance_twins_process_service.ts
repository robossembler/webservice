import { Result } from "../../../core/helpers/result";
import { TypedEvent } from "../../../core/helpers/typed_event";
import { ExecError, SpawnError, EXEC_EVENT } from "../../../core/models/exec_error_model";
import { ExecutorResult } from "../../../core/models/executor_result";
import { CreateDataBaseModelUseCase } from "../../../core/usecases/create_database_model_usecase";
import { ReadFileAndParseJsonUseCase } from "../../../core/usecases/read_file_and_parse_json";
import { ITopicModel, TopicDatabaseModel } from "../../topics/topic_database_model";
import { Topics } from "../domain/exec_instance_scenario";

export class ExecInstanceTwinsProcessService extends TypedEvent<
  Result<ExecError | SpawnError, ExecutorResult>
> {
  path: string;
  databaseModel: any;
  constructor(path: string, databaseModel: any) {
    super();
    this.path = path;
    this.databaseModel = databaseModel;
    this.on((event) => this.lister(event));
  }

  lister = (event: Result<ExecError | SpawnError, ExecutorResult>) =>
    event.fold(
      async (success) => {
        if (success.event == EXEC_EVENT.END) {
          (await new ReadFileAndParseJsonUseCase().call<Topics>(this.path + "/topics.json")).fold(
            async (model) => {
              await model.topics.forEach(async (el) => {
                el.digitalTwinId = this.databaseModel._id;
                await new CreateDataBaseModelUseCase<ITopicModel>(TopicDatabaseModel).call(el);
              });

              this.databaseModel.status = "END";
              await this.databaseModel.save();
            },

            async (_) => {
              this.databaseModel.status = "ERROR";
              await this.databaseModel.save();
            }
          );
        }
      },
      async (_) => {
        this.databaseModel.status = "ERROR";
        await this.databaseModel.save();
      }
    );
}