import { CrudController } from "../../core/controllers/crud_controller";
import { CreateDigitalTwinsInstanceScenario } from "./domain/create_digital_twins_instance";
import { DeleteDigitalTwinsInstanceScenario } from "./domain/delete_digital_twins_instance";
import { ExecInstanceScenario } from "./domain/exec_instance_scenario";
import { DigitalTwinsInstanceDatabaseModel } from "./model/digital_twins_instance_database_model";
import { DigitalTwinsInstanceValidationModel } from "./model/digital_twins_instance_validation_model";

export class DigitalTwinsInstancePresentation extends CrudController<
  DigitalTwinsInstanceValidationModel,
  typeof DigitalTwinsInstanceDatabaseModel
> {
  constructor() {
    super({
      url: "digital/twins/instance",
      validationModel: DigitalTwinsInstanceValidationModel,
      databaseModel: DigitalTwinsInstanceDatabaseModel,
    });
    super.post(new CreateDigitalTwinsInstanceScenario().call);
    super.delete(new DeleteDigitalTwinsInstanceScenario().call);
    this.subRoutes.push({
      method: "POST",
      fn: new ExecInstanceScenario(),
      subUrl: "exec/instance",
    });
  }
}
