import { Type } from "class-transformer";
import { IsOptional, IsString, ValidateNested } from "class-validator";
import { SkillPackage, ISkillPackage, Module, IModule, BtActionViewModel, TopicViewModel } from "../../../core/models/skill_model";

export class SkillInstanceValidationModel {
    @IsOptional()
    @IsString()
    sid?: string;
    @ValidateNested()
    @Type(() => SkillPackage)
    SkillPackage: ISkillPackage;
    @ValidateNested()
    @Type(() => Module)
    Module: IModule;
    BTAction: BtActionViewModel[];
    topicsOut: TopicViewModel[] = [];
}
