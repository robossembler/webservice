import { Schema, model } from "mongoose";

export interface ISkillsModel {}

export const SkillsSchema = new Schema({
  SkillPackage: {
    type: Schema.Types.Mixed,
  },
  Module: {
    type: Schema.Types.Mixed,
  },
  Settings:{
    type:Schema.Types.Mixed,
  },
  BTAction: {
    type: Schema.Types.Mixed,
  },
  Launch: {
    type: Schema.Types.Mixed,
  },
  topicsOut: {
    type: Schema.Types.Mixed,
  },
  param: {
    type: Schema.Types.Mixed,
  },
  bgColor: {
    type: Schema.Types.String,
  },
  borderColor: {
    type: Schema.Types.String,
  },
}).plugin(require("mongoose-autopopulate"));

export const skillsSchema = "skills";

export const SkillsDBModel = model<ISkillsModel>(skillsSchema, SkillsSchema);
