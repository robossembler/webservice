import { CrudController } from "../../core/controllers/crud_controller";
import { SkillInstanceValidationModel } from "./model/skills_validation_model";
import { SkillsDBModel } from "./model/skills_database_model";
export class SkillsPresentation extends CrudController<SkillInstanceValidationModel, typeof SkillsDBModel> {
  constructor() {
    super({
      url: "skills",
      validationModel: SkillInstanceValidationModel,
      databaseModel: SkillsDBModel,
    });
  }
}
