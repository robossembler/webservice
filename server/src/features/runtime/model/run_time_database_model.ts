import { Schema, model } from "mongoose";
import { ExecRunTimeCommandValidationModel as ExecRunTimeCommandValidationModel } from "./run_time_validation_model";

export interface IDigitalTwinsInstanceModel {
  instancePath: string;
  status?: string;
}

export const ExecRunTimeDataBaseSchema = new Schema({
  execCommand: String,
  date: String,
  status: String,
  delay: Number,
  checkCommand: String,
  filter: Array,
}).plugin(require("mongoose-autopopulate"));

export const ExecRuntimeSchema = "digital_twins_instance";

export const ExecRuntimeDatabaseModel = model<ExecRunTimeCommandValidationModel>(
  ExecRuntimeSchema,
  ExecRunTimeDataBaseSchema
);
