export interface Command {
  tensorBoard: ExecProcess;
  simulationProcess: ExecProcess;
  btBuilderProcess: ExecProcess;
}

export interface ExecProcess {
  execCommand: string;
  date: null;
  status: null | string;
  delay: number;
  checkCommand: null;
  filter: null;
}
