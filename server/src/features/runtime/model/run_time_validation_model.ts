import { IsArray, IsEnum, IsNumber, IsString } from "class-validator";

export enum Status {
  OK = "Ok",
  ERROR = "Error",
}

export class ExecRunTimeCommandValidationModel {
  @IsString()
  execCommand: string;
  @IsString()
  date: string = Date();
  @IsEnum(Status)
  status: Status;
  @IsNumber()
  delay: number;
  @IsString()
  checkCommand: string;
  @IsArray()
  filter: string[] = [];
}
