import { ReadFileAndParseJsonUseCase } from "../../../core/usecases/read_file_and_parse_json";
import { Result } from "../../../core/helpers/result";
import { Command, ExecProcess } from "../model/command";

export class GetCommandScenario {
  call = async (command: string): Promise<Result<unknown, ExecProcess>> =>
    (
      await new ReadFileAndParseJsonUseCase().call<Command>(
        __filename.slice(0, __filename.lastIndexOf("/")).replace("/build/src/features/runtime/domain", "") +
          "/" +
          "command.json"
      )
    ).map((el) => Result.ok(el[command]));
}
