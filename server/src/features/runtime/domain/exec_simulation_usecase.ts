import { App } from "../../../core/controllers/app";
import { CallbackStrategyWithValidationModel, ResponseBase } from "../../../core/controllers/http_controller";
import { Result } from "../../../core/helpers/result";
import { SpawnProcessUseCase } from "../../../core/usecases/exec_process_usecase";
import { ExecProcesssss } from "../model/exec_process";
import { ProcessWatcher } from "../service/process_watcher";
import { GetCommandScenario } from "./get_command_scenario";

export class ExecSimulationUseCase extends CallbackStrategyWithValidationModel<ExecProcesssss> {
  validationModel = new ExecProcesssss();
  call = async (): ResponseBase =>
    (await new GetCommandScenario().call("btBuilderProcess")).map((el) => {
      new SpawnProcessUseCase().call(
        App.staticFilesStoreDir(),
        el.execCommand.replace("${dir_path}", "/Users/idontsudo/train"),
        "btBuilder",
        new ProcessWatcher()
      );
      return Result.ok("200");
    });
}
