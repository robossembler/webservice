import { Result } from "../../../core/helpers/result";
import { TypedEvent } from "../../../core/helpers/typed_event";
import { ExecError, SpawnError } from "../../../core/models/exec_error_model";
import { ExecutorResult } from "../../../core/models/executor_result";

export class ProcessWatcher extends TypedEvent<Result<ExecError | SpawnError, ExecutorResult>> {

  constructor() {
    super();
    this.on(this.lister);

  }

  lister = (event: Result<ExecError | SpawnError, ExecutorResult>) => {
    event.map(async (success) => {
      console.log(success.data)
    });
  };
}
