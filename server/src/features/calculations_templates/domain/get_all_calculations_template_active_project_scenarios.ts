import { ResponseBase } from "../../../core/controllers/http_controller";
import { SearchManyDataBaseModelUseCase } from "../../../core/usecases/search_many_database_model_usecase";
import { GetActiveProjectIdScenario } from "../../projects/domain/get_active_project_id_scenario";
import { ICalculationsTemplateModel, CalculationsTemplateDBModel } from "../models/calculations_template_database_model";

export class GetAllCalculationsTemplateScenarios {
  call = async (): ResponseBase =>
    (await new GetActiveProjectIdScenario().call()).map(
      async (model) => await new SearchManyDataBaseModelUseCase<ICalculationsTemplateModel>(CalculationsTemplateDBModel).call({ project: model.id })
    );
}
