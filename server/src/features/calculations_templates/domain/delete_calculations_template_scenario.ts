import { DeleteTemplateScenario } from "../../../core/scenarios/delete_template_scenario";
import { CalculationsTemplateDBModel } from "../models/calculations_template_database_model";

export class DeleteCalculationsTemplateScenario extends DeleteTemplateScenario {
  databaseModel = CalculationsTemplateDBModel;
}
