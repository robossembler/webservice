import { StaticFilesServer } from "../../../core/models/static_files";
import { CreateTemplateScenario } from "../../../core/scenarios/create_template_scenario";
import { CalculationsTemplateDBModel } from "../models/calculations_template_database_model";
import { CalculationsTemplateValidationModel } from "../models/calculations_template_validation_model";

export class CreateCalculationsTemplateScenario extends CreateTemplateScenario<CalculationsTemplateValidationModel> {
  validationModel: CalculationsTemplateValidationModel = new CalculationsTemplateValidationModel();
  databaseModel = CalculationsTemplateDBModel;
  path: string = StaticFilesServer.process;
}
