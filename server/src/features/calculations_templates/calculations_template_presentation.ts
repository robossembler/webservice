import { CrudController } from "../../core/controllers/crud_controller";
import { DeleteCalculationsTemplateScenario } from "./domain/delete_calculations_template_scenario";
import { CalculationsTemplateValidationModel as CalculationsTemplateValidationModel } from "./models/calculations_template_validation_model";
import { CalculationsTemplateDBModel } from "./models/calculations_template_database_model";
import { CreateCalculationsTemplateScenario } from "./domain/create_calculations_template_scenario";

export class CalculationsTemplatePresentation extends CrudController<
  CalculationsTemplateValidationModel,
  typeof CalculationsTemplateDBModel
> {
  constructor() {
    super({
      url: "calculations/template",
      validationModel: CalculationsTemplateValidationModel,
      databaseModel: CalculationsTemplateDBModel,
    });
    super.post(new CreateCalculationsTemplateScenario().call);
    super.delete(new DeleteCalculationsTemplateScenario().call);
  }
}
