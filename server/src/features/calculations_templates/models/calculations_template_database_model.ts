import { Schema, model } from "mongoose";
import { FormBuilderValidationModel, IDatasetModel } from "../../datasets/models/dataset_validation_model";
import { IProjectModel, projectSchema } from "../../projects/models/project_model_database_model";

export interface ICalculationsTemplateModel {
  path?: string;
  script: string;
  formBuilder: FormBuilderValidationModel;
  type: string;
  name: string;
  isEnd: boolean;
  createDate: Date;
  card?: string;
  lastProcessLogs?: string;
  lastProcessExecCommand?: string;
  lastExecDate?: Date;
  processStatus: string;
  project?: IProjectModel | string;
}

export const CalculationsTemplateSchema = new Schema({
  script: {
    type: String,
  },
  formBuilder: {
    type: Schema.Types.Mixed,
  },
  type: {
    type: String,
  },
  name: {
    type: String,
  },
 
  createDate: {
    type: Number,
  },
  card: {
    type: String,
  },
  project: {
    type: Schema.Types.ObjectId,
    ref: projectSchema,
    autopopulate: true,
    require: true,
  },
  path: {
    type: String,
  },
}).plugin(require("mongoose-autopopulate"));

export const calculationsTemplateSchema = "calculations_templates";

export const CalculationsTemplateDBModel = model<ICalculationsTemplateModel>(calculationsTemplateSchema, CalculationsTemplateSchema);
