import { IsNotEmpty, IsString, IsMongoId } from "class-validator";
import { ICalculationsTemplateModel } from "./calculations_template_database_model";
import { FormBuilderValidationModel } from "../../datasets/models/dataset_validation_model";
import { IProjectModel } from "../../projects/models/project_model_database_model";

export class CalculationsTemplateValidationModel implements ICalculationsTemplateModel {
  processStatus: string;
  @IsMongoId()
  project?: string | IProjectModel;
  @IsString()
  @IsNotEmpty()
  script: string;
  formBuilder: FormBuilderValidationModel;
  @IsString()
  @IsNotEmpty()
  type: string;
  @IsString()
  @IsNotEmpty()
  name: string;
  isEnd: boolean = true;
  createDate: Date = new Date();
  card?: string;
  lastProcessLogs?: string;
  lastProcessExecCommand?: string;
  lastExecDate?: Date;
  path: string;
}
