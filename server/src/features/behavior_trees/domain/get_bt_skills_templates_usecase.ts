import { CallbackStrategyWithEmpty, ResponseBase } from "../../../core/controllers/http_controller";
import { Result } from "../../../core/helpers/result";

export class GetBehaviorTreeSkillsTemplatesUseCase extends CallbackStrategyWithEmpty {
  call = async (): ResponseBase => {
    return Result.ok({
      skills: [
        {
          SkillPackage: { name: "Robossembler", version: "1.0", format: "1" },
          Module: { name: "MoveToPose", description: "Move to Pose skill with cartesian controllers" },
          Launch: { package: "rbss_movetopose", executable: "mtp_cartesian.py", name: "mtp_cartesian" },
          BTAction: [
            {
              name: "move",
              type: "action",
              param: [
                {
                  type: "move_to_pose",
                  dependency: {},
                },
              ],
            },
          ],
          Interface: {
            Input: [
              { name: "robotName", type: "ROBOT" },
              { name: "pose", type: "POSE" },
            ],
            Output: [],
          },
          Settings: [
            { name: "topicServer", value: "cartesian_move_to_pose" },
            { name: "end_effector_velocity", value: 1.0 },
            { name: "end_effector_acceleration", value: 1.0 },
          ],
        },
        {
          Module: { name: "MMM", description: "Move to Pose skill with cartesian controllers" },
          topicOut: [
            {
              topicName: "",
              topicType: "",
            },
          ],
          BTAction: [
            {
              name: "move",
              type: "action",
              param: [
                {
                  type: "move_to_pose",
                  dependency: {},
                },
              ],
            },
          ],
        },
        {
          SkillPackage: { name: "Robossembler", version: "1.0", format: "1", type: "Action" },
          Module: { name: "PoseEstimation", description: "Pose Estimation skill with DOPE" },
          Launch: { package: "rbs_perception", executable: "pe_dope_lc.py", name: "lc_dope" },
          BTAction: [
            {
              name: "peConfigure",
              type: "run",
              param: [
                {
                  type: "weights",
                  dependency: {},
                },
                {
                  type: "camera",
                  dependency: {},
                },
                {
                  type: "topic",
                  dependency: {},
                },
              ],
              result: ["POSE"],
            },
            { name: "peStop", type: "stop", param: [], result: [] },
          ],
          Interface: {
            Input: [
              { name: "cameraLink", type: "CAMERA" },
              { name: "object_name", type: "MODEL" },
            ],
            Output: [{ name: "pose_estimation_topic", type: "POSE" }],
          },
          Settings: [
            { name: "cameraLink", value: "inner_rgbd_camera" },
            { name: "pose", value: "" },
            { name: "publishDelay", value: 0.5 },
            { name: "tf2_send_pose", value: 1 },
            { name: "mesh_scale", value: 0.001 },
          ],
        },
      ],
    });
  };
}
