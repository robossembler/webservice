import { CallbackStrategyWithEmpty, ResponseBase } from "../../../core/controllers/http_controller";
import { Result } from "../../../core/helpers/result";
import { SearchOneDataBaseModelUseCase } from "../../../core/usecases/search_database_model_usecase";
import { IProjectModel, ProjectDBModel } from "../../projects/models/project_model_database_model";
import { BehaviorTreeDBModel } from "../models/behavior_tree_database_model";

export class GetBehaviorTreeActiveProjectScenario extends CallbackStrategyWithEmpty {
  call = async (): ResponseBase =>
    (
      await new SearchOneDataBaseModelUseCase<IProjectModel>(ProjectDBModel).call(
        { isActive: true },
        "no active projects"
      )
    ).map(async (project) => Result.ok(await BehaviorTreeDBModel.find({ project: project._id })));
}
