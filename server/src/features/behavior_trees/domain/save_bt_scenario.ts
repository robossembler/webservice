import { CallbackStrategyWithValidationModel, ResponseBase } from "../../../core/controllers/http_controller";
import { CreateFileUseCase } from "../../../core/usecases/create_file_usecase";
import { CreateFolderUseCase } from "../../../core/usecases/create_folder_usecase";
import { SearchOneDataBaseModelUseCase } from "../../../core/usecases/search_database_model_usecase";
import { FolderStructure } from "../../projects/domain/upload_file_to_to_project_scenario";
import { IProjectModel, ProjectDBModel } from "../../projects/models/project_model_database_model";
import { BehaviorTreeValidationModel } from "../models/behavior_tree_validation_model";

export class SaveBtScenario extends CallbackStrategyWithValidationModel<BehaviorTreeValidationModel> {
  validationModel: BehaviorTreeValidationModel = new BehaviorTreeValidationModel();
  call = async (model: BehaviorTreeValidationModel): ResponseBase =>
    (
      await new SearchOneDataBaseModelUseCase<IProjectModel>(ProjectDBModel).call(
        { isActive: true },
        "no active projects"
      )
    ).map(async (project) => {
      const folder = `${project.rootDir}/${FolderStructure.behaviorTrees}/`;

      (await new CreateFolderUseCase().call(folder)).map(async () =>
        (await new CreateFolderUseCase().call(`${folder}${model.name}/`)).map(async () =>
          (await new CreateFileUseCase().call(`${folder}${model.name}/bt.xml`, Buffer.from(model.xml))).map(
            async () =>
              await new CreateFileUseCase().call(
                `${folder}${model.name}/skills.json`,
                Buffer.from(JSON.stringify(model.skills))
              )
          )
        )
      );
    });
}
