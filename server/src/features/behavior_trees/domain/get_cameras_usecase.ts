import { CallbackStrategyWithEmpty, ResponseBase } from "../../../core/controllers/http_controller"
import { Result } from "../../../core/helpers/result"

export class GetCameraUseCase extends CallbackStrategyWithEmpty {
    call = async (): ResponseBase => {
        return Result.ok({
            "camera": [
                {
                    "sid": "1",
                    "DevicePackage": {
                        "name": "Robossembler",
                        "version": "1.0",
                        "format": "1"
                    },
                    "Module": {
                        "name": "RealSense Dxx",
                        "description": "ROS Wrapper for Intel(R) RealSense(TM) Cameras"
                    },
                    "Launch": {
                        "package": "realsense2_camera",
                        "executable": "rs_launch.py"
                    },
                    "DTwin": [
                        {
                            "interface": {
                                "input": [
                                    {
                                        "camera_namespace": "robot1"
                                    },
                                    {
                                        "camera_name": "455_1"
                                    },
                                    {
                                        "serial_port": "USB_1"
                                    },
                                    {
                                        "pose": "[0.0,0.0,0.0,0.0,0.0,0.0]"
                                    }
                                ]
                            }
                        }
                    ],
                    "Interface": {
                        "param": [
                            {
                                "type": "camera",
                                "dependency": {}
                            }
                        ]
                    },
                    "Settings": [
                        {
                            "name": "camera_config",
                            "description": "Camera Config",
                            "type": "file",
                            "defaultValue": "{ rgb_camera.profile: 1280x720x15 }"
                        }
                    ]
                },
                {
                    "sid": "2",
                    "DevicePackage": {
                        "name": "Robossembler",
                        "version": "1.0",
                        "format": "1"
                    },
                    "Module": {
                        "name": "RealSense Dxx",
                        "description": "ROS Wrapper for Intel(R) RealSense(TM) Cameras"
                    },
                    "Launch": {
                        "package": "realsense2_camera",
                        "executable": "rs_launch.py"
                    },
                    
                    "DTwin": [
                        {
                            "interface": {
                                "input": [
                                    {
                                        "camera_namespace": "robot1"
                                    },
                                    {
                                        "camera_name": "455_1"
                                    },
                                    {
                                        "serial_port": "USB_1"
                                    },
                                    {
                                        "pose": "[0.0,0.0,0.0,0.0,0.0,0.0]"
                                    }
                                ]
                            }
                        }
                    ],
                    "Interface": {
                        "param": [
                            {
                                "type": "camera",
                                "dependency": {}
                            }
                        ]
                    },
                    "Settings": [
                        {
                            "name": "camera_config",
                            "description": "Camera Config",
                            "type": "file",
                            "defaultValue": "{ rgb_camera.profile: 1280x720x15 }"
                        }
                    ]
                }
            ]
        })
    }

}


 