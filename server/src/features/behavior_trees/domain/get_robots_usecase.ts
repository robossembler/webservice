import { CallbackStrategyWithEmpty, ResponseBase } from "../../../core/controllers/http_controller";
import { Result } from "../../../core/helpers/result";

export class GetRobotsUseCase extends CallbackStrategyWithEmpty {
    call = async (): ResponseBase => {
        return Result.ok({
            "robots": [
                {
                    "sid": "1",
                    "DevicePackage": {
                        "name": "Robossembler",
                        "version": "1.0",
                        "format": "1"
                    },
                    "Module": {
                        "name": "RBS",
                        "description": "Main Robot"
                    },
                    "Launch": {
                        "package": "rbs_bringup",
                        "executable": "single_robot.launch.py"
                    },
                    "DTwin": [
                        {
                            "interface": {
                                "input": [
                                    {
                                        "robot_namespace": "robot1"
                                    },
                                    {
                                        "dof": 6
                                    }
                                ]
                            }
                        }
                    ],
                    "Settings": [
                        {
                            "name": "robot_type",
                            "description": "Type of robot by name",
                            "defaultValue": "rbs_arm"
                        }
                    ]
                }
            ]
        })
    }

}

