import { CallbackStrategyWithEmpty, ResponseBase } from "../../../core/controllers/http_controller";
import { Result } from "../../../core/helpers/result";

export class GetTopicsUseCase extends CallbackStrategyWithEmpty {
    call = async (): ResponseBase => {
        return Result.ok({
            "topics": [
                {
                    "sid": "1",
                    "name": "topic camera 1",
                    "type": "sensor_msgs/Image"
                },
                {
                    "sid": "2",
                    "name": "topic camera 2",
                    "type": "sensor_msgs/Image"
                }
            ]
        }
        )

    }
}