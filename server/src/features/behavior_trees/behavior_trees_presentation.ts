import { CrudController } from "../../core/controllers/crud_controller";
import { GetBehaviorTreeSkillsTemplatesUseCase } from "./domain/get_bt_skills_templates_usecase";
import { BehaviorTreeValidationModel } from "./models/behavior_tree_validation_model";
import { BehaviorTreeDBModel } from "./models/behavior_tree_database_model";
import { ReadByIdDataBaseModelScenario } from "../../core/scenarios/read_by_id_database_model_scenario";
import { GetBehaviorTreeActiveProjectScenario } from "./domain/get_behavior_tree_active_project_scenario";
import { SaveBtScenario as FillBtScenario } from "./domain/save_bt_scenario";
import { GetCameraUseCase } from "./domain/get_cameras_usecase";
import { GetRobotsUseCase } from "./domain/get_robots_usecase";
import { GetTopicsUseCase } from "./domain/get_topics_usecase";

export class BehaviorTreesPresentation extends CrudController<BehaviorTreeValidationModel, typeof BehaviorTreeDBModel> {
  constructor() {
    super({
      url: "behavior/trees",
      validationModel: BehaviorTreeValidationModel,
      databaseModel: BehaviorTreeDBModel,
    });
    super.get(new GetBehaviorTreeActiveProjectScenario().call);
    this.subRoutes.push({
      method: "POST",
      subUrl: "fill/tree",
      fn: new FillBtScenario(),
    });
    this.subRoutes.push({ method: "GET", subUrl: "templates", fn: new GetBehaviorTreeSkillsTemplatesUseCase() });
    this.subRoutes.push({
      method: "GET",
      subUrl: "by_id",
      fn: new ReadByIdDataBaseModelScenario<BehaviorTreeValidationModel>(BehaviorTreeDBModel),
    });
    this.subRoutes.push({
      method: "GET",
      subUrl: "cameras",
      fn: new GetCameraUseCase()
    })
    this.subRoutes.push({
      method: "GET",
      subUrl: "robots",
      fn: new GetRobotsUseCase()
    })
    this.subRoutes.push({
      method: "GET",
      subUrl: "topics",
      fn: new GetTopicsUseCase()
    })
  }
}

