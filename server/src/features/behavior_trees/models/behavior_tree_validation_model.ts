import { IsMongoId, IsNumber, IsString } from "class-validator";
import { IBehaviorTreeModel } from "./behavior_tree_database_model";

export class BehaviorTreeValidationModel implements IBehaviorTreeModel {
  @IsString()
  public name: string;
  @IsMongoId()
  public project:string;
  public skills:any;
  public xml:string;
  @IsNumber()
  public unixTime: number
}
