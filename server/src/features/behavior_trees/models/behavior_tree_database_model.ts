import { Schema, model } from "mongoose";
import { IProjectModel, projectSchema } from "../../projects/models/project_model_database_model";

export interface IBehaviorTreeModel {
  name: string;
  project?: IProjectModel | string;
  unixTime?: number;
  local_path?: string;
}

export const BehaviorTreeSchema = new Schema({
  name: {
    type: String,
  },
  description: { type: String },
  scene: {
    type: Schema.Types.Mixed,
    default: [],
  },
  local_path: {
    type: String,
  },
  unixTime: {
    type: Number,
  },
  isFinished: {
    type: Boolean,
    default: false,
  },
  dependency: {
    type: Schema.Types.Mixed,
  },
  skills: {
    type: Schema.Types.Mixed,
  },
  sceneId: {
    type: String,
  },
  project: {
    type: Schema.Types.ObjectId,
    ref: projectSchema,
    autopopulate: true,
    require: true,
  },
}).plugin(require("mongoose-autopopulate"));

export const behaviorTreeSchema = "Behavior";

export const BehaviorTreeDBModel = model<IBehaviorTreeModel>(behaviorTreeSchema, BehaviorTreeSchema);
