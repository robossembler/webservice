import { BehaviorTreesPresentation } from "../../features/behavior_trees/behavior_trees_presentation";
import { DatasetsPresentation } from "../../features/datasets/datasets_presentation";
import { CalculationsTemplatePresentation } from "../../features/calculations_templates/calculations_template_presentation";
import { ProjectsPresentation } from "../../features/projects/projects_presentation";
import { extensions } from "../extensions/extensions";
import { Routes } from "../interfaces/router";
import { ScenesPresentation } from "../../features/scenes/scenes_presentation";
import { CalculationsInstancesPresentation } from "../../features/calculations_instance/calculations_instance_presentation";
import { DigitalTwinsInstancePresentation } from "../../features/digital_twins_instance/digital_twins_instance_presentation";
import { DigitalTwinsTemplatePresentation } from "../../features/digital_twins_template/digital_twins_template_presentation";
import { TopicsPresentation } from "../../features/topics/topics_presentation";
import { SkillsPresentation } from "../../features/skills/skill_presentation";
import { RunTimePresentation } from "../../features/runtime/runtime_presentation";

extensions();

export const httpRoutes: Routes[] = [
  new ProjectsPresentation(),
  new DatasetsPresentation(),
  new ScenesPresentation(),
  new BehaviorTreesPresentation(),
  new CalculationsTemplatePresentation(),
  new CalculationsInstancesPresentation(),
  new DigitalTwinsTemplatePresentation(),
  new DigitalTwinsInstancePresentation(),
  new TopicsPresentation(),
  new SkillsPresentation(),
  new RunTimePresentation(),
].map((el) => el.call());
