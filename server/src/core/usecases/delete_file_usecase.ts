import { Result } from "../helpers/result";
import { FileSystemRepository } from "../repository/file_system_repository";

export class DeleteFileUseCase {
  fileSystemRepository = new FileSystemRepository();
  call = (path: string) => {
    try {
      this.fileSystemRepository.delete(path);
      return Result.ok("file delete");
    } catch {
      return Result.error("Unknown error");
    }
  };
}
