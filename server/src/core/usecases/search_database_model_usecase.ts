import { Result } from "../helpers/result";

export class SearchOneDataBaseModelUseCase<T> {
  model: any;

  constructor(model: any) {
    this.model = model;
  }

  call = async (findFilter: Partial<T>, error: string = "not found database"): Promise<Result<string, T>> => {
    const result = await this.model.findOne(findFilter);
    if (result === null) {
      return Result.error(error);
    } else {
      return Result.ok(result);
    }
  };
}
