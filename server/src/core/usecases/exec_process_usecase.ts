import * as cp from "child_process";
import { CallbackStrategyWithEmpty } from "../controllers/http_controller";
import { Result } from "../helpers/result";
import { TypedEvent } from "../helpers/typed_event";
import { EXEC_EVENT, EXEC_TYPE, ExecError, SpawnError } from "../models/exec_error_model";
import { ExecutorResult } from "../models/executor_result";
import { ExecutorProgramService } from "../services/executor_program_service";

export const executorProgramService = new ExecutorProgramService("");
export class KillLastProcessUseCase extends CallbackStrategyWithEmpty {
  call = async (): Promise<Result<undefined, string>> => {
    executorProgramService.deleteWorker();
    return Result.ok("ok");
  };
}

export class IsHaveActiveProcessUseCase extends CallbackStrategyWithEmpty {
  call = async (): Promise<Result<string, string>> => {
    if (executorProgramService.worker === undefined) {
      return Result.ok("process not work");
    }
    return Result.error("process is exists");
  };
}

export class ExecProcessUseCase {
  call = async (
    path: string,
    command: string,
    id: string,
    watcher?: TypedEvent<Result<ExecError | SpawnError, ExecutorResult>>
  ): Promise<Result<Error, string>> => {
    try {
      executorProgramService.execPath = path;
      executorProgramService.on((event) => {
        if (watcher) watcher.emit(event);
      });
      executorProgramService.call(EXEC_TYPE.EXEC, command, undefined, id);

      return Result.ok("ok");
    } catch (error) {
      return Result.error(error);
    }
  };
}

export class SpawnProcessUseCase {
  call = async (
    path: string,
    command: string,
    id: string,
    watcher?: TypedEvent<Result<ExecError | SpawnError, ExecutorResult>>
  ): Promise<Result<Error, string>> => {
    try {
      const commands = command.split(" ");

      const subprocess = cp.spawn(commands.at(0), commands.slice(1, commands.length), {
        cwd: path,
      });
      const sendWatcher = (event: Buffer) => {
        if (watcher) {
          watcher.emit(Result.ok(new ExecutorResult(EXEC_TYPE.SPAWN, EXEC_EVENT.PROGRESS, event.toString())));
        }
      };
      subprocess.stdout.on("data", (data) => sendWatcher(data));

      subprocess.stderr.on("data", (data) => sendWatcher(data));

      subprocess.on("error", (error) => {
        console.error(`Ошибка: ${error.message}`);
      });

      subprocess.on("close", (code) => {
        console.log(`Процесс завершился с кодом: ${code}`);
      });
      return Result.ok('ok');
    } catch (error) {
      return Result.error(error);
    }
  };
}
