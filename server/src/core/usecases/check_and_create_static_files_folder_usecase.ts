import { App } from "../controllers/app";
import { StaticFilesServer } from "../models/static_files";
import { FileSystemRepository } from "../repository/file_system_repository";
import { CreateFolderUseCase } from "./create_folder_usecase";

export class CheckAndCreateStaticFilesFolderUseCase {
  fileSystemRepository: FileSystemRepository = new FileSystemRepository();
  call = async (): Promise<void> => {
    Object.keys(StaticFilesServer).forEach(async (key) => {
      if (!(await this.fileSystemRepository.dirIsExists(App.staticFilesStoreDir() + key))) {
        await new CreateFolderUseCase().call(App.staticFilesStoreDir() + key);
      }
    });
    if (await this.fileSystemRepository.dirIsExists(App.staticFilesStoreDir())) {
      return;
    }
    await new CreateFolderUseCase().call(App.staticFilesStoreDir());
  };
}
