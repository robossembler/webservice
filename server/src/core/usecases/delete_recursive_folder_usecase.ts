import { Result } from "../helpers/result";
import { FileSystemRepository } from "../repository/file_system_repository";

export class DeleteRecursiveFolderUseCase{
    repository:FileSystemRepository = new FileSystemRepository()
    call = async (path:string):Promise<Result<void,void>> =>{
        await this.repository.deleteDirRecursive(path)
        return Result.ok()
    }

}