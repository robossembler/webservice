import { Result } from "../helpers/result";

export class ReadByIdDataBaseModelUseCase<D> {
  databaseModel: D;

  constructor(model) {
    this.databaseModel = model;
  }
  call = async (id: string): Promise<Result<Error, D>> => {
    try {
      const dbModel = this.databaseModel as any;
      const model = await dbModel.findById(id);
      if (model === null) {
        return Result.error(
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-expect-error
          Error(`the database table ${this.databaseModel.modelName} does not contain an object with this ID`)
        );
      }
      return Result.ok(model);
    } catch (error) {
      return Result.error(error);
    }
  };
}
