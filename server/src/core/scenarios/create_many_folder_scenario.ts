import { Result } from "../helpers/result";
import { CreateFolderUseCase } from "../usecases/create_folder_usecase";

export class CreateManyFolderScenario {
  call = async (path: string, foldersNames: string[]): Promise<Result<Error, string>> => {
    try {
      foldersNames.forEach(async (folder) => {
        await new CreateFolderUseCase().call(`${path}${folder}`.normalize());
      });
      return Result.ok("many folder created");
    } catch (error) {
      return Result.error(new Error(error));
    }
  };
}

