import { CalculationsTemplateValidationModel } from "../../features/calculations_templates/models/calculations_template_validation_model";
import { CallbackStrategyWithIdQuery, ResponseBase } from "../controllers/http_controller";
import { DeleteDataBaseModelUseCase } from "../usecases/delete_database_model_usecase";
import { DeleteRecursiveFolderUseCase } from "../usecases/delete_recursive_folder_usecase";
import { ReadByIdDataBaseModelUseCase } from "../usecases/read_by_id_database_model_usecase";
import { CoreValidation } from "../validations/core_validation";
import { MongoIdValidation } from "../validations/mongo_id_validation";

export abstract class DeleteTemplateScenario extends CallbackStrategyWithIdQuery {
  abstract databaseModel: any;
  idValidationExpression: CoreValidation = new MongoIdValidation();
  call = async (id: string): ResponseBase =>
    (await new ReadByIdDataBaseModelUseCase<CalculationsTemplateValidationModel>(this.databaseModel).call(id)).map(
      async (model) => {
        return (await new DeleteRecursiveFolderUseCase().call(model.path)).map(
          async () => await new DeleteDataBaseModelUseCase(this.databaseModel).call(id)
        );
      }
    );
}
