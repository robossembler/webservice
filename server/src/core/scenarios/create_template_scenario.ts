import { App } from "../controllers/app";
import { CallbackStrategyWithValidationModel, ResponseBase } from "../controllers/http_controller";
import { StaticFilesServer } from "../models/static_files";
import { CreateDataBaseModelUseCase } from "../usecases/create_database_model_usecase";
import { CreateFolderUseCase } from "../usecases/create_folder_usecase";

interface IModel {
  path: string;
  name: string;
}

export abstract class CreateTemplateScenario<V extends IModel> extends CallbackStrategyWithValidationModel<V> {
  validationModel: V;
  abstract databaseModel: any;
  abstract path: string;
  call = async (model: V): ResponseBase => {
    model.path = App.staticFilesStoreDir() + StaticFilesServer.process + model.name;
    return (await new CreateFolderUseCase().call(model.path)).map(() => {
      return new CreateDataBaseModelUseCase(this.databaseModel).call(model);
    });
  };
}
