

import { IProjectModel, ProjectDBModel } from "../../features/projects/models/project_model_database_model";
import { App } from "../controllers/app";
import { PipelineRealTimeService } from "../services/pipeline_real_time_service";
import { CreateFolderUseCase } from "../usecases/create_folder_usecase";
import { SearchOneDataBaseModelUseCase } from "../usecases/search_database_model_usecase";

export const pipelineRealTimeService = new PipelineRealTimeService();


export class SetLastActivePipelineToRealTimeServiceScenario {
  call = async (): Promise<void> => {
    return (
      await new SearchOneDataBaseModelUseCase<IProjectModel>(ProjectDBModel).call({
        isActive: true,
      })
    ).fold(
      async (projectModel) => {
        
        const projectPath = App.staticFilesStoreDir() + projectModel.rootDir + "/";
        await new CreateFolderUseCase().call(projectPath);
        pipelineRealTimeService.setPipelineDependency(
          projectPath,
          projectModel._id,
          projectModel.rootDir
        );
      },
      async (_e) => {
        console.log("not found active pipeline");
      }
    );
  };
}
