import { CallbackStrategyWithIdQuery } from "../controllers/http_controller";
import { Result } from "../helpers/result";
import { ReadByIdDataBaseModelUseCase } from "../usecases/read_by_id_database_model_usecase";
import { CoreValidation } from "../validations/core_validation";
import { MongoIdValidation } from "../validations/mongo_id_validation";

export class ReadByIdDataBaseModelScenario<D> extends CallbackStrategyWithIdQuery {
  idValidationExpression: CoreValidation = new MongoIdValidation();
  databaseModel: D;

  constructor(model) {
    super();
    this.databaseModel = model;
  }
  call = async (id: string): Promise<Result<Error, D>> => {
    try {
      return new ReadByIdDataBaseModelUseCase<D>(this.databaseModel).call(id);
    } catch (error) {
      return Result.error(error);
    }
  };
}
