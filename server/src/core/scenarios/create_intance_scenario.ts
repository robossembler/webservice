import { CallbackStrategyWithValidationModel, ResponseBase } from "../controllers/http_controller";
import { Instance } from "../models/instance";
import { CreateDataBaseModelUseCase } from "../usecases/create_database_model_usecase";
import { CreateFolderUseCase } from "../usecases/create_folder_usecase";

export abstract class CreateInstanceScenario<V extends Instance> extends CallbackStrategyWithValidationModel<V> {
  abstract validationModel: V;
  abstract databaseModel: any;
  call = async (model: V): ResponseBase => {
    model.instancePath = `${model.path}/${model.instanceName}`;
    console.log("INSTANCE PATh");
    console.log(model.instancePath)
    return (await new CreateFolderUseCase().call(model.instancePath)).map(
      async () => await new CreateDataBaseModelUseCase(this.databaseModel).call(model)
    );
  };
}
