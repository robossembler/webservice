export interface Instance {
  instanceName: string;
  path: string;
  instancePath: string;
}
