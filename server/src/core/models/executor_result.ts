import { EXEC_EVENT, EXEC_TYPE } from "./exec_error_model";

export class ExecutorResult {
  type: EXEC_TYPE;
  event: EXEC_EVENT;
  data: any;
  id?:string
  constructor(type: EXEC_TYPE, event: EXEC_EVENT, data: any) {
    this.type = type;
    this.event = event;
    this.data = data;
  }
  static isExecutorResult(value: any): void | ExecutorResult {
    try {
      if (value) {
        if ("type" in value && "event" in value && "data" in value) {
          return new ExecutorResult(value.type, value.event, value.data);
        }
      }
    } catch (error) {
      console.log(error);
    }
  }
}
