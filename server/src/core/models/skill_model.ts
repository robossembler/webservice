import { IsArray, IsEnum, IsNotEmpty, IsString, ValidateNested } from "class-validator";
import { Type } from "class-transformer";
export class TopicViewModel {
  @IsNotEmpty()
  @IsString()
  name: string;
  @IsNotEmpty()
  @IsString()
  type: string;
  constructor(name: string, type: string) {
    this.name = name;
    this.type = type;
  }
  static empty() {
    return new TopicViewModel("", "");
  }
}

export interface IParam {
  type: string;
  dependency: Object;
}
export interface Skill {
  SkillPackage: ISkillPackage;
  Module: IModule;
  Launch: ILaunch;
  ROS2: IRos2;
  BTAction: IBTAction[];
  Interface: IInterface;
  Settings: ISetting[];
  xxx: IXxx;
}

export interface IBTAction {
  name: string;
  format: string;
  type: string;
  param: IParam[];
  result: string[];
}

export interface IInterface {
  Input: IPut[];
  Output: IPut[];
}

export interface IPut {
  name: string;
  type: string;
}

export interface ILaunch {
  executable: string;
}

export interface IModule {
  name: string;
  description: string;
}

export interface IRos2 {
  node_name: string;
}

export interface ISetting {
  name: string;
  value: number | string;
}

export interface ISkillPackage {
  name: string;
  version: string;
  format: string;
}

export interface IXxx {
  cameraLink: string;
  topicImage: string;
  topicCameraInfo: string;
}

export class SkillPackage implements ISkillPackage {
  @IsString()
  name: string;
  @IsString()
  version: string;
  @IsString()
  format: string;
}
export class Module implements IModule {
  @IsString()
  name: string;
  @IsString()
  description: string;
}
export class BTAction implements IBTAction {
  @IsString()
  name: string;
  @IsString()
  format: string;
  @IsString()
  type: string;
  @IsArray()
  param: IParam[];
  @IsArray()
  result: string[];
}
export class Launch implements ILaunch {
  @IsString()
  executable: string;
}
export class Ros2 implements IRos2 {
  @IsString()
  node_name: string;
}
export class Put implements IPut {
  @IsString()
  name: string;
  @IsString()
  type: string;
}
export class Interface implements IInterface {
  @ValidateNested()
  @Type(() => Put)
  Input: IPut[];
  @ValidateNested()
  @Type(() => Put)
  Output: IPut[];
}
export class Setting implements ISetting {
  name: string;
  value: string | number;
}
export class Xxx implements IXxx {
  cameraLink: string;
  topicImage: string;
  topicCameraInfo: string;
}
export class SkillModel implements Skill {
  @ValidateNested()
  @Type(() => SkillPackage)
  SkillPackage: ISkillPackage;
  @ValidateNested()
  @Type(() => Module)
  Module: IModule;
  @ValidateNested()
  @Type(() => Launch)
  Launch: ILaunch;
  @ValidateNested()
  @Type(() => Ros2)
  ROS2: IRos2;
  @ValidateNested()
  @IsArray()
  @Type(() => BTAction)
  BTAction: IBTAction[];
  @ValidateNested()
  @Type(() => Interface)
  Interface: IInterface;
  @ValidateNested()
  @IsArray()
  @Type(() => Setting)
  Settings: ISetting[];
  @ValidateNested()
  @Type(() => Xxx)
  xxx: IXxx;
}

export enum BtAction {
  ACTION = "ACTION",
  CONDITION = "CONDITION",
}
export class BtActionViewModel implements IBTAction {
  @IsNotEmpty()
  @IsString()
  name: string;
  @IsNotEmpty()
  @IsString()
  format: string;
  @IsNotEmpty()
  @IsString()
  type: string;
  param: IParam[];
  @IsNotEmpty()
  @IsString()
  result: string[];
  @IsNotEmpty()
  @IsEnum(BtAction)
  typeAction: BtAction;
}
