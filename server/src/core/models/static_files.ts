export enum StaticFilesProject {
  robossembler_assets = "robossembler_assets.json",
  assets = "/assets/assets.json",
  parts = "/assets/parts.json",
  scenes = "/scenes/",
  behaviorTrees = "behavior_trees",
  robots = 'robots'
}

export enum StaticFilesServer {
  process = "/process/",
  digitalTwins = "/digital_twins/",
}
