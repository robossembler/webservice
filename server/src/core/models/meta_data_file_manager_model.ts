export enum EventsFileChanger {
  update = "update",
  delete = "delete",
  create = "create",
  static = "static",
}

export class MetaDataFileManagerModel {
  path: string;
  md5Hash: string | null;
  event: EventsFileChanger;
  unixTime: number;

  constructor(path: string, md5Hash: string | null, event: EventsFileChanger) {
    this.path = path;
    this.md5Hash = md5Hash;
    this.event = event;
    this.unixTime = Date.now();
  }

  public get timeString(): string {
    const date = new Date(this.unixTime * 1000);
    const hours = date.getHours();
    const minutes = "0" + date.getMinutes();
    const seconds = "0" + date.getSeconds();

    return hours + ":" + minutes.substr(-2) + ":" + seconds.substr(-2);
  }
}
