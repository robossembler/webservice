import { EXEC_TYPE } from "./exec_error_model";

export interface Trigger {}
export interface IPipeline {
  process: IProcess;
  trigger?: Trigger;
  env: Env | null;
  stackGenerateType: StackGenerateType;
}

export enum StackGenerateType {
  MAP = "MAP",
  SINGLETON = "SINGLETON",
}

export interface Env {
  ssh_key: string;
  isUserInput: boolean;
  isExtends: string;
}

export interface IProcess {
  type: EXEC_TYPE;
  command: string;
  isGenerating: boolean;
  isLocaleCode: boolean;
  issueType: IssueType;
  timeout?: number;
  commit?: string | undefined;
}

export enum IssueType {
  WARNING = "WARNING",
  ERROR = "ERROR",
}
