import { extensions } from "../extensions/extensions";

extensions();

export class ExecError extends Error {
  id?:string;
  script: string;
  unixTime: number;
  type = EXEC_TYPE.EXEC;
  error: any;

  constructor(script: string, ...args: any) {
    super(...args);
    this.script = script;
    this.unixTime = Date.now();
    this.error = args.firstElement();
  }
  
  static isExecError(e: any): ExecError | void {
    try {
      if (e) {
        if ("type" in e && "script" in e && "unixTime" in e && "error" in e) {
          return new ExecError(e.type, e.event, e.data);
        }
      }
    } catch (error) {
      console.log(error);
    }
  }
}

export class SpawnError extends Error {
  environment: string;
  script: string;
  unixTime: number;
  type = EXEC_TYPE.SPAWN;

  constructor(environment: string, script: string, ...args: any) {
    super(...args);
    this.environment = environment;
    this.script = script;
    this.unixTime = Date.now();
  }
  static isError(errorType: any): SpawnError | void {
    try {
      if (errorType) {
        if ("command" in errorType && "error" in errorType && "execPath" in errorType) {
          return new SpawnError(errorType.command, errorType.execPath, errorType.error);
        }
      }
    } catch (error) {
      console.log(error);
    }
  }
}

export enum EXEC_TYPE {
  SPAWN = "SPAWN",
  EXEC = "EXEC",
}

export enum EXEC_EVENT {
  PROGRESS = "PROGRESS",
  ERROR = "ERROR",
  END = "END",
}
