import { IsArray, IsString } from "class-validator";
import { Type } from "class-transformer";
class Asset {
  @IsString()
  public name: string;
  @IsString()
  public mesh: string;
  @IsString()
  public image: string;
}
export class RobossemblerAssets {
  @IsArray()
  @Type(() => Asset)
  assets: Asset[];
  convertLocalPathsToServerPaths(serverAddress: string): RobossemblerAssets {
    this.assets = this.assets.map((el) => {
      el.mesh = `${serverAddress}/${el.mesh.slice(2, el.mesh.length)}`;
      el.image = `${serverAddress}/${el.image.slice(2, el.image.length)}`;
      return el;
    });
    return this;
  }
}
 