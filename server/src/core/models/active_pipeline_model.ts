export class ActivePipeline {
  pipelineIsRunning: boolean;
  projectId?: string | null;
  lastProcessCompleteCount: number | null;
  error: any;
  rootDir: string;
  path: string;
  constructor(
    pipelineIsRunning: boolean,
    lastProcessCompleteCount: number | null,
    error: any,
    path: string | null,
    projectId: string | null,
    rootDir: string | null
  ) {
    this.pipelineIsRunning = pipelineIsRunning;
    this.projectId = projectId;
    this.lastProcessCompleteCount = lastProcessCompleteCount;
    this.error = error;
    this.path = path;
    this.rootDir = rootDir;
  }

  static empty() {
    return new ActivePipeline(false, null, null, null, null, null);
  }
}
