import cluster, { Worker } from "node:cluster";
import { TypedEvent } from "../helpers/typed_event";
import { Result } from "../helpers/result";
import { WorkerDataExec, WorkerType } from "../helpers/worker_computed";
import { delay } from "../helpers/delay";
import { ExecutorResult } from "../models/executor_result";
import { EXEC_TYPE, ExecError, SpawnError } from "../models/exec_error_model";

abstract class IExecutorProgramService {
  abstract execPath: string;
}

export class ExecutorProgramService
  extends TypedEvent<Result<ExecError | SpawnError, ExecutorResult>>
  implements IExecutorProgramService
{
  static event = "ExecutorProgramService";
  execPath: string;
  worker: Worker | any;
  maxTime: null | number = null;

  constructor(execPath: string, maxTime: number | null = null) {
    super();
    this.execPath = execPath.pathNormalize();
    this.maxTime = maxTime;
  }

  private async workerExecuted(
    command: string,
    workerType: WorkerType,
    args: Array<string> | undefined = undefined,
    id: string | undefined = undefined
  ) {
    try {
      cluster.setupPrimary({
        exec: __dirname + "/../helpers/worker_computed.js",
      });

      const worker = cluster.fork();

      await delay(300);

      this.worker = worker;
      
      const workerDataExec: WorkerDataExec = {
        command: command,
        execPath: this.execPath,
        type: workerType,
        cliArgs: args,
      };
      worker.send(workerDataExec);
      worker.on("message", (e) => {
        console.log(JSON.stringify(e));
        const spawnError = SpawnError.isError(e);

        if (spawnError instanceof SpawnError) {
          this.emit(Result.error(spawnError));
          this.worker = undefined;
          return;
        }
        const execError = ExecError.isExecError(e);

        if (execError instanceof ExecError) {
          execError.id = id;
          this.emit(Result.error(execError));
          this.worker = undefined;
          return;
        }

        const executorResult = ExecutorResult.isExecutorResult(e);
        if (executorResult instanceof ExecutorResult) {
          executorResult.id = id;
          this.emit(Result.ok(executorResult));
          this.worker = undefined;
          return;
        }
      });
      if (this.maxTime != null) {
        setTimeout(() => {
          this.worker.kill();
          this.emit(
            Result.error(
              WorkerType.EXEC ? new ExecError(command, "timeout err") : new SpawnError(command, "timeout err")
            )
          );
        }, this.maxTime!);
      }
    } catch (error) {
      console.log(error);
    }
  }
  public deleteWorker() {
    if (this.worker) this.worker.kill();
    this.worker = undefined;
  }
  public async call(
    type: EXEC_TYPE,
    command: string,
    args: Array<string> | undefined = undefined,
    id: string | undefined = undefined
  ): Promise<void> {
    if (type == EXEC_TYPE.EXEC) {
      this.workerExecuted(command, WorkerType.EXEC, undefined, id);

      return;
    }
    const commands = command.split(" ");
    this.workerExecuted(commands.at(0), WorkerType.SPAWN, commands.slice(1, commands.length), id);

    return;
  }
}
