import { TypedEvent } from "../helpers/typed_event";
import { ExecError } from "../models/exec_error_model";
import { ExecutorResult } from "../models/executor_result";
import { ActivePipeline } from "../models/active_pipeline_model";
import { IPipeline } from "../models/process_model";
import { Iteration, StackService } from "./stack_service";

export class PipelineRealTimeService extends TypedEvent<ActivePipeline> {
  status: ActivePipeline;
  pipelineModels?: IPipeline[];
  constructor() {
    super();
    this.init();
  }
  private init(): void {
    this.status = ActivePipeline.empty();
  }
  pipelineSubscriber = (iterations: Iteration[]): void => {
    if (this.status["lastProcessCompleteCount"] === 0) {
      this.status["lastProcessCompleteCount"] = 0;
    }
    this.status.lastProcessCompleteCount += 1;
    this.pipelineCompleted();
    this.iterationHelper(iterations[iterations.length - 1]);
    this.emit(this.status);
  };

  iterationHelper(iteration: Iteration): void {
    this.iterationErrorObserver(iteration);

    // TODO(IDONTSUDO): implements
    this.iterationLogSaver(iteration);
  }

  iterationLogSaver(_iteration: Iteration): void {
    // throw new Error("Method not implemented.");
  }

  iterationErrorObserver(iteration: Iteration): void {
    if (this.status.lastProcessCompleteCount === 1) {
      return;
    }
    const result = iteration?.result;
    const executorResult = ExecutorResult.isExecutorResult(result);
    const execError = ExecError.isExecError(result);

    if (executorResult instanceof ExecutorResult) {
      this.status.error = executorResult;
    }

    if (execError instanceof ExecError) {
      this.status.error = execError;
    }
  }
  pipelineCompleted() {
    const pipelineIsCompleted = this.status.lastProcessCompleteCount === this.pipelineModels?.length;
    if (pipelineIsCompleted) {
      this.status.pipelineIsRunning = false;
    }
  }
  setPipelineDependency(path: string, projectId: string, rootDir: string) {
     this.status["projectId"] = projectId;
    this.status["path"] = path;
    this.status["rootDir"] = rootDir;
  }
  runPipeline(): void {
    const stack = new StackService(this.pipelineModels, this.status.path);

    this.status["pipelineIsRunning"] = true;
    stack.on(this.pipelineSubscriber);
    stack.call();
  }
}
