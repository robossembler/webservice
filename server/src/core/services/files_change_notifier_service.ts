import * as fs from "fs";
import { resolve } from "node:path";
import { createHash } from "node:crypto";
import { BinaryLike } from "crypto";
import { EventsFileChanger, MetaDataFileManagerModel } from "../models/meta_data_file_manager_model";
import { Result } from "../helpers/result";
import { TypedEvent } from "../helpers/typed_event";
import { FileSystemRepository } from "../repository/file_system_repository";

function md5(content: Buffer | BinaryLike): Promise<string> {
  return new Promise((resolve, _reject) => {
    return resolve(createHash("md5").update(content).digest("hex"));
  });
}

export interface IHashesCache {
  [key: string]: MetaDataFileManagerModel;
}

export abstract class IFilesChangeNotifierService {
  abstract directory: string;
}

export class FilesChangeNotifierService
  extends TypedEvent<Result<Error, IHashesCache>>
  implements IFilesChangeNotifierService
{
  fileSystemRepository: FileSystemRepository;
  watcher!: fs.FSWatcher;
  directory: string;
  constructor(directory: string) {
    super();
    this.directory = directory;
    this.init();
    this.fileSystemRepository = new FileSystemRepository();
  }
  hashes: IHashesCache = {};
  async init() {
    const files = await this.getFiles(this.directory);
    for (const file of files) {
      await this.setHash(file);
    }
  }
  async setHash(file: string) {
    const data = await this.fileSystemRepository.readFileAsync(file);
    const md5Current = await md5(data);

    this.hashes[file] = new MetaDataFileManagerModel(file, md5Current, EventsFileChanger.static);
    this.emit(Result.ok(this.hashes));
  }
  async getFiles(dir: string): Promise<string | any[]> {
    const subdirs = await new FileSystemRepository().readdir(dir);
    const files = await Promise.all(
      subdirs.map(async (subdir) => {
        const res = resolve(dir, subdir);
        return (await this.fileSystemRepository.stat(res)).isDirectory() ? this.getFiles(res) : res;
      })
    );
    return files.reduce((a: string | any[], f: any) => a.concat(f), []);
  }

  public call = (): Result<Error, boolean> => {
    try {
      let md5Previous: string | null = null;
      let fsWait: NodeJS.Timeout | boolean = false;
      const watcher = fs.watch(this.directory, { recursive: true }, async (_e, filename) => {
        const filePath = this.directory + filename;
        if (filename) {
          if (fsWait) return;
          fsWait = setTimeout(() => {
            fsWait = false;
          }, 100);
          try {
            const file = await this.fileSystemRepository.readFileAtBuffer(filePath);
            const md5Current = await md5(file);
            if (md5Current === md5Previous) {
              return;
            }
            const status = this.hashes[filePath] === undefined ? EventsFileChanger.create : EventsFileChanger.update;

            const model = new MetaDataFileManagerModel(filePath, md5Current, status);
            this.hashes[filePath] = model;
            md5Previous = md5Current;
            this.emit(Result.ok(this.hashes));
          } catch (error) {
            this.emit(Result.ok(this.hashes));
            this.hashes[filePath] = new MetaDataFileManagerModel(filePath, null, EventsFileChanger.delete);
          }
        }
      });
      this.watcher = watcher;
      return Result.ok(true);
    } catch (error) {
      return Result.error(error as Error);
    }
  };
  cancel() {
    if (this.watcher != undefined) {
      this.watcher.close();
      this.listenersOnces = [];
    }
  }
}
